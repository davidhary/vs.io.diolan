# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.3.7632] - 2020-11-23
* Converted to C#

## [1.3.7612] - 2020-11-03
* Using C# framework

## [1.3.6667] - 2018-04-03
* 2018 release.

## [1.2.6333] - 2021705-04
* Uses cancel details event arguments instead of referenced details.

## [1.1.6097] - 2016-09-10
* Uses DLN Library 3.4.x. Adds support for No Hau handler.

## [1.1.5941] - 2016-04-07
* Uses DLN Library 3.1.x. Fixes disconnection. Handles the lost connection event. Uses device ID instead of serial numbers as keys.

## [1.0.5654] - 2015-06-25
* Resets emulator before and after driver.

## [1.0.5649] - 2015-06-20
* Separates configuration from initialization to prevent driver from registering bogus emulator events. Validates configuration.

## [1.0.5647] - 2015-06-18
* Adds a base class for modality connector. Uses the base class for the handler Driver Emulator.

## [1.0.5646] - 2015-06-17
* Adds an integrated handler driver+emulator classes with the console.

## [1.0.5644] - 2015-06-15
* Auto list panels. Change to using active low nomenclature. Improves handling of open server with detached instruments. Publishes known state.

## [1.0.5624] - 2015-05-26
* Created.

\(C\) 2015 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*Library*
*Forms*
*etc.*
```

