Imports Dln.Gpio
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Diolan.Gpio
Namespace GpioTests

    ''' <summary>
    ''' This is a test class for GpioOutputPinTest and is intended to contain all GpioOutputPinTest
    ''' Unit Tests.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    <TestClass()>
    Public Class GpioOutputPinTest

        ''' <summary>
        ''' Gets or sets the test context which provides information about and functionality for the
        ''' current test run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

                Private WithEvents Input_Pin As Dln.Gpio.Pin

        ''' <summary> Input pin condition met thread safe. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Condition met event information. </param>
        Private Sub InputPin_ConditionMetThreadSafe(sender As Object, e As Dln.Gpio.ConditionMetEventArgs) Handles Input_Pin.ConditionMetThreadSafe
            If e IsNot Nothing Then
                System.Diagnostics.Debug.WriteLine($"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}")

            End If
        End Sub

        ''' <summary> A test for BitValue. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        <TestMethod()>
        Public Sub BitValueTest()
            Dim serverConnector As LocalhostConnector = LocalhostConnector.SingleInstance
            serverConnector.Connect()
            If serverConnector.IsConnected Then
                Using deviceConnector As New DeviceConnector(serverConnector)
                    Dim e As New isr.Core.ActionEventArgs
                    ' TO_DO: Add selection of device id.
                    If deviceConnector.TryOpenDevice(DeviceInfo.DefaultDeviceId, DeviceModalities.None, e) Then
                        Dim pins As Pins = deviceConnector.Device.Gpio.Pins
                        Dim pinNumber As Integer = 0
                        Dim target As GpioOutputPin = New GpioOutputPin(pins, pinNumber, ActiveLogic.ActiveLow)
                        Dim expected As Byte = 0
                        Dim actual As Byte
                        target.BitValue = expected
                        actual = target.BitValue
                        Assert.AreEqual(expected, actual)
                        deviceConnector.CloseDevice(DeviceModalities.Gpio)
                    Else
                        Assert.Fail(e.Details)
                    End If
                End Using
            End If
        End Sub

        ''' <summary> Tests start. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="outputPinNumber"> The output pin number. </param>
        ''' <param name="inputPinNumber">  The input pin number. </param>
        ''' <param name="activeLogic">     The active logic. </param>
        ''' <param name="logicalValue">    The logical value. </param>
        Public Shared Sub StartTestTest(ByVal outputPinNumber As Integer, inputPinNumber As Integer,
                                 ByVal activeLogic As ActiveLogic, ByVal logicalValue As Byte)
            Dim serverConnector As LocalhostConnector = LocalhostConnector.SingleInstance
            serverConnector.Connect()
            If serverConnector.IsConnected Then
                Using deviceConnector As New DeviceConnector(serverConnector)
                    Dim e As New isr.Core.ActionEventArgs
                    If deviceConnector.TryOpenDevice(DeviceInfo.DefaultDeviceId, DeviceModalities.None, e) Then
                        Dim pins As Pins = deviceConnector.Device.Gpio.Pins
                        Dim outputPin As GpioOutputPin = New GpioOutputPin(pins, outputPinNumber, activeLogic)
                        Dim inputPin As GpioInputPin = New GpioInputPin(pins, inputPinNumber, activeLogic)
                        Dim expected As Byte = CByte(1) And logicalValue
                        Dim actual As Byte
                        outputPin.LogicalValue = logicalValue
                        actual = inputPin.LogicalValue
                        Assert.AreEqual(expected, actual)
                        deviceConnector.CloseDevice(DeviceModalities.Gpio)
                    Else
                        Assert.Fail(e.Details)
                    End If
                End Using
            End If

        End Sub

        ''' <summary> A test for start test off. Output from pin 4 to pin 0. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        <TestMethod()>
        Public Sub StartTestOff()
            GpioOutputPinTest.StartTestTest(4, 0, ActiveLogic.ActiveLow, 0)
        End Sub

        ''' <summary> A test for start test on. Output from pin 4 to pin 0. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        <TestMethod()>
        Public Sub StartTestOn()
            GpioOutputPinTest.StartTestTest(4, 0, ActiveLogic.ActiveLow, 1)
        End Sub

    End Class
End Namespace
