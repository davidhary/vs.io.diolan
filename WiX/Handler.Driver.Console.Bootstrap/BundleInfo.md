Copyright 2016 Integrated Scientific Resources, Inc. All rights
reserved.

Licensed under [The MIT License](http://opensource.org/licenses/MIT).

**Installation Information.** This program installs the ISR handler
Consoler test program. The program may also install .Net Framework
version 4.0, which requires an Internet connection.

**Required Software.** This software requires the Diolan drivers^1^.

**Required Hardware.** The program communicates with Diolan devices.

Downloads:

\(1\) Diolan Driver: <http://dlnware.com/downloads/windows-setup>
