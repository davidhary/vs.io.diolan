Copyright 2015 Integrated Scientific Resources, Inc. All rights
reserved.

Licensed under The MIT License**^1^**.

**Installation Information.** This program installs a program for the
control of a part handler using the Diolan GPIO interface.

**Required Software and Hardware.** The program is based on
Diolan**^2^** drivers from Diolan Ltd. Connecting to the handler
interface requires that proper drivers are installed.

**Additional Resources.** For additional information please review the
read me and release information files installs with the program.

\(1\) [The MIT
License.](http://opensource.org/licenses/MIT)

\(2\) [**[Diolan]{.underline}**](http://www.diolan.com)
