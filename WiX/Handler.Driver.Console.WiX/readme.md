**Test program for Handler control using Diolan DLN-4M**

Installation Information

This program installs a program for controlling a handler using the
Diolan DLN-4M hardware and the ISR DIOLAN Libraries.

**Table of Contents**
-   Microsoft .NET Framework is Required
-   Diolan Device Setup is required
-   Uninstall
-   Contact Information
-   Copyrights and Trademarks

**Microsoft .NET Framework (.NETFX) Version Is Required**

[**[Microsoft .NET
Framework]{.underline}**](http://www.microsoft.com/en-us/download/details.aspx?id=17851)
Version 4.0 must installed before proceeding with this installation.

**Diolan Setup program is required**


[**[Diolan
setup]{.underline}**](http://dlnware.com/downloads/windows-setup) is
required for accessing DLN-4 devices.

**Uninstalling**
=

Uninstall the program from the Add/Remove Programs applets of the
Control Panel.

**Contact Information**
support[at]IntegratedScientificResources[.]com
**Copyrights and Trademarks**

The ISR logo is a trademark of Integrated Scientific Resources. Windows
and .NET Framework are trademarks of Microsoft Corporation. Diolan and
DLN-4M are trademarks for Diolan Ltd. Other trademarks are property of
their owners.

\(c\) 2016 Integrated Scientific Resources, Inc. All rights reserved.

<a name="The-MIT-License"></a>
### The MIT License
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

You are hereby granted permission to add your license notice in
accordance with your agreement with ISR.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:


Diolan Core Library:  
[Diolan Core](https://bitbucket.org/davidhary/vs.IOdiolan)  
[Enum Extender](http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx)  
[Enum Flags](http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx)

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[DLN Library](http://www.dlnware.com)
