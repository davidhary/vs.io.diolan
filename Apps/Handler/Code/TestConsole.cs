using System;
using System.Diagnostics;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Diolan.Handler
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Test console. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-06 </para>
    /// </remarks>
    public partial class TestConsole : Core.Forma.FormBase
    {

        #region " CONSTRUCTION "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public TestConsole()
        {

            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._SeverConnector = LocalhostConnector.SingleInstance();
            this._GpioPanel.ServerConnector = this._SeverConnector;
            this._GpioHandlerPanel.DriverEmulator.ServerConnector = this._SeverConnector;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._SeverConnector is object )
                        this._SeverConnector = null;
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " SERVER CONNECTOR "

        /// <summary> The sever connector. </summary>
        private LocalhostConnector _SeverConnector;

        #endregion

    }
}
