using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Handler
{
    [DesignerGenerated()]
    public partial class TestConsole : Core.Forma.FormBase
    {

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources when called from the
        /// runtime finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!IsDisposed && disposing)
                {
                    OnCustomDispose(disposing);
                    if (components is object)
                    {
                        components.Dispose();
                        components = null;
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(TestConsole));
            _Tabs = new TabControl();
            _HandlerTabPage = new TabPage();
            _GpioHandlerPanel = new Forms.GpioHandlerDriverView();
            _GpioTabPage = new TabPage();
            _GpioPanel = new Forms.GpioView();
            _Tabs.SuspendLayout();
            _HandlerTabPage.SuspendLayout();
            _GpioTabPage.SuspendLayout();
            SuspendLayout();
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_HandlerTabPage);
            _Tabs.Controls.Add(_GpioTabPage);
            _Tabs.Dock = DockStyle.Fill;
            _Tabs.Location = new Point(0, 0);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new Size(443, 492);
            _Tabs.TabIndex = 0;
            // 
            // _HandlerTabPage
            // 
            _HandlerTabPage.Controls.Add(_GpioHandlerPanel);
            _HandlerTabPage.Location = new Point(4, 26);
            _HandlerTabPage.Name = "_HandlerTabPage";
            _HandlerTabPage.Padding = new Padding(3);
            _HandlerTabPage.Size = new Size(435, 462);
            _HandlerTabPage.TabIndex = 0;
            _HandlerTabPage.Text = "Handler";
            _HandlerTabPage.UseVisualStyleBackColor = true;
            // 
            // _GpioHandlerPanel
            // 
            _GpioHandlerPanel.Dock = DockStyle.Fill;
            _GpioHandlerPanel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _GpioHandlerPanel.Location = new Point(3, 3);
            _GpioHandlerPanel.Name = "_GpioHandlerPanel";
            _GpioHandlerPanel.Size = new Size(429, 456);
            _GpioHandlerPanel.TabIndex = 0;
            // 
            // _GpioTabPage
            // 
            _GpioTabPage.Controls.Add(_GpioPanel);
            _GpioTabPage.Location = new Point(4, 26);
            _GpioTabPage.Name = "_GpioTabPage";
            _GpioTabPage.Padding = new Padding(3);
            _GpioTabPage.Size = new Size(435, 462);
            _GpioTabPage.TabIndex = 1;
            _GpioTabPage.Text = "GPIO";
            _GpioTabPage.UseVisualStyleBackColor = true;
            // 
            // _GpioPanel
            // 
            _GpioPanel.DeviceConnector = null;
            _GpioPanel.Dock = DockStyle.Fill;
            _GpioPanel.Font = new Font("Segoe UI", 9.75f, FontStyle.Regular, GraphicsUnit.Point, Conversions.ToByte(0));
            _GpioPanel.Location = new Point(3, 3);
            _GpioPanel.Name = "_GpioPanel";
            _GpioPanel.Size = new Size(429, 456);
            _GpioPanel.TabIndex = 0;
            // 
            // TestConsole
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(443, 492);
            Controls.Add(_Tabs);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Margin = new Padding(3, 4, 3, 4);
            Name = "TestConsole";
            Text = "Handler Driver Console";
            _Tabs.ResumeLayout(false);
            _HandlerTabPage.ResumeLayout(false);
            _GpioTabPage.ResumeLayout(false);
            ResumeLayout(false);
        }

        private TabControl _Tabs;
        private TabPage _HandlerTabPage;
        private TabPage _GpioTabPage;
        private Forms.GpioHandlerDriverView _GpioHandlerPanel;
        private Forms.GpioView _GpioPanel;
    }
}
