using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Diolan.Handler.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public TestConsole m_TestConsole;

            public TestConsole TestConsole
            {
                [DebuggerHidden]
                get
                {
                    m_TestConsole = Create__Instance__(m_TestConsole);
                    return m_TestConsole;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_TestConsole))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_TestConsole);
                }
            }
        }
    }
}
