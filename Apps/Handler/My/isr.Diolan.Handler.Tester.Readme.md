## ISR Diolan Handler Port Tester<sub>&trade;</sub>: Diolan Tester for Handler Port.
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)
* [Installation Information] (#Installation-Information)

### Revision History [](#){name=Revision-History}

*1.1.6097 2016-09-10*  
Uses DLN Library 3.4.x. Adds support for No Hau
handler.

*1.1.5941 2016-04-07*  
Uses DLN Library 3.1.x. Fixes disconnection. Handles
the lost connection event. Uses device ID instead of serial numbers as
keys.

*1.0.5722 2015-09-01*  
Adds software ID tag for software asset management.

*1.0.5712 2015-08-22*  
VS2015 and WiX 3.10.

*1.0.5654 2015-06-25*  
Resets emulator before and after driver.

*1.0.5649 2015-06-20*  
Separates configuration from initialization to prevent
driver from registering bogus emulator events. Validates configuration.

*1.0.5647 2015-06-18*  
Adds a base class for modality connector. Uses the
base class for the handler Driver Emulator.

*1.0.5646 2015-06-17*  
Uses an integrated handler driver+emulator classes
with the console.

*1.0.5644 2015-06-15*  
Auto list panels. Change to using active low
nomenclature. Improves handling of open server with detached
instruments. Publishes known state.

*1.0.5624 2015-05-26*  
Created.

### Installation Information [](#){name=Installation-Information}

This program installs a program for controlling a handler using the
Diolan DLN-4M hardware and the ISR DIOLAN Libraries.

**Microsoft .NET Framework (.NETFX) Version Is Required**

[**[Microsoft .NET Framework]{.underline}**](http://www.microsoft.com/en-us/download/details.aspx?id=17851)
Version 4.0 must installed before proceeding with this installation.

**Diolan Setup program is required**

[**[Diolan setup]{.underline}**](http://dlnware.com/downloads/windows-setup) is
required for accessing DLN-4 devices.

**Uninstalling**

Uninstall the program from the Add/Remove Programs applets of the
Control Panel.

**Contact Information**
support[at]IntegratedScientificResources[.]com

\(C\) 2015 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[DIOLAN Libraries](https://bitbucket.org/davidhary/vs.IOdiolan)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[DLN Library](http://www.dlnware.com)
