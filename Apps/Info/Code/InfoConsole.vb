Imports isr.Core.WinForms.ErrorProviderExtensions
Imports System.Globalization
Imports System.Drawing.Drawing2D
Imports isr.Diolan.SubsystemExtensions
Imports isr.Diolan.Forms.SubsystemExtensions

''' <summary> Versions console. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-30 </para>
''' </remarks>
Public Class InfoConsole
    Inherits isr.Core.Forma.FormBase

    ''' <summary> Displays an information described by sender. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    Private Sub DisplayInfo(ByVal sender As Control)
        LocalhostConnector.SingleInstance.Connect()
        If LocalhostConnector.SingleInstance.IsConnected Then
            Using deviceSelector As New isr.Diolan.DeviceConnector(LocalhostConnector.SingleInstance)
                Dim e As New isr.Core.ActionEventArgs
                If deviceSelector.TryOpenDevice(CLng(Me._DeviceIdNumeric.Value), DeviceModalities.None, e) Then
                    Me._DeviceInfoTextBox.Text = deviceSelector.Device.Caption
                    Me._ListView.SuspendLayout()
                    Me._ListView.Clear()
                    Me._ListView.OwnerDraw = True
                    ' when allowing owner draw, the column sizes no longer display correctly.
                    Me._ListView.OwnerDraw = False
                    Me._ListView.View = View.Details
                    Me._ListView.MultiSelect = True
                    Me._ListView.HideSelection = False
                    Me._ListView.HeaderStyle = ColumnHeaderStyle.Nonclickable
                    Me._ListView.Columns.Add("Name", 146, HorizontalAlignment.Left)
                    Me._ListView.Columns.Add("Value", 146, HorizontalAlignment.Left)

                    For Each kvp As KeyValuePair(Of String, String) In LocalhostConnector.SingleInstance.Connection.ConnectionInfo
                        Me._ListView.Items.Add(New ListViewItem(New String() {kvp.Key, kvp.Value}))
                    Next
                    For Each kvp As KeyValuePair(Of String, String) In deviceSelector.Device.DeviceInfo
                        Me._ListView.Items.Add(New ListViewItem(New String() {kvp.Key, kvp.Value}))
                    Next
                    For Each kvp As KeyValuePair(Of String, String) In deviceSelector.Device.ModalityInfo
                        Me._ListView.Items.Add(New ListViewItem(New String() {kvp.Key, kvp.Value}))
                    Next
                    For Each c As ColumnHeader In Me._ListView.Columns
                        c.AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                    Next
                    Me._ListView.ResumeLayout()
                    deviceSelector.CloseDevice(DeviceModalities.None)
                Else
                    Me._DeviceInfoTextBox.Text = "No devices"
                    Me._ErrorProvider.Annunciate(sender, e.Details)
                End If
            End Using
        Else
            Me._DeviceInfoTextBox.Text = "Not connected"
        End If
    End Sub

    ''' <summary> Shows the versions button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ShowVersionsButton_Click(sender As System.Object, e As System.EventArgs) Handles _ShowDeviceInformationButton.Click
        Dim s As Control = TryCast(sender, Control)
        Try
            Me._ErrorProvider.Clear()
            If s IsNot Nothing Then Me.DisplayInfo(s)
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(s, ex.ToString)
        Finally
            LocalhostConnector.SingleInstance.Disconnect()
        End Try
    End Sub


#Region " LIST VIEW "

    ''' <summary> List view draw item. Draws the backgrounds for entire ListView items. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Draw list view item event information. </param>
    Private Sub ListView_DrawItem(ByVal sender As Object, ByVal e As DrawListViewItemEventArgs) Handles _ListView.DrawItem

        If False AndAlso Not (e.State And ListViewItemStates.Selected) = 0 Then
            ' Draw the background for a selected item.
            e.Graphics.FillRectangle(Brushes.Maroon, e.Bounds)
            e.DrawFocusRectangle()
        ElseIf False Then
            ' Draw the background for an unselected item.
            Dim brush As New LinearGradientBrush(e.Bounds, Color.Orange, Color.Maroon, LinearGradientMode.Horizontal)
            Try

                e.Graphics.FillRectangle(brush, e.Bounds)
            Finally
                brush.Dispose()
            End Try
        End If

        ' Draw the item text for views other than the Details view.
        If Not Me._ListView.View = View.Details Then
            e.DrawText()
        End If

    End Sub

    ''' <summary> List view draw sub item. Draws sub item text and applies content-based formatting. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Draw list view sub item event information. </param>
    Private Sub ListView_DrawSubItem(ByVal sender As Object, ByVal e As DrawListViewSubItemEventArgs) Handles _ListView.DrawSubItem

        Dim flags As TextFormatFlags = TextFormatFlags.Left

        Dim sf As New StringFormat()
        Try

            ' Store the column text alignment, letting it default
            ' to Left if it has not been set to Center or Right.

            Select Case e.Header.TextAlign
                Case HorizontalAlignment.Center
                    sf.Alignment = StringAlignment.Center
                    flags = TextFormatFlags.HorizontalCenter
                Case HorizontalAlignment.Right
                    sf.Alignment = StringAlignment.Far
                    flags = TextFormatFlags.Right
            End Select

            ' Draw the text and background for a sub-item with a negative value. 
            Dim subItemValue As Double
            If e.ColumnIndex > 0 AndAlso
                Double.TryParse(e.SubItem.Text, NumberStyles.Currency,
                NumberFormatInfo.CurrentInfo, subItemValue) AndAlso
                subItemValue < 0 Then

                ' Unless the item is selected, draw the standard 
                ' background to make it stand out from the gradient.
                If (e.ItemState And ListViewItemStates.Selected) = 0 Then
                    e.DrawBackground()
                End If

                ' Draw the sub-item text in red to highlight it. 
                e.Graphics.DrawString(e.SubItem.Text,
                    Me._ListView.Font, Brushes.Red, e.Bounds, sf)

                Return

            End If

            ' Draw normal text for a sub-item with a nonnegative or non-numerical value.
            e.DrawText(flags)

        Finally
            sf.Dispose()
        End Try

    End Sub

    ''' <summary> Event handler. Called by ListView for draw column header events. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Draw list view column header event information. </param>
    Private Sub ListView_DrawColumnHeader(ByVal sender As Object, ByVal e As DrawListViewColumnHeaderEventArgs)

        Dim sf As New StringFormat()
        Try

            ' set the column text alignment, letting it default to Left if it has not been set to Center or Right.
            Select Case e.Header.TextAlign
                Case HorizontalAlignment.Left


                Case HorizontalAlignment.Center
                    sf.Alignment = StringAlignment.Center
                Case HorizontalAlignment.Right
                    sf.Alignment = StringAlignment.Far
            End Select

            ' Draw the standard header background.
            e.DrawBackground()

            ' Draw the header text.
            Dim headerFont As New Font("Helvetica", 10, FontStyle.Bold)
            Try
                e.Graphics.DrawString(e.Header.Text, headerFont, Brushes.Black, e.Bounds, sf)
            Finally
                headerFont.Dispose()
            End Try

        Finally
            sf.Dispose()
        End Try

    End Sub

    ''' <summary> List view draw column header 2. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Draw list view column header event information. </param>
    Private Sub ListView_DrawColumnHeader2(ByVal sender As Object, ByVal e As DrawListViewColumnHeaderEventArgs) Handles _ListView.DrawColumnHeader
        e.DrawText()
    End Sub

    ''' <summary> Event handler. Called by ListView for draw column header 1 events. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Draw list view column header event information. </param>
    Private Sub ListView_DrawColumnHeader1(ByVal sender As Object, ByVal e As DrawListViewColumnHeaderEventArgs)
        Using sf As New StringFormat()


            ' Set the column text alignment, letting it default to Left if it has not been set to Center or Right.
            Select Case e.Header.TextAlign
                Case HorizontalAlignment.Center
                    sf.Alignment = StringAlignment.Center
                Case HorizontalAlignment.Right
                    sf.Alignment = StringAlignment.Far
            End Select

            ' Draw the standard header background.
            e.DrawBackground()

            ' fill if you wish
            ' e.Graphics.FillRectangle(Brushes.Pink, e.Bounds)

            ' Draw the header text.
            Using headerFont As New Font(MyBase.Font.FontFamily, MyBase.Font.Size, FontStyle.Bold)
                e.Graphics.DrawString(e.Header.Text, headerFont, Brushes.Black, e.Bounds, sf)
            End Using

        End Using
    End Sub

#End Region

End Class
