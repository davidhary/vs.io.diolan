Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.SubsystemExtensions
Imports isr.Diolan.Forms.SubsystemExtensions

''' <summary> Form for viewing the main. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-29 </para>
''' </remarks>
Partial Public Class GpioConsole

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnModalityClosed()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " MODALITY "

    ''' <summary> The modality. </summary>
    ''' <value> The modality. </value>
    Public Property Modality As DeviceModalities = DeviceModalities.Gpio

    ''' <summary> Executes the modality closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityClosed()
        Me._GpioPinControl1.CloseModality()
        Me._GpioPinControl2.CloseModality()
        Me._PinComboBox.DataSource = Nothing
        Me._PinComboBox.Items.Clear()
        Me._PinDirectionComboBox.DataSource = Nothing
        Me._PinDirectionComboBox.Items.Clear()
        Me._EventTypeComboBox.DataSource = Nothing
        Me._EventTypeComboBox.Items.Clear()
        Me._Device = Nothing
    End Sub

    ''' <summary> Executes the modality closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnModalityClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            If Me.IsModalityOpen Then
                ' un-register event handlers for all pins
                For Each pin As Dln.Gpio.Pin In Me.Device.Gpio.Pins
                    RemoveHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                Next
            End If
        End If
    End Sub

    ''' <summary> Executes the modality opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityOpening()

        ' Fill controls
        Me._PinDirectionComboBox.DataSource = Nothing
        Me._PinDirectionComboBox.Items.Clear()
        Me._PinDirectionComboBox.ListEnumNames(Of isr.Diolan.PinDirection)

        Me._Device = Me._DeviceConnector.Device
        Me._PinComboBox.DataSource = Nothing
        Me._PinComboBox.Items.Clear()
        Me._ErrorProvider.Clear()

        ' Get port count
        If Me.Device.Gpio.Pins.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton, "Adapter '{0}' doesn't support GPIO interface.", Me.Device.Caption)
            Me._Device = Nothing
            Me._DeviceInfoTextBox.Text = "not supported"
        Else
            Me._DeviceInfoTextBox.Text = Me.Device.Caption
            Me.Device.Gpio.Pins.ListNumbers(Me._PinComboBox)
            Me._GpioPinControl1.OpenModality(Me._Device)
            Me._GpioPinControl2.OpenModality(Me._Device)

            'Set current context to run thread safe events in main form thread
            Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current

            ' Register event handler for all pins
            For Each pin As Dln.Gpio.Pin In Me.Device.Gpio.Pins
                AddHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
            Next

            ' Get subsystem parameters: Debounce
            Me.ReadDebounceProperties()

            Me._PinComboBox.SelectedIndex = 0

        End If

    End Sub

    ''' <summary> Queries if a modality is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
    Private Function IsModalityOpen() As Boolean
        Return Me._PinComboBox.DataSource IsNot Nothing AndAlso Me._PinComboBox.Items.Count > 0
    End Function

#End Region

#Region " EVENT LOG "

    ''' <summary> Handler, called when the condition met event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Condition met event information. </param>
    Private Sub ConditionMetEventHandler(ByVal sender As Object, ByVal e As Dln.Gpio.ConditionMetEventArgs)
        Me.AppendEvent(e)
    End Sub

    ''' <summary> Appends an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Condition met event information. </param>
    Private Sub AppendEvent(ByVal e As Dln.Gpio.ConditionMetEventArgs)
        If e IsNot Nothing Then
            Dim data As String = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}"
            ' This event is handled in main thread,
            ' so it is not needed to invoke when modifying form's controls.
            Me._EventLogTextBox.AppendText(data)
        End If

    End Sub

#End Region

#Region " SUBSYSTEM "

    ''' <summary> Reads the debounce properties from the device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub ReadDebounceProperties()
        ' Get subsystem parameters: Debounce
        If Me.Device.Gpio.Restrictions.Debounce = Dln.Restriction.NotSupported Then
            Me._DebounceNumeric.Enabled = False
            Me._SetDebounceButton.Enabled = False
            Me._GetDebounceButton.Enabled = False
        Else
            Me._DebounceNumeric.Value = CDec(Me._Device.Gpio.Debounce)
        End If
    End Sub

    ''' <summary> Gets debounce button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GetDebounceButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetDebounceButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Me._DebounceNumeric.Value = CDec(Me._Device.Gpio.Debounce)
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try

    End Sub

    ''' <summary> Sets debounce button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetDebounceButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetDebounceButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Me._Device.Gpio.Debounce = CInt(Me._DebounceNumeric.Value)
                Me._DebounceNumeric.Value = CDec(Me._Device.Gpio.Debounce)
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception

            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region

#Region " PIN "

    ''' <summary> Gets pin configuration button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GetPinConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetPinConfigButton.Click
        'Get pin parameters
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim pinNumber As Integer = Me._PinComboBox.SelectedIndex
                If pinNumber >= 0 AndAlso pinNumber < Me.Device.Gpio.Pins.Count Then
                    Me.ReadPinProperties(Me.Device.Gpio.Pins(pinNumber))
                Else

                    Me._ErrorProvider.Annunciate(sender, "Pin index {0} is out of range of [0,{1}]", Me.Device.Gpio.Pins.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Reads pin number properties. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pin"> The pin. </param>
    Private Sub ReadPinProperties(ByVal pin As Dln.Gpio.Pin)
        'Main settings are present in all devices,
        'check availability only for extra settings like pull downs, debounce and events.

        Me._PinEnabledCheckBox.Checked = pin.Enabled
        Me._PinDirectionComboBox.SelectedIndex = pin.Direction

        ' Pull up
        If pin.Restrictions.PullupEnabled = Dln.Restriction.NotSupported Then
            Me._PinPullupCheckBox.Enabled = False
        Else
            Me._PinPullupCheckBox.Enabled = True
            Me._PinPullupCheckBox.Checked = pin.PullupEnabled
        End If

        ' Pull down
        If pin.Restrictions.PulldownEnabled = Dln.Restriction.NotSupported Then
            Me._PulldownCheckBox.Enabled = False
        Else
            Me._PulldownCheckBox.Enabled = True
            Me._PulldownCheckBox.Checked = pin.PulldownEnabled
        End If

        ' Open drain
        If pin.Restrictions.OpendrainEnabled = Dln.Restriction.NotSupported Then
            Me._OpenDrainCheckBox.Enabled = False
        Else
            Me._OpenDrainCheckBox.Enabled = True
            Me._OpenDrainCheckBox.Checked = pin.OpendrainEnabled
        End If

        ' Debounce
        If pin.Restrictions.DebounceEnabled = Dln.Restriction.NotSupported Then
            Me._ToolTip.SetToolTip(Me._DebounceEnabledCheckBox, "Not supported.")
        ElseIf pin.Restrictions.DebounceEnabled = Dln.Restriction.NoRestriction Then
            Me._ToolTip.SetToolTip(Me._DebounceEnabledCheckBox, "Enabled if checked.")
        End If
        If pin.Restrictions.DebounceEnabled = Dln.Restriction.NotSupported Then
            Me._DebounceEnabledCheckBox.Enabled = False
        ElseIf pin.Restrictions.DebounceEnabled = Dln.Restriction.MustBeDisabled Then
            Me._DebounceEnabledCheckBox.Enabled = False
        Else
            Me._DebounceEnabledCheckBox.Enabled = True
            Me._DebounceEnabledCheckBox.Checked = pin.DebounceEnabled
        End If

        'Event type
        If pin.Restrictions.EventType = Dln.Restriction.NotSupported Then
            Me._EventTypeComboBox.Enabled = False
        Else
            Me._EventTypeComboBox.Enabled = True
            Me._EventTypeComboBox.DataSource = pin.SupportedEventTypes
            Me._EventTypeComboBox.SelectedItem = pin.EventType
        End If

        'Event period
        If pin.Restrictions.EventPeriod = Dln.Restriction.NotSupported Then
            Me._EventPeriodNumeric.Enabled = False
        Else
            Me._EventPeriodNumeric.Enabled = True
            Me._EventPeriodNumeric.Value = CDec(pin.EventPeriod)
        End If
    End Sub

    ''' <summary> Applies the pin configuration described by pin. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pin"> The pin. </param>
    Private Sub ApplyPinConfiguration(ByVal pin As Dln.Gpio.Pin)
        If pin IsNot Nothing Then
            'Set common controls
            pin.Enabled = Me._PinEnabledCheckBox.Checked
            pin.Direction = Me._PinDirectionComboBox.SelectedIndex

            'Extra functions are not present if we disable controls when getting settings
            If (Me._PinPullupCheckBox.Enabled) Then
                pin.PullupEnabled = Me._PinPullupCheckBox.Checked
            End If
            If (Me._PulldownCheckBox.Enabled) Then
                pin.PulldownEnabled = Me._PulldownCheckBox.Checked
            End If
            If (Me._OpenDrainCheckBox.Enabled) Then
                pin.OpendrainEnabled = Me._OpenDrainCheckBox.Checked
            End If
            If (Me._DebounceEnabledCheckBox.Enabled) Then
                pin.DebounceEnabled = Me._DebounceEnabledCheckBox.Checked
            End If
            If (Me._EventTypeComboBox.Enabled) Then
                pin.SetEventConfiguration(CType(Me._EventTypeComboBox.SelectedItem, Dln.Gpio.EventType), Convert.ToUInt16(Me._EventPeriodNumeric.Value))
            End If
        End If
    End Sub

    ''' <summary> Sets pin configuration button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetPinConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetPinConfigButton.Click
        ' Set pin configuration
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim pinNumber As Integer = Me._PinComboBox.SelectedIndex
                If pinNumber >= 0 AndAlso pinNumber < Me.Device.Gpio.Pins.Count Then
                    Me.ApplyPinConfiguration(Me.Device.Gpio.Pins(pinNumber))

                Else
                    Me._ErrorProvider.Annunciate(sender, "Pin index {0} is out of range of [0,{1}]", Me.Device.Gpio.Pins.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Pin combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PinComboBox_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles _PinComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim pinNumber As Integer = Me._PinComboBox.SelectedIndex
                If pinNumber >= 0 AndAlso pinNumber < Me.Device.Gpio.Pins.Count Then
                    Me.ReadPinProperties(Me.Device.Gpio.Pins(pinNumber))

                Else
                    Me._ErrorProvider.Annunciate(sender, "Pin index {0} is out of range of [0,{1}]", Me.Device.Gpio.Pins.Count - 1)
                End If
            ElseIf Me.IsModalityOpen AndAlso Me._ErrorProvider IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region

End Class
