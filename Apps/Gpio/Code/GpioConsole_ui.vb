
Imports isr.Core.WinForms.ErrorProviderExtensions

Public Class GpioConsole
    Inherits isr.Core.Forma.FormBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnDispose(disposing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Me.OnCustomDispose(disposing)
        If Not Me.IsDisposed AndAlso disposing Then
            If Me._Device IsNot Nothing Then Me._Device = Nothing
            If Me._DeviceConnector IsNot Nothing Then Me._DeviceConnector.Dispose() : Me._DeviceConnector = Nothing
        End If
    End Sub

#End Region

#Region " DEVICE "

        Private WithEvents _DeviceConnector As DeviceConnector

    ''' <summary> Device connector device closed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceClosed(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceClosed
        Me._DeviceInfoTextBox.Text = "closed"
        Me.OnModalityClosed()
    End Sub

    ''' <summary> Device connector device closing. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub DeviceConnector_DeviceClosing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _DeviceConnector.DeviceClosing
        Me.OnModalityClosing(e)
    End Sub

    ''' <summary> Device connector device opened. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceOpened(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceOpened
    End Sub

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    Private ReadOnly Property Device As Dln.Device

    ''' <summary> Opens the device for the modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="deviceId"> Identifier for the device. </param>
    ''' <param name="sender">   The sender. </param>
    Private Sub OpenDeviceModality(ByVal deviceId As Long, ByVal sender As Control)


        LocalhostConnector.SingleInstance.Connect()
        Me._DeviceConnector = New DeviceConnector(LocalhostConnector.SingleInstance)

        ' Open device
        Dim e As New isr.Core.ActionEventArgs
        If Me._DeviceConnector.TryOpenDevice(deviceId, Me.Modality, e) Then
            Me.OnModalityOpening()
        Else
            If sender IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, e.Details)
            End If
            Me._DeviceInfoTextBox.Text = "No devices"
        End If

    End Sub

    ''' <summary> Closes device modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub CloseDeviceModality()
        Me._DeviceConnector.CloseDevice(Me.Modality)
        LocalhostConnector.SingleInstance.Disconnect()
        Dim e As New System.ComponentModel.CancelEventArgs
        Me.OnModalityClosing(e)
        If Not e.Cancel Then
            Me.OnModalityClosed()
        End If
    End Sub

    ''' <summary> Opens device button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenDeviceModalityButton_Click(sender As Object, e As System.EventArgs) Handles _OpenDeviceModalityButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Me.CloseDeviceModality()
            Else
                Me.OpenDeviceModality(CLng(Me._DeviceIdNumeric.Value), TryCast(sender, Control))
            End If
        Catch ex As Exception

            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        Finally
            If Me.IsDeviceModalityOpen Then
                Me._OpenDeviceModalityButton.Text = "Close"
            Else
                Me._OpenDeviceModalityButton.Text = "Open"
            End If
        End Try
    End Sub

    ''' <summary> Gets a value indicating whether a device is open. </summary>
    ''' <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
    Private ReadOnly Property IsDeviceModalityOpen As Boolean
        Get
            Return Me._DeviceConnector IsNot Nothing AndAlso Me._DeviceConnector.IsDeviceOpen AndAlso Me.IsModalityOpen
        End Get
    End Property

#End Region

#Region " EVENT LOG "

    ''' <summary> Event log text box double click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EventLogTextBox_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EventLogTextBox.DoubleClick
        Me._EventLogTextBox.Clear()
    End Sub

#End Region

End Class

