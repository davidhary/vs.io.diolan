﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GpioConsole

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GpioConsole))
        Me._EventPeriodNumericLabel = New System.Windows.Forms.Label()
        Me._EventTypeComboBoxLabel = New System.Windows.Forms.Label()
        Me._EventTypeComboBox = New System.Windows.Forms.ComboBox()
        Me._SubsystemGroupBox = New System.Windows.Forms.GroupBox()
        Me._DebounceNumeric = New System.Windows.Forms.NumericUpDown()
        Me._GetDebounceButton = New System.Windows.Forms.Button()
        Me._SetDebounceButton = New System.Windows.Forms.Button()
        Me._DebounceNumericLabel = New System.Windows.Forms.Label()
        Me._DebounceEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._OpenDrainCheckBox = New System.Windows.Forms.CheckBox()
        Me._PulldownCheckBox = New System.Windows.Forms.CheckBox()
        Me._PinPullupCheckBox = New System.Windows.Forms.CheckBox()
        Me._GetPinConfigButton = New System.Windows.Forms.Button()
        Me._SetPinConfigButton = New System.Windows.Forms.Button()
        Me._PinConfigGroupBox = New System.Windows.Forms.GroupBox()
        Me._EventPeriodNumeric = New System.Windows.Forms.NumericUpDown()
        Me._PinDirectionComboBox = New System.Windows.Forms.ComboBox()
        Me._PinDirectionComboBoxLabel = New System.Windows.Forms.Label()
        Me._PinEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._PinComboBox = New System.Windows.Forms.ComboBox()
        Me._PinComboBoxLabel = New System.Windows.Forms.Label()
        Me._EventLogTextBox = New System.Windows.Forms.TextBox()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._EventLogGroupBox = New System.Windows.Forms.GroupBox()
        Me._GpioPinControl1 = New isr.Diolan.Forms.GpioPinControl()
        Me._GpioPinControl2 = New isr.Diolan.Forms.GpioPinControl()
        Me._DeviceGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceIdNumeric = New System.Windows.Forms.NumericUpDown()
        Me._DeviceIdNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceInfoTextBox = New System.Windows.Forms.TextBox()
        Me._OpenDeviceModalityButton = New System.Windows.Forms.Button()
        Me._SubsystemGroupBox.SuspendLayout()
        CType(Me._DebounceNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._PinConfigGroupBox.SuspendLayout()
        CType(Me._EventPeriodNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._EventLogGroupBox.SuspendLayout()
        Me._DeviceGroupBox.SuspendLayout()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_EventPeriodNumericLabel
        '
        Me._EventPeriodNumericLabel.AutoSize = True
        Me._EventPeriodNumericLabel.Location = New System.Drawing.Point(309, 101)
        Me._EventPeriodNumericLabel.Name = "_EventPeriodNumericLabel"
        Me._EventPeriodNumericLabel.Size = New System.Drawing.Size(113, 17)
        Me._EventPeriodNumericLabel.TabIndex = 17
        Me._EventPeriodNumericLabel.Text = "Event Period [ms]:"
        Me._EventPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EventTypeComboBoxLabel
        '
        Me._EventTypeComboBoxLabel.AutoSize = True
        Me._EventTypeComboBoxLabel.Location = New System.Drawing.Point(309, 66)
        Me._EventTypeComboBoxLabel.Name = "_EventTypeComboBoxLabel"
        Me._EventTypeComboBoxLabel.Size = New System.Drawing.Size(73, 17)
        Me._EventTypeComboBoxLabel.TabIndex = 16
        Me._EventTypeComboBoxLabel.Text = "Event Type:"
        Me._EventTypeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EventTypeComboBox
        '
        Me._EventTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._EventTypeComboBox.FormattingEnabled = True
        Me._EventTypeComboBox.Location = New System.Drawing.Point(385, 62)
        Me._EventTypeComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EventTypeComboBox.Name = "_EventTypeComboBox"
        Me._EventTypeComboBox.Size = New System.Drawing.Size(101, 25)
        Me._EventTypeComboBox.TabIndex = 15
        Me._ToolTip.SetToolTip(Me._EventTypeComboBox, "Selects the event type.")
        '
        '_SubsystemGroupBox
        '
        Me._SubsystemGroupBox.Controls.Add(Me._DebounceNumeric)
        Me._SubsystemGroupBox.Controls.Add(Me._GetDebounceButton)
        Me._SubsystemGroupBox.Controls.Add(Me._SetDebounceButton)
        Me._SubsystemGroupBox.Controls.Add(Me._DebounceNumericLabel)
        Me._SubsystemGroupBox.Location = New System.Drawing.Point(14, 137)
        Me._SubsystemGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SubsystemGroupBox.Name = "_SubsystemGroupBox"
        Me._SubsystemGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SubsystemGroupBox.Size = New System.Drawing.Size(493, 55)
        Me._SubsystemGroupBox.TabIndex = 4
        Me._SubsystemGroupBox.TabStop = False
        Me._SubsystemGroupBox.Text = "GPIO Subsystem Settings"
        '
        '_DebounceNumeric
        '
        Me._DebounceNumeric.Location = New System.Drawing.Point(151, 22)
        Me._DebounceNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DebounceNumeric.Maximum = New Decimal(New Integer() {32000, 0, 0, 0})
        Me._DebounceNumeric.Name = "_DebounceNumeric"
        Me._DebounceNumeric.Size = New System.Drawing.Size(80, 25)
        Me._DebounceNumeric.TabIndex = 4
        '
        '_GetDebounceButton
        '
        Me._GetDebounceButton.Location = New System.Drawing.Point(426, 19)
        Me._GetDebounceButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GetDebounceButton.Name = "_GetDebounceButton"
        Me._GetDebounceButton.Size = New System.Drawing.Size(61, 30)
        Me._GetDebounceButton.TabIndex = 3
        Me._GetDebounceButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetDebounceButton, "Gets the debounce interval")
        Me._GetDebounceButton.UseVisualStyleBackColor = True
        '
        '_SetDebounceButton
        '
        Me._SetDebounceButton.Location = New System.Drawing.Point(358, 19)
        Me._SetDebounceButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetDebounceButton.Name = "_SetDebounceButton"
        Me._SetDebounceButton.Size = New System.Drawing.Size(61, 30)
        Me._SetDebounceButton.TabIndex = 2
        Me._SetDebounceButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetDebounceButton, "Sets the debounce interval")
        Me._SetDebounceButton.UseVisualStyleBackColor = True
        '
        '_DebounceNumericLabel
        '
        Me._DebounceNumericLabel.AutoSize = True
        Me._DebounceNumericLabel.Location = New System.Drawing.Point(7, 26)
        Me._DebounceNumericLabel.Name = "_DebounceNumericLabel"
        Me._DebounceNumericLabel.Size = New System.Drawing.Size(142, 17)
        Me._DebounceNumericLabel.TabIndex = 0
        Me._DebounceNumericLabel.Text = "Debounce Interval [µs]:"
        Me._DebounceNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_DebounceEnabledCheckBox
        '
        Me._DebounceEnabledCheckBox.AutoSize = True
        Me._DebounceEnabledCheckBox.Location = New System.Drawing.Point(239, 27)
        Me._DebounceEnabledCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DebounceEnabledCheckBox.Name = "_DebounceEnabledCheckBox"
        Me._DebounceEnabledCheckBox.Size = New System.Drawing.Size(86, 21)
        Me._DebounceEnabledCheckBox.TabIndex = 14
        Me._DebounceEnabledCheckBox.Text = "Debounce"
        Me._ToolTip.SetToolTip(Me._DebounceEnabledCheckBox, "Debounce is enabled if checked.")
        Me._DebounceEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_OpenDrainCheckBox
        '
        Me._OpenDrainCheckBox.AutoSize = True
        Me._OpenDrainCheckBox.Location = New System.Drawing.Point(186, 99)
        Me._OpenDrainCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._OpenDrainCheckBox.Name = "_OpenDrainCheckBox"
        Me._OpenDrainCheckBox.Size = New System.Drawing.Size(94, 21)
        Me._OpenDrainCheckBox.TabIndex = 13
        Me._OpenDrainCheckBox.Text = "Open Drain"
        Me._ToolTip.SetToolTip(Me._OpenDrainCheckBox, "Pin is open drain is checked.")
        Me._OpenDrainCheckBox.UseVisualStyleBackColor = True
        '
        '_PulldownCheckBox
        '
        Me._PulldownCheckBox.AutoSize = True
        Me._PulldownCheckBox.Location = New System.Drawing.Point(94, 99)
        Me._PulldownCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PulldownCheckBox.Name = "_PulldownCheckBox"
        Me._PulldownCheckBox.Size = New System.Drawing.Size(84, 21)
        Me._PulldownCheckBox.TabIndex = 12
        Me._PulldownCheckBox.Text = "Pull Down"
        Me._ToolTip.SetToolTip(Me._PulldownCheckBox, "Pin is pulled down if checked")
        Me._PulldownCheckBox.UseVisualStyleBackColor = True
        '
        '_PinPullupCheckBox
        '
        Me._PinPullupCheckBox.AutoSize = True
        Me._PinPullupCheckBox.Location = New System.Drawing.Point(18, 99)
        Me._PinPullupCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinPullupCheckBox.Name = "_PinPullupCheckBox"
        Me._PinPullupCheckBox.Size = New System.Drawing.Size(68, 21)
        Me._PinPullupCheckBox.TabIndex = 11
        Me._PinPullupCheckBox.Text = "Pull Up"
        Me._ToolTip.SetToolTip(Me._PinPullupCheckBox, "pin is pulled up if checked.")
        Me._PinPullupCheckBox.UseVisualStyleBackColor = True
        '
        '_GetPinConfigButton
        '
        Me._GetPinConfigButton.Location = New System.Drawing.Point(426, 22)
        Me._GetPinConfigButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GetPinConfigButton.Name = "_GetPinConfigButton"
        Me._GetPinConfigButton.Size = New System.Drawing.Size(61, 30)
        Me._GetPinConfigButton.TabIndex = 9
        Me._GetPinConfigButton.Text = "Get"
        Me._GetPinConfigButton.UseVisualStyleBackColor = True
        '
        '_SetPinConfigButton
        '
        Me._SetPinConfigButton.Location = New System.Drawing.Point(358, 22)
        Me._SetPinConfigButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetPinConfigButton.Name = "_SetPinConfigButton"
        Me._SetPinConfigButton.Size = New System.Drawing.Size(61, 30)
        Me._SetPinConfigButton.TabIndex = 8
        Me._SetPinConfigButton.Text = "Set"
        Me._SetPinConfigButton.UseVisualStyleBackColor = True
        '
        '_PinConfigGroupBox
        '
        Me._PinConfigGroupBox.Controls.Add(Me._OpenDrainCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._PinPullupCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._EventPeriodNumeric)
        Me._PinConfigGroupBox.Controls.Add(Me._PulldownCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._DebounceEnabledCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._EventPeriodNumericLabel)
        Me._PinConfigGroupBox.Controls.Add(Me._EventTypeComboBoxLabel)
        Me._PinConfigGroupBox.Controls.Add(Me._EventTypeComboBox)
        Me._PinConfigGroupBox.Controls.Add(Me._GetPinConfigButton)
        Me._PinConfigGroupBox.Controls.Add(Me._SetPinConfigButton)
        Me._PinConfigGroupBox.Controls.Add(Me._PinDirectionComboBox)
        Me._PinConfigGroupBox.Controls.Add(Me._PinDirectionComboBoxLabel)
        Me._PinConfigGroupBox.Controls.Add(Me._PinEnabledCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._PinComboBox)
        Me._PinConfigGroupBox.Controls.Add(Me._PinComboBoxLabel)
        Me._PinConfigGroupBox.Location = New System.Drawing.Point(14, 201)
        Me._PinConfigGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinConfigGroupBox.Name = "_PinConfigGroupBox"
        Me._PinConfigGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinConfigGroupBox.Size = New System.Drawing.Size(493, 129)
        Me._PinConfigGroupBox.TabIndex = 5
        Me._PinConfigGroupBox.TabStop = False
        Me._PinConfigGroupBox.Text = "Pin Configuration"
        '
        '_EventPeriodNumeric
        '
        Me._EventPeriodNumeric.Location = New System.Drawing.Point(424, 97)
        Me._EventPeriodNumeric.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me._EventPeriodNumeric.Name = "_EventPeriodNumeric"
        Me._EventPeriodNumeric.Size = New System.Drawing.Size(60, 25)
        Me._EventPeriodNumeric.TabIndex = 18
        Me._ToolTip.SetToolTip(Me._EventPeriodNumeric, "Selects the event period in ms")
        '
        '_PinDirectionComboBox
        '
        Me._PinDirectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PinDirectionComboBox.FormattingEnabled = True
        Me._PinDirectionComboBox.Location = New System.Drawing.Point(80, 62)
        Me._PinDirectionComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinDirectionComboBox.Name = "_PinDirectionComboBox"
        Me._PinDirectionComboBox.Size = New System.Drawing.Size(79, 25)
        Me._PinDirectionComboBox.TabIndex = 4
        Me._ToolTip.SetToolTip(Me._PinDirectionComboBox, "Selects pin direction.")
        '
        '_PinDirectionComboBoxLabel
        '
        Me._PinDirectionComboBoxLabel.AutoSize = True
        Me._PinDirectionComboBoxLabel.Location = New System.Drawing.Point(15, 66)
        Me._PinDirectionComboBoxLabel.Name = "_PinDirectionComboBoxLabel"
        Me._PinDirectionComboBoxLabel.Size = New System.Drawing.Size(63, 17)
        Me._PinDirectionComboBoxLabel.TabIndex = 3
        Me._PinDirectionComboBoxLabel.Text = "Direction:"
        Me._PinDirectionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PinEnabledCheckBox
        '
        Me._PinEnabledCheckBox.AutoSize = True
        Me._PinEnabledCheckBox.Location = New System.Drawing.Point(156, 27)
        Me._PinEnabledCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinEnabledCheckBox.Name = "_PinEnabledCheckBox"
        Me._PinEnabledCheckBox.Size = New System.Drawing.Size(74, 21)
        Me._PinEnabledCheckBox.TabIndex = 2
        Me._PinEnabledCheckBox.Text = "Enabled"
        Me._PinEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_PinComboBox
        '
        Me._PinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PinComboBox.FormattingEnabled = True
        Me._PinComboBox.Location = New System.Drawing.Point(40, 25)
        Me._PinComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinComboBox.Name = "_PinComboBox"
        Me._PinComboBox.Size = New System.Drawing.Size(100, 25)
        Me._PinComboBox.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._PinComboBox, "Selects the pin number.")
        '
        '_PinComboBoxLabel
        '
        Me._PinComboBoxLabel.AutoSize = True
        Me._PinComboBoxLabel.Location = New System.Drawing.Point(10, 29)
        Me._PinComboBoxLabel.Name = "_PinComboBoxLabel"
        Me._PinComboBoxLabel.Size = New System.Drawing.Size(28, 17)
        Me._PinComboBoxLabel.TabIndex = 0
        Me._PinComboBoxLabel.Text = "Pin:"
        Me._PinComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EventLogTextBox
        '
        Me._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EventLogTextBox.Location = New System.Drawing.Point(3, 21)
        Me._EventLogTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EventLogTextBox.Multiline = True
        Me._EventLogTextBox.Name = "_EventLogTextBox"
        Me._EventLogTextBox.ReadOnly = True
        Me._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._EventLogTextBox.Size = New System.Drawing.Size(487, 226)
        Me._EventLogTextBox.TabIndex = 7
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_EventLogGroupBox
        '
        Me._EventLogGroupBox.Controls.Add(Me._EventLogTextBox)
        Me._EventLogGroupBox.Location = New System.Drawing.Point(14, 343)
        Me._EventLogGroupBox.Name = "_EventLogGroupBox"
        Me._EventLogGroupBox.Size = New System.Drawing.Size(493, 250)
        Me._EventLogGroupBox.TabIndex = 18
        Me._EventLogGroupBox.TabStop = False
        Me._EventLogGroupBox.Text = "Event Log"
        '
        '_GpioPinControl1
        '
        Me._GpioPinControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._GpioPinControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me._GpioPinControl1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioPinControl1.Location = New System.Drawing.Point(0, 0)
        Me._GpioPinControl1.Modality = isr.Diolan.DeviceModalities.Gpio
        Me._GpioPinControl1.Name = "_GpioPinControl1"
        Me._GpioPinControl1.Size = New System.Drawing.Size(520, 29)
        Me._GpioPinControl1.TabIndex = 19
        '
        '_GpioPinControl2
        '
        Me._GpioPinControl2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._GpioPinControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me._GpioPinControl2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioPinControl2.Location = New System.Drawing.Point(0, 29)
        Me._GpioPinControl2.Modality = isr.Diolan.DeviceModalities.Gpio
        Me._GpioPinControl2.Name = "_GpioPinControl2"
        Me._GpioPinControl2.Size = New System.Drawing.Size(520, 28)
        Me._GpioPinControl2.TabIndex = 20
        '
        '_DeviceGroupBox
        '
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumeric)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumericLabel)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceInfoTextBox)
        Me._DeviceGroupBox.Controls.Add(Me._OpenDeviceModalityButton)
        Me._DeviceGroupBox.Location = New System.Drawing.Point(17, 62)
        Me._DeviceGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceGroupBox.Name = "_DeviceGroupBox"
        Me._DeviceGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceGroupBox.Size = New System.Drawing.Size(310, 65)
        Me._DeviceGroupBox.TabIndex = 36
        Me._DeviceGroupBox.TabStop = False
        Me._DeviceGroupBox.Text = "Device"
        '
        '_DeviceIdNumeric
        '
        Me._DeviceIdNumeric.Location = New System.Drawing.Point(33, 27)
        Me._DeviceIdNumeric.Name = "_DeviceIdNumeric"
        Me._DeviceIdNumeric.Size = New System.Drawing.Size(49, 25)
        Me._DeviceIdNumeric.TabIndex = 11
        Me._ToolTip.SetToolTip(Me._DeviceIdNumeric, "Select device id")
        '
        '_DeviceIdNumericLabel
        '
        Me._DeviceIdNumericLabel.AutoSize = True
        Me._DeviceIdNumericLabel.Location = New System.Drawing.Point(6, 31)
        Me._DeviceIdNumericLabel.Name = "_DeviceIdNumericLabel"
        Me._DeviceIdNumericLabel.Size = New System.Drawing.Size(23, 17)
        Me._DeviceIdNumericLabel.TabIndex = 10
        Me._DeviceIdNumericLabel.Text = "ID:"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Location = New System.Drawing.Point(163, 27)
        Me._DeviceInfoTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(136, 25)
        Me._DeviceInfoTextBox.TabIndex = 7
        '
        '_OpenDeviceModalityButton
        '
        Me._OpenDeviceModalityButton.Location = New System.Drawing.Point(91, 24)
        Me._OpenDeviceModalityButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton"
        Me._OpenDeviceModalityButton.Size = New System.Drawing.Size(70, 30)
        Me._OpenDeviceModalityButton.TabIndex = 5
        Me._OpenDeviceModalityButton.Text = "Open"
        Me._ToolTip.SetToolTip(Me._OpenDeviceModalityButton, "Opens the device")
        Me._OpenDeviceModalityButton.UseVisualStyleBackColor = True
        '
        'GpioConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(520, 602)
        Me.Controls.Add(Me._DeviceGroupBox)
        Me.Controls.Add(Me._GpioPinControl2)
        Me.Controls.Add(Me._GpioPinControl1)
        Me.Controls.Add(Me._EventLogGroupBox)
        Me.Controls.Add(Me._SubsystemGroupBox)
        Me.Controls.Add(Me._PinConfigGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "GpioConsole"
        Me.Text = "General Purpose I/O Console"
        Me._SubsystemGroupBox.ResumeLayout(False)
        Me._SubsystemGroupBox.PerformLayout()
        CType(Me._DebounceNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._PinConfigGroupBox.ResumeLayout(False)
        Me._PinConfigGroupBox.PerformLayout()
        CType(Me._EventPeriodNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._EventLogGroupBox.ResumeLayout(False)
        Me._EventLogGroupBox.PerformLayout()
        Me._DeviceGroupBox.ResumeLayout(False)
        Me._DeviceGroupBox.PerformLayout()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _EventPeriodNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EventTypeComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _EventTypeComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _SubsystemGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _GetDebounceButton As System.Windows.Forms.Button
    Private WithEvents _SetDebounceButton As System.Windows.Forms.Button
    Private WithEvents _DebounceNumericLabel As System.Windows.Forms.Label
    Private WithEvents _DebounceEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _OpenDrainCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PulldownCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PinPullupCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _GetPinConfigButton As System.Windows.Forms.Button
    Private WithEvents _SetPinConfigButton As System.Windows.Forms.Button
    Private WithEvents _PinConfigGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _PinDirectionComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _PinDirectionComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PinEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PinComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _PinComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _EventLogTextBox As System.Windows.Forms.TextBox
    Private WithEvents _DebounceNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _EventPeriodNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _EventLogGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _GpioPinControl2 As isr.Diolan.Forms.GpioPinControl
    Private WithEvents _GpioPinControl1 As isr.Diolan.Forms.GpioPinControl
    Private WithEvents _DeviceIdNumeric As NumericUpDown
    Private WithEvents _DeviceIdNumericLabel As Label
    Private WithEvents _OpenDeviceModalityButton As Button
    Private WithEvents _DeviceInfoTextBox As TextBox
    Private WithEvents _DeviceGroupBox As GroupBox
End Class
