Namespace My

    Partial Friend Class MyApplication

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Diolan Gpio Test Console"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Diolan General Purpose I/O Test Console"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Diolan.Gpio.Console.2019"

    End Class

End Namespace

