﻿Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.SubsystemExtensions
Imports isr.Diolan.Forms.SubsystemExtensions

''' <summary> Form for managing pulse width modulation. </summary>
''' <remarks>
''' Pulse width modulation (PWM) is a method for binary signals generation, which has 2 signal
''' periods (high and low). Pulse Width Modulation can be used to control analog circuits with
''' digital outputs of the processor. PWM is employed in a wide variety of applications, ranging
''' from measurement and communications to power control and conversion.
''' 
''' Pulse width modulation is also used to reduce the total power delivered to a load without
''' resulting in loss, which normally occurs when a power source is limited by a resistive
''' element. The underlying principle in the whole process is that the average power delivered is
''' directly proportional to the duty cycle modulation.
''' 
''' PWM module is present in DLN-1, DLN-2, DLN-4M and DLN-4S USB-GPIO adapters. (c) 2015
''' Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-29 </para>
''' </remarks>
Partial Public Class PwmConsole

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnModalityClosed()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " MODALITY "

    ''' <summary> The modality. </summary>
    ''' <value> The modality. </value>
    Public Property Modality As DeviceModalities = DeviceModalities.Pwm

    ''' <summary> Executes the modality closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityClosed()
        Me._PortComboBox.DataSource = Nothing
        Me._PortComboBox.Items.Clear()
        Me._ChannelComboBox.DataSource = Nothing
        Me._ChannelComboBox.Items.Clear()
        Me._Device = Nothing
    End Sub

    ''' <summary> Executes the modality closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnModalityClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            If Me.IsDeviceModalityOpen Then
            End If
        End If
    End Sub

    ''' <summary> Executes the modality opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityOpening()

        Me._Device = Me._DeviceConnector.Device
        Me._PortComboBox.DataSource = Nothing
        Me._PortComboBox.Items.Clear()
        Me._ChannelComboBox.DataSource = Nothing
        Me._ChannelComboBox.Items.Clear()

        ' Get port count
        If Me.Device.Pwm.Ports.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton,
                                         "Adapter '{0}' doesn't support Pulse-Width-Modulation interface.",
                                         Me.Device.Caption)
            Me._Device = Nothing
            Me._DeviceInfoTextBox.Text = "not supported"
        Else
            Me._DeviceInfoTextBox.Text = Me.Device.Caption
            Me.Device.Pwm.Ports.ListNumbers(Me._PortComboBox)
            Me._PortComboBox.SelectedIndex = 0    ' this will cause "OnChange" event
        End If

    End Sub

    ''' <summary> Queries if a modality is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
    Private Function IsModalityOpen() As Boolean
        Return Me._PortComboBox.DataSource IsNot Nothing AndAlso Me._PortComboBox.Items.Count > 0
    End Function

#End Region

#Region " PORT "

    ''' <summary> Select port. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pwmPort"> The PWM port. </param>
    Private Sub SelectPort(ByVal pwmPort As Dln.Pwm.Port)

        Me._PortEnabledCheckBox.Checked = pwmPort.Enabled

        Me._FrequencyNumeric.Maximum = pwmPort.MaxFrequency
        Me._FrequencyNumeric.Minimum = pwmPort.MinFrequency

        ' list the channels
        pwmPort.Channels.ListNumbers(Me._ChannelComboBox)
        Me._ChannelComboBox.SelectedIndex = 0
    End Sub

    ''' <summary> Port combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PortComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PortComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Pwm.Ports.Count Then
                    Dim pwmPort As Dln.Pwm.Port = Me.Device.Pwm.Ports(portNumber)
                    Me.SelectPort(pwmPort)
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Pwm.Ports.Count - 1)
                End If

            ElseIf Me.IsModalityOpen AndAlso Me._ErrorProvider IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Port enabled check box checked changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PortEnabledCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PortEnabledCheckBox.CheckedChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Pwm.Ports.Count Then
                    Dim pwmPort As Dln.Pwm.Port = Me.Device.Pwm.Ports(portNumber)
                    pwmPort.Enabled = Me._PortEnabledCheckBox.Checked
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Pwm.Ports.Count - 1)

                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region

#Region " CHANNEL "

    ''' <summary> Select port channel. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pwmChannel"> The PWM channel. </param>
    Private Sub SelectPortChannel(ByVal pwmChannel As Dln.Pwm.Channel)
        Me._ChannelEnabledCheckBox.Checked = pwmChannel.Enabled
        Me._FrequencyNumeric.Value = pwmChannel.Frequency
        Me._DutyCycleNumeric.Value = CDec(pwmChannel.DutyCycle)
    End Sub

    ''' <summary> Channel combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ChannelComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ChannelComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Pwm.Ports.Count Then
                    Dim pwmPort As Dln.Pwm.Port = Me.Device.Pwm.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < pwmPort.Channels.Count Then

                        Dim pwmChannel As Dln.Pwm.Channel = pwmPort.Channels(channelNumber)
                        Me.SelectPortChannel(pwmChannel)
                    ElseIf Me._ChannelComboBox.DataSource IsNot Nothing Then
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", pwmPort.Channels.Count - 1)
                    End If
                ElseIf Me._PortComboBox.DataSource IsNot Nothing Then
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Pwm.Ports.Count - 1)
                End If
            ElseIf Me.IsModalityOpen AndAlso Me._ErrorProvider IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Channel enabled check box checked changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ChannelEnabledCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ChannelEnabledCheckBox.CheckedChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Pwm.Ports.Count Then
                    Dim pwmPort As Dln.Pwm.Port = Me.Device.Pwm.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex

                    If channelNumber >= 0 AndAlso channelNumber < pwmPort.Channels.Count Then
                        Dim pwmChannel As Dln.Pwm.Channel = pwmPort.Channels(channelNumber)
                        pwmChannel.Enabled = Me._ChannelEnabledCheckBox.Checked
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", pwmPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Pwm.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Sets frequency button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetFrequencyButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetFrequencyButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Pwm.Ports.Count Then
                    Dim pwmPort As Dln.Pwm.Port = Me.Device.Pwm.Ports(portNumber)

                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < pwmPort.Channels.Count Then
                        Dim pwmChannel As Dln.Pwm.Channel = pwmPort.Channels(channelNumber)
                        pwmChannel.Frequency = CInt(Me._FrequencyNumeric.Value)
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", pwmPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Pwm.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets frequency button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GetFrequencyButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetFrequencyButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Pwm.Ports.Count Then

                    Dim pwmPort As Dln.Pwm.Port = Me.Device.Pwm.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < pwmPort.Channels.Count Then
                        Dim pwmChannel As Dln.Pwm.Channel = pwmPort.Channels(channelNumber)
                        Me._FrequencyNumeric.Value = pwmChannel.Frequency
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", pwmPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Pwm.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Sets duty cycle button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetDutyCycleButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetDutyCycleButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex

                If portNumber >= 0 AndAlso portNumber < Me.Device.Pwm.Ports.Count Then
                    Dim pwmPort As Dln.Pwm.Port = Me.Device.Pwm.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < pwmPort.Channels.Count Then
                        Dim pwmChannel As Dln.Pwm.Channel = pwmPort.Channels(channelNumber)
                        pwmChannel.DutyCycle = Convert.ToSingle(Me._DutyCycleNumeric.Value)
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", pwmPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Pwm.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Gets duty cycle button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GetDutyCycleButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetDutyCycleButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then

                Dim portNumber As Integer = Me._PortComboBox.SelectedIndex
                If portNumber >= 0 AndAlso portNumber < Me.Device.Pwm.Ports.Count Then
                    Dim pwmPort As Dln.Pwm.Port = Me.Device.Pwm.Ports(portNumber)
                    Dim channelNumber As Integer = Me._ChannelComboBox.SelectedIndex
                    If channelNumber >= 0 AndAlso channelNumber < pwmPort.Channels.Count Then
                        Dim pwmChannel As Dln.Pwm.Channel = pwmPort.Channels(channelNumber)
                        Me._DutyCycleNumeric.Value = CDec(pwmChannel.DutyCycle)
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Channel number {0} is out of range of [0,{1}]", pwmPort.Channels.Count - 1)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Port number {0} is out of range of [0,{1}]", Me.Device.Pwm.Ports.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region


End Class
