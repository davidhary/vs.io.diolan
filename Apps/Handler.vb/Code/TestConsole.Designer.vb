﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TestConsole
    Inherits isr.Core.Forma.FormBase

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                          <c>False</c> to release only unmanaged resources when called from the
    '''                          runtime finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnCustomDispose(disposing)
                If components IsNot Nothing Then components.Dispose() : components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TestConsole))
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._HandlerTabPage = New System.Windows.Forms.TabPage()
        Me._GpioHandlerPanel = New isr.Diolan.Forms.GpioHandlerDriverView()
        Me._GpioTabPage = New System.Windows.Forms.TabPage()
        Me._GpioPanel = New isr.Diolan.Forms.GpioView()
        Me._Tabs.SuspendLayout()
        Me._HandlerTabPage.SuspendLayout()
        Me._GpioTabPage.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._HandlerTabPage)
        Me._Tabs.Controls.Add(Me._GpioTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(443, 492)
        Me._Tabs.TabIndex = 0
        '
        '_HandlerTabPage
        '
        Me._HandlerTabPage.Controls.Add(Me._GpioHandlerPanel)
        Me._HandlerTabPage.Location = New System.Drawing.Point(4, 26)
        Me._HandlerTabPage.Name = "_HandlerTabPage"
        Me._HandlerTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._HandlerTabPage.Size = New System.Drawing.Size(435, 462)
        Me._HandlerTabPage.TabIndex = 0
        Me._HandlerTabPage.Text = "Handler"
        Me._HandlerTabPage.UseVisualStyleBackColor = True
        '
        '_GpioHandlerPanel
        '
        Me._GpioHandlerPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._GpioHandlerPanel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioHandlerPanel.Location = New System.Drawing.Point(3, 3)
        Me._GpioHandlerPanel.Name = "_GpioHandlerPanel"
        Me._GpioHandlerPanel.Size = New System.Drawing.Size(429, 456)
        Me._GpioHandlerPanel.TabIndex = 0
        '
        '_GpioTabPage
        '
        Me._GpioTabPage.Controls.Add(Me._GpioPanel)
        Me._GpioTabPage.Location = New System.Drawing.Point(4, 26)
        Me._GpioTabPage.Name = "_GpioTabPage"
        Me._GpioTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._GpioTabPage.Size = New System.Drawing.Size(435, 462)
        Me._GpioTabPage.TabIndex = 1
        Me._GpioTabPage.Text = "GPIO"
        Me._GpioTabPage.UseVisualStyleBackColor = True
        '
        '_GpioPanel
        '
        Me._GpioPanel.DeviceConnector = Nothing
        Me._GpioPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._GpioPanel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioPanel.Location = New System.Drawing.Point(3, 3)
        Me._GpioPanel.Name = "_GpioPanel"
        Me._GpioPanel.Size = New System.Drawing.Size(429, 456)
        Me._GpioPanel.TabIndex = 0
        '
        'TestConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(443, 492)
        Me.Controls.Add(Me._Tabs)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "TestConsole"
        Me.Text = "Handler Driver Console"
        Me._Tabs.ResumeLayout(False)
        Me._HandlerTabPage.ResumeLayout(False)
        Me._GpioTabPage.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Tabs As System.Windows.Forms.TabControl
    Private WithEvents _HandlerTabPage As System.Windows.Forms.TabPage
    Private WithEvents _GpioTabPage As System.Windows.Forms.TabPage
    Private WithEvents _GpioHandlerPanel As isr.Diolan.Forms.GpioHandlerDriverView
    Private WithEvents _GpioPanel As isr.Diolan.Forms.GpioView

End Class
