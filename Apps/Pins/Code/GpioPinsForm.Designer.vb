﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GpioPinsForm

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GpioPinsForm))
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._DeviceTabPage1 = New System.Windows.Forms.TabPage()
        Me._GpioPinsPanel1 = New isr.Diolan.Forms.GpioPinsView()
        Me._DeviceTabPage2 = New System.Windows.Forms.TabPage()
        Me._GpioPinsPanel2 = New isr.Diolan.Forms.GpioPinsView()
        Me._Tabs.SuspendLayout()
        Me._DeviceTabPage1.SuspendLayout()
        Me._DeviceTabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._DeviceTabPage1)
        Me._Tabs.Controls.Add(Me._DeviceTabPage2)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(446, 554)
        Me._Tabs.TabIndex = 0
        '
        '_DeviceTabPage1
        '
        Me._DeviceTabPage1.Controls.Add(Me._GpioPinsPanel1)
        Me._DeviceTabPage1.Location = New System.Drawing.Point(4, 26)
        Me._DeviceTabPage1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceTabPage1.Name = "_DeviceTabPage1"
        Me._DeviceTabPage1.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceTabPage1.Size = New System.Drawing.Size(438, 524)
        Me._DeviceTabPage1.TabIndex = 0
        Me._DeviceTabPage1.Text = "Device 0"
        Me._DeviceTabPage1.UseVisualStyleBackColor = True
        '
        '_GpioPinsPanel1
        '
        Me._GpioPinsPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me._GpioPinsPanel1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioPinsPanel1.Location = New System.Drawing.Point(3, 4)
        Me._GpioPinsPanel1.Name = "_GpioPinsPanel1"
        Me._GpioPinsPanel1.Size = New System.Drawing.Size(432, 516)
        Me._GpioPinsPanel1.TabIndex = 0
        '
        '_DeviceTabPage2
        '
        Me._DeviceTabPage2.Controls.Add(Me._GpioPinsPanel2)
        Me._DeviceTabPage2.Location = New System.Drawing.Point(4, 26)
        Me._DeviceTabPage2.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceTabPage2.Name = "_DeviceTabPage2"
        Me._DeviceTabPage2.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceTabPage2.Size = New System.Drawing.Size(438, 524)
        Me._DeviceTabPage2.TabIndex = 1
        Me._DeviceTabPage2.Text = "Device 2"
        Me._DeviceTabPage2.UseVisualStyleBackColor = True
        '
        '_GpioPinsPanel2
        '
        Me._GpioPinsPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me._GpioPinsPanel2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioPinsPanel2.Location = New System.Drawing.Point(3, 4)
        Me._GpioPinsPanel2.Name = "_GpioPinsPanel2"
        Me._GpioPinsPanel2.Size = New System.Drawing.Size(432, 516)
        Me._GpioPinsPanel2.TabIndex = 0
        '
        'GpioPinsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(446, 554)
        Me.Controls.Add(Me._Tabs)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "GpioPinsForm"
        Me.Text = "Gpio Pins Console"
        Me._Tabs.ResumeLayout(False)
        Me._DeviceTabPage1.ResumeLayout(False)
        Me._DeviceTabPage2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _Tabs As TabControl
    Private WithEvents _DeviceTabPage1 As TabPage
    Private WithEvents _DeviceTabPage2 As TabPage
    Private WithEvents _GpioPinsPanel1 As Forms.GpioPinsView
    Private WithEvents _GpioPinsPanel2 As Forms.GpioPinsView
End Class
