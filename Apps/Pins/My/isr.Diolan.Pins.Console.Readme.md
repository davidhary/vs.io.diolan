## ISR Diolan Pins Console<sub>&trade;</sub>: Console for Controlling Diolan GPIO Pins.
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*1.1.5942 2016-04-08*  
Created.

\(C\) 2016 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:


Core Services Library:\
Safe events: ([Code Project](http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx)
[dream in code](http://www.DreamInCode.net/forums/user/334492-aeonhack/)
[dream in code](http://www.DreamInCode.net/code/snippet5016.htm))  
[Drop Shadow and Fade Form](http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)  
[Engineering Format](http://WallaceKelly.BlogSpot.com/)  
[Efficient Strong Enumerations](http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx)  
[Enumeration Extensions](http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx)  
[String Enumerator](http://www.codeproject.com/Articles/17472/StringEnumerator)

Diolan Core Library:  
[Diolan Core](https://bitbucket.org/davidhary/vs.IOdiolan)  
[Enum Extender](http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx)  
[Enum Flags](http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[DLN Library](http://www.dlnware.com)
