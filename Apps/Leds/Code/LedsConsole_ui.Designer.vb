﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LedsConsole

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LedsConsole))
        Me._SetLedStateButton = New System.Windows.Forms.Button()
        Me._LedStateComboBox = New System.Windows.Forms.ComboBox()
        Me._LedStateComboBoxLabel = New System.Windows.Forms.Label()
        Me._LedNumberComboBox = New System.Windows.Forms.ComboBox()
        Me._LedNumberComboBoxLabel = New System.Windows.Forms.Label()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._DurationNumeric = New System.Windows.Forms.NumericUpDown()
        Me._PulseButtonButton = New System.Windows.Forms.Button()
        Me._LedGroupBox = New System.Windows.Forms.GroupBox()
        Me._DurationNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceIdNumeric = New System.Windows.Forms.NumericUpDown()
        Me._DeviceIdNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceInfoTextBox = New System.Windows.Forms.TextBox()
        Me._OpenDeviceModalityButton = New System.Windows.Forms.Button()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DurationNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._LedGroupBox.SuspendLayout()
        Me._DeviceGroupBox.SuspendLayout()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_SetLedStateButton
        '
        Me._SetLedStateButton.Location = New System.Drawing.Point(231, 42)
        Me._SetLedStateButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetLedStateButton.Name = "_SetLedStateButton"
        Me._SetLedStateButton.Size = New System.Drawing.Size(74, 30)
        Me._SetLedStateButton.TabIndex = 9
        Me._SetLedStateButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetLedStateButton, "Sets the state of the selected LED.")
        Me._SetLedStateButton.UseVisualStyleBackColor = True
        '
        '_LedStateComboBox
        '
        Me._LedStateComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._LedStateComboBox.FormattingEnabled = True
        Me._LedStateComboBox.Location = New System.Drawing.Point(119, 45)
        Me._LedStateComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._LedStateComboBox.Name = "_LedStateComboBox"
        Me._LedStateComboBox.Size = New System.Drawing.Size(104, 25)
        Me._LedStateComboBox.TabIndex = 8
        Me._ToolTip.SetToolTip(Me._LedStateComboBox, "Selects the state of the selected LED.")
        '
        '_LedStateComboBoxLabel
        '
        Me._LedStateComboBoxLabel.AutoSize = True
        Me._LedStateComboBoxLabel.Location = New System.Drawing.Point(117, 24)
        Me._LedStateComboBoxLabel.Name = "_LedStateComboBoxLabel"
        Me._LedStateComboBoxLabel.Size = New System.Drawing.Size(63, 17)
        Me._LedStateComboBoxLabel.TabIndex = 7
        Me._LedStateComboBoxLabel.Text = "LED State"
        '
        '_LedNumberComboBox
        '
        Me._LedNumberComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._LedNumberComboBox.FormattingEnabled = True
        Me._LedNumberComboBox.Location = New System.Drawing.Point(18, 45)
        Me._LedNumberComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._LedNumberComboBox.Name = "_LedNumberComboBox"
        Me._LedNumberComboBox.Size = New System.Drawing.Size(93, 25)
        Me._LedNumberComboBox.TabIndex = 6
        Me._ToolTip.SetToolTip(Me._LedNumberComboBox, "Selected LED number.")
        '
        '_LedNumberComboBoxLabel
        '
        Me._LedNumberComboBoxLabel.AutoSize = True
        Me._LedNumberComboBoxLabel.Location = New System.Drawing.Point(18, 24)
        Me._LedNumberComboBoxLabel.Name = "_LedNumberComboBoxLabel"
        Me._LedNumberComboBoxLabel.Size = New System.Drawing.Size(82, 17)
        Me._LedNumberComboBoxLabel.TabIndex = 5
        Me._LedNumberComboBoxLabel.Text = "LED Number"
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_DurationNumeric
        '
        Me._DurationNumeric.Location = New System.Drawing.Point(120, 83)
        Me._DurationNumeric.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me._DurationNumeric.Name = "_DurationNumeric"
        Me._DurationNumeric.Size = New System.Drawing.Size(103, 25)
        Me._DurationNumeric.TabIndex = 10
        Me._ToolTip.SetToolTip(Me._DurationNumeric, "On duration in ms")
        Me._DurationNumeric.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        '_PulseButtonButton
        '
        Me._PulseButtonButton.Location = New System.Drawing.Point(229, 80)
        Me._PulseButtonButton.Name = "_PulseButtonButton"
        Me._PulseButtonButton.Size = New System.Drawing.Size(74, 30)
        Me._PulseButtonButton.TabIndex = 11
        Me._PulseButtonButton.Text = "Pulse"
        Me._ToolTip.SetToolTip(Me._PulseButtonButton, "Turn on for the specified duration")
        Me._PulseButtonButton.UseVisualStyleBackColor = True
        '
        '_LedGroupBox
        '
        Me._LedGroupBox.Controls.Add(Me._DurationNumericLabel)
        Me._LedGroupBox.Controls.Add(Me._PulseButtonButton)
        Me._LedGroupBox.Controls.Add(Me._DurationNumeric)
        Me._LedGroupBox.Controls.Add(Me._LedNumberComboBox)
        Me._LedGroupBox.Controls.Add(Me._LedNumberComboBoxLabel)
        Me._LedGroupBox.Controls.Add(Me._SetLedStateButton)
        Me._LedGroupBox.Controls.Add(Me._LedStateComboBoxLabel)
        Me._LedGroupBox.Controls.Add(Me._LedStateComboBox)
        Me._LedGroupBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._LedGroupBox.Location = New System.Drawing.Point(0, 72)
        Me._LedGroupBox.Name = "_LedGroupBox"
        Me._LedGroupBox.Size = New System.Drawing.Size(323, 117)
        Me._LedGroupBox.TabIndex = 18
        Me._LedGroupBox.TabStop = False
        Me._LedGroupBox.Text = "LED"
        '
        '_DurationNumericLabel
        '
        Me._DurationNumericLabel.AutoSize = True
        Me._DurationNumericLabel.Location = New System.Drawing.Point(27, 87)
        Me._DurationNumericLabel.Name = "_DurationNumericLabel"
        Me._DurationNumericLabel.Size = New System.Drawing.Size(90, 17)
        Me._DurationNumericLabel.TabIndex = 12
        Me._DurationNumericLabel.Text = "Duration [ms]:"
        Me._DurationNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_DeviceGroupBox
        '
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumeric)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumericLabel)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceInfoTextBox)
        Me._DeviceGroupBox.Controls.Add(Me._OpenDeviceModalityButton)
        Me._DeviceGroupBox.Location = New System.Drawing.Point(6, 5)
        Me._DeviceGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceGroupBox.Name = "_DeviceGroupBox"
        Me._DeviceGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceGroupBox.Size = New System.Drawing.Size(310, 65)
        Me._DeviceGroupBox.TabIndex = 36
        Me._DeviceGroupBox.TabStop = False
        Me._DeviceGroupBox.Text = "Device"
        '
        '_DeviceIdNumeric
        '
        Me._DeviceIdNumeric.Location = New System.Drawing.Point(33, 27)
        Me._DeviceIdNumeric.Name = "_DeviceIdNumeric"
        Me._DeviceIdNumeric.Size = New System.Drawing.Size(49, 25)
        Me._DeviceIdNumeric.TabIndex = 11
        Me._ToolTip.SetToolTip(Me._DeviceIdNumeric, "Select device id")
        '
        '_DeviceIdNumericLabel
        '
        Me._DeviceIdNumericLabel.AutoSize = True
        Me._DeviceIdNumericLabel.Location = New System.Drawing.Point(6, 31)
        Me._DeviceIdNumericLabel.Name = "_DeviceIdNumericLabel"
        Me._DeviceIdNumericLabel.Size = New System.Drawing.Size(23, 17)
        Me._DeviceIdNumericLabel.TabIndex = 10
        Me._DeviceIdNumericLabel.Text = "ID:"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Location = New System.Drawing.Point(163, 27)
        Me._DeviceInfoTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(136, 25)
        Me._DeviceInfoTextBox.TabIndex = 7
        '
        '_OpenDeviceModalityButton
        '
        Me._OpenDeviceModalityButton.Location = New System.Drawing.Point(91, 24)
        Me._OpenDeviceModalityButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton"
        Me._OpenDeviceModalityButton.Size = New System.Drawing.Size(70, 30)
        Me._OpenDeviceModalityButton.TabIndex = 5
        Me._OpenDeviceModalityButton.Text = "Open"
        Me._ToolTip.SetToolTip(Me._OpenDeviceModalityButton, "Opens the device")
        Me._OpenDeviceModalityButton.UseVisualStyleBackColor = True
        '
        'LedsConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(323, 189)
        Me.Controls.Add(Me._DeviceGroupBox)
        Me.Controls.Add(Me._LedGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.Name = "LedsConsole"
        Me.Text = "LED Console"
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DurationNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._LedGroupBox.ResumeLayout(False)
        Me._LedGroupBox.PerformLayout()
        Me._DeviceGroupBox.ResumeLayout(False)
        Me._DeviceGroupBox.PerformLayout()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _SetLedStateButton As System.Windows.Forms.Button
    Private WithEvents _LedStateComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _LedStateComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _LedNumberComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _LedNumberComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _LedGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DurationNumericLabel As System.Windows.Forms.Label
    Private WithEvents _PulseButtonButton As System.Windows.Forms.Button
    Private WithEvents _DurationNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _DeviceIdNumeric As NumericUpDown
    Private WithEvents _DeviceIdNumericLabel As Label
    Private WithEvents _OpenDeviceModalityButton As Button
    Private WithEvents _DeviceInfoTextBox As TextBox
    Private WithEvents _DeviceGroupBox As GroupBox
End Class
