﻿Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.SubsystemExtensions
Imports isr.Diolan.Forms.SubsystemExtensions

''' <summary> Leds console. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-30 </para>
''' </remarks>
Partial Public Class LedsConsole

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnModalityClosed()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " MODALITY "

    ''' <summary> The modality. </summary>
    ''' <value> The modality. </value>
    Public Property Modality As DeviceModalities = DeviceModalities.Leds

    ''' <summary> Executes the modality closed actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityClosed()
        If Me._LedSubsystem IsNot Nothing Then
            Me._LedSubsystem.Dispose()
            Me._LedSubsystem = Nothing
        End If
        Me._LedNumberComboBox.DataSource = Nothing
        Me._LedNumberComboBox.Items.Clear()
        Me._LedStateComboBox.DataSource = Nothing
        Me._LedStateComboBox.Items.Clear()
        Me._Device = Nothing
    End Sub

    ''' <summary> Executes the modality closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnModalityClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            If Me.IsModalityOpen Then
            End If
        End If
    End Sub

    ''' <summary> Executes the modality opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityOpening()

        Me._Device = Me._DeviceConnector.Device
        Me._LedNumberComboBox.DataSource = Nothing
        Me._LedNumberComboBox.Items.Clear()
        If Me.Device.Led.Leds.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton,
                                         "Adapter '{0}' doesn't support LED interface.",
                                         Me.Device.Caption)
            Me._Device = Nothing
            Me._DeviceInfoTextBox.Text = "not supported"
        Else
            Me._DeviceInfoTextBox.Text = Me.Device.Caption
            Me.Device.Led.Leds.ListNumbers(Me._LedNumberComboBox)

            ' This will get state for LED0
            Me._LedNumberComboBox.SelectedIndex = 0
        End If

    End Sub

    ''' <summary> Queries if a modality is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
    Private Function IsModalityOpen() As Boolean
        Return Me._LedNumberComboBox.DataSource IsNot Nothing AndAlso Me._LedNumberComboBox.Items.Count > 0
    End Function

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> LED number combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LedNumberComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _LedNumberComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim ledNumber As Integer = Me._LedNumberComboBox.SelectedIndex
                Dim led As Dln.Led.Led = Me.Device.Led.Leds(ledNumber)
                Me._LedStateComboBox.DataSource = led.SupportedStates
                Me._LedStateComboBox.SelectedItem = led.State
            ElseIf Me.IsModalityOpen AndAlso Me._ErrorProvider IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If

        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

    ''' <summary> Sets LED state button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetLedStateButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetLedStateButton.Click
        Try
            If Me.IsDeviceModalityOpen Then
                Dim ledNumber As Integer = Me._LedNumberComboBox.SelectedIndex
                Dim led As Dln.Led.Led = Me.Device.Led.Leds(ledNumber)
                led.State = CType(Me._LedStateComboBox.SelectedItem, Dln.Led.State)
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception

            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region

#Region " PULSE LED "

    ''' <summary> The LED subsystem. </summary>
    Private _LedSubsystem As isr.Diolan.LedSubsystem

    ''' <summary> Pulse button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PulseButtonButton_Click(sender As Object, e As System.EventArgs) Handles _PulseButtonButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim ledIndex As Integer = Me._LedNumberComboBox.SelectedIndex
                If Me._LedSubsystem Is Nothing Then Me._LedSubsystem = New isr.Diolan.LedSubsystem
                Me._LedSubsystem.PulseLed(Me.Device.Led.Leds(ledIndex), TimeSpan.FromMilliseconds(Me._DurationNumeric.Value))
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)

            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        End Try
    End Sub

#End Region


End Class
