Namespace My

    Partial Friend Class MyApplication

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Diolan Counter Timer Test Console"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Diolan Counter Timer Test Console"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Diolan.Counter.Timer.Test.Console.2019"

    End Class

End Namespace

