﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CounterTimerConsole

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CounterTimerConsole))
        Me._GetEventConfigButton = New System.Windows.Forms.Button()
        Me._SetEventConfigButton = New System.Windows.Forms.Button()
        Me._EventConfigGroupBox = New System.Windows.Forms.GroupBox()
        Me._EventRepeatIntervalUnitsLabel = New System.Windows.Forms.Label()
        Me._EventRepeatIntervalNumeric = New System.Windows.Forms.NumericUpDown()
        Me._RepeatEventTypeCheckBox = New System.Windows.Forms.CheckBox()
        Me._MatchEventTypeCheckBox = New System.Windows.Forms.CheckBox()
        Me._OverflowEventTypeCheckBox = New System.Windows.Forms.CheckBox()
        Me._EventRepeatIntervalNumericLabel = New System.Windows.Forms.Label()
        Me._EventTypeLabel = New System.Windows.Forms.Label()
        Me._GetModeButton = New System.Windows.Forms.Button()
        Me._PortComboBoxLabel = New System.Windows.Forms.Label()
        Me._SetModeButton = New System.Windows.Forms.Button()
        Me._ResetCounterCheckBox = New System.Windows.Forms.CheckBox()
        Me._ResetTimerCheckBox = New System.Windows.Forms.CheckBox()
        Me._ResetCounterTimerButton = New System.Windows.Forms.Button()
        Me._ModeGroupBox = New System.Windows.Forms.GroupBox()
        Me._ModeLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._CounterTimerModeComboBox = New System.Windows.Forms.ComboBox()
        Me._ModeLimitNumericLabel = New System.Windows.Forms.Label()
        Me._CounterTimerModeComboBoxLabel = New System.Windows.Forms.Label()
        Me._GetCounterTimerValuesButton = New System.Windows.Forms.Button()
        Me._ValueGroupBox = New System.Windows.Forms.GroupBox()
        Me._CountValueTextBox = New System.Windows.Forms.TextBox()
        Me._ElapsedTimeTextBox = New System.Windows.Forms.TextBox()
        Me._CountValueTextBoxLabel = New System.Windows.Forms.Label()
        Me._ElapsedTimeTextBoxLabel = New System.Windows.Forms.Label()
        Me._PortEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._PortComboBox = New System.Windows.Forms.ComboBox()
        Me._EventLogTextBox = New System.Windows.Forms.TextBox()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ResolutionTextBox = New System.Windows.Forms.TextBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ResolutionTextBoxLabel = New System.Windows.Forms.Label()
        Me._EventLogGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceGroupBox = New System.Windows.Forms.GroupBox()
        Me._DeviceIdNumeric = New System.Windows.Forms.NumericUpDown()
        Me._DeviceIdNumericLabel = New System.Windows.Forms.Label()
        Me._DeviceInfoTextBox = New System.Windows.Forms.TextBox()
        Me._OpenDeviceModalityButton = New System.Windows.Forms.Button()
        Me._EventConfigGroupBox.SuspendLayout()
        CType(Me._EventRepeatIntervalNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ModeGroupBox.SuspendLayout()
        CType(Me._ModeLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ValueGroupBox.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._EventLogGroupBox.SuspendLayout()
        Me._DeviceGroupBox.SuspendLayout()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_GetEventConfigButton
        '
        Me._GetEventConfigButton.Location = New System.Drawing.Point(194, 57)
        Me._GetEventConfigButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GetEventConfigButton.Name = "_GetEventConfigButton"
        Me._GetEventConfigButton.Size = New System.Drawing.Size(51, 30)
        Me._GetEventConfigButton.TabIndex = 8
        Me._GetEventConfigButton.Text = "Get"
        Me._GetEventConfigButton.UseVisualStyleBackColor = True
        '
        '_SetEventConfigButton
        '
        Me._SetEventConfigButton.Location = New System.Drawing.Point(194, 19)
        Me._SetEventConfigButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetEventConfigButton.Name = "_SetEventConfigButton"
        Me._SetEventConfigButton.Size = New System.Drawing.Size(51, 30)
        Me._SetEventConfigButton.TabIndex = 7
        Me._SetEventConfigButton.Text = "Set"
        Me._SetEventConfigButton.UseVisualStyleBackColor = True
        '
        '_EventConfigGroupBox
        '
        Me._EventConfigGroupBox.Controls.Add(Me._EventRepeatIntervalUnitsLabel)
        Me._EventConfigGroupBox.Controls.Add(Me._EventRepeatIntervalNumeric)
        Me._EventConfigGroupBox.Controls.Add(Me._GetEventConfigButton)
        Me._EventConfigGroupBox.Controls.Add(Me._SetEventConfigButton)
        Me._EventConfigGroupBox.Controls.Add(Me._RepeatEventTypeCheckBox)
        Me._EventConfigGroupBox.Controls.Add(Me._MatchEventTypeCheckBox)
        Me._EventConfigGroupBox.Controls.Add(Me._OverflowEventTypeCheckBox)
        Me._EventConfigGroupBox.Controls.Add(Me._EventRepeatIntervalNumericLabel)
        Me._EventConfigGroupBox.Controls.Add(Me._EventTypeLabel)
        Me._EventConfigGroupBox.Location = New System.Drawing.Point(14, 403)
        Me._EventConfigGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EventConfigGroupBox.Name = "_EventConfigGroupBox"
        Me._EventConfigGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EventConfigGroupBox.Size = New System.Drawing.Size(256, 152)
        Me._EventConfigGroupBox.TabIndex = 13
        Me._EventConfigGroupBox.TabStop = False
        Me._EventConfigGroupBox.Text = "Event Configuration"
        '
        '_EventRepeatIntervalUnitsLabel
        '
        Me._EventRepeatIntervalUnitsLabel.AutoSize = True
        Me._EventRepeatIntervalUnitsLabel.Location = New System.Drawing.Point(192, 120)
        Me._EventRepeatIntervalUnitsLabel.Name = "_EventRepeatIntervalUnitsLabel"
        Me._EventRepeatIntervalUnitsLabel.Size = New System.Drawing.Size(25, 17)
        Me._EventRepeatIntervalUnitsLabel.TabIndex = 17
        Me._EventRepeatIntervalUnitsLabel.Text = "ms"
        '
        '_EventRepeatIntervalNumeric
        '
        Me._EventRepeatIntervalNumeric.Location = New System.Drawing.Point(110, 116)
        Me._EventRepeatIntervalNumeric.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me._EventRepeatIntervalNumeric.Name = "_EventRepeatIntervalNumeric"
        Me._EventRepeatIntervalNumeric.Size = New System.Drawing.Size(80, 25)
        Me._EventRepeatIntervalNumeric.TabIndex = 9
        '
        '_RepeatEventTypeCheckBox
        '
        Me._RepeatEventTypeCheckBox.AutoSize = True
        Me._RepeatEventTypeCheckBox.Location = New System.Drawing.Point(110, 85)
        Me._RepeatEventTypeCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._RepeatEventTypeCheckBox.Name = "_RepeatEventTypeCheckBox"
        Me._RepeatEventTypeCheckBox.Size = New System.Drawing.Size(68, 21)
        Me._RepeatEventTypeCheckBox.TabIndex = 4
        Me._RepeatEventTypeCheckBox.Text = "Repeat"
        Me._RepeatEventTypeCheckBox.UseVisualStyleBackColor = True
        '
        '_MatchEventTypeCheckBox
        '
        Me._MatchEventTypeCheckBox.AutoSize = True
        Me._MatchEventTypeCheckBox.Location = New System.Drawing.Point(110, 55)
        Me._MatchEventTypeCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._MatchEventTypeCheckBox.Name = "_MatchEventTypeCheckBox"
        Me._MatchEventTypeCheckBox.Size = New System.Drawing.Size(63, 21)
        Me._MatchEventTypeCheckBox.TabIndex = 3
        Me._MatchEventTypeCheckBox.Text = "Match"
        Me._MatchEventTypeCheckBox.UseVisualStyleBackColor = True
        '
        '_OverflowEventTypeCheckBox
        '
        Me._OverflowEventTypeCheckBox.AutoSize = True
        Me._OverflowEventTypeCheckBox.Location = New System.Drawing.Point(110, 28)
        Me._OverflowEventTypeCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._OverflowEventTypeCheckBox.Name = "_OverflowEventTypeCheckBox"
        Me._OverflowEventTypeCheckBox.Size = New System.Drawing.Size(79, 21)
        Me._OverflowEventTypeCheckBox.TabIndex = 2
        Me._OverflowEventTypeCheckBox.Text = "Overflow"
        Me._OverflowEventTypeCheckBox.UseVisualStyleBackColor = True
        '
        '_EventRepeatIntervalNumericLabel
        '
        Me._EventRepeatIntervalNumericLabel.AutoSize = True
        Me._EventRepeatIntervalNumericLabel.Location = New System.Drawing.Point(10, 120)
        Me._EventRepeatIntervalNumericLabel.Name = "_EventRepeatIntervalNumericLabel"
        Me._EventRepeatIntervalNumericLabel.Size = New System.Drawing.Size(98, 17)
        Me._EventRepeatIntervalNumericLabel.TabIndex = 1
        Me._EventRepeatIntervalNumericLabel.Text = "Repeat Interval:"
        Me._EventRepeatIntervalNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EventTypeLabel
        '
        Me._EventTypeLabel.AutoSize = True
        Me._EventTypeLabel.Location = New System.Drawing.Point(28, 28)
        Me._EventTypeLabel.Name = "_EventTypeLabel"
        Me._EventTypeLabel.Size = New System.Drawing.Size(79, 17)
        Me._EventTypeLabel.TabIndex = 0
        Me._EventTypeLabel.Text = "Event Types:"
        '
        '_GetModeButton
        '
        Me._GetModeButton.Location = New System.Drawing.Point(194, 57)
        Me._GetModeButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GetModeButton.Name = "_GetModeButton"
        Me._GetModeButton.Size = New System.Drawing.Size(51, 30)
        Me._GetModeButton.TabIndex = 5
        Me._GetModeButton.Text = "Get"
        Me._GetModeButton.UseVisualStyleBackColor = True
        '
        '_PortComboBoxLabel
        '
        Me._PortComboBoxLabel.AutoSize = True
        Me._PortComboBoxLabel.Location = New System.Drawing.Point(51, 90)
        Me._PortComboBoxLabel.Name = "_PortComboBoxLabel"
        Me._PortComboBoxLabel.Size = New System.Drawing.Size(35, 17)
        Me._PortComboBoxLabel.TabIndex = 8
        Me._PortComboBoxLabel.Text = "Port:"
        Me._PortComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SetModeButton
        '
        Me._SetModeButton.Location = New System.Drawing.Point(194, 22)
        Me._SetModeButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetModeButton.Name = "_SetModeButton"
        Me._SetModeButton.Size = New System.Drawing.Size(51, 30)
        Me._SetModeButton.TabIndex = 6
        Me._SetModeButton.Text = "Set"
        Me._SetModeButton.UseVisualStyleBackColor = True
        '
        '_ResetCounterCheckBox
        '
        Me._ResetCounterCheckBox.AutoSize = True
        Me._ResetCounterCheckBox.Location = New System.Drawing.Point(113, 98)
        Me._ResetCounterCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ResetCounterCheckBox.Name = "_ResetCounterCheckBox"
        Me._ResetCounterCheckBox.Size = New System.Drawing.Size(73, 21)
        Me._ResetCounterCheckBox.TabIndex = 7
        Me._ResetCounterCheckBox.Text = "Counter"
        Me._ToolTip.SetToolTip(Me._ResetCounterCheckBox, "Selects counter for reset")
        Me._ResetCounterCheckBox.UseVisualStyleBackColor = True
        '
        '_ResetTimerCheckBox
        '
        Me._ResetTimerCheckBox.AutoSize = True
        Me._ResetTimerCheckBox.Location = New System.Drawing.Point(45, 98)
        Me._ResetTimerCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ResetTimerCheckBox.Name = "_ResetTimerCheckBox"
        Me._ResetTimerCheckBox.Size = New System.Drawing.Size(60, 21)
        Me._ResetTimerCheckBox.TabIndex = 6
        Me._ResetTimerCheckBox.Text = "Timer"
        Me._ToolTip.SetToolTip(Me._ResetTimerCheckBox, "Selects timer for reset")
        Me._ResetTimerCheckBox.UseVisualStyleBackColor = True
        '
        '_ResetCounterTimerButton
        '
        Me._ResetCounterTimerButton.Location = New System.Drawing.Point(194, 94)
        Me._ResetCounterTimerButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ResetCounterTimerButton.Name = "_ResetCounterTimerButton"
        Me._ResetCounterTimerButton.Size = New System.Drawing.Size(51, 30)
        Me._ResetCounterTimerButton.TabIndex = 5
        Me._ResetCounterTimerButton.Text = "Reset"
        Me._ToolTip.SetToolTip(Me._ResetCounterTimerButton, "Resets the counter and or timer")
        Me._ResetCounterTimerButton.UseVisualStyleBackColor = True
        '
        '_ModeGroupBox
        '
        Me._ModeGroupBox.Controls.Add(Me._ModeLimitNumeric)
        Me._ModeGroupBox.Controls.Add(Me._SetModeButton)
        Me._ModeGroupBox.Controls.Add(Me._GetModeButton)
        Me._ModeGroupBox.Controls.Add(Me._CounterTimerModeComboBox)
        Me._ModeGroupBox.Controls.Add(Me._ModeLimitNumericLabel)
        Me._ModeGroupBox.Controls.Add(Me._CounterTimerModeComboBoxLabel)
        Me._ModeGroupBox.Location = New System.Drawing.Point(14, 298)
        Me._ModeGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ModeGroupBox.Name = "_ModeGroupBox"
        Me._ModeGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ModeGroupBox.Size = New System.Drawing.Size(256, 97)
        Me._ModeGroupBox.TabIndex = 12
        Me._ModeGroupBox.TabStop = False
        Me._ModeGroupBox.Text = "Mode"
        '
        '_ModeLimitNumeric
        '
        Me._ModeLimitNumeric.Location = New System.Drawing.Point(55, 60)
        Me._ModeLimitNumeric.Name = "_ModeLimitNumeric"
        Me._ModeLimitNumeric.Size = New System.Drawing.Size(93, 25)
        Me._ModeLimitNumeric.TabIndex = 8
        '
        '_CounterTimerModeComboBox
        '
        Me._CounterTimerModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._CounterTimerModeComboBox.FormattingEnabled = True
        Me._CounterTimerModeComboBox.Location = New System.Drawing.Point(54, 25)
        Me._CounterTimerModeComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CounterTimerModeComboBox.Name = "_CounterTimerModeComboBox"
        Me._CounterTimerModeComboBox.Size = New System.Drawing.Size(93, 25)
        Me._CounterTimerModeComboBox.TabIndex = 2
        '
        '_ModeLimitNumericLabel
        '
        Me._ModeLimitNumericLabel.AutoSize = True
        Me._ModeLimitNumericLabel.Location = New System.Drawing.Point(14, 64)
        Me._ModeLimitNumericLabel.Name = "_ModeLimitNumericLabel"
        Me._ModeLimitNumericLabel.Size = New System.Drawing.Size(38, 17)
        Me._ModeLimitNumericLabel.TabIndex = 1
        Me._ModeLimitNumericLabel.Text = "Limit:"
        Me._ModeLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_CounterTimerModeComboBoxLabel
        '
        Me._CounterTimerModeComboBoxLabel.AutoSize = True
        Me._CounterTimerModeComboBoxLabel.Location = New System.Drawing.Point(6, 29)
        Me._CounterTimerModeComboBoxLabel.Name = "_CounterTimerModeComboBoxLabel"
        Me._CounterTimerModeComboBoxLabel.Size = New System.Drawing.Size(46, 17)
        Me._CounterTimerModeComboBoxLabel.TabIndex = 0
        Me._CounterTimerModeComboBoxLabel.Text = "Mode:"
        Me._CounterTimerModeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_GetCounterTimerValuesButton
        '
        Me._GetCounterTimerValuesButton.Location = New System.Drawing.Point(194, 25)
        Me._GetCounterTimerValuesButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GetCounterTimerValuesButton.Name = "_GetCounterTimerValuesButton"
        Me._GetCounterTimerValuesButton.Size = New System.Drawing.Size(51, 30)
        Me._GetCounterTimerValuesButton.TabIndex = 4
        Me._GetCounterTimerValuesButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetCounterTimerValuesButton, "Gets the current time and count values")
        Me._GetCounterTimerValuesButton.UseVisualStyleBackColor = True
        '
        '_ValueGroupBox
        '
        Me._ValueGroupBox.Controls.Add(Me._ResetCounterCheckBox)
        Me._ValueGroupBox.Controls.Add(Me._ResetTimerCheckBox)
        Me._ValueGroupBox.Controls.Add(Me._ResetCounterTimerButton)
        Me._ValueGroupBox.Controls.Add(Me._GetCounterTimerValuesButton)
        Me._ValueGroupBox.Controls.Add(Me._CountValueTextBox)
        Me._ValueGroupBox.Controls.Add(Me._ElapsedTimeTextBox)
        Me._ValueGroupBox.Controls.Add(Me._CountValueTextBoxLabel)
        Me._ValueGroupBox.Controls.Add(Me._ElapsedTimeTextBoxLabel)
        Me._ValueGroupBox.Location = New System.Drawing.Point(14, 158)
        Me._ValueGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ValueGroupBox.Name = "_ValueGroupBox"
        Me._ValueGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ValueGroupBox.Size = New System.Drawing.Size(256, 132)
        Me._ValueGroupBox.TabIndex = 11
        Me._ValueGroupBox.TabStop = False
        Me._ValueGroupBox.Text = "Value"
        '
        '_CountValueTextBox
        '
        Me._CountValueTextBox.Location = New System.Drawing.Point(100, 59)
        Me._CountValueTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._CountValueTextBox.Name = "_CountValueTextBox"
        Me._CountValueTextBox.ReadOnly = True
        Me._CountValueTextBox.Size = New System.Drawing.Size(82, 25)
        Me._CountValueTextBox.TabIndex = 3
        '
        '_ElapsedTimeTextBox
        '
        Me._ElapsedTimeTextBox.Location = New System.Drawing.Point(100, 25)
        Me._ElapsedTimeTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ElapsedTimeTextBox.Name = "_ElapsedTimeTextBox"
        Me._ElapsedTimeTextBox.ReadOnly = True
        Me._ElapsedTimeTextBox.Size = New System.Drawing.Size(82, 25)
        Me._ElapsedTimeTextBox.TabIndex = 2
        '
        '_CountValueTextBoxLabel
        '
        Me._CountValueTextBoxLabel.AutoSize = True
        Me._CountValueTextBoxLabel.Location = New System.Drawing.Point(19, 63)
        Me._CountValueTextBoxLabel.Name = "_CountValueTextBoxLabel"
        Me._CountValueTextBoxLabel.Size = New System.Drawing.Size(79, 17)
        Me._CountValueTextBoxLabel.TabIndex = 1
        Me._CountValueTextBoxLabel.Text = "Pulse Count:"
        Me._CountValueTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ElapsedTimeTextBoxLabel
        '
        Me._ElapsedTimeTextBoxLabel.AutoSize = True
        Me._ElapsedTimeTextBoxLabel.Location = New System.Drawing.Point(9, 29)
        Me._ElapsedTimeTextBoxLabel.Name = "_ElapsedTimeTextBoxLabel"
        Me._ElapsedTimeTextBoxLabel.Size = New System.Drawing.Size(89, 17)
        Me._ElapsedTimeTextBoxLabel.TabIndex = 0
        Me._ElapsedTimeTextBoxLabel.Text = "Elapsed Time:"
        Me._ElapsedTimeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PortEnabledCheckBox
        '
        Me._PortEnabledCheckBox.AutoSize = True
        Me._PortEnabledCheckBox.Location = New System.Drawing.Point(188, 88)
        Me._PortEnabledCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PortEnabledCheckBox.Name = "_PortEnabledCheckBox"
        Me._PortEnabledCheckBox.Size = New System.Drawing.Size(74, 21)
        Me._PortEnabledCheckBox.TabIndex = 10
        Me._PortEnabledCheckBox.Text = "Enabled"
        Me._PortEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_PortComboBox
        '
        Me._PortComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PortComboBox.FormattingEnabled = True
        Me._PortComboBox.Location = New System.Drawing.Point(88, 86)
        Me._PortComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PortComboBox.Name = "_PortComboBox"
        Me._PortComboBox.Size = New System.Drawing.Size(93, 25)
        Me._PortComboBox.TabIndex = 9
        '
        '_EventLogTextBox
        '
        Me._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EventLogTextBox.Location = New System.Drawing.Point(3, 21)
        Me._EventLogTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EventLogTextBox.Multiline = True
        Me._EventLogTextBox.Name = "_EventLogTextBox"
        Me._EventLogTextBox.ReadOnly = True
        Me._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._EventLogTextBox.Size = New System.Drawing.Size(310, 518)
        Me._EventLogTextBox.TabIndex = 15
        '
        '_ResolutionTextBox
        '
        Me._ResolutionTextBox.Location = New System.Drawing.Point(88, 120)
        Me._ResolutionTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ResolutionTextBox.Name = "_ResolutionTextBox"
        Me._ResolutionTextBox.ReadOnly = True
        Me._ResolutionTextBox.Size = New System.Drawing.Size(82, 25)
        Me._ResolutionTextBox.TabIndex = 2
        Me._ToolTip.SetToolTip(Me._ResolutionTextBox, "Counter/Timer resolution")
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_ResolutionTextBoxLabel
        '
        Me._ResolutionTextBoxLabel.AutoSize = True
        Me._ResolutionTextBoxLabel.Location = New System.Drawing.Point(14, 123)
        Me._ResolutionTextBoxLabel.Name = "_ResolutionTextBoxLabel"
        Me._ResolutionTextBoxLabel.Size = New System.Drawing.Size(72, 17)
        Me._ResolutionTextBoxLabel.TabIndex = 0
        Me._ResolutionTextBoxLabel.Text = "Resolution:"
        Me._ResolutionTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EventLogGroupBox
        '
        Me._EventLogGroupBox.Controls.Add(Me._EventLogTextBox)
        Me._EventLogGroupBox.Location = New System.Drawing.Point(281, 13)
        Me._EventLogGroupBox.Name = "_EventLogGroupBox"
        Me._EventLogGroupBox.Size = New System.Drawing.Size(316, 542)
        Me._EventLogGroupBox.TabIndex = 17
        Me._EventLogGroupBox.TabStop = False
        Me._EventLogGroupBox.Text = "Event Log"
        '
        '_DeviceGroupBox
        '
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumeric)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceIdNumericLabel)
        Me._DeviceGroupBox.Controls.Add(Me._DeviceInfoTextBox)
        Me._DeviceGroupBox.Controls.Add(Me._OpenDeviceModalityButton)
        Me._DeviceGroupBox.Location = New System.Drawing.Point(10, 3)
        Me._DeviceGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceGroupBox.Name = "_DeviceGroupBox"
        Me._DeviceGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceGroupBox.Size = New System.Drawing.Size(265, 65)
        Me._DeviceGroupBox.TabIndex = 36
        Me._DeviceGroupBox.TabStop = False
        Me._DeviceGroupBox.Text = "Device"
        '
        '_DeviceIdNumeric
        '
        Me._DeviceIdNumeric.Location = New System.Drawing.Point(33, 27)
        Me._DeviceIdNumeric.Name = "_DeviceIdNumeric"
        Me._DeviceIdNumeric.Size = New System.Drawing.Size(49, 25)
        Me._DeviceIdNumeric.TabIndex = 11
        Me._ToolTip.SetToolTip(Me._DeviceIdNumeric, "Select device id")
        '
        '_DeviceIdNumericLabel
        '
        Me._DeviceIdNumericLabel.AutoSize = True
        Me._DeviceIdNumericLabel.Location = New System.Drawing.Point(6, 31)
        Me._DeviceIdNumericLabel.Name = "_DeviceIdNumericLabel"
        Me._DeviceIdNumericLabel.Size = New System.Drawing.Size(23, 17)
        Me._DeviceIdNumericLabel.TabIndex = 10
        Me._DeviceIdNumericLabel.Text = "ID:"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Location = New System.Drawing.Point(156, 27)
        Me._DeviceInfoTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(104, 25)
        Me._DeviceInfoTextBox.TabIndex = 7
        '
        '_OpenDeviceModalityButton
        '
        Me._OpenDeviceModalityButton.Location = New System.Drawing.Point(90, 24)
        Me._OpenDeviceModalityButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton"
        Me._OpenDeviceModalityButton.Size = New System.Drawing.Size(58, 30)
        Me._OpenDeviceModalityButton.TabIndex = 5
        Me._OpenDeviceModalityButton.Text = "Open"
        Me._ToolTip.SetToolTip(Me._OpenDeviceModalityButton, "Opens the device")
        Me._OpenDeviceModalityButton.UseVisualStyleBackColor = True
        '
        'CounterTimerConsole
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(605, 566)
        Me.Controls.Add(Me._DeviceGroupBox)
        Me.Controls.Add(Me._EventLogGroupBox)
        Me.Controls.Add(Me._EventConfigGroupBox)
        Me.Controls.Add(Me._PortComboBoxLabel)
        Me.Controls.Add(Me._ModeGroupBox)
        Me.Controls.Add(Me._ResolutionTextBox)
        Me.Controls.Add(Me._ValueGroupBox)
        Me.Controls.Add(Me._ResolutionTextBoxLabel)
        Me.Controls.Add(Me._PortEnabledCheckBox)
        Me.Controls.Add(Me._PortComboBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "CounterTimerConsole"
        Me.Text = "Counter/Timer Control Panel"
        Me._EventConfigGroupBox.ResumeLayout(False)
        Me._EventConfigGroupBox.PerformLayout()
        CType(Me._EventRepeatIntervalNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ModeGroupBox.ResumeLayout(False)
        Me._ModeGroupBox.PerformLayout()
        CType(Me._ModeLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ValueGroupBox.ResumeLayout(False)
        Me._ValueGroupBox.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._EventLogGroupBox.ResumeLayout(False)
        Me._EventLogGroupBox.PerformLayout()
        Me._DeviceGroupBox.ResumeLayout(False)
        Me._DeviceGroupBox.PerformLayout()
        CType(Me._DeviceIdNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _GetEventConfigButton As System.Windows.Forms.Button
    Private WithEvents _SetEventConfigButton As System.Windows.Forms.Button
    Private WithEvents _EventConfigGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _RepeatEventTypeCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _MatchEventTypeCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _OverflowEventTypeCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _EventRepeatIntervalNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EventTypeLabel As System.Windows.Forms.Label
    Private WithEvents _GetModeButton As System.Windows.Forms.Button
    Private WithEvents _PortComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _SetModeButton As System.Windows.Forms.Button
    Private WithEvents _ResetCounterCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ResetTimerCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _ResetCounterTimerButton As System.Windows.Forms.Button
    Private WithEvents _ModeGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _CounterTimerModeComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _ModeLimitNumericLabel As System.Windows.Forms.Label
    Private WithEvents _CounterTimerModeComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _GetCounterTimerValuesButton As System.Windows.Forms.Button
    Private WithEvents _ValueGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _CountValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ElapsedTimeTextBox As System.Windows.Forms.TextBox
    Private WithEvents _CountValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ElapsedTimeTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PortEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PortComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _EventLogTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ModeLimitNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _ResolutionTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ResolutionTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _EventRepeatIntervalUnitsLabel As System.Windows.Forms.Label
    Private WithEvents _EventRepeatIntervalNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _EventLogGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DeviceIdNumeric As NumericUpDown
    Private WithEvents _DeviceIdNumericLabel As Label
    Private WithEvents _OpenDeviceModalityButton As Button
    Private WithEvents _DeviceInfoTextBox As TextBox
    Private WithEvents _DeviceGroupBox As GroupBox
End Class
