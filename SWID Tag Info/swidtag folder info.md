**[SWID Tag Folder Info]{.underline}**

File: **Diolan.Handler.Driver.Console.2015.swidtag**\
Installed by: Bootraper\
Location: swidtag folder under the product folder\
Example:\
C:\\Program
Files\\Integrated.Scientific.Resources\\Diolan.Handler.Driver.Console.2015\\swidtag

File: **Diolan.Handler.Driver.Console.2015 (1).swidtag**\
Install by: msi\
Location: swidtag folder under the executables folder\
Example:\
C:\\Program
Files\\Integrated.Scientific.Resources\\Diolan.Handler.Driver.Console.2015\\Programs\\swidtag

**Application directory tree used:**

Product folder: \<program files\>\\\<Company\>\\\<ProductName\>\
Example:\
C:\\Program
Files\\Integrated.Scientific.Resources\\Diolan.Handler.Driver.Console.2015

Executables folder: \<Install fodler\>/Programs\
Example:\
C:\\Program
Files\\Integrated.Scientific.Resources\\Diolan.Handler.Driver.Console.2015\\Programs
