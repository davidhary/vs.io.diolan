using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.CompilerServices;

using Dln.Gpio;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Gpio
{

    /// <summary> A dummy gpio pin. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-04-07 </para>
    /// </remarks>
    public class GpioPinDummy : PinBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        /// <param name="direction">   The direction. </param>
        [CLSCompliant( false )]
        public GpioPinDummy( GpioPinDummyReadOnlyCollection pins, int pinNumber, ActiveLogic activeLogic, PinDirection direction ) : base( pinNumber, activeLogic )
        {
            if ( pins is object && pins.Count > this.PinNumber )
            {
                this._Pin = pins[this.PinNumber];
                this._Pin.Direction = ( int ) direction;
                this._Pin.Enabled = true;
                if ( activeLogic == ActiveLogic.ActiveLow )
                {
                    if ( direction == PinDirection.Input )
                    {
                        this._Pin.Value = 1;
                    }
                    else
                    {
                        this._Pin.OutputValue = 1;
                    }
                }
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        /// <param name="direction">   The direction. </param>
        /// <param name="name">        The name. </param>
        [CLSCompliant( false )]
        public GpioPinDummy( GpioPinDummyReadOnlyCollection pins, int pinNumber, ActiveLogic activeLogic, PinDirection direction, string name ) : base( pinNumber, activeLogic, name )
        {
            if ( pins is object && pins.Count > this.PinNumber )
            {
                this._Pin = pins[this.PinNumber];
                this._Pin.Direction = ( int ) direction;
                this._Pin.Enabled = true;
                if ( activeLogic == ActiveLogic.ActiveLow )
                {
                    if ( direction == PinDirection.Input )
                    {
                        this._Pin.Value = 1;
                    }
                    else
                    {
                        this._Pin.OutputValue = 1;
                    }
                }
            }
        }

        /// <summary> The byte mask. </summary>
        public const byte ByteMask = 0xFF;

#pragma warning disable IDE1006 // Naming Styles
        private PinDummy __Pin;

        private PinDummy _Pin
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__Pin;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__Pin != null )
                {
                    this.__Pin.ConditionMetThreadSafe -= this.Pin_ConditionMetThreadSafe;
                }

                this.__Pin = value;
                if ( this.__Pin != null )
                {
                    this.__Pin.ConditionMetThreadSafe += this.Pin_ConditionMetThreadSafe;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets the pin. </summary>
        /// <value> The pin. </value>
        [CLSCompliant( false )]
        public PinDummy Pin => this._Pin;

        /// <summary> Gets or sets the pin enabled state. </summary>
        /// <value> The pin enabled state. </value>
        public bool Enabled
        {
            get => this.Pin.Enabled;

            set {
                if ( value != this.Enabled )
                {
                    this.Pin.Enabled = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the pin direction. </summary>
        /// <value> The pin direction. </value>
        public PinDirection Direction
        {
            get => ( PinDirection ) Conversions.ToInteger( this._Pin.Direction );

            set {
                if ( this.Direction != value )
                {
                    this._Pin.Direction = ( int ) value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Open Drain Enabled conditions. </summary>
        /// <value> <c>True</c> if Open Drain Enabled; otherwise, <c>False</c>. </value>
        public bool OpenDrainEnabled
        {
            get => this.Pin.OpenDrainEnabled;

            set {
                if ( value != this.OpenDrainEnabled )
                {
                    this.Pin.OpenDrainEnabled = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Pull-up Enabled conditions. </summary>
        /// <value> <c>True</c> if Pull-up Enabled; otherwise, <c>False</c>. </value>
        public bool PullupEnabled
        {
            get => this.Pin.PullupEnabled;

            set {
                if ( value != this.PullupEnabled )
                {
                    this.Pin.PullupEnabled = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the output bit value. </summary>
        /// <value> The bit value. </value>
        public override byte BitValue
        {
            get => this.Direction == PinDirection.Output ? ( byte ) (GpioPin.ByteMask & this._Pin.OutputValue) : ( byte ) (GpioPin.ByteMask & this._Pin.Value);

            set {
                if ( ( PinDirection ) Conversions.ToInteger( this._Pin.Direction ) == PinDirection.Output )
                {
                    if ( this._Pin.OutputValue != value )
                    {
                        this._Pin.OutputValue = value;
                        this.SyncNotifyPropertyChanged();
                    }
                }
                else if ( this._Pin.Value != value )
                {
                    this._Pin.Value = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Pin condition met thread safe. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Condition met event information. </param>
        private void Pin_ConditionMetThreadSafe( object sender, ConditionMetEventArgs e )
        {
            this.SyncNotifyPropertyChanged( nameof( GpioPin.BitValue ) );
        }
    }

    /// <summary> Gpio input pin. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    public class GpioInputPinDummy : GpioPinDummy
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        [CLSCompliant( false )]
        public GpioInputPinDummy( GpioPinDummyReadOnlyCollection pins, int pinNumber, ActiveLogic activeLogic ) : base( pins, pinNumber, activeLogic, PinDirection.Input )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        /// <param name="name">        The name. </param>
        [CLSCompliant( false )]
        public GpioInputPinDummy( GpioPinDummyReadOnlyCollection pins, int pinNumber, ActiveLogic activeLogic, string name ) : base( pins, pinNumber, activeLogic, PinDirection.Input, name )
        {
        }
    }

    /// <summary> Gpio output pin. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    public class GpioOutputPinDummy : GpioPinDummy
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        [CLSCompliant( false )]
        public GpioOutputPinDummy( GpioPinDummyReadOnlyCollection pins, int pinNumber, ActiveLogic activeLogic ) : base( pins, pinNumber, activeLogic, PinDirection.Output )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        /// <param name="name">        The name. </param>
        [CLSCompliant( false )]
        public GpioOutputPinDummy( GpioPinDummyReadOnlyCollection pins, int pinNumber, ActiveLogic activeLogic, string name ) : base( pins, pinNumber, activeLogic, PinDirection.Output, name )
        {
        }
    }

    /// <summary> Collection of gpio pins. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-04-07 </para>
    /// </remarks>
    public class GpioPinDummyCollection : PinBaseCollection
    {
    }

    /// <summary> A pin dummy. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public class PinDummy
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public PinDummy() : base()
        {
        }

        /// <summary> Gets or sets the enabled. </summary>
        /// <value> The enabled. </value>
        public bool Enabled { get; set; }

        /// <summary> Gets or sets the direction. </summary>
        /// <value> The direction. </value>
        public int Direction { get; set; }

        /// <summary> Gets or sets the output value. </summary>
        /// <value> The output value. </value>
        public int OutputValue { get; set; }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public int Value { get; set; }

        /// <summary> Gets or sets the open drain enabled. </summary>
        /// <value> The open drain enabled. </value>
        public bool OpenDrainEnabled { get; set; }

        /// <summary> Gets or sets the pullup enabled. </summary>
        /// <value> The pullup enabled. </value>
        public bool PullupEnabled { get; set; }

        /// <summary> Gets or sets the pulldown enabled. </summary>
        /// <value> The pulldown enabled. </value>
        public bool PulldownEnabled { get; set; }

        /// <summary> Gets or sets the debounce enabled. </summary>
        /// <value> The debounce enabled. </value>
        public bool DebounceEnabled { get; set; }

        /// <summary> Gets or sets the type of the event. </summary>
        /// <value> The type of the event. </value>
        [CLSCompliant( false )]
        public EventType EventType { get; private set; }

        /// <summary> Gets or sets the event period. </summary>
        /// <value> The event period. </value>
        public int EventPeriod { get; private set; }

        /// <summary> Gets or sets a list of types of the supported events. </summary>
        /// <value> A list of types of the supported events. </value>
        [CLSCompliant( false )]
        public ReadOnlyCollection<EventType> SupportedEventTypes { get; private set; }

        /// <summary> Gets or sets the restrictions. </summary>
        /// <value> The restrictions. </value>
        [CLSCompliant( false )]
        public DummyRestrictions Restrictions { get; private set; } = new DummyRestrictions();

        /// <summary> Event queue for all listeners interested in ConditionMet events. </summary>
        [CLSCompliant( false )]
        public event EventHandler<ConditionMetEventArgs> ConditionMet;

        /// <summary> Event queue for all listeners interested in ConditionMetThreadSafe events. </summary>
        [CLSCompliant( false )]
        public event EventHandler<ConditionMetEventArgs> ConditionMetThreadSafe;

        /// <summary> Sets event configuration. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="eventType">   Type of the event. </param>
        /// <param name="eventPeriod"> The event period. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [CLSCompliant( false )]
        public void SetEventConfiguration( EventType eventType, int eventPeriod )
        {
        }
    }

    /// <summary> A dummy restrictions. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    [CLSCompliant( false )]
    public class DummyRestrictions
    {

        /// <summary> Gets or sets the enabled. </summary>
        /// <value> The enabled. </value>
        public Dln.Restriction Enabled { get; private set; }

        /// <summary> Gets or sets the direction. </summary>
        /// <value> The direction. </value>
        public Dln.Restriction Direction { get; private set; }

        /// <summary> Gets or sets the output value. </summary>
        /// <value> The output value. </value>
        public Dln.Restriction OutputValue { get; private set; }

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public Dln.Restriction Value { get; private set; }

        /// <summary> Gets or sets the open drain enabled. </summary>
        /// <value> The open drain enabled. </value>
        public Dln.Restriction OpenDrainEnabled { get; private set; }

        /// <summary> Gets or sets the pullup enabled. </summary>
        /// <value> The pullup enabled. </value>
        public Dln.Restriction PullupEnabled { get; private set; }

        /// <summary> Gets or sets the pulldown enabled. </summary>
        /// <value> The pulldown enabled. </value>
        public Dln.Restriction PulldownEnabled { get; private set; }

        /// <summary> Gets or sets the debounce enabled. </summary>
        /// <value> The debounce enabled. </value>
        public Dln.Restriction DebounceEnabled { get; private set; }

        /// <summary> Gets or sets the type of the event. </summary>
        /// <value> The type of the event. </value>
        public Dln.Restriction EventType { get; private set; }

        /// <summary> Gets or sets the event period. </summary>
        /// <value> The event period. </value>
        public Dln.Restriction EventPeriod { get; private set; }

        /// <summary> Gets or sets the set event configuration. </summary>
        /// <value> The set event configuration. </value>
        public Dln.Restriction SetEventConfiguration { get; private set; }
    }

    /// <summary> Collection of dummy gpio pins. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    [DefaultMember( "Item" )]
    public class GpioPinDummyReadOnlyCollection : ReadOnlyCollection<PinDummy>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> class that is a read-
        /// only wrapper around the specified list.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins"> The list to wrap. </param>
        public GpioPinDummyReadOnlyCollection( IList<PinDummy> pins ) : base( pins )
        {
        }
    }
}
