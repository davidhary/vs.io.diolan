using System;
using System.Runtime.CompilerServices;

using Dln.Gpio;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Gpio
{

    /// <summary> A gpio pin. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-04-07 </para>
    /// </remarks>
    public class GpioPin : PinBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        /// <param name="direction">   The direction. </param>
        [CLSCompliant( false )]
        public GpioPin( Pins pins, int pinNumber, ActiveLogic activeLogic, PinDirection direction ) : base( pinNumber, activeLogic )
        {
            if ( pins is object && pins.Count > this.PinNumber )
            {
                this.PinThis = pins[this.PinNumber];
                this.PinThis.Direction = ( int ) direction;
                this.PinThis.Enabled = true;
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        /// <param name="direction">   The direction. </param>
        /// <param name="name">        The name. </param>
        [CLSCompliant( false )]
        public GpioPin( Pins pins, int pinNumber, ActiveLogic activeLogic, PinDirection direction, string name ) : base( pinNumber, activeLogic, name )
        {
            if ( pins is object && pins.Count > this.PinNumber )
            {
                this.PinThis = pins[this.PinNumber];
                this.PinThis.Direction = ( int ) direction;
                this.PinThis.Enabled = true;
            }
        }

        /// <summary> The byte mask. </summary>
        public const byte ByteMask = 0xFF;

        private Pin _PinThis;

        private Pin PinThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._PinThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._PinThis != null )
                {
                    this._PinThis.ConditionMetThreadSafe -= this.Pin_ConditionMetThreadSafe;
                }

                this._PinThis = value;
                if ( this._PinThis != null )
                {
                    this._PinThis.ConditionMetThreadSafe += this.Pin_ConditionMetThreadSafe;
                }
            }
        }

        /// <summary> Gets the pin. </summary>
        /// <value> The pin. </value>
        [CLSCompliant( false )]
        public Pin Pin => this.PinThis;

        /// <summary> Gets or sets the enabled. </summary>
        /// <value> The enabled. </value>
        public bool Enabled
        {
            get => this.Pin.Enabled;

            set {
                if ( value != this.Enabled )
                {
                    this.Pin.Enabled = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the pin direction. </summary>
        /// <value> The pin direction. </value>
        public PinDirection Direction
        {
            get => ( PinDirection ) Conversions.ToInteger( this.PinThis.Direction );

            set {
                if ( this.Direction != value )
                {
                    this.PinThis.Direction = ( int ) value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Open Drain Enabled conditions. </summary>
        /// <value> <c>True</c> if Open Drain Enabled; otherwise, <c>False</c>. </value>
        public bool OpenDrainEnabled
        {
            get => this.Pin.OpendrainEnabled;

            set {
                if ( value != this.OpenDrainEnabled )
                {
                    this.Pin.OpendrainEnabled = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Pull-up Enabled conditions. </summary>
        /// <value> <c>True</c> if Pull-up Enabled; otherwise, <c>False</c>. </value>
        public bool PullupEnabled
        {
            get => this.Pin.PullupEnabled;

            set {
                if ( value != this.PullupEnabled )
                {
                    this.Pin.PullupEnabled = value;
                    this.AsyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the output bit value. </summary>
        /// <value> The bit value. </value>
        public override byte BitValue
        {
            get => this.Direction == PinDirection.Output ? ( byte ) (ByteMask & this.PinThis.OutputValue) : ( byte ) (ByteMask & this.PinThis.Value);

            set {
                if ( ( PinDirection ) Conversions.ToInteger( this.PinThis.Direction ) == PinDirection.Output )
                {
                    if ( this.PinThis.OutputValue != value )
                    {
                        this.PinThis.OutputValue = value;
                        this.SyncNotifyPropertyChanged();
                    }
                }
            }
        }

        /// <summary> Pin condition met thread safe. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Condition met event information. </param>
        private void Pin_ConditionMetThreadSafe( object sender, ConditionMetEventArgs e )
        {
            this.SyncNotifyPropertyChanged( nameof( this.BitValue ) );
        }
    }

    /// <summary> Gpio input pin. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    public class GpioInputPin : GpioPin
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        [CLSCompliant( false )]
        public GpioInputPin( Pins pins, int pinNumber, ActiveLogic activeLogic ) : base( pins, pinNumber, activeLogic, PinDirection.Input )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        /// <param name="name">        The name. </param>
        [CLSCompliant( false )]
        public GpioInputPin( Pins pins, int pinNumber, ActiveLogic activeLogic, string name ) : base( pins, pinNumber, activeLogic, PinDirection.Input, name )
        {
        }
    }

    /// <summary> Gpio output pin. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    public class GpioOutputPin : GpioPin
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        [CLSCompliant( false )]
        public GpioOutputPin( Pins pins, int pinNumber, ActiveLogic activeLogic ) : base( pins, pinNumber, activeLogic, PinDirection.Output )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="pinNumber">   The pin number. </param>
        /// <param name="activeLogic"> The active logic. </param>
        /// <param name="name">        The name. </param>
        [CLSCompliant( false )]
        public GpioOutputPin( Pins pins, int pinNumber, ActiveLogic activeLogic, string name ) : base( pins, pinNumber, activeLogic, PinDirection.Output, name )
        {
        }
    }

    /// <summary> Collection of gpio pins. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-04-07 </para>
    /// </remarks>
    public class GpioPinCollection : PinBaseCollection
    {
    }
}
