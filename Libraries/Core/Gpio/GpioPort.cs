﻿using System;

using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Gpio
{

    /// <summary> Gpio input port. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    public class GpioInputPort : PortBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="mask">        The mask. </param>
        /// <param name="activeLogic"> The active logic. </param>
        [CLSCompliant( false )]
        public GpioInputPort( Dln.Gpio.Pins pins, long mask, ActiveLogic activeLogic ) : base( mask, activeLogic )
        {
            this._Pins = pins;
            int firstPin = (( ulong ) mask).FirstPinNumber();
            var pin = this._Pins[firstPin];
            pin.Direction = ( int ) PinDirection.Input;
            pin.Enabled = true;
            this._Pins.Configure( ( ulong ) mask, pin );
            this.BitCount = (( ulong ) mask).BitCount();
            this.ValueMask = ( long ) (Math.Pow( 2d, this.BitCount ) - 1d);
        }

        /// <summary> The pins. </summary>
        private readonly Dln.Gpio.Pins _Pins;

        /// <summary> Gets or sets the input port value. </summary>
        /// <value> The port value. </value>
        public override long PortValue
        {
            get => ( long ) this._Pins.InputValue( ( ulong ) this.Mask );

            set {
            }
        }
    }

    /// <summary> Gpio output port. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    public class GpioOutputPort : PortBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pins">        The pins. </param>
        /// <param name="mask">        The mask. </param>
        /// <param name="activeLogic"> The active logic. </param>
        [CLSCompliant( false )]
        public GpioOutputPort( Dln.Gpio.Pins pins, long mask, ActiveLogic activeLogic ) : base( mask, activeLogic )
        {
            this._Pins = pins;
            int firstPin = (( ulong ) mask).FirstPinNumber();
            var pin = this._Pins[firstPin];
            pin.Direction = ( int ) PinDirection.Output;
            pin.Enabled = true;
            this._Pins.Configure( ( ulong ) mask, pin );
            this.BitCount = (( ulong ) mask).BitCount();
            this.ValueMask = ( long ) (Math.Pow( 2d, this.BitCount ) - 1d);
        }

        /// <summary> The pins. </summary>
        private readonly Dln.Gpio.Pins _Pins;

        /// <summary> Gets or sets the output port value. </summary>
        /// <value> The port value. </value>
        public override long PortValue
        {
            get => ( long ) this._Pins.OutputValueGetter( ( ulong ) this.Mask );

            set => _ = this._Pins.OutputValueSetter( ( ulong ) this.Mask, ( ulong ) value );
        }
    }
}