using System;
using System.ComponentModel;

namespace isr.Diolan
{
    namespace Gpio
    {

        /// <summary> Pin base. </summary>
        /// <remarks>
        /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2015-06-08 </para>
        /// </remarks>
        public abstract class PinBase : Core.Models.ViewModelBase
        {

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="pinNumber">   The pin number. </param>
            /// <param name="activeLogic"> The active logic. </param>
            protected PinBase( int pinNumber, ActiveLogic activeLogic ) : base()
            {
                this.PinNumber = pinNumber;
                this._ActiveLogic = activeLogic;
                this._Name = this.PinNumber.ToString();
            }

            /// <summary> Specialized constructor for use only by derived class. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="pinNumber">   The pin number. </param>
            /// <param name="activeLogic"> The active logic. </param>
            /// <param name="name">        The name. </param>
            protected PinBase( int pinNumber, ActiveLogic activeLogic, string name ) : this( pinNumber, activeLogic )
            {
                this._Name = name;
            }

            /// <summary> The name. </summary>
            private string _Name;

            /// <summary> Gets or sets the name. </summary>
            /// <value> The name. </value>
            public string Name
            {
                get => this._Name;

                set {
                    if ( !string.Equals( value, this.Name ) )
                    {
                        this._Name = value;
                        this.AsyncNotifyPropertyChanged();
                    }
                }
            }

            /// <summary> Gets or sets the pin number. </summary>
            /// <value> The pin number. </value>
            public int PinNumber { get; private set; }

            /// <summary> The active logic. </summary>
            private ActiveLogic _ActiveLogic;

            /// <summary> Gets or sets the active logic. </summary>
            /// <value> The active logic. </value>
            public ActiveLogic ActiveLogic
            {
                get => this._ActiveLogic;

                set {
                    if ( value != this._ActiveLogic )
                    {
                        this._ActiveLogic = value;
                        this.AsyncNotifyPropertyChanged();
                    }
                }
            }

            /// <summary> Gets or sets the bit value. </summary>
            /// <value> The bit value. </value>
            public abstract byte BitValue { get; set; }

            /// <summary> The bit zero mask. </summary>
            public const byte BitZeroMask = 1;

            /// <summary> Gets or sets the logical value. </summary>
            /// <value> The logical value. </value>
            public byte LogicalValue
            {
                get => ( byte ) (this.ActiveLogic == ActiveLogic.ActiveLow ? BitZeroMask & ~this.BitValue : BitZeroMask & this.BitValue);

                set {
                    byte v = ( byte ) (this.ActiveLogic == ActiveLogic.ActiveLow ? BitZeroMask & ~value : BitZeroMask & value);
                    // If Me.ActiveLogic = ActiveLogic.ActiveLow Then
                    // Me.BitValue = PinBase.BitZeroMask And Not value
                    // Else
                    // Me.BitValue = PinBase.BitZeroMask And value
                    // End If
                    if ( v != this.BitValue )
                    {
                        this.BitValue = v;
                    }
                }
            }

            /// <summary> Gets or sets the logical state of the pin. </summary>
            /// <value> The logical state of the pin. </value>
            public LogicalState LogicalState
            {
                get => this.LogicalValue == 0 ? LogicalState.Inactive : LogicalState.Active;

                set {
                    byte v = ( byte ) (value == LogicalState.Inactive ? 0 : 1);
                    if ( this.LogicalValue != v )
                    {
                        // Me.LogicalValue = If(value = LogicalState.Inactive, CByte(0), CByte(1))
                        this.LogicalValue = v;
                    }
                }
            }

            /// <summary>
            /// Executes the property changed on a different thread, and waits for the result.
            /// </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="value"> The value. </param>
            public void InvokePropertyChanged( string value )
            {
                this.AsyncNotifyPropertyChanged( value );
            }
        }

        /// <summary> Collection of pin bases. </summary>
        /// <remarks>
        /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2017-07-31 </para>
        /// </remarks>
        public class PinBaseCollection : System.Collections.ObjectModel.KeyedCollection<int, PinBase>
        {

            /// <summary>
            /// When implemented in a derived class, extracts the key from the specified element.
            /// </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
            /// <param name="item"> The element from which to extract the key. </param>
            /// <returns> The key for the specified element. </returns>
            protected override int GetKeyForItem( PinBase item )
            {
                return item is null ? throw new ArgumentNullException( nameof( item ) ) : item.PinNumber;
            }
        }
    }

    /// <summary> Values that represent active logics. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum ActiveLogic
    {
        /// <summary>   An enum constant representing the active low option. </summary>
        [Description( "Active Low" )]
        ActiveLow = 0,

        /// <summary>   An enum constant representing the active high option. </summary>
        [Description( "Active High" )]
        ActiveHigh = 1
    }

    /// <summary> Values that represent logical states. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum LogicalState
    {
        /// <summary>   An enum constant representing the inactive option. </summary>
        [Description( "Inactive (Off)" )]
        Inactive = 0,

        /// <summary>   An enum constant representing the active option. </summary>
        [Description( "Active (On)" )]
        Active = 1
    }
}
