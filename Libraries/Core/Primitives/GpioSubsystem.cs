using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.Diolan
{

    /// <summary> Gpio subsystem. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-28 </para>
    /// </remarks>
    public class GpioSubsystem : IDisposable
    {

        #region " CONSTRUCTOR "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioSubsystem() : base()
        {
        }

        #region "I Disposable Support"

        /// <summary> Gets or sets the disposed sentinel. </summary>
        /// <value> The disposed sentinel. </value>
        private bool Disposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to
        /// release only
        /// unmanaged resources
        /// when called from
        /// the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed && disposing )
                {
                    if ( this._PulseTimer is object )
                    {
                        this._PulseTimer.Dispose();
                        this._PulseTimer = null;
                    }
                }
            }
            finally
            {
                this.Disposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " PIN "

#pragma warning disable IDE1006 // Naming Styles
        /// <summary> The pulse timer. </summary>
        private System.Timers.Timer __PulseTimer;

        private System.Timers.Timer _PulseTimer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__PulseTimer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__PulseTimer != null )
                {
                    this.__PulseTimer.Elapsed -= this.PulseTimer_Elapsed;
                }

                this.__PulseTimer = value;
                if ( this.__PulseTimer != null )
                {
                    this.__PulseTimer.Elapsed += this.PulseTimer_Elapsed;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> The pulsed pin. </summary>
        private Dln.Gpio.Pin _PulsedPin;

        /// <summary> Pulse pin. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pin">      The pin. </param>
        /// <param name="duration"> The duration. </param>
        [CLSCompliant( false )]
        public void PulsePin( Dln.Gpio.Pin pin, TimeSpan duration )
        {
            if ( pin is null )
                throw new ArgumentNullException( nameof( pin ) );
            if ( this._PulseTimer is null )
            {
                this._PulseTimer = new System.Timers.Timer();
            }

            this._PulseTimer.AutoReset = false;
            this._PulseTimer.Interval = duration.TotalMilliseconds;
            // toggle the pin value
            this._PulsedPin = pin;
            this._PulsedPin.OutputValue = ~this._PulsedPin.OutputValue & 1;
            this._PulseTimer.Start();
        }

        /// <summary> Pulse timer elapsed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Elapsed event information. </param>
        private void PulseTimer_Elapsed( object sender, System.Timers.ElapsedEventArgs e )
        {
            if ( this._PulsedPin is object )
            {
                this._PulsedPin.OutputValue = ~this._PulsedPin.OutputValue & 1;
            }
            // the timer will not start again.
        }

        #endregion

    }
}
