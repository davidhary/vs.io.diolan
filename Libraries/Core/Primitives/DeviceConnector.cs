using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Core;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan
{

    /// <summary> Device connector. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-30 </para>
    /// </remarks>
    public class DeviceConnector : IDisposable
    {

        #region " CONSTRUCTOR "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="serverConnector"> The server connector. </param>
        [CLSCompliant( false )]
        public DeviceConnector( LocalhostConnector serverConnector ) : base()
        {
            this._ServerConnector = serverConnector;
        }

        #region " DISPOSABLE SUPPORT "

        /// <summary> Gets the disposed sentinel. </summary>
        /// <value> The disposed sentinel. </value>
        private bool Disposed { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to
        /// release only
        /// unmanaged resources
        /// when called from
        /// the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.Disposed && disposing )
                {
                    // dispose managed state (managed objects).
                    this.RemoveDeviceOpenedEventHandlers();
                    this.RemoveDeviceClosedEventHandlers();
                    this.RemoveDeviceClosingEventHandlers();
                    if ( this.Device is object )
                        this.Device = null;
                    if ( this._ServerConnector is object )
                        this._ServerConnector = null;
                }
            }
            finally
            {
                this.Disposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " CONNECTION "

        /// <summary> The server connector. </summary>
        private LocalhostConnector _ServerConnector;

        /// <summary> Gets the connection. </summary>
        /// <value> The connector. </value>
        [CLSCompliant( false )]
        public Dln.Connection Connection => this._ServerConnector?.Connection;

        /// <summary> Gets a value indicating whether this object is local connection. </summary>
        /// <value> <c>true</c> if this object is local connection; otherwise <c>false</c> </value>
        public bool IsLocalConnection => this.Connection is object && this.Connection.IsConnected() && this.Connection.Host.Equals( LocalhostConnector.DefaultHost, StringComparison.OrdinalIgnoreCase );

        /// <summary> Attaches. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="serverConnector"> The server connector. </param>
        private void Attach( LocalhostConnector serverConnector )
        {
            if ( serverConnector is object )
                serverConnector.Attach( this._Device );
        }

        /// <summary> Detaches the given device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="serverConnector"> The server connector. </param>
        private void Detach( LocalhostConnector serverConnector )
        {
            if ( serverConnector is object )
                serverConnector.Detach( this._Device );
        }

        #endregion

        #region " DEVICE "

#pragma warning disable IDE1006 // Naming Styles
        private Dln.Device __Device;

        private Dln.Device _Device
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__Device;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__Device != null )
                {
                    this.__Device.DeviceRemovedThreadSafe -= this.Device_DeviceRemovedThreadSafe;
                }

                this.__Device = value;
                if ( this.__Device != null )
                {
                    this.__Device.DeviceRemovedThreadSafe += this.Device_DeviceRemovedThreadSafe;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [CLSCompliant( false )]
        public Dln.Device Device
        {
            get => this._Device;

            set => this._Device = value;
        }

        /// <summary> Attempts to open device from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="deviceId">         Identifier for the device. </param>
        /// <param name="requiredModality"> The required modality. </param>
        /// <param name="e">                Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryOpenDevice( long deviceId, DeviceModalities requiredModality, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( this.Connection is null )
            {
                e.RegisterFailure( "Device connection not set" );
            }
            else if ( this.Connection.IsConnected() )
            {
                // Open device
                if ( Dln.Device.Count() == 0L )
                {
                    e.RegisterFailure( "No DLN-series adapters have been detected." );
                }
                else if ( !this.IsDeviceOpen )
                {
                    this._Device = this._ServerConnector.IsAttached( deviceId ) ? this._ServerConnector.AttachedDevice( deviceId ) : Dln.Device.OpenById( ( uint ) deviceId );
                    if ( requiredModality == DeviceModalities.None || this.Device.SupportsAny( requiredModality ) )
                    {
                        this.OnDeviceOpened( requiredModality );
                    }
                    else
                    {
                        e.RegisterFailure( $"{this.Device.HardwareType} does not support {requiredModality}." );
                        this._Device = null;
                    }
                }
            }
            else
            {
                e.RegisterFailure( "Device not connected" );
            }

            return !e.Failed;
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        public bool IsDeviceOpen => this._Device is object;

        /// <summary> Closes the device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="requiredModality"> The required modality. </param>
        public void CloseDevice( DeviceModalities requiredModality )
        {
            _ = Detach( this._Device, requiredModality );
            if ( this.IsDeviceOpen && !HasAttachedModalities( this._Device ) )
            {
                var e = new System.ComponentModel.CancelEventArgs();
                this.OnDeviceClosing( e );
                if ( !e.Cancel )
                {
                    this.OnDeviceClosed();
                }
            }
        }


        #region " DeviceOpened "

        /// <summary> Executes the device Open action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="requiredModality"> The required modality. </param>
        private void OnDeviceOpened( DeviceModalities requiredModality )
        {
            _ = Attach( this._Device, requiredModality );
            if ( !this._ServerConnector.IsAttached( this._Device ) )
            {
                this.Attach( this._ServerConnector );
                this.SyncNotifyDeviceOpened( EventArgs.Empty );
            }
        }

        /// <summary> Removes the DeviceOpened event handlers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected void RemoveDeviceOpenedEventHandlers()
        {
            this._DeviceOpenedEventHandlers?.RemoveAll();
        }

        /// <summary> The DeviceOpened event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _DeviceOpenedEventHandlers = new EventHandlerContextCollection<EventArgs>();

        /// <summary> Event queue for all listeners interested in DeviceOpened events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> DeviceOpened
        {
            add {
                this._DeviceOpenedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._DeviceOpenedEventHandlers.RemoveValue( value );
            }
        }

        private void OnDeviceOpened( object sender, EventArgs e )
        {
            this._DeviceOpenedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the <see cref="DeviceOpened">DeviceOpened Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyDeviceOpened( EventArgs e )
        {
            this._DeviceOpenedEventHandlers.Send( this, e );
        }

        #endregion

        #region " DEVICE CLOSING "

        /// <summary> Allows taking actions before DeviceClosing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnDeviceClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( !e.Cancel )
            {
                this.SyncNotifyDeviceClosing( e );
            }
        }

        /// <summary> Removes the DeviceClosing event handlers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected void RemoveDeviceClosingEventHandlers()
        {
            this._DeviceClosingEventHandlers?.RemoveAll();
        }

        /// <summary> The DeviceClosing event handlers. </summary>
        private readonly EventHandlerContextCollection<System.ComponentModel.CancelEventArgs> _DeviceClosingEventHandlers = new EventHandlerContextCollection<System.ComponentModel.CancelEventArgs>();

        /// <summary> Event queue for all listeners interested in DeviceClosing events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<System.ComponentModel.CancelEventArgs> DeviceClosing
        {
            add {
                this._DeviceClosingEventHandlers.Add( new EventHandlerContext<System.ComponentModel.CancelEventArgs>( value ) );
            }

            remove {
                this._DeviceClosingEventHandlers.RemoveValue( value );
            }
        }

        private void OnDeviceClosing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._DeviceClosingEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the <see cref="DeviceClosing">DeviceClosing
        /// Event</see>.
        /// </summary>
        /// <remarks>   David, 2020-10-24. </remarks>
        /// <param name="e">    The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyDeviceClosing( System.ComponentModel.CancelEventArgs e )
        {
            this._DeviceClosingEventHandlers.Send( this, e );
        }

        #endregion

        #region " DEVICE CLOSED "

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnDeviceClosed()
        {
            if ( this.IsDeviceOpen && !HasAttachedModalities( this._Device ) )
            {
                this.Detach( this._ServerConnector );
                this._Device = null;
                this.SyncNotifyDeviceClosed( EventArgs.Empty );
            }
        }

        /// <summary> Device removed event hander. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        [CLSCompliant( false )]
        protected virtual void OnDeviceRemoved( Dln.Device device )
        {
            if ( device is object && this._Device is object && this.Device.ID == device.ID )
            {
                Detach( device );
                this.Detach( this._ServerConnector );
                this._Device = null;
                this.SyncNotifyDeviceClosed( EventArgs.Empty );
            }
        }

        /// <summary> Device removed thread safe. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Device_DeviceRemovedThreadSafe( object sender, EventArgs e )
        {
            this.OnDeviceRemoved( sender as Dln.Device );
        }

        /// <summary> Removes the DeviceClosed event handlers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected void RemoveDeviceClosedEventHandlers()
        {
            this._DeviceClosedEventHandlers?.RemoveAll();
        }

        /// <summary> The DeviceClosed event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _DeviceClosedEventHandlers = new EventHandlerContextCollection<EventArgs>();


        /// <summary> Event queue for all listeners interested in DeviceClosed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> DeviceClosed
        {
            add {
                this._DeviceClosedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._DeviceClosedEventHandlers.RemoveValue( value );
            }
        }

        private void OnDeviceClosed( object sender, EventArgs e )
        {
            this._DeviceClosedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously sends or invokes the
        /// <see cref="DeviceClosed">DeviceClosed Event</see>.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyDeviceClosed( EventArgs e )
        {
            this._DeviceClosedEventHandlers.Send( this, e );
        }

        #endregion

        #endregion

        #region " ATTACHED MODALITIES "

        /// <summary> Query if 'device' is attached. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="modality"> The modality. </param>
        /// <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
        [CLSCompliant( false )]
        public static bool IsAttached( Dln.Device device, DeviceModalities modality )
        {
            return _AttachedModalities is object && _AttachedModalities.IsAttached( device, modality );
        }

        /// <summary> The attached modalities. </summary>
        private static AttachedDeviceModalities _AttachedModalities;

        /// <summary> Query if 'device' has attached modalities. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        /// <returns> True if attached modalities, false if not. </returns>
        [CLSCompliant( false )]
        public static bool HasAttachedModalities( Dln.Device device )
        {
            return _AttachedModalities is object && _AttachedModalities.HasAttachedModalities( device );
        }

        /// <summary> Attaches. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="modality"> The modality. </param>
        /// <returns> An Integer. </returns>
        [CLSCompliant( false )]
        public static int Attach( Dln.Device device, DeviceModalities modality )
        {
            if ( _AttachedModalities is null )
            {
                _AttachedModalities = new AttachedDeviceModalities();
            }

            return _AttachedModalities.Attach( device, modality );
        }

        /// <summary> Detaches the given device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="modality"> The modality. </param>
        /// <returns> An Integer. </returns>
        [CLSCompliant( false )]
        public static int Detach( Dln.Device device, DeviceModalities modality )
        {
            if ( _AttachedModalities is null )
            {
                _AttachedModalities = new AttachedDeviceModalities();
            }

            return _AttachedModalities.Detach( device, modality );
        }

        /// <summary> Detaches the given device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        [CLSCompliant( false )]
        public static void Detach( Dln.Device device )
        {
            if ( _AttachedModalities is object )
            {
                _AttachedModalities.Detach( device );
            }
        }

        /// <summary> Attached device modalities. </summary>
        /// <remarks>
        /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2015-06-02 </para>
        /// </remarks>
        private class AttachedDeviceModalities : Dictionary<uint, AttachedModalities>
        {

            /// <summary> Attaches. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device">   The device. </param>
            /// <param name="modality"> The modality. </param>
            /// <returns> An Integer. </returns>
            public int Attach( Dln.Device device, DeviceModalities modality )
            {
                if ( !this.ContainsKey( device.ID ) )
                {
                    this.Add( device.ID, new AttachedModalities() );
                }

                return this[device.ID].Attach( modality );
            }

            /// <summary> Detaches. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device"> The device. </param>
            public void Detach( Dln.Device device )
            {
                if ( this.ContainsKey( device.ID ) )
                {
                    _ = this.Remove( device.ID );
                }
            }

            /// <summary> Detaches. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device">   The device. </param>
            /// <param name="modality"> The modality. </param>
            /// <returns> An Integer. </returns>
            public int Detach( Dln.Device device, DeviceModalities modality )
            {
                if ( !this.ContainsKey( device.ID ) )
                {
                    this.Add( device.ID, new AttachedModalities() );
                    return 0;
                }
                else
                {
                    return this[device.ID].Detach( modality );
                }
            }

            /// <summary> Query if 'device' is attached. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device">   The device. </param>
            /// <param name="modality"> The modality. </param>
            /// <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
            public bool IsAttached( Dln.Device device, DeviceModalities modality )
            {
                return this.Any() && this.ContainsKey( device.ID ) && this[device.ID][modality] > 0;
            }

            /// <summary> Query if this device has attached Modalities. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="device"> The device. </param>
            /// <returns> <c>true</c> if attached Modalities; otherwise <c>false</c> </returns>
            public bool HasAttachedModalities( Dln.Device device )
            {
                bool result = false;
                if ( this.Any() )
                {
                    foreach ( int v in this[device.ID].Values )
                    {
                        if ( v > 0 )
                        {
                            result = true;
                            break;
                        }
                    }
                }

                return result;
            }
        }

        /// <summary> Attached modalities. </summary>
        /// <remarks>
        /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2015-06-02 </para>
        /// </remarks>
        private class AttachedModalities : Dictionary<DeviceModalities, int>
        {

            /// <summary> Attaches the given modality. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="modality"> The modality. </param>
            /// <returns> An Integer. </returns>
            public int Attach( DeviceModalities modality )
            {
                if ( this.ContainsKey( modality ) )
                {
                    this[modality] += 1;
                }
                else
                {
                    this.Add( modality, 1 );
                }

                return this[modality];
            }

            /// <summary> Detaches the given modality. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="modality"> The modality. </param>
            /// <returns> An Integer. </returns>
            public int Detach( DeviceModalities modality )
            {
                if ( this.ContainsKey( modality ) )
                {
                    this[modality] -= 1;
                }
                else
                {
                    this.Add( modality, 0 );
                }

                return this[modality];
            }

            /// <summary> Query if 'modality' is attached. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <param name="modality"> The modality. </param>
            /// <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
            public bool IsAttached( DeviceModalities modality )
            {
                return this.Any() && this.ContainsKey( modality ) && this[modality] > 0;
            }

            /// <summary> Query if this object has attached Modalities. </summary>
            /// <remarks> David, 2020-10-24. </remarks>
            /// <returns> <c>true</c> if attached Modalities; otherwise <c>false</c> </returns>
            public bool HasAttachedModalities()
            {
                bool result = false;
                if ( this.Any() )
                {
                    foreach ( int v in this.Values )
                    {
                        if ( v > 0 )
                        {
                            result = true;
                            break;
                        }
                    }
                }

                return result;
            }
        }


        #endregion

    }
}
