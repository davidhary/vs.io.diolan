using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Core;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan
{

    /// <summary> Connector for device modality or modalities. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-18, 5647. </para>
    /// </remarks>
    public abstract class ModalityConnectorBase : Core.Models.ViewModelBase, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected ModalityConnectorBase() : base()
        {
        }

        #region " I DISPOSABLE SUPPORT "

        /// <summary> Gets or sets the disposed sentinel. </summary>
        /// <value> The disposed sentinel. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to
        /// release only
        /// unmanaged resources
        /// when called from
        /// the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    try
                    {
                        this.OnModalityClosed();
                    }
                    catch ( Exception ex )
                    {
                        Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                    }

                    if ( this.Device is object )
                        this.Device = null;
                    if ( this.DeviceConnectorThis is object )
                    {
                        this.DeviceConnectorThis.Dispose();
                        this.DeviceConnectorThis = null;
                    }

                    if ( this.ServerConnectorThis is object )
                        this.ServerConnectorThis = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " SERVER "

        /// <summary> Gets or sets the server connector source. </summary>
        /// <value> The server connector source. </value>
        public string ServerConnectorSource { get; private set; } = $"{nameof( ServerConnector )}.";

        /// <summary> Server connector property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void ServerConnector_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object )
                this.SyncNotifyPropertyChanged( $"{this.ServerConnectorSource}{e.PropertyName}" );
        }

        private LocalhostConnector _ServerConnectorThis;

        /// <summary>   Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        private LocalhostConnector ServerConnectorThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ServerConnectorThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ServerConnectorThis != null )
                {
                    this._ServerConnectorThis.PropertyChanged -= this.ServerConnector_PropertyChanged;
                }

                this._ServerConnectorThis = value;
                if ( this._ServerConnectorThis != null )
                {
                    this._ServerConnectorThis.PropertyChanged += this.ServerConnector_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        public LocalhostConnector ServerConnector
        {
            get => this.ServerConnectorThis;

            set => this.ServerConnectorThis = value;
        }

        #endregion

        #region " DEVICE CONNECTOR "

        private DeviceConnector _DeviceConnectorThis;

        private DeviceConnector DeviceConnectorThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceConnectorThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceConnectorThis != null )
                {
                    this._DeviceConnectorThis.DeviceClosed -= this.DeviceConnector_DeviceClosed;
                    this._DeviceConnectorThis.DeviceClosing -= this.DeviceConnector_DeviceClosing;
                    this._DeviceConnectorThis.DeviceOpened -= this.DeviceConnector_DeviceOpened;
                }

                this._DeviceConnectorThis = value;
                if ( this._DeviceConnectorThis != null )
                {
                    this._DeviceConnectorThis.DeviceClosed += this.DeviceConnector_DeviceClosed;
                    this._DeviceConnectorThis.DeviceClosing += this.DeviceConnector_DeviceClosing;
                    this._DeviceConnectorThis.DeviceOpened += this.DeviceConnector_DeviceOpened;
                }
            }
        }

        /// <summary> Gets or sets the device connector. </summary>
        /// <value> The device connector. </value>
        public DeviceConnector DeviceConnector
        {
            get => this.DeviceConnectorThis;

            set => this.DeviceConnectorThis = value;
        }

        /// <summary> Device connector device closed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceClosed( object sender, EventArgs e )
        {
            this.DeviceCaption = "closed";
            this.SyncNotifyPropertyChanged( nameof( this.DeviceCaption ) );
            this.OnModalityClosed();
        }

        /// <summary> Device connector device closing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void DeviceConnector_DeviceClosing( object sender, CancelEventArgs e )
        {
            this.OnModalityClosing( e );
        }

        /// <summary> Device connector device opened. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceOpened( object sender, EventArgs e )
        {
            this.DeviceCaption = this.DeviceConnectorThis.Device.Caption();
            this.SyncNotifyPropertyChanged( nameof( this.DeviceCaption ) );
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets information describing the device. </summary>
        /// <value> Information describing the device. </value>
        public string DeviceCaption { get; private set; }

        /// <summary> Gets the device. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The device. </value>
        [CLSCompliant( false )]
        protected Dln.Device Device { get; private set; }

        /// <summary> Opens the device for the modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <param name="e">        Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryOpenDeviceModality( long deviceId, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            // connect if not connected
            this.ServerConnector.Connect();
            this.DeviceConnectorThis = new DeviceConnector( this.ServerConnectorThis );

            // Open device
            if ( this.DeviceConnector.TryOpenDevice( deviceId, this.Modality, e ) )
            {
                this.Device = this.DeviceConnectorThis.Device;
                if ( this.TryOpenModality( e ) )
                {
                }
                else
                {
                    this.Device = null;
                }
            }
            else
            {
                this.Device = null;
                this.DeviceCaption = "failed";
                this.SyncNotifyPropertyChanged( nameof( this.DeviceCaption ) );
            }

            this.SyncNotifyPropertyChanged( nameof( this.IsDeviceModalityOpen ) );
            return !e.Failed;
        }

        /// <summary> Closes device modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void CloseDeviceModality()
        {
            this.DeviceConnector.CloseDevice( this.Modality );
            var e = new CancelEventArgs();
            this.OnModalityClosing( e );
            if ( !e.Cancel )
            {
                this.OnModalityClosed();
                this.SyncNotifyPropertyChanged( nameof( this.IsDeviceModalityOpen ) );
            }
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        public bool IsDeviceModalityOpen => this.Device is object && this.IsModalityOpen();

        #endregion

        #region " SUBSYSTEM MODALITIES "

        /// <summary> The modality. </summary>
        /// <value> The modality. </value>
        public abstract DeviceModalities Modality { get; }

        /// <summary> Executes the modality closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected virtual void OnModalityClosed()
        {
            this.Device = null;
            if ( this.ServerConnector?.IsConnected == true )
            {
                if ( !this.ServerConnector.HasAttachedDevices() )
                {
                    this.ServerConnector.Disconnect();
                }
            }
        }

        /// <summary> Executes the modality closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected abstract void OnModalityClosing( CancelEventArgs e );

        /// <summary> Executes the modality opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected abstract bool TryOpenModality( ActionEventArgs e );

        /// <summary> Queries if a modality is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
        public abstract bool IsModalityOpen();

        #endregion

        #region " EVENT LOG "

        /// <summary> The event caption. </summary>
        private string _EventCaption;

        /// <summary> Gets or sets the event caption. </summary>
        /// <value> The event caption. </value>
        public string EventCaption
        {
            get => this._EventCaption;

            protected set {
                if ( !string.Equals( value, this.EventCaption ) )
                {
                    this._EventCaption = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
