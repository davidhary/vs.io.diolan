using System;
using System.Collections.Generic;

using isr.Diolan.SubsystemExtensions;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Diolan
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Information about the device. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    [CLSCompliant( false )]
    public class DeviceInfo
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="device"> The device. </param>
        public DeviceInfo( Dln.Device device ) : base()
        {
            if ( device is object )
            {
                this.Id = device.ID;
                this.SerialNumber = device.SN;
                this.HardwareType = device.HardwareType;
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="caption"> The caption. </param>
        public DeviceInfo( string caption ) : base()
        {
            this.ParseThis( caption );
        }

        /// <summary> The default device identifier. </summary>
        public const int DefaultDeviceId = 0;

        /// <summary> Gets the identifier. </summary>
        /// <value> The identifier. </value>
        public long Id { get; set; }

        /// <summary> Gets the type of the hardware. </summary>
        /// <value> The type of the hardware. </value>
        public Dln.HardwareType HardwareType { get; set; }

        /// <summary> Gets the serial number. </summary>
        /// <value> The serial number. </value>
        public long SerialNumber { get; set; }

        /// <summary> Gets the caption. </summary>
        /// <value> The caption. </value>
        public string Caption => $"{this.HardwareType}.{this.Id}.{this.SerialNumber}";


        /// <summary> Parses. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="caption"> The caption. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>" )]
        private void ParseThis( string caption )
        {
            if ( string.IsNullOrWhiteSpace( caption ) )
                throw new ArgumentNullException( nameof( caption ) );
            var elements = new Stack<string>( caption.Split( '.' ) );
            if ( elements.Count < 3 )
            {
                throw new InvalidOperationException( $"Unable to parse {caption} because it has fewer than 3 elements" );
            }
            else
            {
                string value = elements.Pop();
                bool localTryParse() { long argresult = this.SerialNumber; var ret = long.TryParse( value, out argresult ); this.SerialNumber = argresult; return ret; }

                if ( !localTryParse() )
                {
                    throw new InvalidOperationException( $"Unable to parse {value} into a serial number" );
                }

                value = elements.Pop();
                bool localTryParse1() { long argresult = this.Id; var ret = long.TryParse( value, out argresult ); this.Id = argresult; return ret; }

                if ( !localTryParse1() )
                {
                    throw new InvalidOperationException( $"Unable to parse {value} into a device id" );
                }

                value = elements.Pop();
                bool localTryParse2() { var argresult = this.HardwareType; var ret = Enum.TryParse( value, out argresult ); this.HardwareType = argresult; return ret; }

                if ( !localTryParse2() )
                {
                    throw new InvalidOperationException( $"Unable to parse {value} into hardware type" );
                }
            }
        }
    }

    /// <summary> Collection of device information. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    [CLSCompliant( false )]
    public class DeviceInfoCollection : System.Collections.ObjectModel.Collection<DeviceInfo>
    {

        /// <summary> Attempts to enumerate devices from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="connection"> The connection. </param>
        /// <param name="details">    The details. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryEnumerateDevices( Dln.Connection connection, string details )
        {
            this.Clear();
            if ( connection is null )
            {
                details = "Not created";
            }
            else if ( !connection.IsConnected() )
            {
                details = "Not connected";
            }
            else if ( Dln.Device.Count() == 0L )
            {
                details = $"No DLN-series adapters found @{connection.Address()}.";
            }
            else
            {
                for ( long i = 0L, loopTo = Dln.Device.Count() - 1L; i <= loopTo; i++ )
                {
                    var d = Dln.Device.Open( ( int ) i );
                    if ( d is object )
                        this.Add( new DeviceInfo( d ) );
                }
            }

            return string.IsNullOrWhiteSpace( details );
        }
    }
}
