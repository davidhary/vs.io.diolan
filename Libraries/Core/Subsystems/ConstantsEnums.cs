using System.ComponentModel;

namespace isr.Diolan
{
    #region " GPIO "

    /// <summary> Values that represent pin directions. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum PinDirection
    {
        /// <summary>   An enum constant representing the input option. </summary>
        [Description( "Input" )]
        Input = 0,

        /// <summary>   An enum constant representing the output option. </summary>
        [Description( "Output" )]
        Output = 1
    }

    /// <summary> Values that represent pin values. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum PinValue
    {
        /// <summary>   An enum constant representing the logic zero option. </summary>
        [Description( "Logic 0" )]
        LogicZero = 0,

        /// <summary>   An enum constant representing the logic one option. </summary>
        [Description( "Logic 1" )]
        LogicOne = 1
    }
}


#endregion

