using System;
using System.Diagnostics;

namespace isr.Diolan.Gpio
{

    /// <summary> Gpio handler driver. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    [CLSCompliant( false )]
    public class GpioHandlerDriver : GpioHandlerDriverBase
    {

        #region " CONSTRUCTOR "

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    this.ReleasePins();
                }
            }
            catch ( Exception )
            {
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PUBLISHER "

        /// <summary> Publishes all values by raising the property changed events. </summary>
        /// <remarks>
        /// This is handled at the top level class in case the inheriting class added property over the
        /// base class.
        /// </remarks>
        public override void PublishAll()
        {
            foreach ( System.Reflection.PropertyInfo p in System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.GetProperties() )
                this.AsyncNotifyPropertyChanged( p.Name );
        }

        #endregion

        #region " CONFIGURE "

        /// <summary> Configures start test signal. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected override void ConfigureStartTest( Dln.Gpio.Pins pins )
        {
            if ( pins is null )
                throw new ArgumentNullException( nameof( pins ) );
            if ( pins.Count == 0 )
                throw new InvalidOperationException( "GPIO Port has no pins to configure for the handler" );
            this.StartTestPin = new GpioInputPin( pins, this.StartTestPinNumber, this.ActiveLogic );
        }

        /// <summary> Registers the start test pin events. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void RegisterStartTestPinEvents()
        {
            if ( !this.RegisteredStartTestPinEvents )
            {
                var pin = this.Pins[this.StartTestPin.PinNumber];
                pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                pin.SetEventConfiguration( Dln.Gpio.EventType.Change, 0 );
                base.OnRegisteringStartTestPinEvents();
            }
        }

        /// <summary> Configures end test signal. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected override void ConfigureEndTest( Dln.Gpio.Pins pins )
        {
            if ( pins is null )
                throw new ArgumentNullException( nameof( pins ) );
            if ( pins.Count == 0 )
                throw new InvalidOperationException( "GPIO Port has no pins to configure for the handler" );
            this.EndTestPin = new GpioOutputPin( pins, this.EndTestPinNumber, this.ActiveLogic );
        }

        /// <summary> Configures bin port. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected override void ConfigureBinPort( Dln.Gpio.Pins pins )
        {
            if ( pins is null )
                throw new ArgumentNullException( nameof( pins ) );
            if ( pins.Count == 0 )
                throw new InvalidOperationException( "GPIO Port has no pins to configure for the handler" );
            this.BinPort = new GpioOutputPort( pins, this.BinMask, this.ActiveLogic );
        }

        /// <summary> Executes the initialize known state action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public override void InitializeKnownState()
        {
            // this needs to happen after the emulator is configured.
            this.RegisterStartTestPinEvents();
            this.EndTestPin.LogicalValue = 0;
            base.InitializeKnownState();
        }

        #endregion

        #region " EVENTS "

        /// <summary> Raises the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnStartTestEventOccurred( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                base.OnStartTestEventOccurred( e );
                if ( e.Value == this.BitValue( 1 ) )
                {
                    // if logical 1, start of test was received.
                    this.OnStartTestReceived();
                }
                else if ( this.State == HandlerState.EndTestSent )
                {
                    // acknowledge is value if end of test was sent so as to prevent acknowledging when enabling start test.
                    // if logical zero, end of test was acknowledged if using continuous duty.
                    this.OnEndTestAcknowledged();
                }
            }
        }

        #endregion

        #region " ACTIONS "

        /// <summary> Tests enable start. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void EnableStartTest()
        {

            // clear to start
            this.EndTestPin.LogicalValue = 0;
            this.EndTestLogicalValue = this.EndTestPin.LogicalValue == 1;
            this.OnStartTestEnabled();
        }

        /// <summary> Outputs the end of test sequence. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="binValue"> The bin value. </param>
        public void OutputEndTest( long binValue )
        {

            // Set the bin port -- this is turned off upon receipt of Start Of Test.
            this.BinPort.MaskedValue = binValue;

            // show the bin value
            this.BinValue = binValue;

            // wait a bit
            Core.ApplianceBase.DoEventsWait( this.BinEndTestOnsetDelay );
            this.OnEndTestSent();

            // send the end of test signal.
            switch ( this.EndTestMode )
            {
                case EndTestMode.ActiveBin:
                    {
                        // in active bin, the EOT is logical high to signal binning
                        this.EndTestPin.LogicalValue = 1;
                        break;
                    }

                case EndTestMode.ActiveTest:
                    {
                        // in active Test, the EOT is turns from logical high to low to signal binning

                        this.EndTestPin.LogicalValue = 0;
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, $"Unhandled end test mode {this.EndTestMode} at driver On Start Test Received" );
                        break;
                    }
            }

            this.EndTestLogicalValue = this.EndTestPin.LogicalValue == 1;
        }

        /// <summary> Executes the start test received action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected override void OnStartTestReceived()
        {
            // clear the bin output
            this.BinPort.MaskedValue = 0L;
            switch ( this.EndTestMode )
            {
                case EndTestMode.ActiveBin:
                    {
                        // in active bin, the EOT is logical low during the test.
                        this.EndTestPin.LogicalValue = 0;
                        break;
                    }

                case EndTestMode.ActiveTest:
                    {
                        // in active Test, the EOT is logical high during the test.
                        this.EndTestPin.LogicalValue = 1;
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, $"Unhandled end test mode {this.EndTestMode} at driver On Start Test Received" );
                        break;
                    }
            }

            this.EndTestLogicalValue = this.EndTestPin.LogicalValue == 1;
            base.OnStartTestReceived();
        }

        /// <summary> Executes the end test acknowledged action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected override void OnEndTestAcknowledged()
        {
            // possibly, the end of test acknowledged does not indicate that the handler saw the Bin, in which case
            // the bin can be left and rest after SOT is received again. 
            // Me._BinPort.Value = 0
            base.OnEndTestAcknowledged();
        }

        #endregion

    }
}
