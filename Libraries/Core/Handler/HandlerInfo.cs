﻿using System;
using System.ComponentModel;

namespace isr.Diolan.Gpio
{

    /// <summary> Information about the handler. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-09-09 </para>
    /// </remarks>
    public class HandlerInfo
    {

        /// <summary> Gets the name. </summary>
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets the bin mask. </summary>
        /// <value> The bin mask. </value>
        public long BinMask { get; set; }

        /// <summary> Gets the end of test delay after bin onset. </summary>
        /// <value> The bin to end of test onset delay. </value>
        public TimeSpan BinEndTestOnsetDelay { get; set; }

        /// <summary> Gets the end test pin number. </summary>
        /// <value> The end test pin number. </value>
        public int EndTestPinNumber { get; set; }

        /// <summary> Gets the start test pin number. </summary>
        /// <value> The start test pin number. </value>
        public int StartTestPinNumber { get; set; }

        /// <summary> Gets the active logic. </summary>
        /// <value> The active logic. </value>
        public ActiveLogic ActiveLogic { get; set; }

        /// <summary> Gets the end test mode. </summary>
        /// <value> The end test mode. </value>
        public EndTestMode EndTestMode { get; set; }

        /// <summary> Gets the open drain enabled. </summary>
        /// <value> The open drain enabled. </value>
        public bool OpenDrainEnabled { get; set; }

        /// <summary> Gets the pull-up enabled. </summary>
        /// <value> The pull-up enabled. </value>
        public bool PullupEnabled { get; set; }

        #region " NO HAU HANDLER "

        /// <summary> Information describing the no hau handler. </summary>
        private static HandlerInfo _NoHauHandlerInfo;

        /// <summary> Gets information describing the No Hau handler. </summary>
        /// <value> Information describing the No Hau handler. </value>
        public static HandlerInfo NoHauHandlerInfo
        {
            get {
                if ( _NoHauHandlerInfo is null )
                {
                    _NoHauHandlerInfo = new HandlerInfo() {
                        Name = "No Hau",
                        BinMask = Convert.ToInt32( "1100", 2 ),
                        BinEndTestOnsetDelay = TimeSpan.FromTicks( TimeSpan.TicksPerMillisecond * 5L ),
                        StartTestPinNumber = 0,
                        EndTestPinNumber = 1,
                        ActiveLogic = ActiveLogic.ActiveHigh,
                        PullupEnabled = true,
                        OpenDrainEnabled = false,
                        EndTestMode = EndTestMode.ActiveTest
                    };
                }

                return _NoHauHandlerInfo;
            }
        }

        /// <summary> Information describing the no hau emulator. </summary>
        private static HandlerInfo _NoHauEmulatorInfo;

        /// <summary> Gets information describing the No Hau handler. </summary>
        /// <value> Information describing the No Hau handler. </value>
        public static HandlerInfo NoHauEmulatorInfo
        {
            get {
                if ( _NoHauEmulatorInfo is null )
                {
                    _NoHauEmulatorInfo = new HandlerInfo() {
                        Name = "No Hau",
                        BinMask = Convert.ToInt32( "11000000", 2 ),
                        BinEndTestOnsetDelay = TimeSpan.FromTicks( TimeSpan.TicksPerMillisecond * 5L ),
                        StartTestPinNumber = 4,
                        EndTestPinNumber = 5,
                        ActiveLogic = ActiveLogic.ActiveHigh,
                        PullupEnabled = true,
                        OpenDrainEnabled = false,
                        EndTestMode = EndTestMode.ActiveTest
                    };
                }

                return _NoHauEmulatorInfo;
            }
        }

        #endregion

        #region " AETRIUM HANDLER "

        /// <summary> Information describing the aetrium handler. </summary>
        private static HandlerInfo _AetriumHandlerInfo;

        /// <summary> Gets information describing the Aetrium handler. </summary>
        /// <value> Information describing the no how handler. </value>
        public static HandlerInfo AetriumHandlerInfo
        {
            get {
                if ( _AetriumHandlerInfo is null )
                {
                    _AetriumHandlerInfo = new HandlerInfo() {
                        Name = "Aetrium",
                        BinMask = Convert.ToInt32( "1100", 2 ),
                        BinEndTestOnsetDelay = TimeSpan.FromTicks( TimeSpan.TicksPerMillisecond * 4L ),
                        StartTestPinNumber = 0,
                        EndTestPinNumber = 1,
                        ActiveLogic = ActiveLogic.ActiveLow,
                        PullupEnabled = true,
                        OpenDrainEnabled = false,
                        EndTestMode = EndTestMode.ActiveBin
                    };
                }

                return _AetriumHandlerInfo;
            }
        }

        /// <summary> Information describing the aetrium emulator. </summary>
        private static HandlerInfo _AetriumEmulatorInfo;

        /// <summary> Gets information describing the Aetrium handler. </summary>
        /// <value> Information describing the no how handler. </value>
        public static HandlerInfo AetriumEmulatorInfo
        {
            get {
                if ( _AetriumEmulatorInfo is null )
                {
                    _AetriumEmulatorInfo = new HandlerInfo() {
                        Name = "Aetrium",
                        BinMask = Convert.ToInt32( "11000000", 2 ),
                        BinEndTestOnsetDelay = TimeSpan.FromTicks( TimeSpan.TicksPerMillisecond * 4L ),
                        StartTestPinNumber = 4,
                        EndTestPinNumber = 5,
                        ActiveLogic = ActiveLogic.ActiveLow,
                        PullupEnabled = true,
                        OpenDrainEnabled = false,
                        EndTestMode = EndTestMode.ActiveBin
                    };
                }

                return _AetriumEmulatorInfo;
            }
        }

        #endregion

    }

    /// <summary> Values that represent supported handlers. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum SupportedHandler
    {

        /// <summary> Aetrium handler. </summary>
        [Description( "Aetrium" )]
        Aetrium,

        /// <summary> No Hau Handler. </summary>
        [Description( "No Hau" )]
        NoHau
    }

    /// <summary> Values that represent end test modes. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public enum EndTestMode
    {

        /// <summary> The end of test signal remains low during the test and turns on after turning on the binning bits. </summary>
        [Description( "Active Bin" )]
        ActiveBin,

        /// <summary> The end of test signal turns high during the test and turns low after turning on the binning bits. </summary>
        [Description( "Active Test" )]
        ActiveTest
    }
}