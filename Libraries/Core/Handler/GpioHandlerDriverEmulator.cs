using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Core;
using isr.Diolan.ExceptionExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Gpio
{

    /// <summary> Gpio handler driver and emulator. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-16, 5645. </para>
    /// </remarks>
    public class GpioHandlerDriverEmulator : ModalityConnectorBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private GpioHandlerDriverEmulator() : base()
        {
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static GpioHandlerDriverEmulator Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static GpioHandlerDriverEmulator Get()
        {
            if ( Instance is null || Instance.IsDisposed )
            {
                lock ( SyncLocker )
                    Instance = new GpioHandlerDriverEmulator();
            }

            return Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( SyncLocker )
                    return Instance is object && !Instance.IsDisposed;
            }
        }

        /// <summary> Dispose instance. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public static void DisposeInstance()
        {
            lock ( SyncLocker )
            {
                if ( Instance is object && !Instance.IsDisposed )
                {
                    Instance.Dispose();
                    Instance = null;
                }
            }
        }

        #region " I DISPOSABLE SUPPORT "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to
        /// release only
        /// unmanaged resources
        /// when called from
        /// the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._Driver is object )
                    {
                        this._Driver.Dispose();
                        this._Driver = null;
                    }

                    if ( this._Emulator is object )
                    {
                        this._Emulator.Dispose();
                        this._Emulator = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SUBSYSTEM MODALITIES "

        /// <summary> The modality. </summary>
        private readonly DeviceModalities _Modality = DeviceModalities.Gpio;

        /// <summary> The modality. </summary>
        /// <value> The modality. </value>
        public override DeviceModalities Modality => this._Modality;

        /// <summary> Executes the modality closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected override void OnModalityClosed()
        {
            if ( this._Emulator is object )
            {
                this._Emulator.Dispose();
                this._Emulator = null;
            }

            if ( this._Driver is object )
            {
                this._Driver.Dispose();
                this._Driver = null;
            }

            base.OnModalityClosed();
        }

        /// <summary> Executes the modality closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnModalityClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsModalityOpen() )
                {
                    // un-register event handlers for all pins
                    foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                        pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                }
            }
        }

        /// <summary> Executes the modality opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected override bool TryOpenModality( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            this._Driver = null;
            this._Emulator = null;

            // Get port count
            if ( this.Device.Gpio.Pins.Count == 0 )
            {
                // this is already done when opening the device.
                e.RegisterFailure( $"Adapter '{this.Device.Caption()}' doesn't support GPIO interface." );
            }
            else
            {

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handler for all pins
                foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                    pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                this._Driver = new GpioHandlerDriver();
                this._Emulator = new GpioHandlerEmulator();
            }

            return !e.Failed;
        }

        /// <summary> Queries if a modality is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
        public override bool IsModalityOpen()
        {
            return this._Driver is object;
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                this.EventCaption = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}";
            }
        }

        #endregion

        #region " DRIVER + EMULATOR "

        /// <summary> Attempts to configure from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool TryConfigure( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( this.TryConfigureDriver( e ) && this.TryConfigureEmulator( e ) && this.TryInitializeKnownState( e ) )
            {
            }

            return !e.Failed;
        }

        /// <summary> Attempts to initialize know state. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryInitializeKnownState( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            try
            {
                if ( this.Driver is null )
                {
                    e.RegisterFailure( "Handler driver not set" );
                }
                else if ( this.Emulator is null )
                {
                    e.RegisterFailure( "Handler emulator not set" );
                }
                else if ( !this.Driver.IsConfigured )
                {
                    e.RegisterFailure( "Handler driver not configured" );
                }
                else if ( !this.Emulator.IsConfigured )
                {
                    e.RegisterFailure( "Handler emulator Not configured" );
                }
                else
                {
                    this.Emulator.InitializeKnownState();
                    this.Driver.InitializeKnownState();
                    this.Emulator.InitializeKnownState();
                }
            }
            catch ( Exception ex )
            {
                e.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
            }

            return !e.Failed;
        }

        #endregion

        #region " DRIVER "

        /// <summary> Gets or sets the driver source. </summary>
        /// <value> The driver source. </value>
        public string DriverSource { get; private set; } = $"{nameof( Driver )}.";

#pragma warning disable IDE1006 // Naming Styles
        private GpioHandlerDriver __Driver;

        private GpioHandlerDriver _Driver
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__Driver;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__Driver != null )
                {
                    this.__Driver.PropertyChanged -= this.Driver_PropertyChanged;
                }

                this.__Driver = value;
                if ( this.__Driver != null )
                {
                    this.__Driver.PropertyChanged += this.Driver_PropertyChanged;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the driver. </summary>
        /// <value> The driver. </value>
        [CLSCompliant( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandlerDriver Driver
        {
            get => this._Driver;

            set => this._Driver = value;
        }

        /// <summary> Attempts to configure driver from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryConfigureDriver( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            try
            {
                if ( !this.IsDeviceModalityOpen )
                {
                    e.RegisterFailure( "Device not open" );
                }
                else if ( this.Device.Gpio is null )
                {
                    e.RegisterFailure( $"Gpio not supported on {this.Device.Caption()}" );
                }
                else if ( this.Device.Gpio.Pins is null )
                {
                    e.RegisterFailure( $"Gpio not supported on {this.Device.Caption()}" );
                }
                else if ( this.Driver is null )
                {
                    e.RegisterFailure( $"Driver not configured {this.Device.Caption()}" );
                }
                else if ( this.Driver.TryValidate( this.Device.Gpio.Pins, e ) )
                {
                    this.Driver.Configure( this.Device.Gpio.Pins );
                }
            }
            catch ( Exception ex )
            {
                e.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
            }

            return !e.Failed;
        }

        /// <summary> Driver property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void Driver_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object )
            {
                this.SyncNotifyPropertyChanged( $"{nameof( this.Driver )}.{e.PropertyName}" );
            }
        }


        #endregion

        #region " EMULATOR "

        /// <summary> Gets or sets the emulator source. </summary>
        /// <value> The emulator source. </value>
        public string EmulatorSource { get; private set; } = $"{nameof( Emulator )}.";

#pragma warning disable IDE1006 // Naming Styles
        private GpioHandlerEmulator __Emulator;

        private GpioHandlerEmulator _Emulator
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__Emulator;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__Emulator != null )
                {

                    this.__Emulator.PropertyChanged -= this.Emulator_PropertyChanged;
                }

                this.__Emulator = value;
                if ( this.__Emulator != null )
                {
                    this.__Emulator.PropertyChanged += this.Emulator_PropertyChanged;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the handler. </summary>
        /// <value> The handler. </value>
        [CLSCompliant( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandlerEmulator Emulator
        {
            get => this._Emulator;

            set => this._Emulator = value;
        }

        /// <summary> Attempts to configure emulator from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryConfigureEmulator( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            try
            {
                if ( !this.IsDeviceModalityOpen )
                {
                    e.RegisterFailure( "Device not open" );
                }
                else if ( this.Device.Gpio is null )
                {
                    e.RegisterFailure( $"Gpio not supported on {this.Device.Caption()}" );
                }
                else if ( this.Device.Gpio.Pins is null )
                {
                    e.RegisterFailure( $"Gpio not supported on {this.Device.Caption()}" );
                }
                else if ( this.Emulator is null )
                {
                    e.RegisterFailure( "Emulator not created" );
                }
                else if ( this.Emulator.TryValidate( this.Device.Gpio.Pins, e ) )
                {
                    this.Emulator.Configure( this.Device.Gpio.Pins );
                }
            }
            catch ( Exception ex )
            {
                e.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
            }

            return !e.Failed;
        }

        /// <summary> Emulator property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void Emulator_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object )
            {
                this.SyncNotifyPropertyChanged( $"{this.EmulatorSource}{e.PropertyName}" );
            }
        }

        #endregion

        #region " PLAY "

        /// <summary> Attempts to clear start from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryClearStart( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            try
            {
                if ( this.Driver is null )
                {
                    e.RegisterFailure( "Handler driver not set" );
                }
                else if ( !this.Driver.IsConfigured )
                {
                    e.RegisterFailure( "Handler driver not configured" );
                }
                else if ( this.Driver.State == HandlerState.Idle || this.Driver.State == HandlerState.EndTestAcknowledged )
                {
                    this.Driver.EnableStartTest();
                }
                else
                {
                    e.RegisterFailure( $"Invalid handler state @'{this._Driver.State}'; state should be '{HandlerState.Idle}' or '{HandlerState.EndTestAcknowledged}'" );
                }
            }
            catch ( Exception ex )
            {
                e.RegisterError( ex.ToFullBlownString() );
            }

            return !e.Failed;
        }

        /// <summary> Attempts to start test from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel details event information. </param>
        /// <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryStartTest( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            try
            {
                if ( this.Driver is null )
                {
                    e.RegisterFailure( "Handler driver not set" );
                }
                else if ( !this.Driver.IsConfigured )
                {
                    e.RegisterFailure( "Handler driver not configured" );
                }
                else if ( this.Emulator is null )
                {
                    e.RegisterFailure( "Handler emulator not set" );
                }
                else if ( !this.Emulator.IsConfigured )
                {
                    e.RegisterFailure( "Handler emulator not configured" );
                }
                else if ( this.Driver.State != HandlerState.StartTestEnabled )
                {
                    e.RegisterFailure( $"Invalid handler state @'{this.Driver.State}'; state should be @'{HandlerState.StartTestEnabled}'" );
                }
                else
                {
                    this.Emulator.OutputStartTest();
                }
            }
            catch ( Exception ex )
            {
                e.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
            }

            return !e.Failed;
        }

        /// <summary> Sends the end test command. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="binValue"> The bin value. </param>
        /// <param name="e">        Cancel details event information. </param>
        /// <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
        public bool TryOutputEndTest( int binValue, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( this.Driver is null )
            {
                e.RegisterFailure( "Handler driver not set" );
            }
            else if ( !this.Driver.IsConfigured )
            {
                e.RegisterFailure( "Handler driver not configured" );
            }
            else if ( this.Driver.State == HandlerState.StartTestReceived )
            {
                this.Driver.OutputEndTest( binValue );
            }
            else if ( !this.TryInitializeKnownState( e ) )
            {
                // ? 20170504 was if me.try....
                e.RegisterFailure( $"Invalid handler state @'{this.Driver.State}'; state should be @'{HandlerState.Idle}'; {e.Details}" );
            }

            return !e.Failed;
        }

        #endregion

    }
}
