using System;

using isr.Core;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Gpio
{

    /// <summary> Gpio handler driver Base. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-09 </para>
    /// </remarks>
    [CLSCompliant( false )]
    public abstract class GpioHandlerDriverBase : HandlerDriverBase
    {

        #region " CONSTRUCTOR "

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    this.ReleasePins();
                    this.StartTestPin = null;
                    this.EndTestPin = null;
                    this.BinPort = null;
                }
            }
            catch ( Exception )
            {
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PINS AND PORTS "

        /// <summary> Gets the start test pin. </summary>
        /// <value> The start test pin. </value>
        protected PinBase StartTestPin { get; set; }

        /// <summary> Gets the end test pin. </summary>
        /// <value> The end test pin. </value>
        protected PinBase EndTestPin { get; set; }

        /// <summary> Gets the bin port. </summary>
        /// <value> The bin port. </value>
        protected PortBase BinPort { get; set; }

        /// <summary> Gets a value indicating whether the registered start test pin events. </summary>
        /// <value> <c>true</c> if registered start test pin events; otherwise <c>false</c> </value>
        protected bool RegisteredStartTestPinEvents { get; set; }

        /// <summary> Gets a value indicating whether the registered end test pin events. </summary>
        /// <value> <c>true</c> if registered end test pin events; otherwise <c>false</c> </value>
        protected bool RegisteredEndTestPinEvents { get; set; }

        /// <summary> Gets a value indicating whether the registered port pin events. </summary>
        /// <value> <c>true</c> if registered port pin events; otherwise <c>false</c> </value>
        protected bool RegisteredBinPortPinEvents { get; set; }

        /// <summary> Executes the registering start test pin events action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected virtual void OnRegisteringStartTestPinEvents()
        {
            this.RegisteredStartTestPinEvents = true;
        }

        /// <summary> Executes the registering end test pin events action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected virtual void OnRegisteringEndTestPinEvents()
        {
            this.RegisteredEndTestPinEvents = true;
        }

        /// <summary> Executes the 'registering bin port pin events' action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected virtual void OnRegisteringBinPortPinEvents()
        {
            this.RegisteredBinPortPinEvents = true;
        }

        /// <summary> Releases the pins. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        protected override void ReleasePins()
        {
            if ( this.RegisteredStartTestPinEvents && this.StartTestPin is object && this.Pins is object )
            {
                var pin = this.Pins[this.StartTestPin.PinNumber];
                pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                pin.SetEventConfiguration( Dln.Gpio.EventType.None, 0 );
            }

            this.RegisteredStartTestPinEvents = false;
            this.StartTestPin = null;
            if ( this.RegisteredEndTestPinEvents && this.EndTestPin is object && this.Pins is object )
            {
                var pin = this.Pins[this.EndTestPin.PinNumber];
                pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                pin.SetEventConfiguration( Dln.Gpio.EventType.None, 0 );
            }

            this.RegisteredEndTestPinEvents = false;
            this.EndTestPin = null;
            if ( this.RegisteredBinPortPinEvents && this.BinPort is object )
            {
                Dln.Gpio.Pin pin;
                foreach ( int i in this.Pins.SelectMulti( ( ulong ) this.BinPort.Mask ) )
                {
                    pin = this.Pins[i];
                    pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                    pin.SetEventConfiguration( Dln.Gpio.EventType.None, 0 );
                }
            }

            this.RegisteredBinPortPinEvents = false;
            this.BinPort = null;
            this.Pins = null;
            base.ReleasePins();
        }

        #endregion

        #region " CONFIGURE "

        /// <summary> Gets a value indicating whether this object is configured. </summary>
        /// <value> <c>true</c> if this object is configured; otherwise <c>false</c> </value>
        public override bool IsConfigured => base.IsConfigured && this.Pins is object && this.StartTestPin is object && this.EndTestPin is object && this.BinPort is object;

        /// <summary> Gets the pins. </summary>
        /// <value> The pins. </value>
        protected Dln.Gpio.Pins Pins { get; private set; }

        /// <summary> Tests configure start. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected abstract void ConfigureStartTest( Dln.Gpio.Pins pins );

        /// <summary> Tests configure end. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected abstract void ConfigureEndTest( Dln.Gpio.Pins pins );

        /// <summary> Configure bin port. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        protected abstract void ConfigureBinPort( Dln.Gpio.Pins pins );

        /// <summary> Configures the given pins. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="pins"> The pins. </param>
        [CLSCompliant( false )]
        public void Configure( Dln.Gpio.Pins pins )
        {
            if ( pins is null )
                throw new ArgumentNullException( nameof( pins ) );
            if ( pins.Count == 0 )
                throw new InvalidOperationException( "GPIO Port has no pins to configure for the handler" );
            this.ReleasePins();
            this.Configure();
            this.Pins = pins;
            this.ConfigureStartTest( pins );
            this.ConfigureEndTest( pins );
            this.ConfigureBinPort( pins );
            this.AsyncNotifyPropertyChanged( nameof( this.IsConfigured ) );
        }

        /// <summary> Attempts to validate from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pins"> The pins. </param>
        /// <param name="e">    Action event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [CLSCompliant( false )]
        public bool TryValidate( Dln.Gpio.Pins pins, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( pins is null )
            {
                e.RegisterFailure( "Gpio not supported--pins not instantiated" );
            }
            else if ( pins.Count < 4 )
            {
                e.RegisterFailure( $"Gpio has only {pins.Count} pins" );
            }
            else if ( this.StartTestPinNumber < 0 || this.StartTestPinNumber >= pins.Count )
            {
                e.RegisterFailure( $"Start test pin number {this.StartTestPinNumber} must be between 0 and {pins.Count - 1}" );
            }
            else if ( this.EndTestPinNumber < 0 || this.EndTestPinNumber >= pins.Count )
            {
                e.RegisterFailure( $"End test pin number {this.EndTestPinNumber} must be between 0 and {pins.Count - 1}" );
            }
            else if ( this.StartTestPinNumber == this.EndTestPinNumber )
            {
                e.RegisterFailure( $"Start test pin number {this.StartTestPinNumber} must be different from end test pin number {this.EndTestPinNumber}" );
            }
            else if ( this.BinMask < 0L )
            {
                e.RegisterFailure( $"Bin mask {this.BinMask} must be positive" );
            }
            else if ( (this.BinMask & ( int ) Math.Pow( 2d, this.StartTestPinNumber )) != 0L )
            {
                e.RegisterFailure( $"Bin mask {this.BinMask} must not contain the start test pin number" );
            }
            else if ( (this.BinMask & ( int ) Math.Pow( 2d, this.EndTestPinNumber )) != 0L )
            {
                e.RegisterFailure( $"Bin mask {this.BinMask} must not contain the end test pin number" );
            }

            return !e.Failed;
        }

        /// <summary> Executes the initialize known state action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public override void InitializeKnownState()
        {
            this.StartTestLogicalValue = this.StartTestPin.LogicalValue == 1;
            this.EndTestLogicalValue = this.EndTestPin.LogicalValue == 1;
            base.InitializeKnownState();
        }

        #endregion

        #region " BIT VALUE AND LOGICAL VALUES "

        /// <summary> Query if 'value' is not active state. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if not active state; otherwise <c>false</c> </returns>
        public bool IsNotActiveState( int value )
        {
            return value == this.BitValue( 0 );
        }

        /// <summary> Query if 'value' is active state. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if active state; otherwise <c>false</c> </returns>
        public bool IsActiveState( int value )
        {
            return value == this.BitValue( 1 );
        }

        /// <summary> Converts a value to a logical state. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> value as a LogicalState. </returns>
        public LogicalState ToLogicalState( int value )
        {
            return this.IsActiveState( value ) ? LogicalState.Active : LogicalState.Inactive;
        }

        /// <summary> Active logical state bit value. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> An Integer. </returns>
        public int ActiveLogicalStateBitValue()
        {
            return this.BitValue( 1 );
        }

        /// <summary> Not active logical state bit value. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> An Integer. </returns>
        public int NotActiveLogicalStateBitValue()
        {
            return this.BitValue( 0 );
        }

        /// <summary> Bit value. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="logicalValue"> The logical value. </param>
        /// <returns> An Integer. </returns>
        public int BitValue( int logicalValue )
        {
            return this.ActiveLogic == ActiveLogic.ActiveLow ? PinBase.BitZeroMask & ~logicalValue : PinBase.BitZeroMask & logicalValue;
        }

        #endregion

        #region " EVENTS "

        /// <summary> Raises the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnStartTestEventOccurred( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                this.StartTestLogicalValue = e.Value == this.BitValue( 1 );
            }
        }

        /// <summary> Raises the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnEndTestEventOccurred( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                this.EndTestLogicalValue = e.Value == this.BitValue( 1 );
            }
        }

        /// <summary> Raises the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnBinPortPinEventOccurred( Dln.Gpio.ConditionMetEventArgs e )
        {
        }

        /// <summary> Handler, called when start test signal was send by the handler. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        protected virtual void ConditionMetEventHandler( object sender, Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( sender is null || e is null )
                return;
            switch ( e.Pin )
            {
                case var @case when @case == this.StartTestPinNumber:
                    {
                        this.OnStartTestEventOccurred( e );
                        break;
                    }

                case var case1 when case1 == this.EndTestPinNumber:
                    {
                        this.OnEndTestEventOccurred( e );
                        break;
                    }

                default:
                    {
                        int pinMask = ( int ) Math.Pow( 2d, e.Pin );
                        if ( (this.BinMask & pinMask) == pinMask )
                        {
                            this.OnBinPortPinEventOccurred( e );
                        }

                        break;
                    }
            }
        }

        #endregion

    }
}
