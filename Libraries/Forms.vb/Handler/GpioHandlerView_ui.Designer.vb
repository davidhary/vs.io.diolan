﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GpioHandlerView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._BottomToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TabComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._HandlerComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._SelectServerButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ServerNameTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._DefaultServerMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ConnectServerButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenDeviceModalityButton = New System.Windows.Forms.ToolStripButton()
        Me._SelectDeviceSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DevicesComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._DeviceInfoTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._ResetButton = New System.Windows.Forms.ToolStripButton()
        Me._Tabs = New isr.Diolan.Forms.ExtendedTabControl()
        Me._DriverTabPage = New System.Windows.Forms.TabPage()
        Me._ConfigureInterfaceTabLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ConfigureGroupBox = New System.Windows.Forms.GroupBox()
        Me._EndTestDelayNumericLabel = New System.Windows.Forms.Label()
        Me._EndTestDelayNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ConfigureHandlerInterfaceButton = New System.Windows.Forms.Button()
        Me._DigitalLogicLabel = New System.Windows.Forms.Label()
        Me._ActiveHighLogicRadioButton = New System.Windows.Forms.RadioButton()
        Me._ActiveLowLogicRadioButton = New System.Windows.Forms.RadioButton()
        Me._BinPortMaskTextBoxLabel = New System.Windows.Forms.Label()
        Me._StartTestPinNumberNumericLabel = New System.Windows.Forms.Label()
        Me._BinPortMaskTextBox = New System.Windows.Forms.TextBox()
        Me._EndTestPinNumberNumericLabel = New System.Windows.Forms.Label()
        Me._EndTestPinNumberNumeric = New System.Windows.Forms.NumericUpDown()
        Me._StartTestPinNumberNumeric = New System.Windows.Forms.NumericUpDown()
        Me._EmulatorTabPage = New System.Windows.Forms.TabPage()
        Me._EmulatorLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ConfigureEmulatorGroupBox = New System.Windows.Forms.GroupBox()
        Me._ConfigureEmulatorButton = New System.Windows.Forms.Button()
        Me._EmulatorBinPortMaskTextBoxLabel = New System.Windows.Forms.Label()
        Me._EmulatorStartTestPinNumberNumericLabel = New System.Windows.Forms.Label()
        Me._EmulatorBinPortMaskTextBox = New System.Windows.Forms.TextBox()
        Me._EmulatorEndTestPinNumberNumericLabel = New System.Windows.Forms.Label()
        Me._EmulatorEndTestPinNumberNumeric = New System.Windows.Forms.NumericUpDown()
        Me._EmulatorStartTestPinNumberNumeric = New System.Windows.Forms.NumericUpDown()
        Me._PlayTabPage = New System.Windows.Forms.TabPage()
        Me._PlayTabPageLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._HandlerInterfacePlayGroupBox = New System.Windows.Forms.GroupBox()
        Me._FailBinValueNumeric = New System.Windows.Forms.NumericUpDown()
        Me._OutputFailBinButton = New System.Windows.Forms.Button()
        Me._PassBinValueNumeric = New System.Windows.Forms.NumericUpDown()
        Me._OutputPassBinButton = New System.Windows.Forms.Button()
        Me._ClearStartButton = New System.Windows.Forms.Button()
        Me._HandlerEmulatorPlayGroupBox = New System.Windows.Forms.GroupBox()
        Me._StartTestButton = New System.Windows.Forms.Button()
        Me._EventLogTabPage = New System.Windows.Forms.TabPage()
        Me._EventLogTextBox = New System.Windows.Forms.TextBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ToolStripContainer = New System.Windows.Forms.ToolStripContainer()
        Me._TopToolStrip = New System.Windows.Forms.ToolStrip()
        Me._HandlerStateLabel = New System.Windows.Forms.ToolStripLabel()
        Me._EmulatorStateLabel = New System.Windows.Forms.ToolStripLabel()
        Me._HandlerInterfaceBinValueLabel = New System.Windows.Forms.ToolStripLabel()
        Me._EmulatorBinValueLabel = New System.Windows.Forms.ToolStripLabel()
        Me._SotLabel = New System.Windows.Forms.ToolStripLabel()
        Me._EotLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BottomToolStrip.SuspendLayout()
        Me._Tabs.SuspendLayout()
        Me._DriverTabPage.SuspendLayout()
        Me._ConfigureInterfaceTabLayout.SuspendLayout()
        Me._ConfigureGroupBox.SuspendLayout()
        CType(Me._EndTestDelayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._EndTestPinNumberNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._StartTestPinNumberNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._EmulatorTabPage.SuspendLayout()
        Me._EmulatorLayout.SuspendLayout()
        Me._ConfigureEmulatorGroupBox.SuspendLayout()
        CType(Me._EmulatorEndTestPinNumberNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._EmulatorStartTestPinNumberNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._PlayTabPage.SuspendLayout()
        Me._PlayTabPageLayout.SuspendLayout()
        Me._HandlerInterfacePlayGroupBox.SuspendLayout()
        CType(Me._FailBinValueNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._PassBinValueNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._HandlerEmulatorPlayGroupBox.SuspendLayout()
        Me._EventLogTabPage.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ToolStripContainer.BottomToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.ContentPanel.SuspendLayout()
        Me._ToolStripContainer.TopToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.SuspendLayout()
        Me._TopToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_BottomToolStrip
        '
        Me._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._BottomToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TabComboBox, Me._HandlerComboBox, Me._SelectServerButton, Me._ConnectServerButton, Me._OpenDeviceModalityButton, Me._SelectDeviceSplitButton, Me._DeviceInfoTextBox, Me._ResetButton})
        Me._BottomToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._BottomToolStrip.Name = "_BottomToolStrip"
        Me._BottomToolStrip.Size = New System.Drawing.Size(417, 29)
        Me._BottomToolStrip.Stretch = True
        Me._BottomToolStrip.TabIndex = 0
        '
        '_TabComboBox
        '
        Me._TabComboBox.Name = "_TabComboBox"
        Me._TabComboBox.Size = New System.Drawing.Size(81, 29)
        Me._TabComboBox.ToolTipText = "Select panel"
        '
        '_HandlerComboBox
        '
        Me._HandlerComboBox.Items.AddRange(New Object() {"Aetrium", "No Hau"})
        Me._HandlerComboBox.Name = "_HandlerComboBox"
        Me._HandlerComboBox.Size = New System.Drawing.Size(75, 29)
        Me._HandlerComboBox.Text = "No Hau"
        Me._HandlerComboBox.ToolTipText = "Handler"
        '
        '_SelectServerButton
        '
        Me._SelectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectServerButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServerNameTextBox, Me._DefaultServerMenuItem})
        Me._SelectServerButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Network_server
        Me._SelectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectServerButton.Name = "_SelectServerButton"
        Me._SelectServerButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectServerButton.Text = "Select Server"
        '
        '_ServerNameTextBox
        '
        Me._ServerNameTextBox.Name = "_ServerNameTextBox"
        Me._ServerNameTextBox.Size = New System.Drawing.Size(100, 23)
        Me._ServerNameTextBox.Text = "localhost:9656"
        '
        '_DefaultServerMenuItem
        '
        Me._DefaultServerMenuItem.Checked = True
        Me._DefaultServerMenuItem.CheckOnClick = True
        Me._DefaultServerMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._DefaultServerMenuItem.Name = "_DefaultServerMenuItem"
        Me._DefaultServerMenuItem.Size = New System.Drawing.Size(198, 22)
        Me._DefaultServerMenuItem.Text = "User Default Server:Port"
        '
        '_ConnectServerButton
        '
        Me._ConnectServerButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ConnectServerButton.ForeColor = System.Drawing.Color.Red
        Me._ConnectServerButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.WIFI_open_22
        Me._ConnectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ConnectServerButton.Name = "_ConnectServerButton"
        Me._ConnectServerButton.Size = New System.Drawing.Size(41, 26)
        Me._ConnectServerButton.Text = "X"
        Me._ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me._ConnectServerButton.ToolTipText = "Connect or disconnect the server and show count of attached devices."
        '
        '_OpenDeviceModalityButton
        '
        Me._OpenDeviceModalityButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._OpenDeviceModalityButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._OpenDeviceModalityButton.Enabled = False
        Me._OpenDeviceModalityButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.User_offline_2
        Me._OpenDeviceModalityButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._OpenDeviceModalityButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton"
        Me._OpenDeviceModalityButton.Size = New System.Drawing.Size(26, 26)
        Me._OpenDeviceModalityButton.Text = "Open"
        Me._OpenDeviceModalityButton.ToolTipText = "Open or close the device."
        '
        '_SelectDeviceSplitButton
        '
        Me._SelectDeviceSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._SelectDeviceSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectDeviceSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DevicesComboBox})
        Me._SelectDeviceSplitButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Network_server_database
        Me._SelectDeviceSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton"
        Me._SelectDeviceSplitButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectDeviceSplitButton.Text = "Device"
        Me._SelectDeviceSplitButton.ToolTipText = "Select Device"
        '
        '_DevicesComboBox
        '
        Me._DevicesComboBox.Name = "_DevicesComboBox"
        Me._DevicesComboBox.Size = New System.Drawing.Size(121, 23)
        Me._DevicesComboBox.Text = "DLN-4M.1.1"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(50, 29)
        Me._DeviceInfoTextBox.Text = "closed"
        Me._DeviceInfoTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_ResetButton
        '
        Me._ResetButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._ResetButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ResetButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.System_quick_restart
        Me._ResetButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ResetButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ResetButton.Name = "_ResetButton"
        Me._ResetButton.Size = New System.Drawing.Size(26, 26)
        Me._ResetButton.Text = "Reset Known State"
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._DriverTabPage)
        Me._Tabs.Controls.Add(Me._EmulatorTabPage)
        Me._Tabs.Controls.Add(Me._PlayTabPage)
        Me._Tabs.Controls.Add(Me._EventLogTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.HideTabHeaders = True
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(417, 452)
        Me._Tabs.TabIndex = 0
        '
        '_DriverTabPage
        '
        Me._DriverTabPage.Controls.Add(Me._ConfigureInterfaceTabLayout)
        Me._DriverTabPage.Location = New System.Drawing.Point(4, 26)
        Me._DriverTabPage.Name = "_DriverTabPage"
        Me._DriverTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._DriverTabPage.Size = New System.Drawing.Size(409, 422)
        Me._DriverTabPage.TabIndex = 0
        Me._DriverTabPage.Text = "Driver"
        Me._DriverTabPage.UseVisualStyleBackColor = True
        '
        '_ConfigureInterfaceTabLayout
        '
        Me._ConfigureInterfaceTabLayout.ColumnCount = 3
        Me._ConfigureInterfaceTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConfigureInterfaceTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ConfigureInterfaceTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConfigureInterfaceTabLayout.Controls.Add(Me._ConfigureGroupBox, 1, 1)
        Me._ConfigureInterfaceTabLayout.Location = New System.Drawing.Point(3, 3)
        Me._ConfigureInterfaceTabLayout.Name = "_ConfigureInterfaceTabLayout"
        Me._ConfigureInterfaceTabLayout.RowCount = 3
        Me._ConfigureInterfaceTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConfigureInterfaceTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ConfigureInterfaceTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConfigureInterfaceTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ConfigureInterfaceTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ConfigureInterfaceTabLayout.Size = New System.Drawing.Size(403, 416)
        Me._ConfigureInterfaceTabLayout.TabIndex = 8
        '
        '_ConfigureGroupBox
        '
        Me._ConfigureGroupBox.Controls.Add(Me._EndTestDelayNumericLabel)
        Me._ConfigureGroupBox.Controls.Add(Me._EndTestDelayNumeric)
        Me._ConfigureGroupBox.Controls.Add(Me._ConfigureHandlerInterfaceButton)
        Me._ConfigureGroupBox.Controls.Add(Me._DigitalLogicLabel)
        Me._ConfigureGroupBox.Controls.Add(Me._ActiveHighLogicRadioButton)
        Me._ConfigureGroupBox.Controls.Add(Me._ActiveLowLogicRadioButton)
        Me._ConfigureGroupBox.Controls.Add(Me._BinPortMaskTextBoxLabel)
        Me._ConfigureGroupBox.Controls.Add(Me._StartTestPinNumberNumericLabel)
        Me._ConfigureGroupBox.Controls.Add(Me._BinPortMaskTextBox)
        Me._ConfigureGroupBox.Controls.Add(Me._EndTestPinNumberNumericLabel)
        Me._ConfigureGroupBox.Controls.Add(Me._EndTestPinNumberNumeric)
        Me._ConfigureGroupBox.Controls.Add(Me._StartTestPinNumberNumeric)
        Me._ConfigureGroupBox.Location = New System.Drawing.Point(57, 85)
        Me._ConfigureGroupBox.Name = "_ConfigureGroupBox"
        Me._ConfigureGroupBox.Size = New System.Drawing.Size(289, 246)
        Me._ConfigureGroupBox.TabIndex = 2
        Me._ConfigureGroupBox.TabStop = False
        Me._ConfigureGroupBox.Text = "Configure Interface"
        '
        '_EndTestDelayNumericLabel
        '
        Me._EndTestDelayNumericLabel.AutoSize = True
        Me._EndTestDelayNumericLabel.Location = New System.Drawing.Point(33, 160)
        Me._EndTestDelayNumericLabel.Name = "_EndTestDelayNumericLabel"
        Me._EndTestDelayNumericLabel.Size = New System.Drawing.Size(117, 17)
        Me._EndTestDelayNumericLabel.TabIndex = 9
        Me._EndTestDelayNumericLabel.Text = "End test delay, ms:"
        Me._ToolTip.SetToolTip(Me._EndTestDelayNumericLabel, "Time from setting the bin to turning on the end of test signal.")
        '
        '_EndTestDelayNumeric
        '
        Me._EndTestDelayNumeric.Location = New System.Drawing.Point(152, 156)
        Me._EndTestDelayNumeric.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me._EndTestDelayNumeric.Name = "_EndTestDelayNumeric"
        Me._EndTestDelayNumeric.Size = New System.Drawing.Size(77, 25)
        Me._EndTestDelayNumeric.TabIndex = 10
        Me._EndTestDelayNumeric.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        '_ConfigureHandlerInterfaceButton
        '
        Me._ConfigureHandlerInterfaceButton.Location = New System.Drawing.Point(206, 201)
        Me._ConfigureHandlerInterfaceButton.Name = "_ConfigureHandlerInterfaceButton"
        Me._ConfigureHandlerInterfaceButton.Size = New System.Drawing.Size(61, 30)
        Me._ConfigureHandlerInterfaceButton.TabIndex = 12
        Me._ConfigureHandlerInterfaceButton.Text = "Apply"
        Me._ToolTip.SetToolTip(Me._ConfigureHandlerInterfaceButton, "Applies the settings to configure the handler interface")
        Me._ConfigureHandlerInterfaceButton.UseVisualStyleBackColor = True
        '
        '_DigitalLogicLabel
        '
        Me._DigitalLogicLabel.AutoSize = True
        Me._DigitalLogicLabel.Location = New System.Drawing.Point(29, 128)
        Me._DigitalLogicLabel.Name = "_DigitalLogicLabel"
        Me._DigitalLogicLabel.Size = New System.Drawing.Size(121, 17)
        Me._DigitalLogicLabel.TabIndex = 6
        Me._DigitalLogicLabel.Text = "Digital Logic: Active"
        Me._DigitalLogicLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ActiveHighLogicRadioButton
        '
        Me._ActiveHighLogicRadioButton.AutoSize = True
        Me._ActiveHighLogicRadioButton.Location = New System.Drawing.Point(204, 126)
        Me._ActiveHighLogicRadioButton.Name = "_ActiveHighLogicRadioButton"
        Me._ActiveHighLogicRadioButton.Size = New System.Drawing.Size(53, 21)
        Me._ActiveHighLogicRadioButton.TabIndex = 8
        Me._ActiveHighLogicRadioButton.TabStop = True
        Me._ActiveHighLogicRadioButton.Text = "High"
        Me._ToolTip.SetToolTip(Me._ActiveHighLogicRadioButton, "Active High Logic: Logical 1 = 'High'")
        Me._ActiveHighLogicRadioButton.UseVisualStyleBackColor = True
        '
        '_ActiveLowLogicRadioButton
        '
        Me._ActiveLowLogicRadioButton.AutoSize = True
        Me._ActiveLowLogicRadioButton.Checked = True
        Me._ActiveLowLogicRadioButton.Location = New System.Drawing.Point(155, 126)
        Me._ActiveLowLogicRadioButton.Name = "_ActiveLowLogicRadioButton"
        Me._ActiveLowLogicRadioButton.Size = New System.Drawing.Size(49, 21)
        Me._ActiveLowLogicRadioButton.TabIndex = 7
        Me._ActiveLowLogicRadioButton.TabStop = True
        Me._ActiveLowLogicRadioButton.Text = "Low"
        Me._ToolTip.SetToolTip(Me._ActiveLowLogicRadioButton, "Active low logic: Logic 1 = 'Low'")
        Me._ActiveLowLogicRadioButton.UseVisualStyleBackColor = True
        '
        '_BinPortMaskTextBoxLabel
        '
        Me._BinPortMaskTextBoxLabel.AutoSize = True
        Me._BinPortMaskTextBoxLabel.Location = New System.Drawing.Point(20, 97)
        Me._BinPortMaskTextBoxLabel.Name = "_BinPortMaskTextBoxLabel"
        Me._BinPortMaskTextBoxLabel.Size = New System.Drawing.Size(130, 17)
        Me._BinPortMaskTextBoxLabel.TabIndex = 4
        Me._BinPortMaskTextBoxLabel.Text = "Bin Port Binary Mask:"
        Me._BinPortMaskTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_StartTestPinNumberNumericLabel
        '
        Me._StartTestPinNumberNumericLabel.AutoSize = True
        Me._StartTestPinNumberNumericLabel.Location = New System.Drawing.Point(12, 31)
        Me._StartTestPinNumberNumericLabel.Name = "_StartTestPinNumberNumericLabel"
        Me._StartTestPinNumberNumericLabel.Size = New System.Drawing.Size(138, 17)
        Me._StartTestPinNumberNumericLabel.TabIndex = 0
        Me._StartTestPinNumberNumericLabel.Text = "Start Test Pin Number:"
        Me._StartTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_BinPortMaskTextBox
        '
        Me._BinPortMaskTextBox.Location = New System.Drawing.Point(152, 93)
        Me._BinPortMaskTextBox.Name = "_BinPortMaskTextBox"
        Me._BinPortMaskTextBox.Size = New System.Drawing.Size(115, 25)
        Me._BinPortMaskTextBox.TabIndex = 5
        Me._BinPortMaskTextBox.Text = "1100"
        Me._ToolTip.SetToolTip(Me._BinPortMaskTextBox, "Bin port binary mask")
        '
        '_EndTestPinNumberNumericLabel
        '
        Me._EndTestPinNumberNumericLabel.AutoSize = True
        Me._EndTestPinNumberNumericLabel.Location = New System.Drawing.Point(17, 64)
        Me._EndTestPinNumberNumericLabel.Name = "_EndTestPinNumberNumericLabel"
        Me._EndTestPinNumberNumericLabel.Size = New System.Drawing.Size(133, 17)
        Me._EndTestPinNumberNumericLabel.TabIndex = 2
        Me._EndTestPinNumberNumericLabel.Text = "End Test Pin Number:"
        Me._EndTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EndTestPinNumberNumeric
        '
        Me._EndTestPinNumberNumeric.Location = New System.Drawing.Point(153, 60)
        Me._EndTestPinNumberNumeric.Maximum = New Decimal(New Integer() {47, 0, 0, 0})
        Me._EndTestPinNumberNumeric.Name = "_EndTestPinNumberNumeric"
        Me._EndTestPinNumberNumeric.Size = New System.Drawing.Size(41, 25)
        Me._EndTestPinNumberNumeric.TabIndex = 3
        Me._ToolTip.SetToolTip(Me._EndTestPinNumberNumeric, "End test pin number")
        Me._EndTestPinNumberNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_StartTestPinNumberNumeric
        '
        Me._StartTestPinNumberNumeric.Location = New System.Drawing.Point(153, 27)
        Me._StartTestPinNumberNumeric.Maximum = New Decimal(New Integer() {47, 0, 0, 0})
        Me._StartTestPinNumberNumeric.Name = "_StartTestPinNumberNumeric"
        Me._StartTestPinNumberNumeric.Size = New System.Drawing.Size(41, 25)
        Me._StartTestPinNumberNumeric.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._StartTestPinNumberNumeric, "Start test pin number")
        '
        '_EmulatorTabPage
        '
        Me._EmulatorTabPage.Controls.Add(Me._EmulatorLayout)
        Me._EmulatorTabPage.Location = New System.Drawing.Point(4, 26)
        Me._EmulatorTabPage.Name = "_EmulatorTabPage"
        Me._EmulatorTabPage.Size = New System.Drawing.Size(409, 422)
        Me._EmulatorTabPage.TabIndex = 3
        Me._EmulatorTabPage.Text = "Emulator"
        Me._EmulatorTabPage.UseVisualStyleBackColor = True
        '
        '_EmulatorLayout
        '
        Me._EmulatorLayout.ColumnCount = 3
        Me._EmulatorLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._EmulatorLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._EmulatorLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._EmulatorLayout.Controls.Add(Me._ConfigureEmulatorGroupBox, 1, 1)
        Me._EmulatorLayout.Location = New System.Drawing.Point(3, 3)
        Me._EmulatorLayout.Name = "_EmulatorLayout"
        Me._EmulatorLayout.RowCount = 3
        Me._EmulatorLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._EmulatorLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._EmulatorLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._EmulatorLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._EmulatorLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._EmulatorLayout.Size = New System.Drawing.Size(403, 416)
        Me._EmulatorLayout.TabIndex = 9
        '
        '_ConfigureEmulatorGroupBox
        '
        Me._ConfigureEmulatorGroupBox.Controls.Add(Me._ConfigureEmulatorButton)
        Me._ConfigureEmulatorGroupBox.Controls.Add(Me._EmulatorBinPortMaskTextBoxLabel)
        Me._ConfigureEmulatorGroupBox.Controls.Add(Me._EmulatorStartTestPinNumberNumericLabel)
        Me._ConfigureEmulatorGroupBox.Controls.Add(Me._EmulatorBinPortMaskTextBox)
        Me._ConfigureEmulatorGroupBox.Controls.Add(Me._EmulatorEndTestPinNumberNumericLabel)
        Me._ConfigureEmulatorGroupBox.Controls.Add(Me._EmulatorEndTestPinNumberNumeric)
        Me._ConfigureEmulatorGroupBox.Controls.Add(Me._EmulatorStartTestPinNumberNumeric)
        Me._ConfigureEmulatorGroupBox.Location = New System.Drawing.Point(58, 122)
        Me._ConfigureEmulatorGroupBox.Name = "_ConfigureEmulatorGroupBox"
        Me._ConfigureEmulatorGroupBox.Size = New System.Drawing.Size(286, 171)
        Me._ConfigureEmulatorGroupBox.TabIndex = 2
        Me._ConfigureEmulatorGroupBox.TabStop = False
        Me._ConfigureEmulatorGroupBox.Text = "Configure Emulator"
        '
        '_ConfigureEmulatorButton
        '
        Me._ConfigureEmulatorButton.Location = New System.Drawing.Point(208, 131)
        Me._ConfigureEmulatorButton.Name = "_ConfigureEmulatorButton"
        Me._ConfigureEmulatorButton.Size = New System.Drawing.Size(61, 30)
        Me._ConfigureEmulatorButton.TabIndex = 6
        Me._ConfigureEmulatorButton.Text = "Apply"
        Me._ToolTip.SetToolTip(Me._ConfigureEmulatorButton, "Applies the settings to configure the handler emulator")
        Me._ConfigureEmulatorButton.UseVisualStyleBackColor = True
        '
        '_EmulatorBinPortMaskTextBoxLabel
        '
        Me._EmulatorBinPortMaskTextBoxLabel.AutoSize = True
        Me._EmulatorBinPortMaskTextBoxLabel.Location = New System.Drawing.Point(22, 97)
        Me._EmulatorBinPortMaskTextBoxLabel.Name = "_EmulatorBinPortMaskTextBoxLabel"
        Me._EmulatorBinPortMaskTextBoxLabel.Size = New System.Drawing.Size(130, 17)
        Me._EmulatorBinPortMaskTextBoxLabel.TabIndex = 4
        Me._EmulatorBinPortMaskTextBoxLabel.Text = "Bin Port Binary Mask:"
        Me._EmulatorBinPortMaskTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EmulatorStartTestPinNumberNumericLabel
        '
        Me._EmulatorStartTestPinNumberNumericLabel.AutoSize = True
        Me._EmulatorStartTestPinNumberNumericLabel.Location = New System.Drawing.Point(14, 31)
        Me._EmulatorStartTestPinNumberNumericLabel.Name = "_EmulatorStartTestPinNumberNumericLabel"
        Me._EmulatorStartTestPinNumberNumericLabel.Size = New System.Drawing.Size(138, 17)
        Me._EmulatorStartTestPinNumberNumericLabel.TabIndex = 0
        Me._EmulatorStartTestPinNumberNumericLabel.Text = "Start Test Pin Number:"
        Me._EmulatorStartTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EmulatorBinPortMaskTextBox
        '
        Me._EmulatorBinPortMaskTextBox.Location = New System.Drawing.Point(154, 93)
        Me._EmulatorBinPortMaskTextBox.Name = "_EmulatorBinPortMaskTextBox"
        Me._EmulatorBinPortMaskTextBox.Size = New System.Drawing.Size(115, 25)
        Me._EmulatorBinPortMaskTextBox.TabIndex = 5
        Me._EmulatorBinPortMaskTextBox.Text = "11000000"
        Me._ToolTip.SetToolTip(Me._EmulatorBinPortMaskTextBox, "Emulator bin port binary mask")
        '
        '_EmulatorEndTestPinNumberNumericLabel
        '
        Me._EmulatorEndTestPinNumberNumericLabel.AutoSize = True
        Me._EmulatorEndTestPinNumberNumericLabel.Location = New System.Drawing.Point(19, 64)
        Me._EmulatorEndTestPinNumberNumericLabel.Name = "_EmulatorEndTestPinNumberNumericLabel"
        Me._EmulatorEndTestPinNumberNumericLabel.Size = New System.Drawing.Size(133, 17)
        Me._EmulatorEndTestPinNumberNumericLabel.TabIndex = 2
        Me._EmulatorEndTestPinNumberNumericLabel.Text = "End Test Pin Number:"
        Me._EmulatorEndTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EmulatorEndTestPinNumberNumeric
        '
        Me._EmulatorEndTestPinNumberNumeric.Location = New System.Drawing.Point(155, 60)
        Me._EmulatorEndTestPinNumberNumeric.Maximum = New Decimal(New Integer() {47, 0, 0, 0})
        Me._EmulatorEndTestPinNumberNumeric.Name = "_EmulatorEndTestPinNumberNumeric"
        Me._EmulatorEndTestPinNumberNumeric.Size = New System.Drawing.Size(41, 25)
        Me._EmulatorEndTestPinNumberNumeric.TabIndex = 3
        Me._ToolTip.SetToolTip(Me._EmulatorEndTestPinNumberNumeric, "Emulator end test pin number")
        Me._EmulatorEndTestPinNumberNumeric.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        '_EmulatorStartTestPinNumberNumeric
        '
        Me._EmulatorStartTestPinNumberNumeric.Location = New System.Drawing.Point(155, 27)
        Me._EmulatorStartTestPinNumberNumeric.Maximum = New Decimal(New Integer() {47, 0, 0, 0})
        Me._EmulatorStartTestPinNumberNumeric.Name = "_EmulatorStartTestPinNumberNumeric"
        Me._EmulatorStartTestPinNumberNumeric.Size = New System.Drawing.Size(41, 25)
        Me._EmulatorStartTestPinNumberNumeric.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._EmulatorStartTestPinNumberNumeric, "Emulator start test pin number")
        Me._EmulatorStartTestPinNumberNumeric.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        '_PlayTabPage
        '
        Me._PlayTabPage.Controls.Add(Me._PlayTabPageLayout)
        Me._PlayTabPage.Location = New System.Drawing.Point(4, 26)
        Me._PlayTabPage.Name = "_PlayTabPage"
        Me._PlayTabPage.Size = New System.Drawing.Size(409, 422)
        Me._PlayTabPage.TabIndex = 1
        Me._PlayTabPage.Text = "Play"
        Me._PlayTabPage.UseVisualStyleBackColor = True
        '
        '_PlayTabPageLayout
        '
        Me._PlayTabPageLayout.ColumnCount = 3
        Me._PlayTabPageLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PlayTabPageLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._PlayTabPageLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PlayTabPageLayout.Controls.Add(Me._HandlerInterfacePlayGroupBox, 1, 1)
        Me._PlayTabPageLayout.Controls.Add(Me._HandlerEmulatorPlayGroupBox, 1, 2)
        Me._PlayTabPageLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._PlayTabPageLayout.Location = New System.Drawing.Point(0, 0)
        Me._PlayTabPageLayout.Name = "_PlayTabPageLayout"
        Me._PlayTabPageLayout.RowCount = 4
        Me._PlayTabPageLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PlayTabPageLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._PlayTabPageLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._PlayTabPageLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PlayTabPageLayout.Size = New System.Drawing.Size(409, 422)
        Me._PlayTabPageLayout.TabIndex = 0
        '
        '_HandlerInterfacePlayGroupBox
        '
        Me._HandlerInterfacePlayGroupBox.Controls.Add(Me._FailBinValueNumeric)
        Me._HandlerInterfacePlayGroupBox.Controls.Add(Me._OutputFailBinButton)
        Me._HandlerInterfacePlayGroupBox.Controls.Add(Me._PassBinValueNumeric)
        Me._HandlerInterfacePlayGroupBox.Controls.Add(Me._OutputPassBinButton)
        Me._HandlerInterfacePlayGroupBox.Controls.Add(Me._ClearStartButton)
        Me._HandlerInterfacePlayGroupBox.Location = New System.Drawing.Point(108, 95)
        Me._HandlerInterfacePlayGroupBox.Name = "_HandlerInterfacePlayGroupBox"
        Me._HandlerInterfacePlayGroupBox.Size = New System.Drawing.Size(192, 147)
        Me._HandlerInterfacePlayGroupBox.TabIndex = 0
        Me._HandlerInterfacePlayGroupBox.TabStop = False
        Me._HandlerInterfacePlayGroupBox.Text = "Handler Interface"
        '
        '_FailBinValueNumeric
        '
        Me._FailBinValueNumeric.Location = New System.Drawing.Point(135, 110)
        Me._FailBinValueNumeric.Name = "_FailBinValueNumeric"
        Me._FailBinValueNumeric.Size = New System.Drawing.Size(40, 25)
        Me._FailBinValueNumeric.TabIndex = 4
        Me._ToolTip.SetToolTip(Me._FailBinValueNumeric, "Failed bin value")
        Me._FailBinValueNumeric.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        '_OutputFailBinButton
        '
        Me._OutputFailBinButton.Location = New System.Drawing.Point(18, 106)
        Me._OutputFailBinButton.Name = "_OutputFailBinButton"
        Me._OutputFailBinButton.Size = New System.Drawing.Size(111, 33)
        Me._OutputFailBinButton.TabIndex = 3
        Me._OutputFailBinButton.Text = "Output Fail Bin"
        Me._ToolTip.SetToolTip(Me._OutputFailBinButton, "Sends the failed end of test sequence")
        Me._OutputFailBinButton.UseVisualStyleBackColor = True
        '
        '_PassBinValueNumeric
        '
        Me._PassBinValueNumeric.Location = New System.Drawing.Point(135, 70)
        Me._PassBinValueNumeric.Name = "_PassBinValueNumeric"
        Me._PassBinValueNumeric.Size = New System.Drawing.Size(40, 25)
        Me._PassBinValueNumeric.TabIndex = 2
        Me._ToolTip.SetToolTip(Me._PassBinValueNumeric, "Pass bin value")
        Me._PassBinValueNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_OutputPassBinButton
        '
        Me._OutputPassBinButton.Location = New System.Drawing.Point(18, 66)
        Me._OutputPassBinButton.Name = "_OutputPassBinButton"
        Me._OutputPassBinButton.Size = New System.Drawing.Size(111, 33)
        Me._OutputPassBinButton.TabIndex = 1
        Me._OutputPassBinButton.Text = "Output Pass Bin"
        Me._ToolTip.SetToolTip(Me._OutputPassBinButton, "Sends the passed end of test sequence")
        Me._OutputPassBinButton.UseVisualStyleBackColor = True
        '
        '_ClearStartButton
        '
        Me._ClearStartButton.Location = New System.Drawing.Point(17, 25)
        Me._ClearStartButton.Name = "_ClearStartButton"
        Me._ClearStartButton.Size = New System.Drawing.Size(156, 34)
        Me._ClearStartButton.TabIndex = 0
        Me._ClearStartButton.Text = "Clear to Start"
        Me._ToolTip.SetToolTip(Me._ClearStartButton, "Allows handler to send start test.")
        Me._ClearStartButton.UseVisualStyleBackColor = True
        '
        '_HandlerEmulatorPlayGroupBox
        '
        Me._HandlerEmulatorPlayGroupBox.Controls.Add(Me._StartTestButton)
        Me._HandlerEmulatorPlayGroupBox.Location = New System.Drawing.Point(108, 248)
        Me._HandlerEmulatorPlayGroupBox.Name = "_HandlerEmulatorPlayGroupBox"
        Me._HandlerEmulatorPlayGroupBox.Size = New System.Drawing.Size(192, 78)
        Me._HandlerEmulatorPlayGroupBox.TabIndex = 1
        Me._HandlerEmulatorPlayGroupBox.TabStop = False
        Me._HandlerEmulatorPlayGroupBox.Text = "Handler Emulator"
        '
        '_StartTestButton
        '
        Me._StartTestButton.Location = New System.Drawing.Point(18, 29)
        Me._StartTestButton.Name = "_StartTestButton"
        Me._StartTestButton.Size = New System.Drawing.Size(156, 34)
        Me._StartTestButton.TabIndex = 0
        Me._StartTestButton.Text = "Start Test"
        Me._ToolTip.SetToolTip(Me._StartTestButton, "Sends start test from handler to interface")
        Me._StartTestButton.UseVisualStyleBackColor = True
        '
        '_EventLogTabPage
        '
        Me._EventLogTabPage.Controls.Add(Me._EventLogTextBox)
        Me._EventLogTabPage.Location = New System.Drawing.Point(4, 26)
        Me._EventLogTabPage.Name = "_EventLogTabPage"
        Me._EventLogTabPage.Size = New System.Drawing.Size(409, 422)
        Me._EventLogTabPage.TabIndex = 2
        Me._EventLogTabPage.Text = "Events"
        Me._EventLogTabPage.UseVisualStyleBackColor = True
        '
        '_EventLogTextBox
        '
        Me._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EventLogTextBox.Location = New System.Drawing.Point(0, 0)
        Me._EventLogTextBox.Multiline = True
        Me._EventLogTextBox.Name = "_EventLogTextBox"
        Me._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._EventLogTextBox.Size = New System.Drawing.Size(409, 422)
        Me._EventLogTextBox.TabIndex = 1
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_ToolStripContainer
        '
        '
        '_ToolStripContainer.BottomToolStripPanel
        '
        Me._ToolStripContainer.BottomToolStripPanel.Controls.Add(Me._BottomToolStrip)
        '
        '_ToolStripContainer.ContentPanel
        '
        Me._ToolStripContainer.ContentPanel.Controls.Add(Me._Tabs)
        Me._ToolStripContainer.ContentPanel.Size = New System.Drawing.Size(417, 452)
        Me._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ToolStripContainer.Location = New System.Drawing.Point(0, 0)
        Me._ToolStripContainer.Name = "_ToolStripContainer"
        Me._ToolStripContainer.Size = New System.Drawing.Size(417, 506)
        Me._ToolStripContainer.TabIndex = 0
        Me._ToolStripContainer.Text = "ToolStripContainer1"
        '
        '_ToolStripContainer.TopToolStripPanel
        '
        Me._ToolStripContainer.TopToolStripPanel.Controls.Add(Me._TopToolStrip)
        '
        '_TopToolStrip
        '
        Me._TopToolStrip.AutoSize = False
        Me._TopToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TopToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._HandlerStateLabel, Me._EmulatorStateLabel, Me._HandlerInterfaceBinValueLabel, Me._EmulatorBinValueLabel, Me._SotLabel, Me._EotLabel})
        Me._TopToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._TopToolStrip.Name = "_TopToolStrip"
        Me._TopToolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me._TopToolStrip.Size = New System.Drawing.Size(417, 25)
        Me._TopToolStrip.Stretch = True
        Me._TopToolStrip.TabIndex = 0
        '
        '_HandlerStateLabel
        '
        Me._HandlerStateLabel.BackColor = System.Drawing.Color.Black
        Me._HandlerStateLabel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me._HandlerStateLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._HandlerStateLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._HandlerStateLabel.ForeColor = System.Drawing.Color.Aqua
        Me._HandlerStateLabel.Name = "_HandlerStateLabel"
        Me._HandlerStateLabel.Size = New System.Drawing.Size(89, 22)
        Me._HandlerStateLabel.Text = "interface state"
        Me._HandlerStateLabel.ToolTipText = "Handler interface state"
        '
        '_EmulatorStateLabel
        '
        Me._EmulatorStateLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._EmulatorStateLabel.BackColor = System.Drawing.Color.Black
        Me._EmulatorStateLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EmulatorStateLabel.ForeColor = System.Drawing.Color.Aqua
        Me._EmulatorStateLabel.Name = "_EmulatorStateLabel"
        Me._EmulatorStateLabel.Size = New System.Drawing.Size(89, 22)
        Me._EmulatorStateLabel.Text = "emulator state"
        Me._EmulatorStateLabel.ToolTipText = "Handler Emulator State"
        '
        '_HandlerInterfaceBinValueLabel
        '
        Me._HandlerInterfaceBinValueLabel.BackColor = System.Drawing.Color.Black
        Me._HandlerInterfaceBinValueLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._HandlerInterfaceBinValueLabel.ForeColor = System.Drawing.Color.SpringGreen
        Me._HandlerInterfaceBinValueLabel.Margin = New System.Windows.Forms.Padding(1, 1, 0, 2)
        Me._HandlerInterfaceBinValueLabel.Name = "_HandlerInterfaceBinValueLabel"
        Me._HandlerInterfaceBinValueLabel.Size = New System.Drawing.Size(24, 22)
        Me._HandlerInterfaceBinValueLabel.Text = "bin"
        Me._HandlerInterfaceBinValueLabel.ToolTipText = "Bin value"
        '
        '_EmulatorBinValueLabel
        '
        Me._EmulatorBinValueLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._EmulatorBinValueLabel.BackColor = System.Drawing.Color.Black
        Me._EmulatorBinValueLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EmulatorBinValueLabel.ForeColor = System.Drawing.Color.SpringGreen
        Me._EmulatorBinValueLabel.Margin = New System.Windows.Forms.Padding(0, 1, 1, 2)
        Me._EmulatorBinValueLabel.Name = "_EmulatorBinValueLabel"
        Me._EmulatorBinValueLabel.Size = New System.Drawing.Size(24, 22)
        Me._EmulatorBinValueLabel.Text = "bin"
        Me._EmulatorBinValueLabel.ToolTipText = "Emulate bin value"
        '
        '_SotLabel
        '
        Me._SotLabel.BackColor = System.Drawing.Color.Transparent
        Me._SotLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me._SotLabel.Image = Global.isr.Diolan.Forms.My.Resources.Resources.User_invisible
        Me._SotLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SotLabel.Name = "_SotLabel"
        Me._SotLabel.Size = New System.Drawing.Size(28, 22)
        Me._SotLabel.Text = "SOT"
        Me._SotLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay
        Me._SotLabel.ToolTipText = "Start test pin logical status"
        '
        '_EotLabel
        '
        Me._EotLabel.BackColor = System.Drawing.Color.Transparent
        Me._EotLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow
        Me._EotLabel.Image = Global.isr.Diolan.Forms.My.Resources.Resources.User_invisible
        Me._EotLabel.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._EotLabel.Name = "_EotLabel"
        Me._EotLabel.Size = New System.Drawing.Size(28, 22)
        Me._EotLabel.Text = "EOT"
        Me._EotLabel.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay
        '
        'GpioHandlerPanel
        '
        Me.Controls.Add(Me._ToolStripContainer)
        Me.Name = "GpioHandlerPanel"
        Me.Size = New System.Drawing.Size(417, 506)
        Me._BottomToolStrip.ResumeLayout(False)
        Me._BottomToolStrip.PerformLayout()
        Me._Tabs.ResumeLayout(False)
        Me._DriverTabPage.ResumeLayout(False)
        Me._ConfigureInterfaceTabLayout.ResumeLayout(False)
        Me._ConfigureGroupBox.ResumeLayout(False)
        Me._ConfigureGroupBox.PerformLayout()
        CType(Me._EndTestDelayNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._EndTestPinNumberNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._StartTestPinNumberNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._EmulatorTabPage.ResumeLayout(False)
        Me._EmulatorLayout.ResumeLayout(False)
        Me._ConfigureEmulatorGroupBox.ResumeLayout(False)
        Me._ConfigureEmulatorGroupBox.PerformLayout()
        CType(Me._EmulatorEndTestPinNumberNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._EmulatorStartTestPinNumberNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._PlayTabPage.ResumeLayout(False)
        Me._PlayTabPageLayout.ResumeLayout(False)
        Me._HandlerInterfacePlayGroupBox.ResumeLayout(False)
        CType(Me._FailBinValueNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._PassBinValueNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._HandlerEmulatorPlayGroupBox.ResumeLayout(False)
        Me._EventLogTabPage.ResumeLayout(False)
        Me._EventLogTabPage.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ToolStripContainer.BottomToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.BottomToolStripPanel.PerformLayout()
        Me._ToolStripContainer.ContentPanel.ResumeLayout(False)
        Me._ToolStripContainer.TopToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.ResumeLayout(False)
        Me._ToolStripContainer.PerformLayout()
        Me._TopToolStrip.ResumeLayout(False)
        Me._TopToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _BottomToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _DeviceInfoTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _OpenDeviceModalityButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _DriverTabPage As System.Windows.Forms.TabPage
    Private WithEvents _ToolStripContainer As System.Windows.Forms.ToolStripContainer
    Private WithEvents _Tabs As ExtendedTabControl
    Private WithEvents _DevicesComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _SelectServerButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _ServerNameTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _DefaultServerMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ConnectServerButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _SelectDeviceSplitButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _ConfigureInterfaceTabLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _ConfigureGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DigitalLogicLabel As System.Windows.Forms.Label
    Private WithEvents _ActiveHighLogicRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _ActiveLowLogicRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents _BinPortMaskTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _StartTestPinNumberNumericLabel As System.Windows.Forms.Label
    Private WithEvents _BinPortMaskTextBox As System.Windows.Forms.TextBox
    Private WithEvents _EndTestPinNumberNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EndTestPinNumberNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _StartTestPinNumberNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _PlayTabPage As System.Windows.Forms.TabPage
    Private WithEvents _PlayTabPageLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _EventLogTabPage As System.Windows.Forms.TabPage
    Private WithEvents _EventLogTextBox As System.Windows.Forms.TextBox
    Private WithEvents _TabComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ConfigureHandlerInterfaceButton As System.Windows.Forms.Button
    Private WithEvents _EmulatorTabPage As System.Windows.Forms.TabPage
    Private WithEvents _TopToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _EmulatorLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _ConfigureEmulatorGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _ConfigureEmulatorButton As System.Windows.Forms.Button
    Private WithEvents _EmulatorBinPortMaskTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _EmulatorStartTestPinNumberNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EmulatorBinPortMaskTextBox As System.Windows.Forms.TextBox
    Private WithEvents _EmulatorEndTestPinNumberNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EmulatorEndTestPinNumberNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _EmulatorStartTestPinNumberNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _HandlerInterfacePlayGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _HandlerEmulatorPlayGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _ClearStartButton As System.Windows.Forms.Button
    Private WithEvents _StartTestButton As System.Windows.Forms.Button
    Private WithEvents _ResetButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _FailBinValueNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _OutputFailBinButton As System.Windows.Forms.Button
    Private WithEvents _PassBinValueNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _OutputPassBinButton As System.Windows.Forms.Button
    Private WithEvents _EndTestDelayNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EndTestDelayNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _SotLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _EotLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _HandlerStateLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _EmulatorBinValueLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _EmulatorStateLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _HandlerInterfaceBinValueLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _HandlerComboBox As Windows.Forms.ToolStripComboBox
End Class
