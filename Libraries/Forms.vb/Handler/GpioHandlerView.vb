Imports System.ComponentModel
Imports isr.Core
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Core.EnumExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Diolan.Gpio
Imports isr.Diolan.SubsystemExtensions

''' <summary> User interface for managing a GPIO Handler interface. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-06-01 </para>
''' </remarks>
Partial Public Class GpioHandlerView

#Region " CONSTRUCTION "

    ''' <summary> Executes the custom dispose action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> true to disposing. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnModalityClosed()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " MODALITY "

    ''' <summary> The modality. </summary>
    ''' <value> The modality. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Modality As DeviceModalities = DeviceModalities.Gpio

    ''' <summary> Executes the modality closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityClosed()
        If Me._Emulator IsNot Nothing Then
            Me._Emulator.Dispose()
            Me._Emulator = Nothing
        End If
        If Me._Driver IsNot Nothing Then
            Me._Driver.Dispose()
            Me._Driver = Nothing
        End If
        Me._Device = Nothing
        Me.OnModalityConnectionChanged()
    End Sub

    ''' <summary> Executes the modality closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnModalityClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            If Me.IsModalityOpen Then
                ' un-register event handlers for all pins
                For Each pin As Dln.Gpio.Pin In Me.Device.Gpio.Pins
                    RemoveHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                Next
            End If
        End If
    End Sub

    ''' <summary> Executes the modality opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityOpening()

        Me._Driver = Nothing
        Me._Device = Me._DeviceConnector.Device
        Me._ErrorProvider.Clear()

        ' Get port count
        If Me.Device.Gpio.Pins.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton,
                                             "Adapter '{0}' doesn't support GPIO interface.",
                                             Me.Device.Caption)
            Me._Device = Nothing
            Me._DeviceInfoTextBox.Text = "not supported"
        Else
            Me._DeviceInfoTextBox.Text = Me.Device.Caption

            'Set current context to run thread safe events in main form thread
            Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current

            ' Register event handler for all pins
            For Each pin As Dln.Gpio.Pin In Me.Device.Gpio.Pins
                AddHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
            Next

            Me._Driver = New GpioHandlerDriver()

        End If

    End Sub

    ''' <summary> Queries if a modality is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
    Private Function IsModalityOpen() As Boolean
        Return Me._Driver IsNot Nothing
    End Function

#End Region

#Region " EVENT LOG "

    ''' <summary> Handler, called when the condition met event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Condition met event information. </param>
    Private Sub ConditionMetEventHandler(ByVal sender As Object, ByVal e As Dln.Gpio.ConditionMetEventArgs)
        Me.AppendEvent(e)
    End Sub

    ''' <summary> Appends an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Condition met event information. </param>
    Private Sub AppendEvent(ByVal e As Dln.Gpio.ConditionMetEventArgs)
        If e IsNot Nothing Then
            Dim data As String = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}"
            ' This event is handled in main thread,
            ' so it is not needed to invoke when modifying form's controls.
            Me._EventLogTextBox.AppendText(data)
        End If

    End Sub

#End Region

#Region " DRIVER "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Driver As GpioHandlerDriver
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the driver. </summary>
    ''' <value> The driver. </value>
    <CLSCompliant(False)>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Driver As GpioHandlerDriver
        Get
            Return Me._Driver
        End Get
        Set(value As GpioHandlerDriver)
            Me._Driver = value
            If Me._Driver IsNot Nothing Then
                Me._StartTestPinNumberNumeric.Value = Me._Driver.StartTestPinNumber
                Me._EndTestPinNumberNumeric.Value = Me._Driver.EndTestPinNumber
                Me._ActiveLowLogicRadioButton.Checked = Me._Driver.ActiveLogic = ActiveLogic.ActiveLow
                Me.BinPortMask = Me._Driver.BinMask
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the handler mask. </summary>
    ''' <value> The entered mask. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private Property BinPortMask As Long
        Get
            Return Convert.ToInt32(Me._BinPortMaskTextBox.Text, 2)
        End Get
        Set(value As Long)
            Me._BinPortMaskTextBox.Text = Convert.ToString(value, 2)
        End Set
    End Property

    ''' <summary> Bin port mask text box validating. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BinPortMaskTextBox_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _BinPortMaskTextBox.Validating
        Try
            Me._ErrorProvider.Clear()
            If Me.BinPortMask() <= 0 Then
                Me._ErrorProvider.Annunciate(sender, "Value must be positive")
                e.Cancel = True
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
            e.Cancel = True
        End Try

    End Sub

    ''' <summary> Handles the Configure Emulator button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConfigureHandlerInterfaceButton_Click(sender As Object, e As System.EventArgs) Handles _ConfigureHandlerInterfaceButton.Click
        Dim args As New ActionEventArgs
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                If Not Me.IsDeviceModalityOpen Then
                    args.RegisterFailure("Device not open")
                ElseIf Me.Device.Gpio Is Nothing Then
                    args.RegisterFailure($"Gpio not supported on {Me.Device.Caption}")
                ElseIf Me.Device.Gpio.Pins Is Nothing Then

                    args.RegisterFailure($"Gpio not supported on {Me.Device.Caption}")
                Else
                    If Me._Driver Is Nothing Then
                        Me._Driver = New GpioHandlerDriver()
                    End If
                    Me._Driver.StartTestPinNumber = CInt(Me._StartTestPinNumberNumeric.Value)
                    Me._Driver.EndTestPinNumber = CInt(Me._EndTestPinNumberNumeric.Value)
                    Me._Driver.ActiveLogic = If(Me._ActiveLowLogicRadioButton.Checked, ActiveLogic.ActiveLow, ActiveLogic.ActiveHigh)
                    Me._Driver.BinEndTestOnsetDelay = TimeSpan.FromMilliseconds(Me._EndTestDelayNumeric.Value)
                    Me._Driver.BinMask = Me.BinPortMask
                    If Me._Driver.TryValidate(Me.Device.Gpio.Pins, args) Then
                        Me._Driver.Configure(Me.Device.Gpio.Pins)
                    Else
                    End If
                End If
            Else
                args.RegisterFailure("Device not open")
            End If
        Catch ex As Exception
            args.RegisterError(ex.ToFullBlownString)
        Finally
            If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
        End Try
    End Sub

    ''' <summary> Updates the sot indicator described by sender. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    Private Sub UpdateSotIndicator(ByVal sender As GpioHandlerDriverBase)
        If sender IsNot Nothing Then
            Me._SotLabel.Image = If(sender.StartTestLogicalValue, My.Resources.user_available, My.Resources.user_invisible)
        End If
    End Sub

    ''' <summary> Eot label click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EotLabel_Click(sender As System.Object, e As System.EventArgs) Handles _EotLabel.Click
        Me.UpdateEotIndicator(Me._Driver)
    End Sub

    ''' <summary> Updates the eot indicator described by sender. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    Private Sub UpdateEotIndicator(ByVal sender As GpioHandlerDriverBase)
        If sender IsNot Nothing Then
            Me._EotLabel.Image = If(sender.EndTestLogicalValue, My.Resources.user_available, My.Resources.user_invisible)
        End If
    End Sub

    ''' <summary> Raises the server connector property changed event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnPropertyChanged(ByVal sender As GpioHandlerDriver, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Gpio.GpioHandlerDriverBase.State)
                Me._HandlerStateLabel.Text = sender.State.Description
                Select Case sender.State
                    Case HandlerState.StartTestReceived
                    Case Else
                End Select
            Case NameOf(Gpio.GpioHandlerDriverBase.BinValue)
                Me._HandlerInterfaceBinValueLabel.Text = sender.BinValueCaption("...")
            Case NameOf(Gpio.GpioHandlerDriverBase.StartTestLogicalValue)
                Me.UpdateSotIndicator(sender)
            Case NameOf(Gpio.GpioHandlerDriverBase.EndTestLogicalValue)
                Me.UpdateEotIndicator(sender)
            Case NameOf(Gpio.GpioHandlerDriverBase.BinMask)
                Me.BinPortMask = sender.BinMask
            Case NameOf(Gpio.GpioHandlerDriverBase.BinEndTestOnsetDelay)
                Me._EndTestDelayNumeric.Value = CDec(Me._Driver.BinEndTestOnsetDelay.TotalMilliseconds)
            Case NameOf(Gpio.GpioHandlerDriverBase.StartTestPinNumber)
                Me._StartTestPinNumberNumeric.Value = sender.StartTestPinNumber
            Case NameOf(Gpio.GpioHandlerDriverBase.EndTestPinNumber)
                Me._EndTestPinNumberNumeric.Value = sender.StartTestPinNumber
            Case NameOf(Gpio.GpioHandlerDriverBase.ActiveLogic)
                Me._ActiveLowLogicRadioButton.Checked = sender.ActiveLogic = ActiveLogic.ActiveLow
        End Select
    End Sub

    ''' <summary> Driver property changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Driver_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _Driver.PropertyChanged
        Dim details As String = String.Empty
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.Driver_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, GpioHandlerDriver), e?.PropertyName)
            End If

        Catch ex As Exception
            details = $"Exception handling driver {e?.PropertyName} property change;.  {ex.ToFullBlownString}"
        Finally
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton, details)
        End Try
    End Sub


#End Region

#Region " EMULATOR "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Emulator As GpioHandlerEmulator
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the handler. </summary>
    ''' <value> The handler. </value>
    <CLSCompliant(False)>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Emulator As GpioHandlerEmulator
        Get
            Return Me._Emulator
        End Get
        Set(value As GpioHandlerEmulator)
            Me._Emulator = value
            If Me.Emulator IsNot Nothing Then
                Me._EmulatorStartTestPinNumberNumeric.Value = Me.Emulator.StartTestPinNumber
                Me._EmulatorEndTestPinNumberNumeric.Value = Me.Emulator.EndTestPinNumber
                Me.EmulatorBinMask = Me._Driver.BinMask
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the entered bin mask. </summary>
    ''' <value> The entered mask. </value>
    Private Property EmulatorBinMask As Long
        Get
            Return Convert.ToInt32(Me._EmulatorBinPortMaskTextBox.Text, 2)
        End Get
        Set(value As Long)
            Me._EmulatorBinPortMaskTextBox.Text = Convert.ToString(value, 2)
        End Set
    End Property

    ''' <summary> Emulator Bin port mask text box validating. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub EmulatorBinPortMaskTextBox_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _EmulatorBinPortMaskTextBox.Validating
        Try
            Me._ErrorProvider.Clear()
            If Me.EmulatorBinMask() <= 0 Then
                Me._ErrorProvider.Annunciate(sender, "Value must be positive")
                e.Cancel = True
            End If

        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
            e.Cancel = True
        End Try
    End Sub

    ''' <summary> Applies the button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConfigureEmulatorButton_Click(sender As Object, e As System.EventArgs) Handles _ConfigureEmulatorButton.Click
        Dim args As New ActionEventArgs
        Try
            Me._ErrorProvider.Clear()
            If sender Is Nothing Then
                args.RegisterFailure("Sender is empty")

            ElseIf Me.IsDeviceModalityOpen Then
                If Not Me.IsDeviceModalityOpen Then
                    args.RegisterFailure("Device not open")
                ElseIf Me.Device.Gpio Is Nothing Then
                    args.RegisterFailure($"Gpio not supported on {Me.Device.Caption}")
                ElseIf Me.Device.Gpio.Pins Is Nothing Then
                    args.RegisterFailure($"Gpio not supported on {Me.Device.Caption}")
                Else
                    If Me._Emulator Is Nothing Then
                        Me._Emulator = New GpioHandlerEmulator()
                    End If
                    Me._Emulator.StartTestPinNumber = CInt(Me._EmulatorStartTestPinNumberNumeric.Value)
                    Me._Emulator.EndTestPinNumber = CInt(Me._EmulatorEndTestPinNumberNumeric.Value)
                    Me._Emulator.ActiveLogic = If(Me._ActiveLowLogicRadioButton.Checked, ActiveLogic.ActiveLow, ActiveLogic.ActiveHigh)
                    Me._Emulator.BinMask = Me.EmulatorBinMask
                    If Me._Emulator.TryValidate(Me.Device.Gpio.Pins, args) Then
                        Me._Emulator.Configure(Me.Device.Gpio.Pins)
                    Else
                    End If
                End If
            Else
                args.RegisterFailure("Device not open")
            End If
        Catch ex As Exception
            args.RegisterError(ex.ToFullBlownString)
        Finally
            If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
        End Try
    End Sub

    ''' <summary> Handles the emulator property changed event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As GpioHandlerEmulator, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Gpio.GpioHandlerDriverBase.State)
                Me._HandlerStateLabel.Text = sender.State.Description
                Select Case sender.State
                    Case HandlerState.StartTestReceived
                        Me._EmulatorStateLabel.Text = "e: Start test sent"
                    Case HandlerState.EndTestSent
                        Me._EmulatorStateLabel.Text = "e: End test received"
                    Case Else
                        Me._EmulatorStateLabel.Text = sender.State.Description
                End Select
            Case NameOf(Gpio.GpioHandlerDriverBase.BinValue)
                Me._EmulatorBinValueLabel.Text = sender.BinValueCaption("...")
            Case NameOf(Gpio.GpioHandlerDriverBase.StartTestLogicalValue)
            Case NameOf(Gpio.GpioHandlerDriverBase.EndTestLogicalValue)
            Case NameOf(Gpio.GpioHandlerDriverBase.BinMask)
                Me.EmulatorBinMask = sender.BinMask
            Case NameOf(Gpio.GpioHandlerDriverBase.BinEndTestOnsetDelay)
            Case NameOf(Gpio.GpioHandlerDriverBase.StartTestPinNumber)
                Me._EmulatorStartTestPinNumberNumeric.Value = sender.StartTestPinNumber
            Case NameOf(Gpio.GpioHandlerDriverBase.EndTestPinNumber)
                Me._EmulatorEndTestPinNumberNumeric.Value = sender.StartTestPinNumber
            Case NameOf(Gpio.GpioHandlerDriverBase.ActiveLogic)
                ' TO_DO: Must be the same as the driver.
        End Select
    End Sub

    ''' <summary> Emulator property changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Emulator_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _Emulator.PropertyChanged
        Dim details As String = String.Empty
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.Emulator_PropertyChanged), New Object() {sender, e})

            Else
                Me.OnPropertyChanged(TryCast(sender, GpioHandlerEmulator), e?.PropertyName)
            End If
        Catch ex As Exception
            details = $"Exception handling {NameOf(Gpio.GpioHandlerDriverEmulator.Emulator)}.{e?.PropertyName} change;. {ex.ToFullBlownString}"
        Finally
            If Not String.IsNullOrEmpty(details) Then Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton, details)
        End Try
    End Sub


#End Region

#Region " PLAY "

    ''' <summary> Clears the start button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearStartButton_Click(sender As System.Object, e As System.EventArgs) Handles _ClearStartButton.Click
        Dim details As String = String.Empty
        Try
            Me._ErrorProvider.Clear()

            If sender Is Nothing Then
                details = "Sender is empty"
            ElseIf Me._Driver Is Nothing Then
                details = "Handler interface not set"
            ElseIf Me._Driver.State = HandlerState.Idle OrElse
                       Me._Driver.State = HandlerState.EndTestAcknowledged Then
                Me._Driver.EnableStartTest()
            Else
                details = $"Invalid handler state @'{Me._Driver.State}'; state should be @'{HandlerState.Idle}'"
            End If
        Catch ex As Exception
            details = ex.ToString
        Finally
            Me._ErrorProvider.Annunciate(sender, details)
        End Try
    End Sub

    ''' <summary> Starts test button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StartTestButton_Click(sender As System.Object, e As System.EventArgs) Handles _StartTestButton.Click
        Dim details As String = String.Empty
        Try

            Me._ErrorProvider.Clear()
            If sender Is Nothing Then
                details = "Sender is empty"
            ElseIf Me._Driver Is Nothing Then
                details = "Handler interface not set"
            ElseIf Me._Emulator Is Nothing Then
                details = "Handler emulator not set"
            ElseIf Me._Driver.State <> HandlerState.StartTestEnabled Then
                details = $"Invalid handler state @'{Me._Driver.State}'; state should be @'{HandlerState.StartTestEnabled}'"
            Else
                Me._Emulator.OutputStartTest()
            End If
        Catch ex As Exception
            details = ex.ToString
        Finally
            Me._ErrorProvider.Annunciate(sender, details)
        End Try
    End Sub

    ''' <summary> Resets the button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ResetButton_Click(sender As System.Object, e As System.EventArgs) Handles _ResetButton.Click
        Dim details As String = String.Empty

        Try
            Me._ErrorProvider.Clear()
            If sender Is Nothing Then
                details = "Sender is empty"
            ElseIf Me._Driver Is Nothing Then
                details = "Handler interface not set"
            Else
                Me._Driver.InitializeKnownState()
                If Me._Emulator IsNot Nothing Then
                    Me._Emulator.Dispose()
                    Me._Emulator = Nothing
                End If
            End If
        Catch ex As Exception
            details = ex.ToString
        Finally
            Me._ErrorProvider.Annunciate(sender, details)
        End Try
    End Sub

    ''' <summary> Outputs the end test signals. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">   The sender. </param>
    ''' <param name="binValue"> The bin value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OutputEndTest(sender As System.Object, ByVal binValue As Integer)
        Dim details As String = String.Empty
        Try
            Me._ErrorProvider.Clear()
            If sender Is Nothing Then
                details = "Sender is empty"
            ElseIf Me._Driver Is Nothing Then
                details = "Handler interface not set"
            ElseIf Me._Driver.State = HandlerState.StartTestReceived Then
                Me._Driver.OutputEndTest(binValue)
            Else
                details = $"Invalid handler state @'{Me._Driver.State}'; state should be @'{HandlerState.Idle}'"

                Me._Driver.InitializeKnownState()
                If Me._Emulator IsNot Nothing Then
                    Me._Emulator.InitializeKnownState()
                End If
            End If
        Catch ex As Exception
            details = ex.ToString
        Finally
            Me._ErrorProvider.Annunciate(sender, details)
        End Try
    End Sub

    ''' <summary> Output pass bin button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OutputPassBinButton_Click(sender As System.Object, e As System.EventArgs) Handles _OutputPassBinButton.Click

        Me.OutputEndTest(sender, CInt(Me._PassBinValueNumeric.Value))
    End Sub

    ''' <summary> Output fail bin button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OutputFailBinButton_Click(sender As System.Object, e As System.EventArgs) Handles _OutputFailBinButton.Click
        Me.OutputEndTest(sender, CInt(Me._FailBinValueNumeric.Value))
    End Sub

#End Region

#Region " HANDLER SELECTION "

    ''' <summary> Handler combo box click event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandlerComboBox_Click(sender As Object, e As EventArgs) Handles _HandlerComboBox.Click
        If Me.InitializingComponents Then Return
        Try
            Me._ErrorProvider.Clear()
            Dim handler As SupportedHandler = CType(Me._HandlerComboBox.ComboBox.SelectedValue, SupportedHandler)
            Select Case handler
                Case SupportedHandler.Aetrium
                    Me.Driver.ApplyHandlerInfo(HandlerInfo.AetriumHandlerInfo)
                    Me.Emulator.ApplyHandlerInfo(HandlerInfo.AetriumEmulatorInfo)

                Case SupportedHandler.NoHau
                    Me.Driver.ApplyHandlerInfo(HandlerInfo.NoHauHandlerInfo)
                    Me.Emulator.ApplyHandlerInfo(HandlerInfo.NoHauHandlerInfo)
            End Select
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        Finally
        End Try

    End Sub

#End Region

End Class
