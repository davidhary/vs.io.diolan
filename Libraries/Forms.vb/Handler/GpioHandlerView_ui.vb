Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Core
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Core.EnumExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Diolan.Forms.SubsystemExtensions

Public Class GpioHandlerView
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()

        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._TabComboBox.ComboBox.DataSource = Me._Tabs.TabPages
        Me._TabComboBox.ComboBox.DisplayMember = "Text"

        Me._HandlerComboBox.ComboBox.DataSource = Nothing
        Me._HandlerComboBox.ComboBox.Items.Clear()
        Me._HandlerComboBox.ComboBox.DataSource = GetType(Gpio.SupportedHandler).ValueDescriptionPairs
        Me._HandlerComboBox.ComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._HandlerComboBox.ComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._HandlerComboBox.ComboBox.Enabled = True
        Me.InitializingComponents = False

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            Me.OnDispose(disposing)
            If Not Me.IsDisposed AndAlso disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Me.OnCustomDispose(disposing)
        If Not Me.IsDisposed AndAlso disposing Then
            If Me._Device IsNot Nothing Then Me._Device = Nothing
            If Me._DeviceConnector IsNot Nothing Then Me._DeviceConnector.Dispose() : Me._DeviceConnector = Nothing
            If Me._ServerConnector IsNot Nothing Then Me._ServerConnector = Nothing
        End If
    End Sub

#End Region

#Region " KNOWN STATE "

    ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Sub InitializeKnownState()
        Me.OnServerConnectionChanged()
    End Sub

#End Region

#Region " SERVER "

    ''' <summary> Raises the server connector property changed event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnPropertyChanged(ByVal sender As LocalhostConnector, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim details As String = String.Empty
        Try
            If sender Is Nothing Then
                details = "Sender is empty"
            Else
                If e IsNot Nothing Then
                    Select Case e.PropertyName
                        Case NameOf(Diolan.LocalhostConnector.IsConnected)
                            Me.OnServerConnectionChanged()
                        Case NameOf(Diolan.LocalhostConnector.AttachedDevicesCount)
                            Me.OnServerAttachmentChanged()
                    End Select
                End If
            End If
        Catch ex As Exception
            details = ex.ToString
        Finally
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton, details)
        End Try
    End Sub

    ''' <summary> Server connector property changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub ServerConnector_PropertyChanged(sender As Object,
                                                 e As PropertyChangedEventArgs) Handles _ServerConnector.PropertyChanged
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ServerConnector_PropertyChanged), New Object() {sender, e})
        End If
        Me.OnPropertyChanged(TryCast(sender, LocalhostConnector), e)
    End Sub

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _ServerConnector As LocalhostConnector
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the server connector. </summary>
    ''' <value> The server connector. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ServerConnector As LocalhostConnector
        Get
            Return Me._ServerConnector
        End Get
        Set(value As LocalhostConnector)
            Me._ServerConnector = value
        End Set
    End Property

    ''' <summary> Connects a server button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConnectServerButton_Click(sender As System.Object, e As System.EventArgs) Handles _ConnectServerButton.Click
        If Me.ServerConnector.IsConnected Then
            If Not Me.ServerConnector.HasAttachedDevices Then
                Me.ServerConnector.Disconnect()
            End If
        Else
            Me.ServerConnector.Connect()
        End If
    End Sub

    ''' <summary> Executes the server connection changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnServerConnectionChanged()
        If Me.ServerConnector.IsConnected Then
            Me.ServerConnector.Connection.ListDevicesById(Me._DevicesComboBox)
            Me._DevicesComboBox.SelectedIndex = 0
        End If
        Me._OpenDeviceModalityButton.Enabled = Me.ServerConnector.IsConnected
        Me._SelectDeviceSplitButton.Enabled = Me.ServerConnector.IsConnected
        Me.OnServerAttachmentChanged()
    End Sub

    ''' <summary> Executes the server attachment changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub OnServerAttachmentChanged()
        Me._ConnectServerButton.Image = My.Resources.WIFI_open_22
        If Me.ServerConnector.IsConnected Then
            Me._ConnectServerButton.Text = Me.ServerConnector.AttachedDevicesCount.ToString
            Me._ConnectServerButton.ForeColor = Drawing.Color.Black
        Else
            Me._ConnectServerButton.ForeColor = Drawing.Color.Red
            Me._ConnectServerButton.Text = "X"
        End If
    End Sub

#End Region

#Region " DEVICE CONNECTOR "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DeviceConnector As DeviceConnector
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the device connector. </summary>
    ''' <value> The device connector. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DeviceConnector As DeviceConnector
        Get
            Return Me._DeviceConnector
        End Get
        Set(value As DeviceConnector)
            Me._DeviceConnector = value
        End Set
    End Property

    ''' <summary> Device connector device closed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceClosed(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceClosed
        Me.OnServerAttachmentChanged()
        Me._DeviceInfoTextBox.Text = "closed"
        Me.OnModalityClosed()
    End Sub

    ''' <summary> Device connector device closing. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub DeviceConnector_DeviceClosing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _DeviceConnector.DeviceClosing
        Me.OnModalityClosing(e)
    End Sub

    ''' <summary> Device connector device opened. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceOpened(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceOpened

        Me.OnServerAttachmentChanged()
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> Gets the device. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The device. </value>
    Private ReadOnly Property Device As Dln.Device

    ''' <summary> Selected device information. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <returns> A DeviceInfo. </returns>
    Private Function SelectedDeviceInfo() As DeviceInfo
        If String.IsNullOrWhiteSpace(Me._DevicesComboBox.Text) Then
            Throw New InvalidOperationException("No devices selected")
        End If
        Return New DeviceInfo(Me._DevicesComboBox.Text)
    End Function

    ''' <summary> Opens the device for the modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="deviceId"> Identifier for the device. </param>
    ''' <param name="sender">   The sender. </param>
    Private Sub OpenDeviceModality(ByVal deviceId As Long, ByVal sender As Control)

        If Me.ServerConnector.IsConnected Then
            Me._OpenDeviceModalityButton.Enabled = Me.ServerConnector.IsConnected
            Me._SelectDeviceSplitButton.Enabled = Me.ServerConnector.IsConnected

        Else
            Me.ServerConnector.Connect()
        End If
        Me._DeviceConnector = New DeviceConnector(Me._ServerConnector)

        ' Open device
        Dim e As New ActionEventArgs
        If Me._DeviceConnector.TryOpenDevice(deviceId, Me.Modality, e) Then
            Me.OnModalityOpening()
        Else
            If sender IsNot Nothing Then Me._ErrorProvider.Annunciate(sender, e.Details)
            Me._DeviceInfoTextBox.Text = "No devices"
        End If

    End Sub

    ''' <summary> Closes device modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub CloseDeviceModality()
        Me._DeviceConnector.CloseDevice(Me.Modality)
        Dim e As New System.ComponentModel.CancelEventArgs
        Me.OnModalityClosing(e)
        If Not e.Cancel Then
            Me.OnModalityClosed()
        End If
    End Sub

    ''' <summary> Opens device button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenDeviceModalityButton_Click(sender As Object, e As System.EventArgs) Handles _OpenDeviceModalityButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Me.CloseDeviceModality()
            Else

                Dim deviceId As Long = DeviceInfo.DefaultDeviceId
                If Not String.IsNullOrWhiteSpace(Me._DevicesComboBox.Text) Then
                    deviceId = Me.SelectedDeviceInfo.Id
                End If
                Me.OpenDeviceModality(deviceId, TryCast(sender, Control))
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        Finally
            Me.OnModalityConnectionChanged()
        End Try
    End Sub

    ''' <summary> Gets a value indicating whether a device is open. </summary>
    ''' <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
    Private ReadOnly Property IsDeviceModalityOpen As Boolean
        Get
            Return Me._Device IsNot Nothing AndAlso Me.IsModalityOpen
        End Get
    End Property

    ''' <summary> Executes the modality connection changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityConnectionChanged()
        If Me.IsDeviceModalityOpen Then
            Me._OpenDeviceModalityButton.Image = My.Resources.user_online_2
            Me._OpenDeviceModalityButton.Text = "Close"
        Else
            Me._OpenDeviceModalityButton.Image = My.Resources.user_offline_2
            Me._OpenDeviceModalityButton.Text = "Open"
        End If
    End Sub

#End Region

#Region " EVENT LOG "

    ''' <summary> Event log text box double click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EventLogTextBox_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EventLogTextBox.DoubleClick
        Me._EventLogTextBox.Clear()
    End Sub

#End Region

#Region " TABS "

    ''' <summary> Selects the tab. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TabComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles _TabComboBox.SelectedIndexChanged
        If Me._TabComboBox.SelectedIndex >= 0 AndAlso Me._TabComboBox.SelectedIndex < Me._Tabs.TabCount Then
            Me._Tabs.SelectTab(Me._Tabs.TabPages(Me._TabComboBox.SelectedIndex))
        End If

    End Sub

#End Region

End Class

