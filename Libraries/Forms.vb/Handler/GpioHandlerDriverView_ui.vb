Imports isr.Core
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Diolan.Forms.SubsystemExtensions

Public Class GpioHandlerDriverView
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()

        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me._TabComboBox.ComboBox.DataSource = Me._Tabs.TabPages
        Me._TabComboBox.ComboBox.DisplayMember = "Text"

        Me.InitializeModality()
        Me.InitializingComponents = False

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            Me.OnDispose(disposing)
            If Not Me.IsDisposed AndAlso disposing Then
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Executes the dispose action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> true to disposing. </param>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Me.OnCustomDispose(disposing)
    End Sub

#End Region

#Region " KNOWN STATE "

    ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Sub InitializeKnownState()
        Me.OnServerConnectionChanged()
    End Sub

#End Region

#Region " SERVER "

    ''' <summary> Raises the server connector property changed event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnPropertyChanged(ByVal sender As LocalhostConnector, ByVal propertyName As String)
        Dim details As String = String.Empty
        Try
            If sender Is Nothing Then
                details = "Sender is empty"
            Else
                If Not String.IsNullOrWhiteSpace(propertyName) Then
                    Select Case propertyName
                        Case NameOf(Diolan.LocalhostConnector.IsConnected)
                            Me.OnServerConnectionChanged()
                        Case NameOf(Diolan.LocalhostConnector.AttachedDevicesCount)
                            Me.OnServerAttachmentChanged()
                    End Select
                End If
            End If
        Catch ex As Exception
            details = ex.ToString
        Finally
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton, details)
        End Try
    End Sub

    ''' <summary> Connects a server button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConnectServerButton_Click(sender As System.Object, e As System.EventArgs) Handles _ConnectServerButton.Click
        If Me.DriverEmulator.ServerConnector.IsConnected Then
            If Not Me.DriverEmulator.ServerConnector.HasAttachedDevices Then
                Me.DriverEmulator.ServerConnector.Disconnect()
            End If
        Else
            Me.DriverEmulator.ServerConnector.Connect()
        End If
    End Sub

    ''' <summary> Executes the server connection changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnServerConnectionChanged()
        If Me.DriverEmulator.ServerConnector.IsConnected Then
            Me.DriverEmulator.ServerConnector.Connection.ListDevicesById(Me._DevicesComboBox)
            Me._DevicesComboBox.SelectedIndex = 0
        End If
        Me._OpenDeviceModalityButton.Enabled = Me.DriverEmulator.ServerConnector.IsConnected
        Me._SelectDeviceSplitButton.Enabled = Me.DriverEmulator.ServerConnector.IsConnected
        Me.OnServerAttachmentChanged()
    End Sub

    ''' <summary> Executes the server attachment changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub OnServerAttachmentChanged()
        Me._ConnectServerButton.Image = My.Resources.WIFI_open_22
        If Me.DriverEmulator.ServerConnector.IsConnected Then
            Me._ConnectServerButton.Text = Me.DriverEmulator.ServerConnector.AttachedDevicesCount.ToString
            Me._ConnectServerButton.ForeColor = Drawing.Color.Black
        Else
            Me._ConnectServerButton.ForeColor = Drawing.Color.Red
            Me._ConnectServerButton.Text = "X"
        End If
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> Selected device information. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <returns> A DeviceInfo. </returns>
    Private Function SelectedDeviceInfo() As DeviceInfo
        If String.IsNullOrWhiteSpace(Me._DevicesComboBox.Text) Then
            Throw New InvalidOperationException("No devices selected")
        End If
        Return New DeviceInfo(Me._DevicesComboBox.Text)
    End Function

    ''' <summary> Opens device button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenDeviceModalityButton_Click(sender As Object, e As System.EventArgs) Handles _OpenDeviceModalityButton.Click
        Dim args As New ActionEventArgs
        Try
            Me._ErrorProvider.Clear()
            If Me.DriverEmulator.IsDeviceModalityOpen Then
                Me.DriverEmulator.CloseDeviceModality()
            Else
                Me.DriverEmulator.TryOpenDeviceModality(Me.SelectedDeviceInfo.Id, args)
            End If
        Catch ex As Exception

            args.RegisterError(ex.ToFullBlownString)
        Finally
            If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
        End Try
    End Sub

#End Region

#Region " EVENT LOG "

    ''' <summary> Event log text box double click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EventLogTextBox_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EventLogTextBox.DoubleClick
        Me._EventLogTextBox.Clear()
    End Sub

#End Region

#Region " TABS "

    ''' <summary> Selects the tab. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TabComboBox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles _TabComboBox.SelectedIndexChanged
        If Me._TabComboBox.SelectedIndex >= 0 AndAlso Me._TabComboBox.SelectedIndex < Me._Tabs.TabCount Then
            Me._Tabs.SelectTab(Me._Tabs.TabPages(Me._TabComboBox.SelectedIndex))
        End If
    End Sub

#End Region


End Class

