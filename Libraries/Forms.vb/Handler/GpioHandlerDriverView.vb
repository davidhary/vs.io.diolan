Imports System.ComponentModel

Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Diolan.Gpio
Imports isr.Diolan.SubsystemExtensions

''' <summary> User interface for managing a GPIO Handler driver and emulator. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-06-01 </para>
''' </remarks>
Partial Public Class GpioHandlerDriverView

#Region " CONSTRUCTION "

    ''' <summary> Initializes the modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub InitializeModality()

        Me._DriverEmulator = GpioHandlerDriverEmulator.Get

        ' enabled once the driver emulator is assigned.
        Me._HandlerComboBox.ComboBox.Enabled = False
        Me._HandlerComboBox.ComboBox.DataSource = Nothing
        Me._HandlerComboBox.ComboBox.Items.Clear()
        Me._HandlerComboBox.ComboBox.DataSource = GetType(SupportedHandler).ValueDescriptionPairs.ToList
        Me._HandlerComboBox.ComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._HandlerComboBox.ComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)

        Me._EndTestModeComboBox.DataSource = Nothing
        Me._EndTestModeComboBox.Items.Clear()
        Me._EndTestModeComboBox.DataSource = GetType(EndTestMode).ValueDescriptionPairs.ToList
        Me._EndTestModeComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
        Me._EndTestModeComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._EndTestModeComboBox.Enabled = True

        Me._HandlerComboBox.Enabled = Me._DriverEmulator?.Driver IsNot Nothing

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                If Me._DriverEmulator IsNot Nothing Then Me._DriverEmulator.Dispose() : Me._DriverEmulator = Nothing
                If GpioHandlerDriverEmulator.Instantiated Then GpioHandlerDriverEmulator.DisposeInstance()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " GPIO HANDLER DRIVER AND EMULATOR "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DriverEmulator As GpioHandlerDriverEmulator
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the Gpio handler driver and emulator. </summary>
    ''' <value> The Gpio test handler. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DriverEmulator As GpioHandlerDriverEmulator
        Get
            Return Me._DriverEmulator
        End Get
        Set(value As GpioHandlerDriverEmulator)
            Me._DriverEmulator = value
            If Me._DriverEmulator IsNot Nothing Then
                Me.UpdateGui(Me._DriverEmulator.Driver)
                Me.UpdateGui(Me._DriverEmulator.Emulator)
            End If
            Me._HandlerComboBox.Enabled = Me._DriverEmulator?.Driver IsNot Nothing
        End Set
    End Property

    ''' <summary> Handles the server connector, driver and emulator property changed event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As GpioHandlerDriverEmulator, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        If propertyName.StartsWith(sender.ServerConnectorSource, StringComparison.OrdinalIgnoreCase) Then
            Me.OnPropertyChanged(sender.ServerConnector, propertyName.Remove(0, sender.ServerConnectorSource.Length))
        ElseIf propertyName.StartsWith(sender.DriverSource, StringComparison.OrdinalIgnoreCase) Then
            Me.OnPropertyChanged(sender.Driver, propertyName.Remove(0, sender.DriverSource.Length))
        ElseIf propertyName.StartsWith(sender.EmulatorSource, StringComparison.OrdinalIgnoreCase) Then
            Me.OnPropertyChanged(sender.Emulator, propertyName.Remove(0, sender.EmulatorSource.Length))
        Else
            Select Case propertyName
                Case NameOf(Gpio.GpioHandlerDriverEmulator.DeviceCaption)
                    Me._DeviceInfoTextBox.Text = sender.DeviceCaption
                Case NameOf(Gpio.GpioHandlerDriverEmulator.IsDeviceModalityOpen)
                    If sender.IsDeviceModalityOpen Then
                        Me._OpenDeviceModalityButton.Image = My.Resources.user_online_2
                        Me._OpenDeviceModalityButton.Text = "Close"
                    Else
                        Me._OpenDeviceModalityButton.Image = My.Resources.user_offline_2
                        Me._OpenDeviceModalityButton.Text = "Open"
                    End If
                    Me._HandlerComboBox.Enabled = sender.Driver IsNot Nothing
                    If sender.Driver IsNot Nothing Then Me.OnHandlerSelected(Me._HandlerComboBox)
                    sender.Driver?.PublishAll()
                    sender.Emulator?.PublishAll()
                Case NameOf(Gpio.GpioHandlerDriverEmulator.EventCaption)
                    Me._EventLogTextBox.AppendText(sender.EventCaption)
            End Select

        End If
    End Sub

    ''' <summary> Driver emulator property changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DriverEmulator_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _DriverEmulator.PropertyChanged
        Dim details As String = String.Empty
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.DriverEmulator_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, GpioHandlerDriverEmulator), e?.PropertyName)
            End If
        Catch ex As Exception
            details = $"Exception handling driver {e?.PropertyName} property change;.  {ex.ToFullBlownString}"
        Finally

            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton, details)
        End Try
    End Sub


#End Region

#Region " DRIVER "

    ''' <summary> Updates the graphical user interface described by driver. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="driver"> The driver. </param>
    Private Sub UpdateGui(driver As GpioHandlerDriver)
        If driver IsNot Nothing Then
            Me._StartTestPinNumberNumeric.Value = driver.StartTestPinNumber
            Me._EndTestPinNumberNumeric.Value = driver.EndTestPinNumber
            Me._ActiveLowLogicRadioButton.Checked = driver.ActiveLogic = ActiveLogic.ActiveLow
            Me.BinPortMask = driver.BinMask
            Me.EndTestMode = driver.EndTestMode
        End If
    End Sub

    ''' <summary> Gets or sets the handler mask. </summary>
    ''' <value> The entered mask. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private Property BinPortMask As Long
        Get
            Return Convert.ToInt32(Me._BinPortMaskTextBox.Text, 2)
        End Get
        Set(value As Long)
            Me._BinPortMaskTextBox.Text = Convert.ToString(value, 2)
        End Set
    End Property

    ''' <summary> Gets or sets the end test mode. </summary>
    ''' <value> The end test mode. </value>
    Private Property EndTestMode As EndTestMode
        Get
            Return CType(Me._EndTestModeComboBox.SelectedValue, EndTestMode)
        End Get
        Set(value As EndTestMode)
            Me._EndTestModeComboBox.SelectedValue = value
        End Set
    End Property

    ''' <summary> Bin port mask text box validating. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BinPortMaskTextBox_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _BinPortMaskTextBox.Validating
        Try
            Me._ErrorProvider.Clear()
            If Me.BinPortMask() <= 0 Then
                Me._ErrorProvider.Annunciate(sender, "Value must be positive")
                e.Cancel = True
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
            e.Cancel = True

        End Try
    End Sub

    ''' <summary> Ends test mode combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub EndTestModeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _EndTestModeComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()

            If Me.BinPortMask() <= 0 Then
                Me._ErrorProvider.Annunciate(sender, "Value must be positive")
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)

        End Try

    End Sub

    ''' <summary> Handles the Configure Emulator button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConfigureDriverButton_Click(sender As Object, e As System.EventArgs) Handles _ConfigureDriverButton.Click
        Dim args As New ActionEventArgs
        Try
            Me._ErrorProvider.Clear()
            If Not Me.DriverEmulator.IsDeviceModalityOpen Then
                args.RegisterFailure("Device not open")
            ElseIf Me.DriverEmulator.DeviceConnector.Device.Gpio Is Nothing Then
                args.RegisterFailure($"Gpio not supported on {Me.DriverEmulator.DeviceConnector.Device.Caption}")

            ElseIf Me.DriverEmulator.DeviceConnector.Device.Gpio.Pins Is Nothing Then
                args.RegisterFailure($"Gpio not supported on {Me.DriverEmulator.DeviceConnector.Device.Caption}")
            Else
                Me.DriverEmulator.Driver.StartTestPinNumber = CInt(Me._StartTestPinNumberNumeric.Value)
                Me.DriverEmulator.Driver.EndTestPinNumber = CInt(Me._EndTestPinNumberNumeric.Value)
                Me.DriverEmulator.Driver.ActiveLogic = If(Me._ActiveLowLogicRadioButton.Checked, ActiveLogic.ActiveLow, ActiveLogic.ActiveHigh)
                Me.DriverEmulator.Driver.BinEndTestOnsetDelay = TimeSpan.FromMilliseconds(Me._EndTestDelayNumeric.Value)
                Me.DriverEmulator.Driver.BinMask = Me.BinPortMask
                Me.DriverEmulator.Driver.EndTestMode = Me.EndTestMode
                If Me.DriverEmulator.TryConfigureDriver(args) Then
                End If
            End If
        Catch ex As Exception
            args.RegisterError(ex.ToFullBlownString)
        Finally
            If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
        End Try
    End Sub

    ''' <summary> Sot label click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SotLabel_Click(sender As System.Object, e As System.EventArgs) Handles _SotLabel.Click
        Me.UpdateSotIndicator(Me.DriverEmulator.Driver)
    End Sub

    ''' <summary> Updates the sot indicator described by sender. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    Private Sub UpdateSotIndicator(ByVal sender As GpioHandlerDriverBase)
        If sender IsNot Nothing Then
            Me._SotLabel.Image = If(sender.StartTestLogicalValue, My.Resources.user_available, My.Resources.user_invisible)
        End If
    End Sub

    ''' <summary> Eot label click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EotLabel_Click(sender As System.Object, e As System.EventArgs) Handles _EotLabel.Click
        Me.UpdateEotIndicator(Me.DriverEmulator.Driver)
    End Sub

    ''' <summary> Updates the eot indicator described by sender. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    Private Sub UpdateEotIndicator(ByVal sender As GpioHandlerDriverBase)
        If sender IsNot Nothing Then
            Me._EotLabel.Image = If(sender.EndTestLogicalValue, My.Resources.user_available, My.Resources.user_invisible)
        End If
    End Sub

    ''' <summary> Raises the server connector property changed event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As GpioHandlerDriver, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Gpio.GpioHandlerDriver.State)
                Me._DriverStateLabel.Text = sender.State.Description
                Select Case sender.State
                    Case HandlerState.StartTestReceived
                    Case Else
                End Select
            Case NameOf(Gpio.GpioHandlerDriver.BinValue)
                Me._DriverBinValueLabel.Text = sender.BinValueCaption("...")
            Case NameOf(Gpio.GpioHandlerDriver.StartTestLogicalValue)
                Me.UpdateSotIndicator(sender)
            Case NameOf(Gpio.GpioHandlerDriver.EndTestLogicalValue)
                Me.UpdateEotIndicator(sender)
            Case NameOf(Gpio.GpioHandlerDriver.BinMask)
                Me.BinPortMask = sender.BinMask
            Case NameOf(Gpio.GpioHandlerDriver.BinEndTestOnsetDelay)
                Me._EndTestDelayNumeric.Value = CDec(sender.BinEndTestOnsetDelay.TotalMilliseconds)
            Case NameOf(Gpio.GpioHandlerDriver.StartTestPinNumber)
                Me._StartTestPinNumberNumeric.Value = sender.StartTestPinNumber
            Case NameOf(Gpio.GpioHandlerDriver.EndTestPinNumber)
                Me._EndTestPinNumberNumeric.Value = sender.EndTestPinNumber
            Case NameOf(Gpio.GpioHandlerDriver.EndTestMode)
                Me.EndTestMode = sender.EndTestMode
            Case NameOf(Gpio.GpioHandlerDriver.ActiveLogic)
                Me._ActiveLowLogicRadioButton.Checked = sender.ActiveLogic = ActiveLogic.ActiveLow
                Me._ActiveHighLogicRadioButton.Checked = sender.ActiveLogic = ActiveLogic.ActiveHigh
            Case NameOf(Gpio.GpioHandlerDriver.IsConfigured)
                Me._ConfigureDriverButton.Image = If(sender.IsConfigured, My.Resources.dialog_ok_2, My.Resources.configure_2)
        End Select
    End Sub

#End Region

#Region " EMULATOR "

    ''' <summary> Updates the graphical user interface described by driver. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub UpdateGui(value As GpioHandlerEmulator)
        If value IsNot Nothing Then
            Me._EmulatorStartTestPinNumberNumeric.Value = value.StartTestPinNumber
            Me._EmulatorEndTestPinNumberNumeric.Value = value.EndTestPinNumber
            Me.EmulatorBinMask = Me.DriverEmulator.Driver.BinMask
        End If
    End Sub

    ''' <summary> Gets or sets the entered bin mask. </summary>
    ''' <value> The entered mask. </value>
    Private Property EmulatorBinMask As Long
        Get
            Return Convert.ToInt32(Me._EmulatorBinPortMaskTextBox.Text, 2)
        End Get
        Set(value As Long)
            Me._EmulatorBinPortMaskTextBox.Text = Convert.ToString(value, 2)
        End Set
    End Property

    ''' <summary> Emulator Bin port mask text box validating. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub EmulatorBinPortMaskTextBox_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _EmulatorBinPortMaskTextBox.Validating
        Try
            Me._ErrorProvider.Clear()
            If Me.EmulatorBinMask() <= 0 Then
                Me._ErrorProvider.Annunciate(sender, "Value must be positive")

                e.Cancel = True
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
            e.Cancel = True
        End Try
    End Sub

    ''' <summary> Applies the button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConfigureEmulatorButton_Click(sender As Object, e As System.EventArgs) Handles _ConfigureEmulatorButton.Click
        Dim args As New ActionEventArgs
        Try
            Me._ErrorProvider.Clear()

            If sender Is Nothing Then
                args.RegisterFailure("Sender is empty")
            ElseIf Me.DriverEmulator.IsDeviceModalityOpen Then
                If Not Me.DriverEmulator.IsDeviceModalityOpen Then
                    args.RegisterFailure("Device not open")
                ElseIf Me.DriverEmulator.DeviceConnector.Device.Gpio Is Nothing Then
                    args.RegisterFailure($"Gpio not supported on {Me.DriverEmulator.DeviceConnector.Device.Caption}")
                ElseIf Me.DriverEmulator.DeviceConnector.Device.Gpio.Pins Is Nothing Then
                    args.RegisterFailure($"Gpio not supported on {Me.DriverEmulator.DeviceConnector.Device.Caption}")
                Else
                    Me.DriverEmulator.Emulator.StartTestPinNumber = CInt(Me._EmulatorStartTestPinNumberNumeric.Value)
                    Me.DriverEmulator.Emulator.EndTestPinNumber = CInt(Me._EmulatorEndTestPinNumberNumeric.Value)
                    Me.DriverEmulator.Emulator.ActiveLogic = If(Me._ActiveLowLogicRadioButton.Checked, ActiveLogic.ActiveLow, ActiveLogic.ActiveHigh)
                    Me.DriverEmulator.Emulator.BinMask = Me.EmulatorBinMask
                    Me.DriverEmulator.Emulator.EndTestMode = Me.EndTestMode
                    Me.DriverEmulator.TryConfigureEmulator(args)
                End If
            Else
                args.RegisterFailure("Device not open")
            End If
        Catch ex As Exception
            args.RegisterError(ex.ToFullBlownString)
        Finally
            If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
        End Try
    End Sub

    ''' <summary> Handles the emulator property changed event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As GpioHandlerEmulator, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Gpio.GpioHandlerDriverBase.State)
                Select Case sender.State
                    Case HandlerState.StartTestReceived
                        Me._EmulatorStateLabel.Text = "e: Start test sent"
                    Case HandlerState.EndTestSent
                        Me._EmulatorStateLabel.Text = "e: End test received"
                    Case Else
                        Me._EmulatorStateLabel.Text = sender.State.Description
                End Select
            Case NameOf(Gpio.GpioHandlerDriverBase.BinValue)
                Me._EmulatorBinValueLabel.Text = sender.BinValueCaption("...")
            Case NameOf(Gpio.GpioHandlerDriverBase.StartTestLogicalValue)
            Case NameOf(Gpio.GpioHandlerDriverBase.EndTestLogicalValue)
            Case NameOf(Gpio.GpioHandlerDriverBase.BinMask)
                Me.EmulatorBinMask = sender.BinMask
            Case NameOf(Gpio.GpioHandlerDriverBase.BinEndTestOnsetDelay)
            Case NameOf(Gpio.GpioHandlerDriverBase.StartTestPinNumber)
                Me._EmulatorStartTestPinNumberNumeric.Value = sender.StartTestPinNumber
            Case NameOf(Gpio.GpioHandlerDriverBase.EndTestPinNumber)
                Me._EmulatorEndTestPinNumberNumeric.Value = sender.EndTestPinNumber
            Case NameOf(Gpio.GpioHandlerDriverBase.ActiveLogic)
                    ' TO_DO: Must be the same as the driver.
            Case NameOf(Gpio.GpioHandlerDriverBase.IsConfigured)
                Me._ConfigureEmulatorButton.Image = If(sender.IsConfigured, My.Resources.dialog_ok_2, My.Resources.configure_2)
        End Select
    End Sub

#End Region

#Region " PLAY "

    ''' <summary> Clears the start button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ClearStartButton_Click(sender As System.Object, e As System.EventArgs) Handles _ClearStartButton.Click
        Dim args As New ActionEventArgs
        Me._ErrorProvider.Clear()

        Me.DriverEmulator.TryClearStart(args)
        If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
    End Sub

    ''' <summary> Starts test button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub StartTestButton_Click(sender As System.Object, e As System.EventArgs) Handles _StartTestButton.Click
        Dim args As New ActionEventArgs
        Me._ErrorProvider.Clear()
        Me.DriverEmulator.TryStartTest(args)
        If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
    End Sub

    ''' <summary> Resets the button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResetButton_Click(sender As System.Object, e As System.EventArgs) Handles _ResetButton.Click
        Dim args As New ActionEventArgs
        Me._ErrorProvider.Clear()
        Me.DriverEmulator.TryInitializeKnownState(args)
        If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
    End Sub

    ''' <summary> Outputs the end test signals. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">   The sender. </param>
    ''' <param name="binValue"> The bin value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OutputEndTest(sender As System.Object, ByVal binValue As Integer)
        Dim args As New ActionEventArgs
        Me._ErrorProvider.Clear()
        Me.DriverEmulator.TryOutputEndTest(binValue, args)
        If args.Failed Then Me._ErrorProvider.Annunciate(sender, args.Details)
    End Sub

    ''' <summary> Output pass bin button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OutputPassBinButton_Click(sender As System.Object, e As System.EventArgs) Handles _OutputPassBinButton.Click
        Me.OutputEndTest(sender, CInt(Me._PassBinValueNumeric.Value))
    End Sub

    ''' <summary> Output fail bin button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OutputFailBinButton_Click(sender As System.Object, e As System.EventArgs) Handles _OutputFailBinButton.Click
        Me.OutputEndTest(sender, CInt(Me._FailBinValueNumeric.Value))
    End Sub

#End Region

#Region " HANDLER SELECTION "

    ''' <summary> Executes the 'handler selected' action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    Private Sub OnHandlerSelected(ByVal sender As Windows.Forms.ToolStripComboBox)
        If Me.InitializingComponents OrElse sender Is Nothing Then Return
        Dim handler As SupportedHandler = CType(sender.ComboBox.SelectedValue, SupportedHandler)
        Select Case handler
            Case SupportedHandler.Aetrium
                Me.DriverEmulator.Driver.ApplyHandlerInfo(HandlerInfo.AetriumHandlerInfo)
                Me.DriverEmulator.Emulator.ApplyHandlerInfo(HandlerInfo.AetriumEmulatorInfo)
            Case SupportedHandler.NoHau
                Me.DriverEmulator.Driver.ApplyHandlerInfo(HandlerInfo.NoHauHandlerInfo)
                Me.DriverEmulator.Emulator.ApplyHandlerInfo(HandlerInfo.NoHauEmulatorInfo)
        End Select
    End Sub

    ''' <summary> Handler combo box click event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandlerComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _HandlerComboBox.SelectedIndexChanged
        If Me.InitializingComponents Then Return
        Try
            Me._ErrorProvider.Clear()
            Me.OnHandlerSelected(CType(sender, Windows.Forms.ToolStripComboBox))
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        Finally
        End Try

    End Sub

#End Region

End Class

