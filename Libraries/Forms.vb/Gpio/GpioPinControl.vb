Imports System.ComponentModel
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.SubsystemExtensions

''' <summary> Gpio pin control. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-06-05 </para>
''' </remarks>
Public Class GpioPinControl
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            Me.OnDispose(disposing)
            If Not Me.IsDisposed AndAlso disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.CloseModality()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        End Try

    End Sub

#End Region

#Region " TOOL STRIP "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    Private ReadOnly Property Device As Dln.Device

    ''' <summary> Gets or sets the device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device"> The device. </param>
    <CLSCompliant(False)>
    Public Sub OpenModality(ByVal device As Dln.Device)

        Me._Device = device
        Me._PinComboBox.ComboBox.DataSource = Nothing
        Me._ErrorProvider.Clear()

        ' Get port count
        If Me.Device.Gpio.Pins.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._PinComboBox, $"Adapter '{Me.Device.Caption}' doesn't support GPIO interface.")
            Me._Device = Nothing
        Else
            isr.Diolan.Forms.SubsystemExtensions.ListNumbers(Me.Device.Gpio.Pins, Me._PinComboBox.ComboBox)
            ' Me._PinComboBox.ComboBox.ListNumbers(Me.Device.Gpio.Pins)

            Me._OutputValueComboBox.ComboBox.DataSource = Nothing
            Me._OutputValueComboBox.ComboBox.ListEnumNames(Of isr.Diolan.PinValue)

            Me._PinComboBox.SelectedIndex = 0

        End If

    End Sub

    ''' <summary> Closes the modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub CloseModality()
        If Me._GpioSubsystem IsNot Nothing Then
            Me._GpioSubsystem.Dispose()
            Me._GpioSubsystem = Nothing
        End If
        Me._PinComboBox.ComboBox.DataSource = Nothing
        Me._PinComboBox.ComboBox.Items.Clear()
        Me._OutputValueComboBox.ComboBox.DataSource = Nothing
        Me._OutputValueComboBox.ComboBox.Items.Clear()
        Me._Device = Nothing
        Me._ErrorProvider.Clear()
    End Sub

    ''' <summary> The modality. </summary>
    ''' <value> The modality. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Modality As DeviceModalities = DeviceModalities.Gpio

    ''' <summary> Queries if a modality is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
    Private Function IsDeviceModalityOpen() As Boolean
        Return Me._Device IsNot Nothing AndAlso
                   Me._PinComboBox.ComboBox.DataSource IsNot Nothing AndAlso Me._PinComboBox.Items.Count > 0
    End Function

    ''' <summary> Pin combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub PinComboBox_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles _PinComboBox.SelectedIndexChanged
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim pinNumber As Integer = Me._PinComboBox.SelectedIndex
                If pinNumber >= 0 AndAlso pinNumber < Me.Device.Gpio.Pins.Count Then
                    Dim pin As Dln.Gpio.Pin = Me.Device.Gpio.Pins(pinNumber)
                    Me._ValueTextBox.ReadOnly = True

                    Me._SetOutputButton.Visible = pin.Direction = isr.Diolan.PinDirection.Output
                    Me._OutputValueComboBoxLabel.Visible = pin.Direction = isr.Diolan.PinDirection.Output

                    Me._OutputValueComboBox.Visible = pin.Direction = isr.Diolan.PinDirection.Output
                    Me._OutputValueComboBox.Enabled = pin.Enabled

                    Me._PulseDurationTextBoxLabel.Visible = pin.Direction = isr.Diolan.PinDirection.Output
                    Me._PulseDurationTextBox.Visible = pin.Direction = isr.Diolan.PinDirection.Output
                    Me._PulseDurationTextBox.Enabled = pin.Enabled

                    Me._StartButton.Visible = pin.Direction = isr.Diolan.PinDirection.Output
                    Me._StartButton.Enabled = pin.Enabled AndAlso pin.Direction = isr.Diolan.PinDirection.Output

                ElseIf Me._ErrorProvider IsNot Nothing Then
                    Me._ErrorProvider.Annunciate(sender, "Pin index {0} is out of range of [0,{1}]", Me.Device.Gpio.Pins.Count - 1)
                End If
            ElseIf Me._PinComboBox.SelectedIndex >= 0 AndAlso Me._ErrorProvider IsNot Nothing Then
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Sets output button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetOutputButton_Click(sender As System.Object, e As System.EventArgs) Handles _SetOutputButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim pinNumber As Integer = Me._PinComboBox.SelectedIndex
                If pinNumber >= 0 AndAlso pinNumber < Me.Device.Gpio.Pins.Count Then
                    Dim pin As Dln.Gpio.Pin = Me.Device.Gpio.Pins(pinNumber)
                    pin.OutputValue = Me._OutputValueComboBox.SelectedIndex
                    Me._OutputValueComboBox.SelectedIndex = pin.OutputValue
                    Me._ValueTextBox.Text = pin.Value.ToString()
                Else
                    Me._ErrorProvider.Annunciate(sender, "Pin index {0} is out of range of [0,{1}]", Me.Device.Gpio.Pins.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Gets value button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GetValueButton_Click(sender As System.Object, e As System.EventArgs) Handles _GetValueButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim pinNumber As Integer = Me._PinComboBox.SelectedIndex
                If pinNumber >= 0 AndAlso pinNumber < Me.Device.Gpio.Pins.Count Then
                    Dim pin As Dln.Gpio.Pin = Me.Device.Gpio.Pins(pinNumber)
                    Me._OutputValueComboBox.SelectedIndex = pin.OutputValue
                    Me._ValueTextBox.Text = pin.Value.ToString()
                Else
                    Me._ErrorProvider.Annunciate(sender, "Pin index {0} is out of range of [0,{1}]", Me.Device.Gpio.Pins.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> The gpio subsystem. </summary>
    Private _GpioSubsystem As isr.Diolan.GpioSubsystem

    ''' <summary> Starts button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StartButton_Click(sender As System.Object, e As System.EventArgs) Handles _StartButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Dim pinNumber As Integer = Me._PinComboBox.SelectedIndex
                If pinNumber >= 0 AndAlso pinNumber < Me.Device.Gpio.Pins.Count Then
                    If Me._GpioSubsystem Is Nothing Then Me._GpioSubsystem = New isr.Diolan.GpioSubsystem
                    Dim duration As Double = 0
                    If Double.TryParse(Me._PulseDurationTextBox.Text, duration) Then
                        Me._GpioSubsystem.PulsePin(Me.Device.Gpio.Pins(pinNumber), TimeSpan.FromMilliseconds(duration))
                    Else
                        Me._ErrorProvider.Annunciate(sender, "Invalid duration: {0}", Me._PulseDurationTextBox.Text)
                    End If
                Else
                    Me._ErrorProvider.Annunciate(sender, "Pin index {0} is out of range of [0,{1}]", Me.Device.Gpio.Pins.Count - 1)
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

End Class

