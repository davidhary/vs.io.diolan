﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GpioPinToolStrip

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ConfigureButton = New System.Windows.Forms.ToolStripButton()
        Me._PinNumberLabel = New System.Windows.Forms.ToolStripLabel()
        Me._Separator1 = New System.Windows.Forms.ToolStripSeparator()
        Me._PinNameLabel = New System.Windows.Forms.ToolStripLabel()
        Me._LogicalStateButton = New System.Windows.Forms.ToolStripButton()
        Me._ActiveLogicToggleButton = New System.Windows.Forms.ToolStripButton()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolStrip.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ToolStrip
        '
        Me._ToolStrip.BackColor = System.Drawing.SystemColors.Control
        Me._ToolStrip.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ToolStrip.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ConfigureButton, Me._PinNumberLabel, Me._Separator1, Me._PinNameLabel, Me._LogicalStateButton, Me._ActiveLogicToggleButton})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Margin = New System.Windows.Forms.Padding(3)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Padding = New System.Windows.Forms.Padding(0)
        Me._ToolStrip.Size = New System.Drawing.Size(230, 31)
        Me._ToolStrip.Stretch = True
        Me._ToolStrip.TabIndex = 0
        Me._ToolStrip.Text = "Tool Strip"
        '
        '_ConfigureButton
        '
        Me._ConfigureButton.AutoSize = False
        Me._ConfigureButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ConfigureButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Configure_3
        Me._ConfigureButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ConfigureButton.Margin = New System.Windows.Forms.Padding(0)
        Me._ConfigureButton.Name = "_ConfigureButton"
        Me._ConfigureButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._ConfigureButton.Size = New System.Drawing.Size(24, 24)
        Me._ConfigureButton.Text = "Configure"
        '
        '_PinNumberLabel
        '
        Me._PinNumberLabel.Margin = New System.Windows.Forms.Padding(1, 1, 1, 2)
        Me._PinNumberLabel.Name = "_PinNumberLabel"
        Me._PinNumberLabel.Size = New System.Drawing.Size(22, 28)
        Me._PinNumberLabel.Text = "00"
        Me._PinNumberLabel.ToolTipText = "Pin number"
        '
        '_Separator1
        '
        Me._Separator1.Name = "_Separator1"
        Me._Separator1.Size = New System.Drawing.Size(6, 27)
        '
        '_PinNameLabel
        '
        Me._PinNameLabel.Margin = New System.Windows.Forms.Padding(1)
        Me._PinNameLabel.Name = "_PinNameLabel"
        Me._PinNameLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._PinNameLabel.Size = New System.Drawing.Size(58, 29)
        Me._PinNameLabel.Text = "<name>"
        Me._PinNameLabel.ToolTipText = "Pin name"
        '
        '_LogicalStateButton
        '
        Me._LogicalStateButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._LogicalStateButton.CheckOnClick = True
        Me._LogicalStateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._LogicalStateButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Circle_grey
        Me._LogicalStateButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._LogicalStateButton.Margin = New System.Windows.Forms.Padding(1)
        Me._LogicalStateButton.Name = "_LogicalStateButton"
        Me._LogicalStateButton.Size = New System.Drawing.Size(23, 29)
        Me._LogicalStateButton.Text = "Logical State"
        '
        '_ActiveLogicToggleButton
        '
        Me._ActiveLogicToggleButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._ActiveLogicToggleButton.CheckOnClick = True
        Me._ActiveLogicToggleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ActiveLogicToggleButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Minus_Grey
        Me._ActiveLogicToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ActiveLogicToggleButton.Margin = New System.Windows.Forms.Padding(1)
        Me._ActiveLogicToggleButton.Name = "_ActiveLogicToggleButton"
        Me._ActiveLogicToggleButton.Size = New System.Drawing.Size(23, 29)
        Me._ActiveLogicToggleButton.Text = "Active Logic"
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        'GpioPinToolStrip
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Controls.Add(Me._ToolStrip)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "GpioPinToolStrip"
        Me.Size = New System.Drawing.Size(230, 31)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents _ToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ConfigureButton As Windows.Forms.ToolStripButton
    Private WithEvents _PinNumberLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _PinNameLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _LogicalStateButton As Windows.Forms.ToolStripButton
    Private WithEvents _ActiveLogicToggleButton As Windows.Forms.ToolStripButton
    Private WithEvents _ErrorProvider As Windows.Forms.ErrorProvider
    Private WithEvents _Separator1 As Windows.Forms.ToolStripSeparator
End Class
