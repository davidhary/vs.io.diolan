﻿
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class GpioPinConfigControl

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
            Me._OpenDrainCheckBox = New System.Windows.Forms.CheckBox()
            Me._PinPullupCheckBox = New System.Windows.Forms.CheckBox()
            Me._EventPeriodNumeric = New System.Windows.Forms.NumericUpDown()
            Me._PulldownCheckBox = New System.Windows.Forms.CheckBox()
            Me._DebounceEnabledCheckBox = New System.Windows.Forms.CheckBox()
            Me._EventTypeComboBox = New System.Windows.Forms.ComboBox()
            Me._PinDirectionComboBox = New System.Windows.Forms.ComboBox()
            Me._PinNumberNumeric = New System.Windows.Forms.NumericUpDown()
            Me._PinNameTextBox = New System.Windows.Forms.TextBox()
            Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
            Me._EventPeriodNumericLabel = New System.Windows.Forms.Label()
            Me._EventTypeComboBoxLabel = New System.Windows.Forms.Label()
            Me._GetPinConfigButton = New System.Windows.Forms.Button()
            Me._SetPinConfigButton = New System.Windows.Forms.Button()
            Me._PinDirectionComboBoxLabel = New System.Windows.Forms.Label()
            Me._PinEnabledCheckBox = New System.Windows.Forms.CheckBox()
            Me._PinNumericLabel = New System.Windows.Forms.Label()
            Me._PinNameTextBoxLabel = New System.Windows.Forms.Label()
            CType(Me._EventPeriodNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me._PinNumberNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            '_OpenDrainCheckBox
            '
            Me._OpenDrainCheckBox.AutoSize = True
            Me._OpenDrainCheckBox.Location = New System.Drawing.Point(182, 114)
            Me._OpenDrainCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._OpenDrainCheckBox.Name = "_OpenDrainCheckBox"
            Me._OpenDrainCheckBox.Size = New System.Drawing.Size(94, 21)
            Me._OpenDrainCheckBox.TabIndex = 10
            Me._OpenDrainCheckBox.Text = "Open Drain"
            Me._ToolTip.SetToolTip(Me._OpenDrainCheckBox, "Pin is open drain is checked.")
            Me._OpenDrainCheckBox.UseVisualStyleBackColor = True
            '
            '_PinPullupCheckBox
            '
            Me._PinPullupCheckBox.AutoSize = True
            Me._PinPullupCheckBox.Location = New System.Drawing.Point(14, 114)
            Me._PinPullupCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._PinPullupCheckBox.Name = "_PinPullupCheckBox"
            Me._PinPullupCheckBox.Size = New System.Drawing.Size(68, 21)
            Me._PinPullupCheckBox.TabIndex = 8
            Me._PinPullupCheckBox.Text = "Pull Up"
            Me._ToolTip.SetToolTip(Me._PinPullupCheckBox, "pin is pulled up if checked.")
            Me._PinPullupCheckBox.UseVisualStyleBackColor = True
            '
            '_EventPeriodNumeric
            '
            Me._EventPeriodNumeric.Location = New System.Drawing.Point(128, 186)
            Me._EventPeriodNumeric.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
            Me._EventPeriodNumeric.Name = "_EventPeriodNumeric"
            Me._EventPeriodNumeric.Size = New System.Drawing.Size(60, 25)
            Me._EventPeriodNumeric.TabIndex = 14
            Me._ToolTip.SetToolTip(Me._EventPeriodNumeric, "Selects the event period in ms")
            '
            '_PulldownCheckBox
            '
            Me._PulldownCheckBox.AutoSize = True
            Me._PulldownCheckBox.Location = New System.Drawing.Point(90, 114)
            Me._PulldownCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._PulldownCheckBox.Name = "_PulldownCheckBox"
            Me._PulldownCheckBox.Size = New System.Drawing.Size(84, 21)
            Me._PulldownCheckBox.TabIndex = 9
            Me._PulldownCheckBox.Text = "Pull Down"
            Me._ToolTip.SetToolTip(Me._PulldownCheckBox, "Pin is pulled down if checked")
            Me._PulldownCheckBox.UseVisualStyleBackColor = True
            '
            '_DebounceEnabledCheckBox
            '
            Me._DebounceEnabledCheckBox.AutoSize = True
            Me._DebounceEnabledCheckBox.Location = New System.Drawing.Point(121, 41)
            Me._DebounceEnabledCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._DebounceEnabledCheckBox.Name = "_DebounceEnabledCheckBox"
            Me._DebounceEnabledCheckBox.Size = New System.Drawing.Size(137, 21)
            Me._DebounceEnabledCheckBox.TabIndex = 5
            Me._DebounceEnabledCheckBox.Text = "Debounce Enabled"
            Me._ToolTip.SetToolTip(Me._DebounceEnabledCheckBox, "Debounce is enabled if checked.")
            Me._DebounceEnabledCheckBox.UseVisualStyleBackColor = True
            '
            '_EventTypeComboBox
            '
            Me._EventTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me._EventTypeComboBox.FormattingEnabled = True
            Me._EventTypeComboBox.Location = New System.Drawing.Point(89, 151)
            Me._EventTypeComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._EventTypeComboBox.Name = "_EventTypeComboBox"
            Me._EventTypeComboBox.Size = New System.Drawing.Size(101, 25)
            Me._EventTypeComboBox.TabIndex = 12
            Me._ToolTip.SetToolTip(Me._EventTypeComboBox, "Selects the event type.")
            '
            '_PinDirectionComboBox
            '
            Me._PinDirectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me._PinDirectionComboBox.FormattingEnabled = True
            Me._PinDirectionComboBox.Location = New System.Drawing.Point(76, 77)
            Me._PinDirectionComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._PinDirectionComboBox.Name = "_PinDirectionComboBox"
            Me._PinDirectionComboBox.Size = New System.Drawing.Size(79, 25)
            Me._PinDirectionComboBox.TabIndex = 7
            Me._ToolTip.SetToolTip(Me._PinDirectionComboBox, "Selects pin direction.")
            '
            '_PinNumberNumeric
            '
            Me._PinNumberNumeric.Location = New System.Drawing.Point(48, 3)
            Me._PinNumberNumeric.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
            Me._PinNumberNumeric.Name = "_PinNumberNumeric"
            Me._PinNumberNumeric.Size = New System.Drawing.Size(35, 25)
            Me._PinNumberNumeric.TabIndex = 1
            Me._ToolTip.SetToolTip(Me._PinNumberNumeric, "Pin number")
            '
            '_PinNameTextBox
            '
            Me._PinNameTextBox.Location = New System.Drawing.Point(145, 3)
            Me._PinNameTextBox.Name = "_PinNameTextBox"
            Me._PinNameTextBox.Size = New System.Drawing.Size(131, 25)
            Me._PinNameTextBox.TabIndex = 3
            Me._ToolTip.SetToolTip(Me._PinNameTextBox, "Pin name")
            '
            '_ErrorProvider
            '
            Me._ErrorProvider.ContainerControl = Me
            '
            '_EventPeriodNumericLabel
            '
            Me._EventPeriodNumericLabel.AutoSize = True
            Me._EventPeriodNumericLabel.Location = New System.Drawing.Point(13, 190)
            Me._EventPeriodNumericLabel.Name = "_EventPeriodNumericLabel"
            Me._EventPeriodNumericLabel.Size = New System.Drawing.Size(113, 17)
            Me._EventPeriodNumericLabel.TabIndex = 13
            Me._EventPeriodNumericLabel.Text = "Event Period [ms]:"
            Me._EventPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_EventTypeComboBoxLabel
            '
            Me._EventTypeComboBoxLabel.AutoSize = True
            Me._EventTypeComboBoxLabel.Location = New System.Drawing.Point(13, 155)
            Me._EventTypeComboBoxLabel.Name = "_EventTypeComboBoxLabel"
            Me._EventTypeComboBoxLabel.Size = New System.Drawing.Size(73, 17)
            Me._EventTypeComboBoxLabel.TabIndex = 11
            Me._EventTypeComboBoxLabel.Text = "Event Type:"
            Me._EventTypeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_GetPinConfigButton
            '
            Me._GetPinConfigButton.Location = New System.Drawing.Point(215, 148)
            Me._GetPinConfigButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._GetPinConfigButton.Name = "_GetPinConfigButton"
            Me._GetPinConfigButton.Size = New System.Drawing.Size(61, 30)
            Me._GetPinConfigButton.TabIndex = 15
            Me._GetPinConfigButton.Text = "Get"
            Me._GetPinConfigButton.UseVisualStyleBackColor = True
            '
            '_SetPinConfigButton
            '
            Me._SetPinConfigButton.Location = New System.Drawing.Point(215, 186)
            Me._SetPinConfigButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._SetPinConfigButton.Name = "_SetPinConfigButton"
            Me._SetPinConfigButton.Size = New System.Drawing.Size(61, 30)
            Me._SetPinConfigButton.TabIndex = 16
            Me._SetPinConfigButton.Text = "Set"
            Me._SetPinConfigButton.UseVisualStyleBackColor = True
            '
            '_PinDirectionComboBoxLabel
            '
            Me._PinDirectionComboBoxLabel.AutoSize = True
            Me._PinDirectionComboBoxLabel.Location = New System.Drawing.Point(11, 81)
            Me._PinDirectionComboBoxLabel.Name = "_PinDirectionComboBoxLabel"
            Me._PinDirectionComboBoxLabel.Size = New System.Drawing.Size(63, 17)
            Me._PinDirectionComboBoxLabel.TabIndex = 6
            Me._PinDirectionComboBoxLabel.Text = "Direction:"
            Me._PinDirectionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_PinEnabledCheckBox
            '
            Me._PinEnabledCheckBox.AutoSize = True
            Me._PinEnabledCheckBox.Location = New System.Drawing.Point(39, 41)
            Me._PinEnabledCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me._PinEnabledCheckBox.Name = "_PinEnabledCheckBox"
            Me._PinEnabledCheckBox.Size = New System.Drawing.Size(74, 21)
            Me._PinEnabledCheckBox.TabIndex = 4
            Me._PinEnabledCheckBox.Text = "Enabled"
            Me._PinEnabledCheckBox.UseVisualStyleBackColor = True
            '
            '_PinNumericLabel
            '
            Me._PinNumericLabel.AutoSize = True
            Me._PinNumericLabel.Location = New System.Drawing.Point(5, 7)
            Me._PinNumericLabel.Name = "_PinNumericLabel"
            Me._PinNumericLabel.Size = New System.Drawing.Size(40, 17)
            Me._PinNumericLabel.TabIndex = 0
            Me._PinNumericLabel.Text = "Pin #:"
            Me._PinNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
            '
            '_PinNameTextBoxLabel
            '
            Me._PinNameTextBoxLabel.AutoSize = True
            Me._PinNameTextBoxLabel.Location = New System.Drawing.Point(97, 7)
            Me._PinNameTextBoxLabel.Name = "_PinNameTextBoxLabel"
            Me._PinNameTextBoxLabel.Size = New System.Drawing.Size(46, 17)
            Me._PinNameTextBoxLabel.TabIndex = 2
            Me._PinNameTextBoxLabel.Text = "Name:"
            '
            'GpioPinConfigControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me._PinNameTextBox)
            Me.Controls.Add(Me._PinNameTextBoxLabel)
            Me.Controls.Add(Me._PinNumberNumeric)
            Me.Controls.Add(Me._OpenDrainCheckBox)
            Me.Controls.Add(Me._PinPullupCheckBox)
            Me.Controls.Add(Me._EventPeriodNumeric)
            Me.Controls.Add(Me._PulldownCheckBox)
            Me.Controls.Add(Me._DebounceEnabledCheckBox)
            Me.Controls.Add(Me._EventPeriodNumericLabel)
            Me.Controls.Add(Me._EventTypeComboBoxLabel)
            Me.Controls.Add(Me._EventTypeComboBox)
            Me.Controls.Add(Me._GetPinConfigButton)
            Me.Controls.Add(Me._SetPinConfigButton)
            Me.Controls.Add(Me._PinDirectionComboBox)
            Me.Controls.Add(Me._PinDirectionComboBoxLabel)
            Me.Controls.Add(Me._PinEnabledCheckBox)
            Me.Controls.Add(Me._PinNumericLabel)
            Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me.Name = "GpioPinConfigControl"
            Me.Size = New System.Drawing.Size(283, 222)
            CType(Me._EventPeriodNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me._PinNumberNumeric, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Private WithEvents _ToolTip As Windows.Forms.ToolTip
        Private WithEvents _ErrorProvider As Windows.Forms.ErrorProvider
        Private WithEvents _PinNameTextBox As Windows.Forms.TextBox
        Private WithEvents _PinNameTextBoxLabel As Windows.Forms.Label
        Private WithEvents _PinNumberNumeric As Windows.Forms.NumericUpDown
        Private WithEvents _PinNumericLabel As Windows.Forms.Label
        Private WithEvents _PinEnabledCheckBox As Windows.Forms.CheckBox
        Private WithEvents _PinDirectionComboBoxLabel As Windows.Forms.Label
        Private WithEvents _PinDirectionComboBox As Windows.Forms.ComboBox
        Private WithEvents _SetPinConfigButton As Windows.Forms.Button
        Private WithEvents _GetPinConfigButton As Windows.Forms.Button
        Private WithEvents _EventTypeComboBox As Windows.Forms.ComboBox
        Private WithEvents _EventTypeComboBoxLabel As Windows.Forms.Label
        Private WithEvents _EventPeriodNumericLabel As Windows.Forms.Label
        Private WithEvents _DebounceEnabledCheckBox As Windows.Forms.CheckBox
        Private WithEvents _PulldownCheckBox As Windows.Forms.CheckBox
        Private WithEvents _EventPeriodNumeric As Windows.Forms.NumericUpDown
        Private WithEvents _PinPullupCheckBox As Windows.Forms.CheckBox
        Private WithEvents _OpenDrainCheckBox As Windows.Forms.CheckBox
    End Class
