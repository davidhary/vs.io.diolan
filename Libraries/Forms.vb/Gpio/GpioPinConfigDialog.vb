Imports System.Windows.Forms

''' <summary> Dialog for setting the gpio pin configuration. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-04-07 </para>
''' </remarks>
Public Class GpioPinConfigDialog
    Inherits isr.Core.Forma.FormBase

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Shows the dialog. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="owner"> The owner. </param>
    ''' <param name="pin">   The pin. </param>
    ''' <returns> A DialogResult. </returns>
    Public Overloads Function ShowDialog(ByVal owner As IWin32Window, ByVal pin As Gpio.GpioPin) As DialogResult
        Me._GpioPinConfigControl.Configure(pin)
        Return MyBase.ShowDialog(owner)
    End Function

End Class
