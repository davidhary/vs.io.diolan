﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GpioView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._BottomToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TabComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._SelectServerButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ServerNameTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._DefaultServerMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ConnectServerButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenDeviceModalityButton = New System.Windows.Forms.ToolStripButton()
        Me._SelectDeviceSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DevicesComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._DeviceInfoTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._Tabs = New isr.Diolan.Forms.ExtendedTabControl()
        Me._PrimaryTabPage = New System.Windows.Forms.TabPage()
        Me._PrimaryTabLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._SubsystemGroupBox = New System.Windows.Forms.GroupBox()
        Me._DebounceNumeric = New System.Windows.Forms.NumericUpDown()
        Me._GetDebounceButton = New System.Windows.Forms.Button()
        Me._SetDebounceButton = New System.Windows.Forms.Button()
        Me._DebounceNumericLabel = New System.Windows.Forms.Label()
        Me._PinTabPage = New System.Windows.Forms.TabPage()
        Me._PinTabLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._PinConfigGroupBox = New System.Windows.Forms.GroupBox()
        Me._OpenDrainCheckBox = New System.Windows.Forms.CheckBox()
        Me._PinPullupCheckBox = New System.Windows.Forms.CheckBox()
        Me._EventPeriodNumeric = New System.Windows.Forms.NumericUpDown()
        Me._PulldownCheckBox = New System.Windows.Forms.CheckBox()
        Me._DebounceEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._EventPeriodNumericLabel = New System.Windows.Forms.Label()
        Me._EventTypeComboBoxLabel = New System.Windows.Forms.Label()
        Me._EventTypeComboBox = New System.Windows.Forms.ComboBox()
        Me._GetPinConfigButton = New System.Windows.Forms.Button()
        Me._SetPinConfigButton = New System.Windows.Forms.Button()
        Me._PinDirectionComboBox = New System.Windows.Forms.ComboBox()
        Me._PinDirectionComboBoxLabel = New System.Windows.Forms.Label()
        Me._PinEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._PinComboBox = New System.Windows.Forms.ComboBox()
        Me._PinComboBoxLabel = New System.Windows.Forms.Label()
        Me._EventLogTabPage = New System.Windows.Forms.TabPage()
        Me._EventLogTextBox = New System.Windows.Forms.TextBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ToolStripContainer = New System.Windows.Forms.ToolStripContainer()
        Me._GpioPinControl2 = New isr.Diolan.Forms.GpioPinControl()
        Me._GpioPinControl1 = New isr.Diolan.Forms.GpioPinControl()
        Me._BottomToolStrip.SuspendLayout()
        Me._Tabs.SuspendLayout()
        Me._PrimaryTabPage.SuspendLayout()
        Me._PrimaryTabLayout.SuspendLayout()
        Me._SubsystemGroupBox.SuspendLayout()
        CType(Me._DebounceNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._PinTabPage.SuspendLayout()
        Me._PinTabLayout.SuspendLayout()
        Me._PinConfigGroupBox.SuspendLayout()
        CType(Me._EventPeriodNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._EventLogTabPage.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ToolStripContainer.BottomToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.ContentPanel.SuspendLayout()
        Me._ToolStripContainer.SuspendLayout()
        Me.SuspendLayout()
        '
        '_BottomToolStrip
        '
        Me._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._BottomToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TabComboBox, Me._SelectServerButton, Me._ConnectServerButton, Me._OpenDeviceModalityButton, Me._SelectDeviceSplitButton, Me._DeviceInfoTextBox})
        Me._BottomToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._BottomToolStrip.Name = "_BottomToolStrip"
        Me._BottomToolStrip.Size = New System.Drawing.Size(417, 29)
        Me._BottomToolStrip.Stretch = True
        Me._BottomToolStrip.TabIndex = 0
        '
        '_TabComboBox
        '
        Me._TabComboBox.Name = "_TabComboBox"
        Me._TabComboBox.Size = New System.Drawing.Size(81, 29)
        Me._TabComboBox.ToolTipText = "Select panel"
        '
        '_SelectServerButton
        '
        Me._SelectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectServerButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServerNameTextBox, Me._DefaultServerMenuItem})
        Me._SelectServerButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Network_server
        Me._SelectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectServerButton.Name = "_SelectServerButton"
        Me._SelectServerButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectServerButton.Text = "Select Server"
        '
        '_ServerNameTextBox
        '
        Me._ServerNameTextBox.Name = "_ServerNameTextBox"
        Me._ServerNameTextBox.Size = New System.Drawing.Size(100, 23)
        Me._ServerNameTextBox.Text = "localhost:9656"
        '
        '_DefaultServerMenuItem
        '
        Me._DefaultServerMenuItem.Checked = True
        Me._DefaultServerMenuItem.CheckOnClick = True
        Me._DefaultServerMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._DefaultServerMenuItem.Name = "_DefaultServerMenuItem"
        Me._DefaultServerMenuItem.Size = New System.Drawing.Size(198, 22)
        Me._DefaultServerMenuItem.Text = "User Default Server:Port"
        '
        '_ConnectServerButton
        '
        Me._ConnectServerButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ConnectServerButton.ForeColor = System.Drawing.Color.Red
        Me._ConnectServerButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.WIFI_open_22_Right_Up
        Me._ConnectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ConnectServerButton.Name = "_ConnectServerButton"
        Me._ConnectServerButton.Size = New System.Drawing.Size(41, 26)
        Me._ConnectServerButton.Text = "X"
        Me._ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me._ConnectServerButton.ToolTipText = "Connect or disconnect serve and show attached devices."
        '
        '_OpenDeviceModalityButton
        '
        Me._OpenDeviceModalityButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._OpenDeviceModalityButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._OpenDeviceModalityButton.Enabled = False
        Me._OpenDeviceModalityButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.User_offline_2
        Me._OpenDeviceModalityButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._OpenDeviceModalityButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton"
        Me._OpenDeviceModalityButton.Size = New System.Drawing.Size(26, 26)
        Me._OpenDeviceModalityButton.Text = "Open"
        Me._OpenDeviceModalityButton.ToolTipText = "Open or close the device."
        '
        '_SelectDeviceSplitButton
        '
        Me._SelectDeviceSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._SelectDeviceSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectDeviceSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DevicesComboBox})
        Me._SelectDeviceSplitButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Network_server_database
        Me._SelectDeviceSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton"
        Me._SelectDeviceSplitButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectDeviceSplitButton.Text = "Device"
        Me._SelectDeviceSplitButton.ToolTipText = "Select Device"
        '
        '_DevicesComboBox
        '
        Me._DevicesComboBox.Name = "_DevicesComboBox"
        Me._DevicesComboBox.Size = New System.Drawing.Size(121, 23)
        Me._DevicesComboBox.Text = "DLN-4M.1.1"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(100, 29)
        Me._DeviceInfoTextBox.Text = "closed"
        Me._DeviceInfoTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._PrimaryTabPage)
        Me._Tabs.Controls.Add(Me._PinTabPage)
        Me._Tabs.Controls.Add(Me._EventLogTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.HideTabHeaders = True
        Me._Tabs.Location = New System.Drawing.Point(0, 57)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(417, 395)
        Me._Tabs.TabIndex = 0
        '
        '_PrimaryTabPage
        '
        Me._PrimaryTabPage.Controls.Add(Me._PrimaryTabLayout)
        Me._PrimaryTabPage.Location = New System.Drawing.Point(4, 26)
        Me._PrimaryTabPage.Name = "_PrimaryTabPage"
        Me._PrimaryTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._PrimaryTabPage.Size = New System.Drawing.Size(409, 365)
        Me._PrimaryTabPage.TabIndex = 0
        Me._PrimaryTabPage.Text = "GPIO"
        Me._PrimaryTabPage.UseVisualStyleBackColor = True
        '
        '_PrimaryTabLayout
        '
        Me._PrimaryTabLayout.ColumnCount = 3
        Me._PrimaryTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PrimaryTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._PrimaryTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PrimaryTabLayout.Controls.Add(Me._SubsystemGroupBox, 1, 1)
        Me._PrimaryTabLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._PrimaryTabLayout.Location = New System.Drawing.Point(3, 3)
        Me._PrimaryTabLayout.Name = "_PrimaryTabLayout"
        Me._PrimaryTabLayout.RowCount = 3
        Me._PrimaryTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PrimaryTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._PrimaryTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PrimaryTabLayout.Size = New System.Drawing.Size(403, 359)
        Me._PrimaryTabLayout.TabIndex = 8
        '
        '_SubsystemGroupBox
        '
        Me._SubsystemGroupBox.Controls.Add(Me._DebounceNumeric)
        Me._SubsystemGroupBox.Controls.Add(Me._GetDebounceButton)
        Me._SubsystemGroupBox.Controls.Add(Me._SetDebounceButton)
        Me._SubsystemGroupBox.Controls.Add(Me._DebounceNumericLabel)
        Me._SubsystemGroupBox.Location = New System.Drawing.Point(24, 147)
        Me._SubsystemGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SubsystemGroupBox.Name = "_SubsystemGroupBox"
        Me._SubsystemGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SubsystemGroupBox.Size = New System.Drawing.Size(355, 64)
        Me._SubsystemGroupBox.TabIndex = 6
        Me._SubsystemGroupBox.TabStop = False
        Me._SubsystemGroupBox.Text = "GPIO Subsystem Settings"
        '
        '_DebounceNumeric
        '
        Me._DebounceNumeric.Location = New System.Drawing.Point(151, 22)
        Me._DebounceNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DebounceNumeric.Maximum = New Decimal(New Integer() {32000, 0, 0, 0})
        Me._DebounceNumeric.Name = "_DebounceNumeric"
        Me._DebounceNumeric.Size = New System.Drawing.Size(63, 25)
        Me._DebounceNumeric.TabIndex = 1
        '
        '_GetDebounceButton
        '
        Me._GetDebounceButton.Location = New System.Drawing.Point(287, 19)
        Me._GetDebounceButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GetDebounceButton.Name = "_GetDebounceButton"
        Me._GetDebounceButton.Size = New System.Drawing.Size(61, 30)
        Me._GetDebounceButton.TabIndex = 3
        Me._GetDebounceButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetDebounceButton, "Gets the debounce interval")
        Me._GetDebounceButton.UseVisualStyleBackColor = True
        '
        '_SetDebounceButton
        '
        Me._SetDebounceButton.Location = New System.Drawing.Point(219, 19)
        Me._SetDebounceButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetDebounceButton.Name = "_SetDebounceButton"
        Me._SetDebounceButton.Size = New System.Drawing.Size(61, 30)
        Me._SetDebounceButton.TabIndex = 2
        Me._SetDebounceButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetDebounceButton, "Sets the debounce interval")
        Me._SetDebounceButton.UseVisualStyleBackColor = True
        '
        '_DebounceNumericLabel
        '
        Me._DebounceNumericLabel.AutoSize = True
        Me._DebounceNumericLabel.Location = New System.Drawing.Point(7, 26)
        Me._DebounceNumericLabel.Name = "_DebounceNumericLabel"
        Me._DebounceNumericLabel.Size = New System.Drawing.Size(142, 17)
        Me._DebounceNumericLabel.TabIndex = 0
        Me._DebounceNumericLabel.Text = "Debounce Interval [µs]:"
        Me._DebounceNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PinTabPage
        '
        Me._PinTabPage.Controls.Add(Me._PinTabLayout)
        Me._PinTabPage.Location = New System.Drawing.Point(4, 26)
        Me._PinTabPage.Name = "_PinTabPage"
        Me._PinTabPage.Size = New System.Drawing.Size(409, 365)
        Me._PinTabPage.TabIndex = 2
        Me._PinTabPage.Text = "Pin"
        Me._PinTabPage.UseVisualStyleBackColor = True
        '
        '_PinTabLayout
        '
        Me._PinTabLayout.ColumnCount = 3
        Me._PinTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PinTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._PinTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PinTabLayout.Controls.Add(Me._PinConfigGroupBox, 1, 1)
        Me._PinTabLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._PinTabLayout.Location = New System.Drawing.Point(0, 0)
        Me._PinTabLayout.Name = "_PinTabLayout"
        Me._PinTabLayout.RowCount = 3
        Me._PinTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PinTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._PinTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PinTabLayout.Size = New System.Drawing.Size(409, 365)
        Me._PinTabLayout.TabIndex = 9
        '
        '_PinConfigGroupBox
        '
        Me._PinConfigGroupBox.Controls.Add(Me._OpenDrainCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._PinPullupCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._EventPeriodNumeric)
        Me._PinConfigGroupBox.Controls.Add(Me._PulldownCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._DebounceEnabledCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._EventPeriodNumericLabel)
        Me._PinConfigGroupBox.Controls.Add(Me._EventTypeComboBoxLabel)
        Me._PinConfigGroupBox.Controls.Add(Me._EventTypeComboBox)
        Me._PinConfigGroupBox.Controls.Add(Me._GetPinConfigButton)
        Me._PinConfigGroupBox.Controls.Add(Me._SetPinConfigButton)
        Me._PinConfigGroupBox.Controls.Add(Me._PinDirectionComboBox)
        Me._PinConfigGroupBox.Controls.Add(Me._PinDirectionComboBoxLabel)
        Me._PinConfigGroupBox.Controls.Add(Me._PinEnabledCheckBox)
        Me._PinConfigGroupBox.Controls.Add(Me._PinComboBox)
        Me._PinConfigGroupBox.Controls.Add(Me._PinComboBoxLabel)
        Me._PinConfigGroupBox.Location = New System.Drawing.Point(48, 58)
        Me._PinConfigGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinConfigGroupBox.Name = "_PinConfigGroupBox"
        Me._PinConfigGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinConfigGroupBox.Size = New System.Drawing.Size(313, 249)
        Me._PinConfigGroupBox.TabIndex = 8
        Me._PinConfigGroupBox.TabStop = False
        Me._PinConfigGroupBox.Text = "Pin Configuration"
        '
        '_OpenDrainCheckBox
        '
        Me._OpenDrainCheckBox.AutoSize = True
        Me._OpenDrainCheckBox.Location = New System.Drawing.Point(187, 139)
        Me._OpenDrainCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._OpenDrainCheckBox.Name = "_OpenDrainCheckBox"
        Me._OpenDrainCheckBox.Size = New System.Drawing.Size(94, 21)
        Me._OpenDrainCheckBox.TabIndex = 10
        Me._OpenDrainCheckBox.Text = "Open Drain"
        Me._ToolTip.SetToolTip(Me._OpenDrainCheckBox, "Pin is open drain is checked.")
        Me._OpenDrainCheckBox.UseVisualStyleBackColor = True
        '
        '_PinPullupCheckBox
        '
        Me._PinPullupCheckBox.AutoSize = True
        Me._PinPullupCheckBox.Location = New System.Drawing.Point(19, 139)
        Me._PinPullupCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinPullupCheckBox.Name = "_PinPullupCheckBox"
        Me._PinPullupCheckBox.Size = New System.Drawing.Size(68, 21)
        Me._PinPullupCheckBox.TabIndex = 8
        Me._PinPullupCheckBox.Text = "Pull Up"
        Me._ToolTip.SetToolTip(Me._PinPullupCheckBox, "pin is pulled up if checked.")
        Me._PinPullupCheckBox.UseVisualStyleBackColor = True
        '
        '_EventPeriodNumeric
        '
        Me._EventPeriodNumeric.Location = New System.Drawing.Point(133, 211)
        Me._EventPeriodNumeric.Maximum = New Decimal(New Integer() {65535, 0, 0, 0})
        Me._EventPeriodNumeric.Name = "_EventPeriodNumeric"
        Me._EventPeriodNumeric.Size = New System.Drawing.Size(60, 25)
        Me._EventPeriodNumeric.TabIndex = 14
        Me._ToolTip.SetToolTip(Me._EventPeriodNumeric, "Selects the event period in ms")
        '
        '_PulldownCheckBox
        '
        Me._PulldownCheckBox.AutoSize = True
        Me._PulldownCheckBox.Location = New System.Drawing.Point(95, 139)
        Me._PulldownCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PulldownCheckBox.Name = "_PulldownCheckBox"
        Me._PulldownCheckBox.Size = New System.Drawing.Size(84, 21)
        Me._PulldownCheckBox.TabIndex = 9
        Me._PulldownCheckBox.Text = "Pull Down"
        Me._ToolTip.SetToolTip(Me._PulldownCheckBox, "Pin is pulled down if checked")
        Me._PulldownCheckBox.UseVisualStyleBackColor = True
        '
        '_DebounceEnabledCheckBox
        '
        Me._DebounceEnabledCheckBox.AutoSize = True
        Me._DebounceEnabledCheckBox.Location = New System.Drawing.Point(126, 66)
        Me._DebounceEnabledCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DebounceEnabledCheckBox.Name = "_DebounceEnabledCheckBox"
        Me._DebounceEnabledCheckBox.Size = New System.Drawing.Size(137, 21)
        Me._DebounceEnabledCheckBox.TabIndex = 5
        Me._DebounceEnabledCheckBox.Text = "Debounce Enabled"
        Me._ToolTip.SetToolTip(Me._DebounceEnabledCheckBox, "Debounce is enabled if checked.")
        Me._DebounceEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_EventPeriodNumericLabel
        '
        Me._EventPeriodNumericLabel.AutoSize = True
        Me._EventPeriodNumericLabel.Location = New System.Drawing.Point(18, 215)
        Me._EventPeriodNumericLabel.Name = "_EventPeriodNumericLabel"
        Me._EventPeriodNumericLabel.Size = New System.Drawing.Size(113, 17)
        Me._EventPeriodNumericLabel.TabIndex = 13
        Me._EventPeriodNumericLabel.Text = "Event Period [ms]:"
        Me._EventPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EventTypeComboBoxLabel
        '
        Me._EventTypeComboBoxLabel.AutoSize = True
        Me._EventTypeComboBoxLabel.Location = New System.Drawing.Point(18, 180)
        Me._EventTypeComboBoxLabel.Name = "_EventTypeComboBoxLabel"
        Me._EventTypeComboBoxLabel.Size = New System.Drawing.Size(73, 17)
        Me._EventTypeComboBoxLabel.TabIndex = 11
        Me._EventTypeComboBoxLabel.Text = "Event Type:"
        Me._EventTypeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EventTypeComboBox
        '
        Me._EventTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._EventTypeComboBox.FormattingEnabled = True
        Me._EventTypeComboBox.Location = New System.Drawing.Point(94, 176)
        Me._EventTypeComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EventTypeComboBox.Name = "_EventTypeComboBox"
        Me._EventTypeComboBox.Size = New System.Drawing.Size(101, 25)
        Me._EventTypeComboBox.TabIndex = 12
        Me._ToolTip.SetToolTip(Me._EventTypeComboBox, "Selects the event type.")
        '
        '_GetPinConfigButton
        '
        Me._GetPinConfigButton.Location = New System.Drawing.Point(158, 22)
        Me._GetPinConfigButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GetPinConfigButton.Name = "_GetPinConfigButton"
        Me._GetPinConfigButton.Size = New System.Drawing.Size(61, 30)
        Me._GetPinConfigButton.TabIndex = 2
        Me._GetPinConfigButton.Text = "Get"
        Me._GetPinConfigButton.UseVisualStyleBackColor = True
        '
        '_SetPinConfigButton
        '
        Me._SetPinConfigButton.Location = New System.Drawing.Point(236, 22)
        Me._SetPinConfigButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetPinConfigButton.Name = "_SetPinConfigButton"
        Me._SetPinConfigButton.Size = New System.Drawing.Size(61, 30)
        Me._SetPinConfigButton.TabIndex = 3
        Me._SetPinConfigButton.Text = "Set"
        Me._SetPinConfigButton.UseVisualStyleBackColor = True
        '
        '_PinDirectionComboBox
        '
        Me._PinDirectionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PinDirectionComboBox.FormattingEnabled = True
        Me._PinDirectionComboBox.Location = New System.Drawing.Point(81, 102)
        Me._PinDirectionComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinDirectionComboBox.Name = "_PinDirectionComboBox"
        Me._PinDirectionComboBox.Size = New System.Drawing.Size(79, 25)
        Me._PinDirectionComboBox.TabIndex = 7
        Me._ToolTip.SetToolTip(Me._PinDirectionComboBox, "Selects pin direction.")
        '
        '_PinDirectionComboBoxLabel
        '
        Me._PinDirectionComboBoxLabel.AutoSize = True
        Me._PinDirectionComboBoxLabel.Location = New System.Drawing.Point(16, 106)
        Me._PinDirectionComboBoxLabel.Name = "_PinDirectionComboBoxLabel"
        Me._PinDirectionComboBoxLabel.Size = New System.Drawing.Size(63, 17)
        Me._PinDirectionComboBoxLabel.TabIndex = 6
        Me._PinDirectionComboBoxLabel.Text = "Direction:"
        Me._PinDirectionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PinEnabledCheckBox
        '
        Me._PinEnabledCheckBox.AutoSize = True
        Me._PinEnabledCheckBox.Location = New System.Drawing.Point(44, 66)
        Me._PinEnabledCheckBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinEnabledCheckBox.Name = "_PinEnabledCheckBox"
        Me._PinEnabledCheckBox.Size = New System.Drawing.Size(74, 21)
        Me._PinEnabledCheckBox.TabIndex = 4
        Me._PinEnabledCheckBox.Text = "Enabled"
        Me._PinEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_PinComboBox
        '
        Me._PinComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PinComboBox.FormattingEnabled = True
        Me._PinComboBox.Location = New System.Drawing.Point(40, 25)
        Me._PinComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._PinComboBox.Name = "_PinComboBox"
        Me._PinComboBox.Size = New System.Drawing.Size(100, 25)
        Me._PinComboBox.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._PinComboBox, "Selects the pin number.")
        '
        '_PinComboBoxLabel
        '
        Me._PinComboBoxLabel.AutoSize = True
        Me._PinComboBoxLabel.Location = New System.Drawing.Point(10, 29)
        Me._PinComboBoxLabel.Name = "_PinComboBoxLabel"
        Me._PinComboBoxLabel.Size = New System.Drawing.Size(28, 17)
        Me._PinComboBoxLabel.TabIndex = 0
        Me._PinComboBoxLabel.Text = "Pin:"
        Me._PinComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EventLogTabPage
        '
        Me._EventLogTabPage.Controls.Add(Me._EventLogTextBox)
        Me._EventLogTabPage.Location = New System.Drawing.Point(4, 22)
        Me._EventLogTabPage.Name = "_EventLogTabPage"
        Me._EventLogTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._EventLogTabPage.Size = New System.Drawing.Size(409, 369)
        Me._EventLogTabPage.TabIndex = 1
        Me._EventLogTabPage.Text = "Events"
        Me._EventLogTabPage.UseVisualStyleBackColor = True
        '
        '_EventLogTextBox
        '
        Me._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EventLogTextBox.Location = New System.Drawing.Point(3, 3)
        Me._EventLogTextBox.Multiline = True
        Me._EventLogTextBox.Name = "_EventLogTextBox"
        Me._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._EventLogTextBox.Size = New System.Drawing.Size(403, 363)
        Me._EventLogTextBox.TabIndex = 0
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_ToolStripContainer
        '
        '
        '_ToolStripContainer.BottomToolStripPanel
        '
        Me._ToolStripContainer.BottomToolStripPanel.Controls.Add(Me._BottomToolStrip)
        '
        '_ToolStripContainer.ContentPanel
        '
        Me._ToolStripContainer.ContentPanel.Controls.Add(Me._Tabs)
        Me._ToolStripContainer.ContentPanel.Controls.Add(Me._GpioPinControl2)
        Me._ToolStripContainer.ContentPanel.Controls.Add(Me._GpioPinControl1)
        Me._ToolStripContainer.ContentPanel.Size = New System.Drawing.Size(417, 452)
        Me._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ToolStripContainer.Location = New System.Drawing.Point(0, 0)
        Me._ToolStripContainer.Name = "_ToolStripContainer"
        Me._ToolStripContainer.Size = New System.Drawing.Size(417, 506)
        Me._ToolStripContainer.TabIndex = 0
        Me._ToolStripContainer.Text = "ToolStripContainer1"
        '
        '_GpioPinControl2
        '
        Me._GpioPinControl2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._GpioPinControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me._GpioPinControl2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioPinControl2.Location = New System.Drawing.Point(0, 29)
        Me._GpioPinControl2.Modality = isr.Diolan.DeviceModalities.Gpio
        Me._GpioPinControl2.Name = "_GpioPinControl2"
        Me._GpioPinControl2.Size = New System.Drawing.Size(417, 28)
        Me._GpioPinControl2.TabIndex = 22
        '
        '_GpioPinControl1
        '
        Me._GpioPinControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me._GpioPinControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me._GpioPinControl1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioPinControl1.Location = New System.Drawing.Point(0, 0)
        Me._GpioPinControl1.Modality = isr.Diolan.DeviceModalities.Gpio
        Me._GpioPinControl1.Name = "_GpioPinControl1"
        Me._GpioPinControl1.Size = New System.Drawing.Size(417, 29)
        Me._GpioPinControl1.TabIndex = 21
        '
        'GpioPanel
        '
        Me.Controls.Add(Me._ToolStripContainer)
        Me.Name = "GpioPanel"
        Me.Size = New System.Drawing.Size(417, 506)
        Me._BottomToolStrip.ResumeLayout(False)
        Me._BottomToolStrip.PerformLayout()
        Me._Tabs.ResumeLayout(False)
        Me._PrimaryTabPage.ResumeLayout(False)
        Me._PrimaryTabLayout.ResumeLayout(False)
        Me._SubsystemGroupBox.ResumeLayout(False)
        Me._SubsystemGroupBox.PerformLayout()
        CType(Me._DebounceNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._PinTabPage.ResumeLayout(False)
        Me._PinTabLayout.ResumeLayout(False)
        Me._PinConfigGroupBox.ResumeLayout(False)
        Me._PinConfigGroupBox.PerformLayout()
        CType(Me._EventPeriodNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._EventLogTabPage.ResumeLayout(False)
        Me._EventLogTabPage.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ToolStripContainer.BottomToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.BottomToolStripPanel.PerformLayout()
        Me._ToolStripContainer.ContentPanel.ResumeLayout(False)
        Me._ToolStripContainer.ResumeLayout(False)
        Me._ToolStripContainer.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _BottomToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _DeviceInfoTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _OpenDeviceModalityButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _PrimaryTabPage As System.Windows.Forms.TabPage
    Private WithEvents _EventLogTabPage As System.Windows.Forms.TabPage
    Private WithEvents _EventLogTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ToolStripContainer As System.Windows.Forms.ToolStripContainer
    Private WithEvents _Tabs As ExtendedTabControl
    Private WithEvents _DevicesComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _SelectServerButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _ServerNameTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _DefaultServerMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SelectDeviceSplitButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _SubsystemGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DebounceNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _GetDebounceButton As System.Windows.Forms.Button
    Private WithEvents _SetDebounceButton As System.Windows.Forms.Button
    Private WithEvents _DebounceNumericLabel As System.Windows.Forms.Label
    Private WithEvents _PrimaryTabLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _PinTabPage As System.Windows.Forms.TabPage
    Private WithEvents _PinConfigGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _OpenDrainCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PinPullupCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _EventPeriodNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _PulldownCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _DebounceEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _EventPeriodNumericLabel As System.Windows.Forms.Label
    Private WithEvents _EventTypeComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _EventTypeComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _GetPinConfigButton As System.Windows.Forms.Button
    Private WithEvents _SetPinConfigButton As System.Windows.Forms.Button
    Private WithEvents _PinDirectionComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _PinDirectionComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _PinEnabledCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _PinComboBox As System.Windows.Forms.ComboBox
    Private WithEvents _PinComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _GpioPinControl2 As isr.Diolan.Forms.GpioPinControl
    Private WithEvents _GpioPinControl1 As isr.Diolan.Forms.GpioPinControl
    Private WithEvents _PinTabLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _TabComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ConnectServerButton As System.Windows.Forms.ToolStripButton

End Class
