Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Core
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Diolan.SubsystemExtensions
Imports isr.Diolan.Forms.SubsystemExtensions
Imports isr.Diolan.Gpio

''' <summary> User interface for gpio pins. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-04-03 </para>
''' </remarks>
Public Class GpioPinsView
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        ' Add any initialization after the InitializeComponent() call.
        Me._TabComboBox.ComboBox.DataSource = Me._Tabs.TabPages
        Me._TabComboBox.ComboBox.DisplayMember = "Text"

        Me._InputPins = New GpioPinCollection
        Me._OutputPins = New GpioPinCollection

    End Sub

    ''' <summary> Updates the unrendered values. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub UpdateUnrenderedValues()
        Me._EventPinValue.Text = "0"
        Me._EventPinValue.Invalidate()
        Me._SubsystemGroupBox.Text = "GPIO Subsystem Settings"
        Me._SubsystemGroupBox.Invalidate()
        Me._DebounceNumericLabel.Text = $"Debounce Interval [{Convert.ToChar(&H3BC)}s]:"
        Me._DebounceNumericLabel.Invalidate()
        Me._DebounceNumeric.Value = If(Me.IsDeviceModalityOpen, CDec(Me._Device.Gpio.Debounce), 0)
        Me._DebounceNumeric.Value += 1D
        Me._DebounceNumeric.Value -= 1D
        Me._DebounceNumeric.Invalidate()
    End Sub

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        Me.UpdateUnrenderedValues()
        Me.Invalidate()
        Windows.Forms.Application.DoEvents()
    End Sub

    ''' <summary> Gpio pins panel load. </summary>
    ''' <remarks> This is required when not calling the owner form within a Using statement. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub GpioPinsPanel_Load(sender As Object, e As EventArgs)
        Me._EventPinValue.Invalidate()
        Windows.Forms.Application.DoEvents()
        Me._DebounceNumericLabel.Invalidate()
        Windows.Forms.Application.DoEvents()
        Me._SubsystemGroupBox.Invalidate()
        Windows.Forms.Application.DoEvents()
        Me._DebounceNumeric.Invalidate()
        Windows.Forms.Application.DoEvents()
        Me.Invalidate()
        Windows.Forms.Application.DoEvents()
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            Me.OnDispose(disposing)
            If Not Me.IsDisposed AndAlso disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Me.OnCustomDispose(disposing)
        If Not Me.IsDisposed AndAlso disposing Then
            If Me._Device IsNot Nothing Then Me._Device = Nothing
            If Me._DeviceConnector IsNot Nothing Then Me._DeviceConnector.Dispose() : Me._DeviceConnector = Nothing
            If Me._ServerConnector IsNot Nothing Then Me._ServerConnector = Nothing
        End If
    End Sub

#End Region

#Region " KNOWN STATE "

    ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Sub InitializeKnownState()
        Me.OnServerConnectionChanged()
    End Sub

#End Region

#Region " SERVER "

    ''' <summary> Raises the server connector property changed event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnPropertyChanged(ByVal sender As LocalhostConnector, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim details As String = String.Empty
        Try
            If sender Is Nothing Then
                details = "Sender is empty"
            Else
                If e IsNot Nothing Then
                    Select Case e.PropertyName
                        Case NameOf(Diolan.LocalhostConnector.IsConnected)
                            Me.OnServerConnectionChanged()
                        Case NameOf(Diolan.LocalhostConnector.AttachedDevicesCount)
                            Me.OnServerAttachmentChanged()
                    End Select
                End If
            End If
        Catch ex As Exception
            details = ex.ToFullBlownString
        Finally
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton, details)
        End Try
    End Sub

    ''' <summary> Server connector property changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub ServerConnector_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _ServerConnector.PropertyChanged
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ServerConnector_PropertyChanged), New Object() {sender, e})
        End If
        Me.OnPropertyChanged(TryCast(sender, LocalhostConnector), e)
    End Sub

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _ServerConnector As LocalhostConnector
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the server connector. </summary>
    ''' <value> The server connector. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ServerConnector As LocalhostConnector
        Get
            Return Me._ServerConnector
        End Get
        Set(value As LocalhostConnector)
            Me._ServerConnector = value
        End Set
    End Property

    ''' <summary> Connects a server button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConnectServerButton_Click(sender As System.Object, e As System.EventArgs) Handles _ConnectServerButton.Click
        If Me.ServerConnector.IsConnected Then
            If Not Me.ServerConnector.HasAttachedDevices Then
                Me.ServerConnector.Disconnect()
            End If
        Else
            Me.ServerConnector.Connect()
        End If
    End Sub

    ''' <summary> Updates the server connection state. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub UpdateServerConnectionState()
        Me._OpenDeviceModalityButton.Enabled = Me.ServerConnector.IsConnected
        Me._SelectDeviceSplitButton.Enabled = Me.ServerConnector.IsConnected
    End Sub

    ''' <summary> Executes the server connection changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnServerConnectionChanged()
        If Me.ServerConnector.IsConnected Then
            Me.ServerConnector.Connection.ListDevicesById(Me._DevicesComboBox)
            Me._DevicesComboBox.SelectedIndex = 0
        End If
        Me.UpdateServerConnectionState()
        Me.OnServerAttachmentChanged()
    End Sub

    ''' <summary> Executes the server attachment changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub OnServerAttachmentChanged()
        Me._ConnectServerButton.Image = My.Resources.WIFI_open_22
        If Me.ServerConnector.IsConnected Then
            Me._ConnectServerButton.Text = Me.ServerConnector.AttachedDevicesCount.ToString
            Me._ConnectServerButton.ForeColor = Drawing.Color.Black
        Else
            Me._ConnectServerButton.ForeColor = Drawing.Color.Red
            Me._ConnectServerButton.Text = "X"
        End If
    End Sub

#End Region

#Region " DEVICE CONNECTOR "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DeviceConnector As DeviceConnector
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the device connector. </summary>
    ''' <value> The device connector. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DeviceConnector As DeviceConnector
        Get
            Return Me._DeviceConnector
        End Get
        Set(value As DeviceConnector)
            Me._DeviceConnector = value
        End Set
    End Property

    ''' <summary> Device connector device closed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceClosed(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceClosed
        Me.OnServerAttachmentChanged()
        Me._DeviceInfoTextBox.Text = "closed"
        Me.OnModalityClosed()
    End Sub

    ''' <summary> Device connector device closing. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub DeviceConnector_DeviceClosing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _DeviceConnector.DeviceClosing
        Me.OnModalityClosing(e)
    End Sub

    ''' <summary> Device connector device opened. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceOpened(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceOpened

        Me.OnServerAttachmentChanged()
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> Gets the device. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    <CLSCompliant(False)>
    Public ReadOnly Property Device As Dln.Device

    ''' <summary> Selected device information. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <returns> A DeviceInfo. </returns>
    Private Function SelectedDeviceInfo() As DeviceInfo
        If String.IsNullOrWhiteSpace(Me._DevicesComboBox.Text) Then
            Throw New InvalidOperationException("No devices selected")
        End If
        Return New DeviceInfo(Me._DevicesComboBox.Text)
    End Function

    ''' <summary> Attempts to open local device from the given data. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="id"> The identifier. </param>
    ''' <param name="e">  Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryOpenLocalDevice(ByVal id As Long, ByVal e As ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Try
            Me._ServerConnector = LocalhostConnector.SingleInstance
            If Me.ServerConnector.IsConnected Then
                Me._OpenDeviceModalityButton.Enabled = Me.ServerConnector.IsConnected
                Me._SelectDeviceSplitButton.Enabled = Me.ServerConnector.IsConnected
            Else
                Me.ServerConnector.Connect()
            End If
            Me._DeviceConnector = New DeviceConnector(Me._ServerConnector)
            If Me._DeviceConnector.TryOpenDevice(id, Me.Modality, e) Then
                Me.OnModalityOpening()
            Else
                Me._ErrorProvider.Annunciate(Me._TopToolStrip, e.Details)
                Me._DeviceInfoTextBox.Text = $"device #{id} failed opening"
                Me._DeviceInfoTextBox.ToolTipText = e.Details
            End If
        Catch
            Try
                Me.CloseDeviceModality()
            Catch
            End Try
            Throw
        Finally
            Me.OnModalityConnectionChanged()
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Opens the device for the modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="deviceId"> Identifier for the device. </param>
    ''' <param name="sender">   The sender. </param>
    Private Sub OpenDeviceModality(ByVal deviceId As Long, ByVal sender As Control)

        If Me.ServerConnector.IsConnected Then
            Me._OpenDeviceModalityButton.Enabled = Me.ServerConnector.IsConnected
            Me._SelectDeviceSplitButton.Enabled = Me.ServerConnector.IsConnected
        Else
            Me.ServerConnector.Connect()
        End If
        Me._DeviceConnector = New DeviceConnector(Me._ServerConnector)

        ' Open device
        Dim e As New ActionEventArgs
        If Me._DeviceConnector.TryOpenDevice(deviceId, Me.Modality, e) Then
            Me.OnModalityOpening()
        Else
            If sender IsNot Nothing Then Me._ErrorProvider.Annunciate(sender, e.Details)
            Me._DeviceInfoTextBox.Text = "No devices"
        End If
    End Sub

    ''' <summary> Closes device modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub CloseDeviceModality()
        Me._DeviceConnector.CloseDevice(Me.Modality)
        Dim e As New System.ComponentModel.CancelEventArgs
        Me.OnModalityClosing(e)
        If Not e.Cancel Then
            Me.OnModalityClosed()
        End If
    End Sub

    ''' <summary> Opens device button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenDeviceModalityButton_Click(sender As Object, e As System.EventArgs) Handles _OpenDeviceModalityButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Me.CloseDeviceModality()
            Else

                If String.IsNullOrWhiteSpace(Me._DevicesComboBox.Text) Then
                    Me._ErrorProvider.Annunciate(sender, "Select a device first")
                Else
                    Me.OpenDeviceModality(Me.SelectedDeviceInfo.Id, TryCast(sender, Control))
                End If
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        Finally
            Me.OnModalityConnectionChanged()
        End Try
    End Sub

    ''' <summary> Gets a value indicating whether a device is open. </summary>
    ''' <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
    Private ReadOnly Property IsDeviceModalityOpen As Boolean
        Get
            Return Me._Device IsNot Nothing AndAlso Me.IsModalityOpen
        End Get
    End Property

    ''' <summary> Executes the modality connection changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityConnectionChanged()
        If Me.IsDeviceModalityOpen Then
            Me._OpenDeviceModalityButton.Image = My.Resources.user_online_2
            Me._OpenDeviceModalityButton.Text = "Close"
        Else
            Me._OpenDeviceModalityButton.Image = My.Resources.user_offline_2
            Me._OpenDeviceModalityButton.Text = "Open"
        End If
    End Sub

#End Region

#Region " EVENT LOG "

    ''' <summary> Event log text box double click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EventLogTextBox_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EventLogTextBox.DoubleClick
        Me._EventLogTextBox.Clear()
    End Sub

#End Region


#Region " TABS "

    ''' <summary> Selects the tab. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TabComboBox_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles _TabComboBox.SelectedIndexChanged
        If Me._TabComboBox.SelectedIndex >= 0 AndAlso Me._TabComboBox.SelectedIndex < Me._Tabs.TabCount Then
            Me._Tabs.SelectTab(Me._Tabs.TabPages(Me._TabComboBox.SelectedIndex))
        End If

    End Sub

#End Region

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.OnModalityClosed()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " MODALITY "

    ''' <summary> The modality. </summary>
    ''' <value> The modality. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Modality As DeviceModalities = DeviceModalities.Gpio

    ''' <summary> Executes the modality closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityClosed()
        Me._Device = Nothing
        Me.OnModalityConnectionChanged()
    End Sub

    ''' <summary> Executes the modality closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnModalityClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            If Me.IsModalityOpen Then
                ' un-register event handlers for all pins
                For Each pin As Dln.Gpio.Pin In Me.Device.Gpio.Pins
                    RemoveHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                Next
            End If
        End If
    End Sub

    ''' <summary> Executes the modality opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityOpening()

        ' Assign the reference to the device
        Me._Device = Me._DeviceConnector.Device
        Me._ErrorProvider.Clear()

        ' Get port count
        If Me.Device.Gpio.Pins.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton,
                                             "Adapter '{0}' doesn't support GPIO interface.",
                                             Me.Device.Caption)
            Me._Device = Nothing
            Me._DeviceInfoTextBox.Text = "not supported"
        Else
            Me._DeviceInfoTextBox.Text = Me.Device.Caption

            'Set current context to run thread safe events in main form thread
            Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current

            ' Register event handler for all pins
            For Each pin As Dln.Gpio.Pin In Me.Device.Gpio.Pins
                AddHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
            Next

            ' Get subsystem parameters: Debounce
            Me.ReadDebounceProperties()

        End If

    End Sub

    ''' <summary> Queries if a modality is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
    Private Function IsModalityOpen() As Boolean
        Return Me._Device IsNot Nothing
    End Function

#End Region

#Region " EVENT LOG "

    ''' <summary> Handler, called when the condition met event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Condition met event information. </param>
    Private Sub ConditionMetEventHandler(ByVal sender As Object, ByVal e As Dln.Gpio.ConditionMetEventArgs)
        Me.AppendEvent(e)
    End Sub

    ''' <summary> Appends an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Condition met event information. </param>
    Private Sub AppendEvent(ByVal e As Dln.Gpio.ConditionMetEventArgs)
        If e IsNot Nothing Then
            Dim data As String = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}"

            ' This event is handled in main thread,
            ' so it is not needed to invoke when modifying form's controls.
            Me._EventLogTextBox.AppendText(data)
        End If

    End Sub

#End Region

#Region " SUBSYSTEM "

    ''' <summary> Reads the debounce properties from the device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub ReadDebounceProperties()
        ' Get subsystem parameters: Debounce
        If Me.Device.Gpio.Restrictions.Debounce = Dln.Restriction.NotSupported Then
            Me._DebounceNumeric.Enabled = False
            Me._SetDebounceButton.Enabled = False
            Me._GetDebounceButton.Enabled = False
        Else
            Me._DebounceNumeric.Value = CDec(Me._Device.Gpio.Debounce)
        End If
    End Sub

    ''' <summary> Gets debounce button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GetDebounceButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetDebounceButton.Click
        Try
            Me._ErrorProvider.Clear()

            If Me.IsDeviceModalityOpen Then
                Me._DebounceNumeric.Value = CDec(Me._Device.Gpio.Debounce)
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Sets debounce button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetDebounceButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetDebounceButton.Click
        Try

            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Me._Device.Gpio.Debounce = CInt(Me._DebounceNumeric.Value)
                Me._DebounceNumeric.Value = CDec(Me._Device.Gpio.Debounce)
            Else
                Me._ErrorProvider.Annunciate(sender, "Device not open for {0}", Me.Modality)
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " TOP TOOL BOX "

    ''' <summary> Shows the top menu. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="value"> true to value. </param>
    Public Sub ShowTopMenu(ByVal value As Boolean)
        Me._TopToolStrip.Visible = value
    End Sub

#End Region

#Region " PINS "

    ''' <summary> Gets or sets the input pins. </summary>
    ''' <value> The input pins. </value>
    Public ReadOnly Property InputPins As GpioPinCollection

    ''' <summary> Gets or sets the output pins. </summary>
    ''' <value> The output pins. </value>
    Public ReadOnly Property OutputPins As GpioPinCollection

    ''' <summary> Adds pin. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="pin"> The pin to add. </param>
    Public Sub Add(ByVal pin As GpioPin)
        If pin Is Nothing Then Throw New ArgumentNullException(NameOf(pin))
        Dim pinToolStrip As New GpioPinToolStrip()
        pinToolStrip.Assign(pin)
        If pin.Direction = PinDirection.Input Then
            GpioPinsView.Add(pinToolStrip, Me._InputPinsLayout, Me.InputPins)
        Else
            GpioPinsView.Add(pinToolStrip, Me._OutputPinsLayout, Me.OutputPins)
        End If
    End Sub

    ''' <summary> Adds pin tool strip. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pinToolStrip"> The pin tool strip. </param>
    ''' <param name="layout">       The layout. </param>
    ''' <param name="pins">         Collection of pins. </param>
    Private Shared Sub Add(ByVal pinToolStrip As GpioPinToolStrip, ByVal layout As TableLayoutPanel,
                        ByVal pins As GpioPinCollection)
        If Not pins.Any Then
            layout.Controls.Clear()
            layout.ColumnStyles.Clear()
            layout.RowStyles.Clear()
            layout.ColumnCount = 4
            layout.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 3))
            layout.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 50))
            layout.ColumnStyles.Add(New ColumnStyle(SizeType.Percent, 50))
            layout.ColumnStyles.Add(New ColumnStyle(SizeType.Absolute, 3))
            layout.RowCount = 3
            layout.RowStyles.Add(New RowStyle(SizeType.Percent, 50))
            layout.RowStyles.Add(New RowStyle(SizeType.AutoSize))
            layout.RowStyles.Add(New RowStyle(SizeType.Percent, 50))
            layout.Dock = DockStyle.Fill
            layout.Invalidate()
        End If
        pins.Add(pinToolStrip.GpioPin)
        Dim rowIndex As Integer = 1 + (pins.Count - 1) \ 2
        Dim columnIndex As Integer = 1 + (pins.Count - 1) Mod 2
        If rowIndex >= layout.RowCount - 1 Then
            layout.RowCount += 1
            ' layout.RowStyles.Insert(layout.RowStyles.Count - 2, New RowStyle(SizeType.AutoSize))
            layout.RowStyles.Insert(rowIndex, New RowStyle(SizeType.AutoSize))
            layout.Invalidate()
        End If
        layout.Controls.Add(pinToolStrip, columnIndex, rowIndex)
        pinToolStrip.Dock = DockStyle.Top
        pinToolStrip.Invalidate()
    End Sub

    ''' <summary> Creates a new pin number text box text changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NewPinNumberTextBox_TextChanged(sender As Object, e As EventArgs) Handles _NewPinNumberTextBox.TextChanged

        If Me.InitializingComponents Then Return
        Dim textBox As ToolStripTextBox = TryCast(sender, ToolStripTextBox)
        If textBox IsNot Nothing Then
            Me._AddInputPinMenuItem.Text = $"Add input pin {textBox.Text}"
            Me._AddOutputPinMenuItem.Text = $"Add output pin {textBox.Text}"
        End If
    End Sub

    ''' <summary> Adds a pin handler to 'direction'. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">    The sender. </param>
    ''' <param name="pinNumber"> The pin number. </param>
    ''' <param name="direction"> The direction. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AddPinHandler(sender As Object, ByVal pinNumber As String, ByVal direction As PinDirection)
        Try
            Me._ErrorProvider.Clear()
            Dim pinNo As Integer = 0
            If Integer.TryParse(pinNumber, pinNo) Then
                If Me.InputPins.Contains(pinNo) Then
                    Me._ErrorProvider.Annunciate(sender, $"Pin {pinNo} already exists as input")
                ElseIf Me.OutputPins.Contains(pinNo) Then
                    Me._ErrorProvider.Annunciate(sender, $"Pin {pinNo} already exists as output")
                Else
                    Me.Add(New GpioPin(Me.Device.Gpio.Pins, pinNo, ActiveLogic.ActiveLow, direction))
                End If
            Else
                Me._ErrorProvider.Annunciate(sender, $"Unable to convert {pinNumber} to a pin number")
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Adds an input pin menu item click to 'e'. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AddInputPinMenuItem_Click(sender As Object, e As EventArgs) Handles _AddInputPinMenuItem.Click
        Me.AddPinHandler(Me._TopToolStrip, Me._NewPinNumberTextBox.Text, PinDirection.Input)
    End Sub

    ''' <summary> Adds an output pin menu item click to 'e'. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AddOutputPinMenuItem_Click(sender As Object, e As EventArgs) Handles _AddOutputPinMenuItem.Click
        Me.AddPinHandler(Me._TopToolStrip, Me._NewPinNumberTextBox.Text, PinDirection.Output)
    End Sub

#End Region

End Class

