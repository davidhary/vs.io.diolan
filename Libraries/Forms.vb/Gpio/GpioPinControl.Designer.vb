﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GpioPinControl

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ToolStrip = New System.Windows.Forms.ToolStrip()
        Me._PinComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PinComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._OutputValueComboBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._OutputValueComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._SetOutputButton = New System.Windows.Forms.ToolStripButton()
        Me._PulseDurationTextBoxLabel = New System.Windows.Forms.ToolStripLabel()
        Me._PulseDurationTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._StartButton = New System.Windows.Forms.ToolStripButton()
        Me._ValueTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._GetValueButton = New System.Windows.Forms.ToolStripButton()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolStrip.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ToolStrip
        '
        Me._ToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._PinComboBoxLabel, Me._PinComboBox, Me._OutputValueComboBoxLabel, Me._OutputValueComboBox, Me._SetOutputButton, Me._PulseDurationTextBoxLabel, Me._PulseDurationTextBox, Me._StartButton, Me._ValueTextBox, Me._GetValueButton})
        Me._ToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._ToolStrip.Name = "_ToolStrip"
        Me._ToolStrip.Size = New System.Drawing.Size(433, 25)
        Me._ToolStrip.TabIndex = 10
        Me._ToolStrip.Text = "ToolStrip1"
        '
        '_PinComboBoxLabel
        '
        Me._PinComboBoxLabel.Name = "_PinComboBoxLabel"
        Me._PinComboBoxLabel.Size = New System.Drawing.Size(27, 22)
        Me._PinComboBoxLabel.Text = "Pin:"
        '
        '_PinComboBox
        '
        Me._PinComboBox.AutoSize = False
        Me._PinComboBox.DropDownWidth = 25
        Me._PinComboBox.Margin = New System.Windows.Forms.Padding(1, 0, 3, 0)
        Me._PinComboBox.Name = "_PinComboBox"
        Me._PinComboBox.Size = New System.Drawing.Size(35, 23)
        Me._PinComboBox.ToolTipText = "Pin number"
        '
        '_OutputValueComboBoxLabel
        '
        Me._OutputValueComboBoxLabel.Name = "_OutputValueComboBoxLabel"
        Me._OutputValueComboBoxLabel.Size = New System.Drawing.Size(30, 22)
        Me._OutputValueComboBoxLabel.Text = "Out:"
        '
        '_OutputValueComboBox
        '
        Me._OutputValueComboBox.AutoSize = False
        Me._OutputValueComboBox.DropDownWidth = 55
        Me._OutputValueComboBox.Name = "_OutputValueComboBox"
        Me._OutputValueComboBox.Size = New System.Drawing.Size(65, 23)
        Me._OutputValueComboBox.Text = "Logic 0"
        Me._OutputValueComboBox.ToolTipText = "Output value"
        '
        '_SetOutputButton
        '
        Me._SetOutputButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._SetOutputButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SetOutputButton.Margin = New System.Windows.Forms.Padding(0, 1, 3, 2)
        Me._SetOutputButton.Name = "_SetOutputButton"
        Me._SetOutputButton.Size = New System.Drawing.Size(27, 22)
        Me._SetOutputButton.Text = "Set"
        Me._SetOutputButton.ToolTipText = "Set the output"
        '
        '_PulseDurationTextBoxLabel
        '
        Me._PulseDurationTextBoxLabel.Name = "_PulseDurationTextBoxLabel"
        Me._PulseDurationTextBoxLabel.Size = New System.Drawing.Size(60, 22)
        Me._PulseDurationTextBoxLabel.Text = "Pulse, ms:"
        '
        '_PulseDurationTextBox
        '
        Me._PulseDurationTextBox.Name = "_PulseDurationTextBox"
        Me._PulseDurationTextBox.Size = New System.Drawing.Size(50, 25)
        Me._PulseDurationTextBox.Text = "10"
        Me._PulseDurationTextBox.ToolTipText = "Pulse duration in ms."
        '
        '_StartButton
        '
        Me._StartButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._StartButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._StartButton.Name = "_StartButton"
        Me._StartButton.Size = New System.Drawing.Size(35, 22)
        Me._StartButton.Text = "Start"
        Me._StartButton.ToolTipText = "Start a single pulse."
        '
        '_ValueTextBox
        '
        Me._ValueTextBox.BackColor = System.Drawing.Color.Black
        Me._ValueTextBox.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ValueTextBox.ForeColor = System.Drawing.Color.Aqua
        Me._ValueTextBox.Name = "_ValueTextBox"
        Me._ValueTextBox.Size = New System.Drawing.Size(25, 25)
        Me._ValueTextBox.Text = "0"
        Me._ValueTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ValueTextBox.ToolTipText = "Value"
        '
        '_GetValueButton
        '
        Me._GetValueButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._GetValueButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._GetValueButton.Margin = New System.Windows.Forms.Padding(0, 1, 3, 2)
        Me._GetValueButton.Name = "_GetValueButton"
        Me._GetValueButton.Size = New System.Drawing.Size(29, 22)
        Me._GetValueButton.Text = "Get"
        Me._GetValueButton.ToolTipText = "Get input or output value"
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        'GpioPinControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._ToolStrip)
        Me.Name = "GpioPinControl"
        Me.Size = New System.Drawing.Size(445, 27)
        Me._ToolStrip.ResumeLayout(False)
        Me._ToolStrip.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _PinComboBoxLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _PinComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _OutputValueComboBoxLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _OutputValueComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _SetOutputButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _PulseDurationTextBoxLabel As System.Windows.Forms.ToolStripLabel
    Private WithEvents _PulseDurationTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _StartButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _ValueTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _GetValueButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider

End Class
