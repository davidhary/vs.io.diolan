﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GpioPinsView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GpioPinsView))
        Me._BottomToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TabComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._SelectServerButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ServerNameTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._DefaultServerMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ConnectServerButton = New System.Windows.Forms.ToolStripButton()
        Me._OpenDeviceModalityButton = New System.Windows.Forms.ToolStripButton()
        Me._SelectDeviceSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DevicesComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._DeviceInfoTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._Tabs = New isr.Diolan.Forms.ExtendedTabControl()
        Me._PrimaryTabPage = New System.Windows.Forms.TabPage()
        Me._PrimaryTabLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._SubsystemGroupBox = New System.Windows.Forms.GroupBox()
        Me._DebounceNumeric = New System.Windows.Forms.NumericUpDown()
        Me._GetDebounceButton = New System.Windows.Forms.Button()
        Me._SetDebounceButton = New System.Windows.Forms.Button()
        Me._DebounceNumericLabel = New System.Windows.Forms.Label()
        Me._InputPinsTabPage = New System.Windows.Forms.TabPage()
        Me._InputPinsLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._OutputPinsTabPage = New System.Windows.Forms.TabPage()
        Me._OutputPinsLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._EventLogTabPage = New System.Windows.Forms.TabPage()
        Me._EventLogTextBox = New System.Windows.Forms.TextBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ToolStripContainer = New System.Windows.Forms.ToolStripContainer()
        Me._TopToolStrip = New System.Windows.Forms.ToolStrip()
        Me._EventPinNumberLabel = New System.Windows.Forms.ToolStripLabel()
        Me._EvetnPinNameLabel = New System.Windows.Forms.ToolStripLabel()
        Me._EventPinValue = New System.Windows.Forms.ToolStripTextBox()
        Me._AddPinsSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._NewPinNumberTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._AddInputPinMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AddOutputPinMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._BottomToolStrip.SuspendLayout()
        Me._Tabs.SuspendLayout()
        Me._PrimaryTabPage.SuspendLayout()
        Me._PrimaryTabLayout.SuspendLayout()
        Me._SubsystemGroupBox.SuspendLayout()
        CType(Me._DebounceNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._InputPinsTabPage.SuspendLayout()
        Me._OutputPinsTabPage.SuspendLayout()
        Me._EventLogTabPage.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ToolStripContainer.BottomToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.ContentPanel.SuspendLayout()
        Me._ToolStripContainer.TopToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.SuspendLayout()
        Me._TopToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_BottomToolStrip
        '
        Me._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._BottomToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TabComboBox, Me._SelectServerButton, Me._ConnectServerButton, Me._OpenDeviceModalityButton, Me._SelectDeviceSplitButton, Me._DeviceInfoTextBox})
        Me._BottomToolStrip.Location = New System.Drawing.Point(0, 0)
        Me._BottomToolStrip.Name = "_BottomToolStrip"
        Me._BottomToolStrip.Size = New System.Drawing.Size(417, 29)
        Me._BottomToolStrip.Stretch = True
        Me._BottomToolStrip.TabIndex = 0
        '
        '_TabComboBox
        '
        Me._TabComboBox.Name = "_TabComboBox"
        Me._TabComboBox.Size = New System.Drawing.Size(81, 29)
        Me._TabComboBox.ToolTipText = "Select panel"
        '
        '_SelectServerButton
        '
        Me._SelectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectServerButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServerNameTextBox, Me._DefaultServerMenuItem})
        Me._SelectServerButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Network_server
        Me._SelectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectServerButton.Name = "_SelectServerButton"
        Me._SelectServerButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectServerButton.Text = "Select Server"
        '
        '_ServerNameTextBox
        '
        Me._ServerNameTextBox.Name = "_ServerNameTextBox"
        Me._ServerNameTextBox.Size = New System.Drawing.Size(100, 23)
        Me._ServerNameTextBox.Text = "localhost:9656"
        '
        '_DefaultServerMenuItem
        '
        Me._DefaultServerMenuItem.Checked = True
        Me._DefaultServerMenuItem.CheckOnClick = True
        Me._DefaultServerMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._DefaultServerMenuItem.Name = "_DefaultServerMenuItem"
        Me._DefaultServerMenuItem.Size = New System.Drawing.Size(198, 22)
        Me._DefaultServerMenuItem.Text = "User Default Server:Port"
        '
        '_ConnectServerButton
        '
        Me._ConnectServerButton.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ConnectServerButton.ForeColor = System.Drawing.Color.Red
        Me._ConnectServerButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.WIFI_open_22_Right_Up
        Me._ConnectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ConnectServerButton.Name = "_ConnectServerButton"
        Me._ConnectServerButton.Size = New System.Drawing.Size(41, 26)
        Me._ConnectServerButton.Text = "X"
        Me._ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me._ConnectServerButton.ToolTipText = "Connect or disconnect serve and show attached devices."
        '
        '_OpenDeviceModalityButton
        '
        Me._OpenDeviceModalityButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._OpenDeviceModalityButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._OpenDeviceModalityButton.Enabled = False
        Me._OpenDeviceModalityButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.User_offline_2
        Me._OpenDeviceModalityButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._OpenDeviceModalityButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton"
        Me._OpenDeviceModalityButton.Size = New System.Drawing.Size(26, 26)
        Me._OpenDeviceModalityButton.Text = "Open"
        Me._OpenDeviceModalityButton.ToolTipText = "Open or close the device."
        '
        '_SelectDeviceSplitButton
        '
        Me._SelectDeviceSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._SelectDeviceSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectDeviceSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DevicesComboBox})
        Me._SelectDeviceSplitButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.Network_server_database
        Me._SelectDeviceSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton"
        Me._SelectDeviceSplitButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectDeviceSplitButton.Text = "Device"
        Me._SelectDeviceSplitButton.ToolTipText = "Select Device"
        '
        '_DevicesComboBox
        '
        Me._DevicesComboBox.Name = "_DevicesComboBox"
        Me._DevicesComboBox.Size = New System.Drawing.Size(121, 23)
        Me._DevicesComboBox.Text = "DLN-4M.1.1"
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(100, 29)
        Me._DeviceInfoTextBox.Text = "closed"
        Me._DeviceInfoTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._PrimaryTabPage)
        Me._Tabs.Controls.Add(Me._InputPinsTabPage)
        Me._Tabs.Controls.Add(Me._OutputPinsTabPage)
        Me._Tabs.Controls.Add(Me._EventLogTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.HideTabHeaders = True
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(417, 452)
        Me._Tabs.TabIndex = 0
        '
        '_PrimaryTabPage
        '
        Me._PrimaryTabPage.Controls.Add(Me._PrimaryTabLayout)
        Me._PrimaryTabPage.Location = New System.Drawing.Point(4, 26)
        Me._PrimaryTabPage.Name = "_PrimaryTabPage"
        Me._PrimaryTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._PrimaryTabPage.Size = New System.Drawing.Size(409, 422)
        Me._PrimaryTabPage.TabIndex = 0
        Me._PrimaryTabPage.Text = "GPIO"
        Me._PrimaryTabPage.UseVisualStyleBackColor = True
        '
        '_PrimaryTabLayout
        '
        Me._PrimaryTabLayout.ColumnCount = 3
        Me._PrimaryTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PrimaryTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._PrimaryTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PrimaryTabLayout.Controls.Add(Me._SubsystemGroupBox, 1, 1)
        Me._PrimaryTabLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._PrimaryTabLayout.Location = New System.Drawing.Point(3, 3)
        Me._PrimaryTabLayout.Name = "_PrimaryTabLayout"
        Me._PrimaryTabLayout.RowCount = 3
        Me._PrimaryTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PrimaryTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._PrimaryTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._PrimaryTabLayout.Size = New System.Drawing.Size(403, 416)
        Me._PrimaryTabLayout.TabIndex = 8
        '
        '_SubsystemGroupBox
        '
        Me._SubsystemGroupBox.Controls.Add(Me._DebounceNumeric)
        Me._SubsystemGroupBox.Controls.Add(Me._GetDebounceButton)
        Me._SubsystemGroupBox.Controls.Add(Me._SetDebounceButton)
        Me._SubsystemGroupBox.Controls.Add(Me._DebounceNumericLabel)
        Me._SubsystemGroupBox.Location = New System.Drawing.Point(26, 176)
        Me._SubsystemGroupBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SubsystemGroupBox.Name = "_SubsystemGroupBox"
        Me._SubsystemGroupBox.Padding = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SubsystemGroupBox.Size = New System.Drawing.Size(350, 64)
        Me._SubsystemGroupBox.TabIndex = 6
        Me._SubsystemGroupBox.TabStop = False
        Me._SubsystemGroupBox.Text = "GPIO Subsystem Settings"
        '
        '_DebounceNumeric
        '
        Me._DebounceNumeric.Location = New System.Drawing.Point(157, 22)
        Me._DebounceNumeric.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._DebounceNumeric.Maximum = New Decimal(New Integer() {32000, 0, 0, 0})
        Me._DebounceNumeric.Name = "_DebounceNumeric"
        Me._DebounceNumeric.Size = New System.Drawing.Size(63, 25)
        Me._DebounceNumeric.TabIndex = 1
        '
        '_GetDebounceButton
        '
        Me._GetDebounceButton.Location = New System.Drawing.Point(282, 19)
        Me._GetDebounceButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GetDebounceButton.Name = "_GetDebounceButton"
        Me._GetDebounceButton.Size = New System.Drawing.Size(53, 30)
        Me._GetDebounceButton.TabIndex = 3
        Me._GetDebounceButton.Text = "Get"
        Me._ToolTip.SetToolTip(Me._GetDebounceButton, "Gets the debounce interval")
        Me._GetDebounceButton.UseVisualStyleBackColor = True
        '
        '_SetDebounceButton
        '
        Me._SetDebounceButton.Location = New System.Drawing.Point(222, 19)
        Me._SetDebounceButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SetDebounceButton.Name = "_SetDebounceButton"
        Me._SetDebounceButton.Size = New System.Drawing.Size(53, 30)
        Me._SetDebounceButton.TabIndex = 2
        Me._SetDebounceButton.Text = "Set"
        Me._ToolTip.SetToolTip(Me._SetDebounceButton, "Sets the debounce interval")
        Me._SetDebounceButton.UseVisualStyleBackColor = True
        '
        '_DebounceNumericLabel
        '
        Me._DebounceNumericLabel.AutoSize = True
        Me._DebounceNumericLabel.Location = New System.Drawing.Point(13, 26)
        Me._DebounceNumericLabel.Name = "_DebounceNumericLabel"
        Me._DebounceNumericLabel.Size = New System.Drawing.Size(142, 17)
        Me._DebounceNumericLabel.TabIndex = 0
        Me._DebounceNumericLabel.Text = "Debounce Interval [us]:"
        Me._DebounceNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_InputPinsTabPage
        '
        Me._InputPinsTabPage.Controls.Add(Me._InputPinsLayout)
        Me._InputPinsTabPage.Location = New System.Drawing.Point(4, 22)
        Me._InputPinsTabPage.Name = "_InputPinsTabPage"
        Me._InputPinsTabPage.Size = New System.Drawing.Size(409, 426)
        Me._InputPinsTabPage.TabIndex = 2
        Me._InputPinsTabPage.Text = "Inputs"
        Me._InputPinsTabPage.UseVisualStyleBackColor = True
        '
        '_InputPinsLayout
        '
        Me._InputPinsLayout.ColumnCount = 2
        Me._InputPinsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._InputPinsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._InputPinsLayout.Location = New System.Drawing.Point(73, 36)
        Me._InputPinsLayout.Name = "_InputPinsLayout"
        Me._InputPinsLayout.RowCount = 2
        Me._InputPinsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._InputPinsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._InputPinsLayout.Size = New System.Drawing.Size(200, 100)
        Me._InputPinsLayout.TabIndex = 0
        '
        '_OutputPinsTabPage
        '
        Me._OutputPinsTabPage.Controls.Add(Me._OutputPinsLayout)
        Me._OutputPinsTabPage.Location = New System.Drawing.Point(4, 22)
        Me._OutputPinsTabPage.Name = "_OutputPinsTabPage"
        Me._OutputPinsTabPage.Size = New System.Drawing.Size(409, 426)
        Me._OutputPinsTabPage.TabIndex = 2
        Me._OutputPinsTabPage.Text = "Outputs"
        Me._OutputPinsTabPage.UseVisualStyleBackColor = True
        '
        '_OutputPinsLayout
        '
        Me._OutputPinsLayout.ColumnCount = 2
        Me._OutputPinsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._OutputPinsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._OutputPinsLayout.Location = New System.Drawing.Point(119, 55)
        Me._OutputPinsLayout.Name = "_OutputPinsLayout"
        Me._OutputPinsLayout.RowCount = 2
        Me._OutputPinsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._OutputPinsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._OutputPinsLayout.Size = New System.Drawing.Size(200, 100)
        Me._OutputPinsLayout.TabIndex = 0
        '
        '_EventLogTabPage
        '
        Me._EventLogTabPage.Controls.Add(Me._EventLogTextBox)
        Me._EventLogTabPage.Location = New System.Drawing.Point(4, 22)
        Me._EventLogTabPage.Name = "_EventLogTabPage"
        Me._EventLogTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._EventLogTabPage.Size = New System.Drawing.Size(409, 426)
        Me._EventLogTabPage.TabIndex = 1
        Me._EventLogTabPage.Text = "Events"
        Me._EventLogTabPage.UseVisualStyleBackColor = True
        '
        '_EventLogTextBox
        '
        Me._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EventLogTextBox.Location = New System.Drawing.Point(3, 3)
        Me._EventLogTextBox.Multiline = True
        Me._EventLogTextBox.Name = "_EventLogTextBox"
        Me._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._EventLogTextBox.Size = New System.Drawing.Size(403, 420)
        Me._EventLogTextBox.TabIndex = 0
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_ToolStripContainer
        '
        '
        '_ToolStripContainer.BottomToolStripPanel
        '
        Me._ToolStripContainer.BottomToolStripPanel.Controls.Add(Me._BottomToolStrip)
        '
        '_ToolStripContainer.ContentPanel
        '
        Me._ToolStripContainer.ContentPanel.Controls.Add(Me._Tabs)
        Me._ToolStripContainer.ContentPanel.Size = New System.Drawing.Size(417, 452)
        Me._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ToolStripContainer.Location = New System.Drawing.Point(0, 0)
        Me._ToolStripContainer.Name = "_ToolStripContainer"
        Me._ToolStripContainer.Size = New System.Drawing.Size(417, 506)
        Me._ToolStripContainer.TabIndex = 0
        Me._ToolStripContainer.Text = "ToolStripContainer1"
        '
        '_ToolStripContainer.TopToolStripPanel
        '
        Me._ToolStripContainer.TopToolStripPanel.Controls.Add(Me._TopToolStrip)
        '
        '_TopToolStrip
        '
        Me._TopToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TopToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._EventPinNumberLabel, Me._EvetnPinNameLabel, Me._EventPinValue, Me._AddPinsSplitButton})
        Me._TopToolStrip.Location = New System.Drawing.Point(3, 0)
        Me._TopToolStrip.Name = "_TopToolStrip"
        Me._TopToolStrip.Size = New System.Drawing.Size(165, 25)
        Me._TopToolStrip.TabIndex = 0
        '
        '_EventPinNumberLabel
        '
        Me._EventPinNumberLabel.Name = "_EventPinNumberLabel"
        Me._EventPinNumberLabel.Size = New System.Drawing.Size(31, 22)
        Me._EventPinNumberLabel.Text = "Pin#"
        '
        '_EvetnPinNameLabel
        '
        Me._EvetnPinNameLabel.Name = "_EvetnPinNameLabel"
        Me._EvetnPinNameLabel.Size = New System.Drawing.Size(53, 22)
        Me._EvetnPinNameLabel.Text = "<name>"
        '
        '_EventPinValue
        '
        Me._EventPinValue.BackColor = System.Drawing.Color.Black
        Me._EventPinValue.ForeColor = System.Drawing.Color.Aqua
        Me._EventPinValue.Name = "_EventPinValue"
        Me._EventPinValue.Size = New System.Drawing.Size(13, 25)
        Me._EventPinValue.Text = "0"
        '
        '_AddPinsSplitButton
        '
        Me._AddPinsSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AddPinsSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._NewPinNumberTextBox, Me._AddInputPinMenuItem, Me._AddOutputPinMenuItem})
        Me._AddPinsSplitButton.Image = CType(resources.GetObject("_AddPinsSplitButton.Image"), System.Drawing.Image)
        Me._AddPinsSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AddPinsSplitButton.Name = "_AddPinsSplitButton"
        Me._AddPinsSplitButton.Size = New System.Drawing.Size(54, 22)
        Me._AddPinsSplitButton.Text = "Add..."
        '
        '_NewPinNumberTextBox
        '
        Me._NewPinNumberTextBox.Name = "_NewPinNumberTextBox"
        Me._NewPinNumberTextBox.Size = New System.Drawing.Size(100, 23)
        Me._NewPinNumberTextBox.Text = "0"
        Me._NewPinNumberTextBox.ToolTipText = "Enter a new pin number to add"
        '
        '_AddInputPinMenuItem
        '
        Me._AddInputPinMenuItem.Name = "_AddInputPinMenuItem"
        Me._AddInputPinMenuItem.Size = New System.Drawing.Size(160, 22)
        Me._AddInputPinMenuItem.Text = "Add Input Pin"
        '
        '_AddOutputPinMenuItem
        '
        Me._AddOutputPinMenuItem.Name = "_AddOutputPinMenuItem"
        Me._AddOutputPinMenuItem.Size = New System.Drawing.Size(160, 22)
        Me._AddOutputPinMenuItem.Text = "Add Output Pin"
        '
        'GpioPinsPanel
        '
        Me.Controls.Add(Me._ToolStripContainer)
        Me.Name = "GpioPinsPanel"
        Me.Size = New System.Drawing.Size(417, 506)
        Me._BottomToolStrip.ResumeLayout(False)
        Me._BottomToolStrip.PerformLayout()
        Me._Tabs.ResumeLayout(False)
        Me._PrimaryTabPage.ResumeLayout(False)
        Me._PrimaryTabLayout.ResumeLayout(False)
        Me._SubsystemGroupBox.ResumeLayout(False)
        Me._SubsystemGroupBox.PerformLayout()
        CType(Me._DebounceNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._InputPinsTabPage.ResumeLayout(False)
        Me._OutputPinsTabPage.ResumeLayout(False)
        Me._EventLogTabPage.ResumeLayout(False)
        Me._EventLogTabPage.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ToolStripContainer.BottomToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.BottomToolStripPanel.PerformLayout()
        Me._ToolStripContainer.ContentPanel.ResumeLayout(False)
        Me._ToolStripContainer.TopToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.TopToolStripPanel.PerformLayout()
        Me._ToolStripContainer.ResumeLayout(False)
        Me._ToolStripContainer.PerformLayout()
        Me._TopToolStrip.ResumeLayout(False)
        Me._TopToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _BottomToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _DeviceInfoTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _OpenDeviceModalityButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _PrimaryTabPage As System.Windows.Forms.TabPage
    Private WithEvents _EventLogTabPage As System.Windows.Forms.TabPage
    Private WithEvents _EventLogTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ToolStripContainer As System.Windows.Forms.ToolStripContainer
    Private WithEvents _Tabs As ExtendedTabControl
    Private WithEvents _DevicesComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _SelectServerButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _ServerNameTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _DefaultServerMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SelectDeviceSplitButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _SubsystemGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _DebounceNumeric As System.Windows.Forms.NumericUpDown
    Private WithEvents _GetDebounceButton As System.Windows.Forms.Button
    Private WithEvents _SetDebounceButton As System.Windows.Forms.Button
    Private WithEvents _DebounceNumericLabel As System.Windows.Forms.Label
    Private WithEvents _PrimaryTabLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _InputPinsTabPage As System.Windows.Forms.TabPage
    Private WithEvents _OutputPinsTabPage As System.Windows.Forms.TabPage
    Private WithEvents _TabComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _ConnectServerButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _TopToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _EventPinNumberLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _EvetnPinNameLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _EventPinValue As Windows.Forms.ToolStripTextBox
    Private WithEvents _AddPinsSplitButton As Windows.Forms.ToolStripSplitButton
    Private WithEvents _NewPinNumberTextBox As Windows.Forms.ToolStripTextBox
    Private WithEvents _AddInputPinMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AddOutputPinMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _InputPinsLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _OutputPinsLayout As Windows.Forms.TableLayoutPanel
End Class
