Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports System.ComponentModel
Imports isr.Diolan.Gpio

''' <summary> A gpio pin configuration control. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-04-07 </para>
''' </remarks>
Public Class GpioPinConfigControl
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()

        ' This call is required by the designer.
        Me.InitializeComponent()

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing AndAlso Me.components IsNot Nothing Then Me.components.Dispose()
                Me._PinDirectionComboBox.DataSource = Nothing
                Me._PinDirectionComboBox.Items.Clear()
                Me._EventTypeComboBox.DataSource = Nothing
                Me._EventTypeComboBox.Items.Clear()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PIN "

    ''' <summary> Gets or sets the gpio pin. </summary>
    ''' <value> The gpio pin. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property GpioPin As GpioPin

    ''' <summary> Configures the given pin. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="pin"> The pin. </param>
    Public Sub Configure(ByVal pin As GpioPin)
        If pin Is Nothing Then Throw New ArgumentNullException(NameOf(pin))
        Me._GpioPin = pin

        Me._PinNumberNumeric.Value = Me.GpioPin.PinNumber
        Me._PinNumberNumeric.ReadOnly = True

        ' Fill controls
        Me._PinDirectionComboBox.DataSource = Nothing
        Me._PinDirectionComboBox.Items.Clear()
        Me._PinDirectionComboBox.ListEnumNames(Of isr.Diolan.PinDirection)
        Me._ErrorProvider.Clear()

        Me.ReadPinProperties(pin)
    End Sub

    ''' <summary> Gets pin configuration button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GetPinConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _GetPinConfigButton.Click
        'Get pin parameters
        Try
            Me._ErrorProvider.Clear()
            Me.ReadPinProperties(Me.GpioPin)
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Reads pin number properties. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pin"> The pin. </param>
    Private Sub ReadPinProperties(ByVal pin As GpioPin)
        'Main settings are present in all devices,
        'check availability only for extra settings like pull downs, debounce and events.

        Me._PinEnabledCheckBox.Checked = pin.Enabled
        Me._PinDirectionComboBox.SelectedIndex = pin.Direction

        Me._PinNameTextBox.Text = pin.Name
        ' Pull up
        If pin.Pin.Restrictions.PullupEnabled = Dln.Restriction.NotSupported Then
            Me._PinPullupCheckBox.Enabled = False
        Else
            Me._PinPullupCheckBox.Enabled = True
            Me._PinPullupCheckBox.Checked = pin.Pin.PullupEnabled
        End If

        ' Pull down
        If pin.Pin.Restrictions.PulldownEnabled = Dln.Restriction.NotSupported Then
            Me._PulldownCheckBox.Enabled = False
        Else
            Me._PulldownCheckBox.Enabled = True
            Me._PulldownCheckBox.Checked = pin.Pin.PulldownEnabled
        End If

        ' Open drain
        If pin.Pin.Restrictions.OpendrainEnabled = Dln.Restriction.NotSupported Then
            Me._OpenDrainCheckBox.Enabled = False
        Else
            Me._OpenDrainCheckBox.Enabled = True
            Me._OpenDrainCheckBox.Checked = pin.Pin.OpendrainEnabled
        End If

        ' Debounce
        If pin.Pin.Restrictions.DebounceEnabled = Dln.Restriction.NotSupported Then
            Me._ToolTip.SetToolTip(Me._DebounceEnabledCheckBox, "Not supported.")
        ElseIf pin.Pin.Restrictions.DebounceEnabled = Dln.Restriction.NoRestriction Then
            Me._ToolTip.SetToolTip(Me._DebounceEnabledCheckBox, "Enabled if checked.")
        End If
        If pin.Pin.Restrictions.DebounceEnabled = Dln.Restriction.NotSupported Then
            Me._DebounceEnabledCheckBox.Enabled = False
        ElseIf pin.Pin.Restrictions.DebounceEnabled = Dln.Restriction.MustBeDisabled Then
            Me._DebounceEnabledCheckBox.Enabled = False
        Else
            Me._DebounceEnabledCheckBox.Enabled = True
            Me._DebounceEnabledCheckBox.Checked = pin.Pin.DebounceEnabled
        End If

        'Event type
        If pin.Pin.Restrictions.EventType = Dln.Restriction.NotSupported Then
            Me._EventTypeComboBox.Enabled = False
        Else
            Me._EventTypeComboBox.Enabled = True
            Me._EventTypeComboBox.DataSource = pin.Pin.SupportedEventTypes
            Me._EventTypeComboBox.SelectedItem = pin.Pin.EventType
        End If

        'Event period
        If pin.Pin.Restrictions.EventPeriod = Dln.Restriction.NotSupported Then
            Me._EventPeriodNumeric.Enabled = False
        Else
            Me._EventPeriodNumeric.Enabled = True
            Me._EventPeriodNumeric.Value = CDec(pin.Pin.EventPeriod)
        End If
    End Sub

    ''' <summary> Applies the pin configuration described by pin. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pin"> The pin. </param>
    Private Sub ApplyPinConfiguration(ByVal pin As GpioPin)
        If pin IsNot Nothing Then
            'Set common controls
            pin.Enabled = Me._PinEnabledCheckBox.Checked
            pin.Direction = CType(Me._PinDirectionComboBox.SelectedIndex, PinDirection)
            pin.Name = Me._PinNameTextBox.Text.Trim

            'Extra functions are not present if we disable controls when getting settings
            If (Me._PinPullupCheckBox.Enabled) Then
                pin.Pin.PullupEnabled = Me._PinPullupCheckBox.Checked
            End If
            If (Me._PulldownCheckBox.Enabled) Then
                pin.Pin.PulldownEnabled = Me._PulldownCheckBox.Checked
            End If
            If (Me._OpenDrainCheckBox.Enabled) Then
                pin.Pin.OpendrainEnabled = Me._OpenDrainCheckBox.Checked
            End If
            If (Me._DebounceEnabledCheckBox.Enabled) Then
                pin.Pin.DebounceEnabled = Me._DebounceEnabledCheckBox.Checked
            End If
            If (Me._EventTypeComboBox.Enabled) Then
                pin.Pin.SetEventConfiguration(CType(Me._EventTypeComboBox.SelectedItem, Dln.Gpio.EventType),
                                                  Convert.ToUInt16(Me._EventPeriodNumeric.Value))
            End If
        End If
    End Sub

    ''' <summary> Sets pin configuration button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SetPinConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SetPinConfigButton.Click
        ' Set pin configuration
        Try
            Me._ErrorProvider.Clear()
            Me.ApplyPinConfiguration(Me.GpioPin)
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try
    End Sub


#End Region

End Class
