﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GpioPinConfigDialog
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GpioPinConfigDialog))
        Me._GpioPinConfigControl = New isr.Diolan.Forms.GpioPinConfigControl()
        Me.SuspendLayout()
        '
        'GpioPinConfigControl1
        '
        Me._GpioPinConfigControl.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._GpioPinConfigControl.Location = New System.Drawing.Point(12, 12)
        Me._GpioPinConfigControl.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._GpioPinConfigControl.Name = "_GpioPinConfigControl"
        Me._GpioPinConfigControl.Size = New System.Drawing.Size(283, 222)
        Me._GpioPinConfigControl.TabIndex = 0
        '
        'GpioPinConfigDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(304, 238)
        Me.Controls.Add(Me._GpioPinConfigControl)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "GpioPinConfigDialog"
        Me.Text = "Gpio Pin Configuration"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _GpioPinConfigControl As GpioPinConfigControl
End Class
