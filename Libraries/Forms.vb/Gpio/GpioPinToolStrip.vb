Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Core
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Diolan.Gpio

''' <summary> A gpio pin tool strip. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-04-07 </para>
''' </remarks>
Public Class GpioPinToolStrip
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTOR "

    ''' <summary>
    ''' A private constructor for this class making it not publicly creatable. This ensure using the
    ''' class as a singleton.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()

        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        ' Add any initialization after the InitializeComponent() call.
        Me.Enabled = False
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso Me.components IsNot Nothing Then
                Me.components.Dispose()
                Me.Gpio_Pin = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " PIN "

        Private WithEvents Gpio_Pin As GpioPin

    ''' <summary> Gets the gpio pin. </summary>
    ''' <value> The gpio pin. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property GpioPin As GpioPin
        Get
            Return Me.Gpio_Pin
        End Get
    End Property

    ''' <summary> Executes the property changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As GpioPin, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Gpio.GpioPin.Name)
                Me._PinNameLabel.Text = sender.Name
            Case NameOf(Gpio.GpioPin.PinNumber)
                Me._PinNumberLabel.Text = CStr(sender.PinNumber)
            Case NameOf(Gpio.GpioPin.BitValue)
                Me._LogicalStateButton.Checked = sender.LogicalState = LogicalState.Active
            Case NameOf(Gpio.GpioPin.ActiveLogic)
                Me._ActiveLogicToggleButton.Checked = sender.ActiveLogic = ActiveLogic.ActiveHigh
            Case NameOf(Gpio.GpioPin.Direction)
                Me._LogicalStateButton.CheckOnClick = sender.Direction = PinDirection.Output
            Case NameOf(Gpio.GpioPin.Enabled)
                Me._ActiveLogicToggleButton.Enabled = sender.Enabled
                Me._LogicalStateButton.Enabled = sender.Enabled
        End Select
    End Sub

    ''' <summary> Gpio pin property changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub GpioPin_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles Gpio_Pin.PropertyChanged
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.GpioPin_PropertyChanged), New Object() {sender, e})
            End If
            Me.OnPropertyChanged(TryCast(sender, GpioPin), e?.PropertyName)
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        End Try

    End Sub

    ''' <summary> Assigns the given pin. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pin"> The pin. </param>
    Public Sub Assign(ByVal pin As GpioPin)
        Me.Gpio_Pin = pin
        Me.Enabled = pin IsNot Nothing
    End Sub

    ''' <summary> Configure button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub ConfigureButton_Click(sender As Object, e As EventArgs) Handles _ConfigureButton.Click
            Try
                Me._ErrorProvider.Clear()
                Using configForm As New GpioPinConfigDialog
                    configForm.ShowDialog(Me, Me.GpioPin)
                End Using
            Catch ex As Exception
                Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
            End Try
        End Sub

    ''' <summary> Logical state button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub LogicalStateButton_Click(sender As Object, e As EventArgs) Handles _LogicalStateButton.Click
            Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
            If button Is Nothing Then Return
            Try
                Me._ErrorProvider.Clear()
                button.Image = If(Me.GpioPin.LogicalState = LogicalState.Active, My.Resources.circle_green, My.Resources.circle_grey)
            Catch ex As Exception
                Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
            End Try
        End Sub

    ''' <summary> Logical state button check state changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Sub LogicalStateButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _LogicalStateButton.CheckStateChanged
            If Me.InitializingComponents Then Return
            Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
            If button Is Nothing Then Return
            Try
                Me._ErrorProvider.Clear()
                If Me.GpioPin.Pin.Direction = PinDirection.Output Then
                    Me.GpioPin.LogicalState = If(button.Checked, LogicalState.Active, LogicalState.Inactive)
                Else

                End If
            Catch ex As Exception
                Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
            Finally
                button.Image = If(Me.GpioPin.LogicalState = LogicalState.Active, My.Resources.circle_green, My.Resources.circle_grey)
            End Try
        End Sub

    ''' <summary> Active logic toggle button check state changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ActiveLogicToggleButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _ActiveLogicToggleButton.CheckStateChanged
        If Me.InitializingComponents Then Return
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button Is Nothing Then Return
        Try
            Me._ErrorProvider.Clear()
            Me.GpioPin.ActiveLogic = If(button.Checked, ActiveLogic.ActiveHigh, ActiveLogic.ActiveLow)
        Catch ex As Exception

            Me._ErrorProvider.Annunciate(sender, ex.ToFullBlownString)
        Finally
            button.Image = If(Me.GpioPin.ActiveLogic = ActiveLogic.ActiveHigh, My.Resources.Plus_Grey, My.Resources.Minus_Grey)
        End Try
    End Sub

#End Region

End Class

