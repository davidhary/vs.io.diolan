Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports System.Windows.Forms

Namespace SubsystemExtensions

    ''' <summary> Includes extensions for <see cref="Dln.Device">DLN Devices</see>. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-05-26, 1.0.5624.x. </para></remarks>
    Public Module Methods

#Region " DEVICES "

        ''' <summary> List devices by identifier. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection">  The connection. </param>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> An Integer. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ListDevicesById(ByVal connection As Dln.Connection, ByVal listControl As Windows.Forms.ListControl) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim count As Integer = 0
            Dim details As String = String.Empty
            Dim deviceInfos As New DeviceInfoCollection
            If deviceInfos.TryEnumerateDevices(connection, details) Then
                listControl.DataSource = deviceInfos.ToList
                listControl.DisplayMember = "Caption"
                listControl.ValueMember = "Id"
                count = deviceInfos.Count
            Else
                listControl.DataSource = New String() {details}

            End If
            Return count

        End Function

        ''' <summary> List devices by identifier. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection">  The connection. </param>
        ''' <param name="listControl"> The list control. </param>
        ''' <returns> An Integer. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ListDevicesById(ByVal connection As Dln.Connection, ByVal listControl As Windows.Forms.ToolStripComboBox) As Integer
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))

            Dim count As Integer = 0
            Dim details As String = String.Empty
            Dim deviceInfos As New DeviceInfoCollection
            If deviceInfos.TryEnumerateDevices(connection, details) Then
                listControl.Items.Clear()
                For Each di As DeviceInfo In deviceInfos
                    listControl.Items.Add(di)
                Next
                ' this does not work with the tool bar....
                'listControl.DataSource = Me.ToArray
                listControl.ComboBox.DisplayMember = "Caption"
                listControl.ComboBox.ValueMember = "Id"
                count = deviceInfos.Count
            Else
                listControl.Text = details
                listControl.Items.Clear()
            End If
            Return count
        End Function

#End Region

#Region " ADC CHANNELS EXTENSIONS "

        ''' <summary> List channel numbers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="channels">    The Channels. </param>
        ''' <param name="listControl"> The list control. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub ListNumbers(ByVal channels As Dln.Adc.Channels, ByVal listControl As Windows.Forms.ListControl)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim numbers As New List(Of Integer)
            If channels IsNot Nothing Then
                For i As Integer = 0 To channels.Count - 1
                    numbers.Add(i)
                Next
            End If
            listControl.DataSource = numbers
        End Sub

#End Region

#Region " ADC PORTS EXTENSIONS "

        ''' <summary> List port numbers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ports">          The ports. </param>
        ''' <param name="listControl">    The list control. </param>
        ''' <param name="withResolution"> True to with resolution. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub ListNumbers(ByVal ports As Dln.Adc.Ports, ByVal listControl As Windows.Forms.ListControl, ByVal withResolution As Boolean)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim values As New List(Of String)
            If ports IsNot Nothing Then
                For i As Integer = 0 To ports.Count - 1
                    If withResolution Then
                        values.Add($"{i} ({ports(i).Resolution} bits)")
                    Else
                        values.Add($"{i}")
                    End If
                Next
            End If
            listControl.DataSource = values
        End Sub

#End Region

#Region " COUNTER TIMER PORTS EXTENSIONS "

        ''' <summary> List port numbers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ports">          The ports. </param>
        ''' <param name="listControl">    The list control. </param>
        ''' <param name="withResolution"> True to with resolution. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub ListNumbers(ByVal ports As Dln.PulseCounter.Ports, ByVal listControl As Windows.Forms.ListControl, ByVal withResolution As Boolean)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim values As New List(Of String)
            If ports IsNot Nothing Then
                For i As Integer = 0 To ports.Count - 1
                    If withResolution Then
                        values.Add($"{i} ({ports(i).Resolution} bits)")
                    Else
                        values.Add($"{i}")
                    End If
                Next
            End If
            listControl.DataSource = values
        End Sub

#End Region

#Region " GPIO PINS EXTENSIONS "

        ''' <summary> List pin numbers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="listControl"> The list control. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub ListNumbers(ByVal pins As Dln.Gpio.Pins, ByVal listControl As Windows.Forms.ListControl)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim numbers As New List(Of Integer)
            If pins IsNot Nothing Then
                For i As Integer = 0 To pins.Count - 1
                    numbers.Add(i)
                Next
            End If
            listControl.DataSource = numbers
        End Sub

        ''' <summary> List channel numbers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="listControl"> The list control. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub ListNumbers(ByVal pins As Dln.Gpio.Pins, ByVal listControl As Windows.Forms.ComboBox)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim numbers As New List(Of Integer)
            If pins IsNot Nothing Then
                For i As Integer = 0 To pins.Count - 1
                    numbers.Add(i)
                Next
            End If
            listControl.DataSource = numbers
        End Sub

#End Region

#Region " LEDS EXTENSIONS "

        ''' <summary> List led numbers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="leds">        The Leds. </param>
        ''' <param name="listControl"> The list control. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub ListNumbers(ByVal leds As Dln.Led.Leds, ByVal listControl As Windows.Forms.ListControl)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim ledNumbers As New List(Of Integer)
            If leds IsNot Nothing Then
                For i As Integer = 0 To leds.Count - 1
                    ledNumbers.Add(i)
                Next
            End If
            listControl.DataSource = ledNumbers
        End Sub

#End Region

#Region " PWM PORTS EXTENSIONS "

        ''' <summary> List port numbers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ports">       The ports. </param>
        ''' <param name="listControl"> The list control. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub ListNumbers(ByVal ports As Dln.Pwm.Ports, ByVal listControl As Windows.Forms.ListControl)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim numbers As New List(Of Integer)
            If ports IsNot Nothing Then
                For i As Integer = 0 To ports.Count - 1
                    numbers.Add(i)
                Next
            End If
            listControl.DataSource = numbers
        End Sub

#End Region

#Region " PWM CHANNELS EXTENSIONS "

        ''' <summary> List channel numbers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="channels">    The Channels. </param>
        ''' <param name="listControl"> The list control. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub ListNumbers(ByVal channels As Dln.Pwm.Channels, ByVal listControl As Windows.Forms.ListControl)
            If listControl Is Nothing Then Throw New ArgumentNullException(NameOf(listControl))
            Dim numbers As New List(Of Integer)
            If channels IsNot Nothing Then
                For i As Integer = 0 To channels.Count - 1
                    numbers.Add(i)
                Next
            End If
            listControl.DataSource = numbers
        End Sub

#End Region

    End Module

End Namespace

