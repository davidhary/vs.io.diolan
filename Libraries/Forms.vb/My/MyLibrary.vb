Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.DigitalInputOutputLan

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Diolan Forms Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Diolan Forms Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Diolan.Forms"

        ''' <summary> Applies the given <see cref="isr.Core.Logger"/>. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="value"> The <see cref="isr.Core.Logger"/>. </param>
        Public Shared Sub Apply(ByVal value As isr.Core.Logger)
            MyLibrary.Appliance.Apply(value)
            isr.Diolan.My.MyLibrary.Appliance.Apply(value)
        End Sub

    End Class

End Namespace

