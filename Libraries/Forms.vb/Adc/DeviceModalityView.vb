Imports System.ComponentModel
Imports isr.Core
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Core.EnumExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Diolan.Gpio
Imports isr.Diolan.SubsystemExtensions
Imports isr.Diolan.Forms.SubsystemExtensions

''' <summary> User interface for editing the device modality. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-06-01 </para>
''' </remarks>
Partial Public Class DeviceModalityView
    Inherits isr.Core.Forma.ModelViewBase

#Region " CONSTRUCTION "

    ''' <summary> Executes the custom dispose action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> true to disposing. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnCustomDispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.OnModalityClosed()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
    End Sub

#End Region

#Region " MODALITY "

    ''' <summary> The modality. </summary>
    ''' <value> The modality. </value>
    Public Property Modality As DeviceModalities = DeviceModalities.Adc

    ''' <summary> Executes the modality closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityClosed()
        Me._PortComboBox.DataSource = Nothing
        Me._PortComboBox.Items.Clear()
        'Me._ChannelComboBox.DataSource = Nothing
        'Me._ChannelComboBox.Items.Clear()
        Me._Device = Nothing
    End Sub

    ''' <summary> Executes the modality closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnModalityClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            If Me.IsDeviceModalityOpen Then
                ' un-register event handlers for all channels
                For Each port As Dln.Adc.Port In Me._Device.Adc.Ports
                    For Each channel As Dln.Adc.Channel In port.Channels
                        RemoveHandler channel.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                    Next
                Next
            End If
        End If
    End Sub

    ''' <summary> Executes the modality opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityOpening()

        Me._Device = Me._DeviceConnector.Device
        Me._PortComboBox.DataSource = Nothing
        Me._PortComboBox.Items.Clear()
        Me.OpenModality(Me._Device.Adc)

    End Sub

    ''' <summary> Opens a modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="modality"> The modality. </param>
    Private Sub OpenModality(ByVal modality As Dln.Adc.Module)

        ' Get port count
        If modality.Ports.Count() = 0 Then
            ' this is already done when opening the device.
            Me._ErrorProvider.Annunciate(Me._OpenDeviceModalityButton,
                                         "Adapter '{0}' doesn't support ADC interface.",
                                         Me._Device.Caption)
            Me._Device = Nothing
            Me._DeviceInfoTextBox.Text = "not supported"
        Else
            Me._DeviceInfoTextBox.Text = Me._Device.Caption

            modality.Ports.ListNumbers(Me._PortComboBox, True)

            ' Set current context to run thread safe events in main form thread
            Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current

            ' Register event handlers for all channels
            For Each port As Dln.Adc.Port In modality.Ports
                For Each channel As Dln.Adc.Channel In port.Channels
                    AddHandler channel.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                Next
            Next

            Me._PortComboBox.SelectedIndex = 0    ' this will cause "OnChange" event
        End If

    End Sub

    ''' <summary> Queries if a modality is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
    Private Function IsModalityOpen() As Boolean
        Return Me._PortComboBox.DataSource IsNot Nothing AndAlso Me._PortComboBox.Items.Count > 0
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> The port combo box. </summary>
    Private ReadOnly _PortComboBox As Windows.Forms.ComboBox

#End Region

#Region " EVENT LOG "

    ''' <summary> Handler, called when the condition met event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Condition met event information. </param>
    Private Sub ConditionMetEventHandler(ByVal sender As Object, ByVal e As Dln.Adc.ConditionMetEventArgs)
        Me.AppendEvent(e)
    End Sub

    ''' <summary> Appends an event. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Condition met event information. </param>
    Private Sub AppendEvent(ByVal e As Dln.Adc.ConditionMetEventArgs)
        If e IsNot Nothing Then
            Dim data As String = $"{DateTimeOffset.Now:hh:mm:ss.fff} {e.Port:D2}!{e.Channel:D2}={e.Value:D5} {e.EventType}{Environment.NewLine}"
            ' This event is handled in main thread--invoke is not needed when modifying form's controls.
            Me._EventLogTextBox.AppendText(data)
        End If
    End Sub


#End Region

End Class
