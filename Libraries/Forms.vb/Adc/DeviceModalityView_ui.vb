Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Core
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.Core.EnumExtensions
Imports isr.Diolan.Forms.ExceptionExtensions
Imports isr.Diolan.Forms.SubsystemExtensions

Public Class DeviceModalityView

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            Me.OnDispose(disposing)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ''' <summary> Executes the dispose action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> true to disposing. </param>
    Private Sub OnDispose(ByVal disposing As Boolean)
        Me.OnCustomDispose(disposing)
        If disposing Then
            If Me._Device IsNot Nothing Then
                Me._Device = Nothing
            End If
            If Me._DeviceConnector IsNot Nothing Then
                Me._DeviceConnector.Dispose()
                Me._DeviceConnector = Nothing
            End If
        End If
    End Sub

#End Region

#Region " SERVER "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _ServerConnector As LocalhostConnector
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the server connector. </summary>
    ''' <value> The server connector. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ServerConnector As LocalhostConnector
        Get
            Return Me._ServerConnector
        End Get
        Set(value As LocalhostConnector)
            Me._ServerConnector = value
        End Set
    End Property

    ''' <summary> Connects a server button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConnectServerButton_Click(sender As System.Object, e As System.EventArgs) Handles _ConnectServerButton.Click
        If Me.ServerConnector.IsConnected Then
            If Not Me.ServerConnector.HasAttachedDevices Then
                Me.ServerConnector.Disconnect()
            End If
        Else
            Me.ServerConnector.Connect()
        End If
    End Sub

    ''' <summary> Executes the server connection changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnServerConnectionChanged()
        If Me.ServerConnector.IsConnected Then
            Me.ServerConnector.Connection.ListDevicesById(Me._DevicesComboBox)
            Me._DevicesComboBox.SelectedIndex = 0
        End If
        Me._OpenDeviceModalityButton.Enabled = Me.ServerConnector.IsConnected
        Me._SelectDeviceSplitButton.Enabled = Me.ServerConnector.IsConnected
        Me._OpenDeviceModalityButton.Enabled = Me.ServerConnector.IsConnected
        Me.OnServerAttachmentChanged()
    End Sub

    ''' <summary> Executes the server attachment changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub OnServerAttachmentChanged()
        Me._ConnectServerButton.Image = My.Resources.WIFI_open_22
        If Me.ServerConnector.IsConnected Then
            Me._ConnectServerButton.Text = Me.ServerConnector.AttachedDevicesCount.ToString
            Me._ConnectServerButton.ForeColor = Drawing.Color.Black
        Else
            Me._ConnectServerButton.ForeColor = Drawing.Color.Red
            Me._ConnectServerButton.Text = "X"
        End If
    End Sub

#End Region

#Region " DEVICE CONNECTOR "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DeviceConnector As DeviceConnector
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the device connector. </summary>
    ''' <value> The device connector. </value>
    Public Property DeviceConnector As DeviceConnector
        Get
            Return Me._DeviceConnector
        End Get
        Set(value As DeviceConnector)
            Me._DeviceConnector = value
        End Set
    End Property

    ''' <summary> Device connector device closed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceClosed(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceClosed
        Me._DeviceInfoTextBox.Text = "closed"
        Me.OnModalityClosed()
    End Sub

    ''' <summary> Device connector device closing. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub DeviceConnector_DeviceClosing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _DeviceConnector.DeviceClosing
        Me.OnModalityClosing(e)
    End Sub

    ''' <summary> Device connector device opened. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceOpened(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceOpened

        Dim connector As DeviceConnector = TryCast(sender, DeviceConnector)
        Me._Device = connector.Device
        Me._DeviceInfoTextBox.Text = Me._Device.Name ' was .caption
        Me.OnModalityOpening()
    End Sub


#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As Dln.Device

    ''' <summary> Selected device information. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <returns> A DeviceInfo. </returns>
    Private Function SelectedDeviceInfo() As DeviceInfo
        If String.IsNullOrWhiteSpace(Me._DevicesComboBox.Text) Then
            Throw New InvalidOperationException("No devices selected")
        End If
        Return New DeviceInfo(Me._DevicesComboBox.Text)
    End Function

    ''' <summary> Opens the device for the modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="deviceId"> Identifier for the device. </param>
    ''' <param name="sender">   The sender. </param>
    Private Sub OpenDeviceModality(ByVal deviceId As Long, ByVal sender As Control)

        If Me.ServerConnector.IsConnected Then
            Me._OpenDeviceModalityButton.Enabled = Me.ServerConnector.IsConnected
            Me._SelectDeviceSplitButton.Enabled = Me.ServerConnector.IsConnected

        Else
            Me.ServerConnector.Connect()
        End If
        Me._DeviceConnector = New DeviceConnector(Me._ServerConnector)

        ' Open device
        Dim e As New ActionEventArgs
        If Me._DeviceConnector.TryOpenDevice(deviceId, Me.Modality, e) Then
            Me.OnModalityOpening()
        Else
            If sender IsNot Nothing Then Me._ErrorProvider.Annunciate(sender, e.Details)
            Me._DeviceInfoTextBox.Text = "No devices"
        End If

    End Sub

    ''' <summary> Closes device modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub CloseDeviceModality()
        Me._DeviceConnector.CloseDevice(Me.Modality)
        Dim e As New System.ComponentModel.CancelEventArgs
        Me.OnModalityClosing(e)
        If Not e.Cancel Then
            Me.OnModalityClosed()
        End If
    End Sub

    ''' <summary> Opens device button click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenDeviceModalityButton_Click(sender As Object, e As System.EventArgs) Handles _OpenDeviceModalityButton.Click
        Try
            Me._ErrorProvider.Clear()
            If Me.IsDeviceModalityOpen Then
                Me.CloseDeviceModality()
            Else

                Dim deviceId As Long = DeviceInfo.DefaultDeviceId
                If Not String.IsNullOrWhiteSpace(Me._DevicesComboBox.Text) Then
                    deviceId = Me.SelectedDeviceInfo.Id
                End If
                Me.OpenDeviceModality(deviceId, TryCast(sender, Control))
            End If
        Catch ex As Exception
            Me._ErrorProvider.Annunciate(sender, ex.ToString)
        Finally
            Me.OnModalityConnectionChanged()
        End Try
    End Sub

    ''' <summary> Gets a value indicating whether a device is open. </summary>
    ''' <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
    Private ReadOnly Property IsDeviceModalityOpen As Boolean
        Get
            Return Me._Device IsNot Nothing AndAlso Me.IsModalityOpen
        End Get
    End Property

    ''' <summary> Executes the modality connection changed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnModalityConnectionChanged()
        If Me.IsDeviceModalityOpen Then
            Me._OpenDeviceModalityButton.Image = My.Resources.user_online_2
            Me._OpenDeviceModalityButton.Text = "Close"
        Else
            Me._OpenDeviceModalityButton.Image = My.Resources.user_offline_2
            Me._OpenDeviceModalityButton.Text = "Open"
        End If
    End Sub

#End Region

#Region " MODALITIES "

    ''' <summary> Gets or sets the modalities. </summary>
    ''' <value> The modalities. </value>
    Public Property SupportedModalities As DeviceModalities

#End Region

#Region " EVENT LOG "

    ''' <summary> Event log text box double click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub EventLogTextBox_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EventLogTextBox.DoubleClick
        Me._EventLogTextBox.Clear()
    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Select tab menu item click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SelectTabMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles _SelectPrimaryTabMenuItem.Click,
                                                                                                _SelectEventsTabMenuItem.Click
        Dim item As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If item IsNot Nothing Then Me._SelectTabButton.Image = item.Image
        Select Case item.Name
            Case "_SelectPrimaryTabMenuItem"
                Me._Tabs.SelectTab(Me._PrimaryTabPage)
            Case "_SelectEventsTabMenuItem"
                Me._Tabs.SelectTab(Me._EventLogTabPage)
        End Select

    End Sub

#End Region

End Class
