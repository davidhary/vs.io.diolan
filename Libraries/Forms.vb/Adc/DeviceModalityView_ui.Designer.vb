<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeviceModalityView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._BottomToolStrip = New System.Windows.Forms.ToolStrip()
        Me._DeviceInfoTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._Tabs = New isr.Diolan.Forms.ExtendedTabControl()
        Me._PrimaryTabPage = New System.Windows.Forms.TabPage()
        Me._EventLogTabPage = New System.Windows.Forms.TabPage()
        Me._EventLogTextBox = New System.Windows.Forms.TextBox()
        Me._TopToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ValueTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ToolStripContainer = New System.Windows.Forms.ToolStripContainer()
        Me._SelectTabButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._SelectPrimaryTabMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SelectEventsTabMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SelectServerButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._ServerNameTextBox = New System.Windows.Forms.ToolStripTextBox()
        Me.DefaultServerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ConnectServerButton = New System.Windows.Forms.ToolStripButton()
        Me._SelectDeviceSplitButton = New System.Windows.Forms.ToolStripSplitButton()
        Me._DevicesComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._OpenDeviceModalityButton = New System.Windows.Forms.ToolStripButton()
        Me._GetValueButton = New System.Windows.Forms.ToolStripButton()
        Me._BottomToolStrip.SuspendLayout()
        Me._Tabs.SuspendLayout()
        Me._EventLogTabPage.SuspendLayout()
        Me._TopToolStrip.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ToolStripContainer.BottomToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.ContentPanel.SuspendLayout()
        Me._ToolStripContainer.TopToolStripPanel.SuspendLayout()
        Me._ToolStripContainer.SuspendLayout()
        Me.SuspendLayout()
        '
        '_BottomToolStrip
        '
        Me._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._BottomToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SelectTabButton, Me._SelectServerButton, Me._ConnectServerButton, Me._DeviceInfoTextBox, Me._SelectDeviceSplitButton, Me._OpenDeviceModalityButton})
        Me._BottomToolStrip.Location = New System.Drawing.Point(3, 0)
        Me._BottomToolStrip.Name = "_BottomToolStrip"
        Me._BottomToolStrip.Size = New System.Drawing.Size(280, 29)
        Me._BottomToolStrip.TabIndex = 0
        '
        '_DeviceInfoTextBox
        '
        Me._DeviceInfoTextBox.Name = "_DeviceInfoTextBox"
        Me._DeviceInfoTextBox.ReadOnly = True
        Me._DeviceInfoTextBox.Size = New System.Drawing.Size(100, 29)
        Me._DeviceInfoTextBox.Text = "closed"
        Me._DeviceInfoTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._PrimaryTabPage)
        Me._Tabs.Controls.Add(Me._EventLogTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.HideTabHeaders = True
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(364, 427)
        Me._Tabs.TabIndex = 0
        '
        '_PrimaryTabPage
        '
        Me._PrimaryTabPage.Location = New System.Drawing.Point(4, 26)
        Me._PrimaryTabPage.Name = "_PrimaryTabPage"
        Me._PrimaryTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._PrimaryTabPage.Size = New System.Drawing.Size(356, 397)
        Me._PrimaryTabPage.TabIndex = 0
        Me._PrimaryTabPage.Text = "Primary"
        Me._PrimaryTabPage.UseVisualStyleBackColor = True
        '
        '_EventLogTabPage
        '
        Me._EventLogTabPage.Controls.Add(Me._EventLogTextBox)
        Me._EventLogTabPage.Location = New System.Drawing.Point(4, 22)
        Me._EventLogTabPage.Name = "_EventLogTabPage"
        Me._EventLogTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._EventLogTabPage.Size = New System.Drawing.Size(356, 401)
        Me._EventLogTabPage.TabIndex = 1
        Me._EventLogTabPage.Text = "Events"
        Me._EventLogTabPage.UseVisualStyleBackColor = True
        '
        '_EventLogTextBox
        '
        Me._EventLogTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._EventLogTextBox.Location = New System.Drawing.Point(3, 3)
        Me._EventLogTextBox.Multiline = True
        Me._EventLogTextBox.Name = "_EventLogTextBox"
        Me._EventLogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._EventLogTextBox.Size = New System.Drawing.Size(350, 395)
        Me._EventLogTextBox.TabIndex = 0
        '
        '_TopToolStrip
        '
        Me._TopToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TopToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ValueTextBox, Me._GetValueButton})
        Me._TopToolStrip.Location = New System.Drawing.Point(3, 0)
        Me._TopToolStrip.Name = "_TopToolStrip"
        Me._TopToolStrip.Size = New System.Drawing.Size(140, 33)
        Me._TopToolStrip.TabIndex = 0
        '
        '_ValueTextBox
        '
        Me._ValueTextBox.BackColor = System.Drawing.Color.Black
        Me._ValueTextBox.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ValueTextBox.ForeColor = System.Drawing.Color.Aquamarine
        Me._ValueTextBox.Name = "_ValueTextBox"
        Me._ValueTextBox.Size = New System.Drawing.Size(100, 33)
        Me._ValueTextBox.Text = "0.0 V"
        Me._ValueTextBox.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ValueTextBox.ToolTipText = "Value"
        '
        '_ErrorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_ToolStripContainer
        '
        '
        '_ToolStripContainer.BottomToolStripPanel
        '
        Me._ToolStripContainer.BottomToolStripPanel.Controls.Add(Me._BottomToolStrip)
        '
        '_ToolStripContainer.ContentPanel
        '
        Me._ToolStripContainer.ContentPanel.Controls.Add(Me._Tabs)
        Me._ToolStripContainer.ContentPanel.Size = New System.Drawing.Size(364, 427)
        Me._ToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ToolStripContainer.Location = New System.Drawing.Point(0, 0)
        Me._ToolStripContainer.Name = "_ToolStripContainer"
        Me._ToolStripContainer.Size = New System.Drawing.Size(364, 489)
        Me._ToolStripContainer.TabIndex = 0
        Me._ToolStripContainer.Text = "ToolStripContainer1"
        '
        '_ToolStripContainer.TopToolStripPanel
        '
        Me._ToolStripContainer.TopToolStripPanel.Controls.Add(Me._TopToolStrip)
        '
        '_SelectTabButton
        '
        Me._SelectTabButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectTabButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._SelectPrimaryTabMenuItem, Me._SelectEventsTabMenuItem})
        Me._SelectTabButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.run_build_configure
        Me._SelectTabButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectTabButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectTabButton.Name = "_SelectTabButton"
        Me._SelectTabButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectTabButton.Text = "Tab"
        '
        '_SelectPrimaryTabMenuItem
        '
        Me._SelectPrimaryTabMenuItem.Image = Global.isr.Diolan.Forms.My.Resources.Resources.run_build_configure
        Me._SelectPrimaryTabMenuItem.Name = "_SelectPrimaryTabMenuItem"
        Me._SelectPrimaryTabMenuItem.Size = New System.Drawing.Size(115, 22)
        Me._SelectPrimaryTabMenuItem.Text = "Primary"
        '
        '_SelectEventsTabMenuItem
        '
        Me._SelectEventsTabMenuItem.Image = Global.isr.Diolan.Forms.My.Resources.Resources.view_calendar_upcoming_events
        Me._SelectEventsTabMenuItem.Name = "_SelectEventsTabMenuItem"
        Me._SelectEventsTabMenuItem.Size = New System.Drawing.Size(115, 22)
        Me._SelectEventsTabMenuItem.Text = "Events"
        '
        '_SelectServerButton
        '
        Me._SelectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectServerButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ServerNameTextBox, Me.DefaultServerToolStripMenuItem})
        Me._SelectServerButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.network_server
        Me._SelectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectServerButton.Name = "_SelectServerButton"
        Me._SelectServerButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectServerButton.Text = "Select Server"
        '
        '_ServerNameTextBox
        '
        Me._ServerNameTextBox.Name = "_ServerNameTextBox"
        Me._ServerNameTextBox.Size = New System.Drawing.Size(100, 23)
        Me._ServerNameTextBox.Text = "localhost:9656"
        '
        'DefaultServerToolStripMenuItem
        '
        Me.DefaultServerToolStripMenuItem.Checked = True
        Me.DefaultServerToolStripMenuItem.CheckOnClick = True
        Me.DefaultServerToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.DefaultServerToolStripMenuItem.Name = "DefaultServerToolStripMenuItem"
        Me.DefaultServerToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.DefaultServerToolStripMenuItem.Text = "User Default Server:Port"
        '
        '_ConnectServerButton
        '
        Me._ConnectServerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._ConnectServerButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.network_disconnect_2
        Me._ConnectServerButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ConnectServerButton.Name = "_ConnectServerButton"
        Me._ConnectServerButton.Size = New System.Drawing.Size(26, 26)
        Me._ConnectServerButton.Text = "Connect Server"
        '
        '_SelectDeviceSplitButton
        '
        Me._SelectDeviceSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._SelectDeviceSplitButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._DevicesComboBox})
        Me._SelectDeviceSplitButton.Enabled = False
        Me._SelectDeviceSplitButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.network_server_database
        Me._SelectDeviceSplitButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton"
        Me._SelectDeviceSplitButton.Size = New System.Drawing.Size(38, 26)
        Me._SelectDeviceSplitButton.Text = "Device"
        Me._SelectDeviceSplitButton.ToolTipText = "Select Device"
        '
        '_DevicesComboBox
        '
        Me._DevicesComboBox.Name = "_DevicesComboBox"
        Me._DevicesComboBox.Size = New System.Drawing.Size(121, 23)
        Me._DevicesComboBox.Text = "DLN-4M.1.1"
        '
        '_OpenDeviceModalityButton
        '
        Me._OpenDeviceModalityButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._OpenDeviceModalityButton.Enabled = False
        Me._OpenDeviceModalityButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.user_offline_2
        Me._OpenDeviceModalityButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._OpenDeviceModalityButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton"
        Me._OpenDeviceModalityButton.Size = New System.Drawing.Size(26, 26)
        Me._OpenDeviceModalityButton.Text = "Open"
        Me._OpenDeviceModalityButton.ToolTipText = "Open or close the device."
        '
        '_GetValueButton
        '
        Me._GetValueButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me._GetValueButton.Image = Global.isr.Diolan.Forms.My.Resources.Resources.media_playback_start_4
        Me._GetValueButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me._GetValueButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._GetValueButton.Name = "_GetValueButton"
        Me._GetValueButton.Size = New System.Drawing.Size(26, 30)
        Me._GetValueButton.Text = "Read Value"
        '
        'DeviceModalityView
        '
        Me.Controls.Add(Me._ToolStripContainer)
        Me.Name = "DeviceModalityView"
        Me.Size = New System.Drawing.Size(364, 489)
        Me._BottomToolStrip.ResumeLayout(False)
        Me._BottomToolStrip.PerformLayout()
        Me._Tabs.ResumeLayout(False)
        Me._EventLogTabPage.ResumeLayout(False)
        Me._EventLogTabPage.PerformLayout()
        Me._TopToolStrip.ResumeLayout(False)
        Me._TopToolStrip.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ToolStripContainer.BottomToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.BottomToolStripPanel.PerformLayout()
        Me._ToolStripContainer.ContentPanel.ResumeLayout(False)
        Me._ToolStripContainer.TopToolStripPanel.ResumeLayout(False)
        Me._ToolStripContainer.TopToolStripPanel.PerformLayout()
        Me._ToolStripContainer.ResumeLayout(False)
        Me._ToolStripContainer.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _BottomToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _DeviceInfoTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _OpenDeviceModalityButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _TopToolStrip As System.Windows.Forms.ToolStrip
    Private WithEvents _ValueTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents _GetValueButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _PrimaryTabPage As System.Windows.Forms.TabPage
    Private WithEvents _EventLogTabPage As System.Windows.Forms.TabPage
    Private WithEvents _EventLogTextBox As System.Windows.Forms.TextBox
    Private WithEvents _ToolStripContainer As System.Windows.Forms.ToolStripContainer
    Private WithEvents _Tabs As ExtendedTabControl
    Private WithEvents _SelectTabButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _SelectEventsTabMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _DevicesComboBox As System.Windows.Forms.ToolStripComboBox
    Private WithEvents _SelectServerButton As System.Windows.Forms.ToolStripSplitButton
    Private WithEvents _ServerNameTextBox As System.Windows.Forms.ToolStripTextBox
    Private WithEvents DefaultServerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _SelectPrimaryTabMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents _ConnectServerButton As System.Windows.Forms.ToolStripButton
    Private WithEvents _SelectDeviceSplitButton As System.Windows.Forms.ToolStripSplitButton

End Class
