﻿
namespace isr.Diolan.Forms.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-24. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Core.ProjectTraceEventId.DigitalInputOutputLan;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Diolan Forms Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Diolan Forms Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Diolan.Forms";

        /// <summary> Applies the given <see cref="isr.Core.Logger"/>. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="value"> The <see cref="isr.Core.Logger"/>. </param>
        public static void Apply( Core.Logger value )
        {
            Appliance.Apply( value );
            Diolan.My.MyLibrary.Appliance.Apply( value );
        }
    }
}