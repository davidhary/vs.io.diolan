﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{
    [DesignerGenerated()]
    public partial class GpioView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _BottomToolStrip = new ToolStrip();
            __TabComboBox = new ToolStripComboBox();
            __TabComboBox.SelectedIndexChanged += new EventHandler(TabComboBox_SelectedIndexChanged);
            _SelectServerButton = new ToolStripSplitButton();
            _ServerNameTextBox = new ToolStripTextBox();
            _DefaultServerMenuItem = new ToolStripMenuItem();
            __ConnectServerButton = new ToolStripButton();
            __ConnectServerButton.Click += new EventHandler(ConnectServerButton_Click);
            __OpenDeviceModalityButton = new ToolStripButton();
            __OpenDeviceModalityButton.Click += new EventHandler(OpenDeviceModalityButton_Click);
            _SelectDeviceSplitButton = new ToolStripSplitButton();
            _DevicesComboBox = new ToolStripComboBox();
            _DeviceInfoTextBox = new ToolStripTextBox();
            _Tabs = new ExtendedTabControl();
            _PrimaryTabPage = new TabPage();
            _PrimaryTabLayout = new TableLayoutPanel();
            _SubsystemGroupBox = new GroupBox();
            _DebounceNumeric = new NumericUpDown();
            __GetDebounceButton = new Button();
            __GetDebounceButton.Click += new EventHandler(GetDebounceButton_Click);
            __SetDebounceButton = new Button();
            __SetDebounceButton.Click += new EventHandler(SetDebounceButton_Click);
            _DebounceNumericLabel = new Label();
            _PinTabPage = new TabPage();
            _PinTabLayout = new TableLayoutPanel();
            _PinConfigGroupBox = new GroupBox();
            _OpenDrainCheckBox = new CheckBox();
            _PinPullupCheckBox = new CheckBox();
            _EventPeriodNumeric = new NumericUpDown();
            _PulldownCheckBox = new CheckBox();
            _DebounceEnabledCheckBox = new CheckBox();
            _EventPeriodNumericLabel = new Label();
            _EventTypeComboBoxLabel = new Label();
            _EventTypeComboBox = new ComboBox();
            __GetPinConfigButton = new Button();
            __GetPinConfigButton.Click += new EventHandler(GetPinConfigButton_Click);
            __SetPinConfigButton = new Button();
            __SetPinConfigButton.Click += new EventHandler(SetPinConfigButton_Click);
            _PinDirectionComboBox = new ComboBox();
            _PinDirectionComboBoxLabel = new Label();
            _PinEnabledCheckBox = new CheckBox();
            __PinComboBox = new ComboBox();
            __PinComboBox.SelectedIndexChanged += new EventHandler(PinComboBox_SelectedIndexChanged);
            _PinComboBoxLabel = new Label();
            _EventLogTabPage = new TabPage();
            __EventLogTextBox = new TextBox();
            __EventLogTextBox.DoubleClick += new EventHandler(EventLogTextBox_DoubleClick);
            _ErrorProvider = new ErrorProvider(components);
            _ToolTip = new ToolTip(components);
            _ToolStripContainer = new ToolStripContainer();
            _GpioPinControl2 = new GpioPinControl();
            _GpioPinControl1 = new GpioPinControl();
            _BottomToolStrip.SuspendLayout();
            _Tabs.SuspendLayout();
            _PrimaryTabPage.SuspendLayout();
            _PrimaryTabLayout.SuspendLayout();
            _SubsystemGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_DebounceNumeric).BeginInit();
            _PinTabPage.SuspendLayout();
            _PinTabLayout.SuspendLayout();
            _PinConfigGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_EventPeriodNumeric).BeginInit();
            _EventLogTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            _ToolStripContainer.ContentPanel.SuspendLayout();
            _ToolStripContainer.SuspendLayout();
            SuspendLayout();
            // 
            // _BottomToolStrip
            // 
            _BottomToolStrip.Dock = DockStyle.None;
            _BottomToolStrip.Items.AddRange(new ToolStripItem[] { __TabComboBox, _SelectServerButton, __ConnectServerButton, __OpenDeviceModalityButton, _SelectDeviceSplitButton, _DeviceInfoTextBox });
            _BottomToolStrip.Location = new System.Drawing.Point(0, 0);
            _BottomToolStrip.Name = "_BottomToolStrip";
            _BottomToolStrip.Size = new System.Drawing.Size(417, 29);
            _BottomToolStrip.Stretch = true;
            _BottomToolStrip.TabIndex = 0;
            // 
            // _TabComboBox
            // 
            __TabComboBox.Name = "__TabComboBox";
            __TabComboBox.Size = new System.Drawing.Size(81, 29);
            __TabComboBox.ToolTipText = "Select panel";
            // 
            // _SelectServerButton
            // 
            _SelectServerButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectServerButton.DropDownItems.AddRange(new ToolStripItem[] { _ServerNameTextBox, _DefaultServerMenuItem });
            _SelectServerButton.Image = My.Resources.Resources.network_server;
            _SelectServerButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectServerButton.Name = "_SelectServerButton";
            _SelectServerButton.Size = new System.Drawing.Size(38, 26);
            _SelectServerButton.Text = "Select Server";
            // 
            // _ServerNameTextBox
            // 
            _ServerNameTextBox.Name = "_ServerNameTextBox";
            _ServerNameTextBox.Size = new System.Drawing.Size(100, 23);
            _ServerNameTextBox.Text = "localhost:9656";
            // 
            // _DefaultServerMenuItem
            // 
            _DefaultServerMenuItem.Checked = true;
            _DefaultServerMenuItem.CheckOnClick = true;
            _DefaultServerMenuItem.CheckState = CheckState.Checked;
            _DefaultServerMenuItem.Name = "_DefaultServerMenuItem";
            _DefaultServerMenuItem.Size = new System.Drawing.Size(198, 22);
            _DefaultServerMenuItem.Text = "User Default Server:Port";
            // 
            // _ConnectServerButton
            // 
            __ConnectServerButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ConnectServerButton.ForeColor = System.Drawing.Color.Red;
            __ConnectServerButton.Image = My.Resources.Resources.WIFI_open_22_Right_Up;
            __ConnectServerButton.ImageScaling = ToolStripItemImageScaling.None;
            __ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ConnectServerButton.Name = "__ConnectServerButton";
            __ConnectServerButton.Size = new System.Drawing.Size(41, 26);
            __ConnectServerButton.Text = "X";
            __ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            __ConnectServerButton.ToolTipText = "Connect or disconnect serve and show attached devices.";
            // 
            // _OpenDeviceModalityButton
            // 
            __OpenDeviceModalityButton.Alignment = ToolStripItemAlignment.Right;
            __OpenDeviceModalityButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __OpenDeviceModalityButton.Enabled = false;
            __OpenDeviceModalityButton.Image = My.Resources.Resources.user_offline_2;
            __OpenDeviceModalityButton.ImageScaling = ToolStripItemImageScaling.None;
            __OpenDeviceModalityButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenDeviceModalityButton.Name = "__OpenDeviceModalityButton";
            __OpenDeviceModalityButton.Size = new System.Drawing.Size(26, 26);
            __OpenDeviceModalityButton.Text = "Open";
            __OpenDeviceModalityButton.ToolTipText = "Open or close the device.";
            // 
            // _SelectDeviceSplitButton
            // 
            _SelectDeviceSplitButton.Alignment = ToolStripItemAlignment.Right;
            _SelectDeviceSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectDeviceSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _DevicesComboBox });
            _SelectDeviceSplitButton.Image = My.Resources.Resources.network_server_database;
            _SelectDeviceSplitButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton";
            _SelectDeviceSplitButton.Size = new System.Drawing.Size(38, 26);
            _SelectDeviceSplitButton.Text = "Device";
            _SelectDeviceSplitButton.ToolTipText = "Select Device";
            // 
            // _DevicesComboBox
            // 
            _DevicesComboBox.Name = "_DevicesComboBox";
            _DevicesComboBox.Size = new System.Drawing.Size(121, 23);
            _DevicesComboBox.Text = "DLN-4M.1.1";
            // 
            // _DeviceInfoTextBox
            // 
            _DeviceInfoTextBox.Alignment = ToolStripItemAlignment.Right;
            _DeviceInfoTextBox.Name = "_DeviceInfoTextBox";
            _DeviceInfoTextBox.ReadOnly = true;
            _DeviceInfoTextBox.Size = new System.Drawing.Size(100, 29);
            _DeviceInfoTextBox.Text = "closed";
            _DeviceInfoTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_PrimaryTabPage);
            _Tabs.Controls.Add(_PinTabPage);
            _Tabs.Controls.Add(_EventLogTabPage);
            _Tabs.Dock = DockStyle.Fill;
            _Tabs.HideTabHeaders = true;
            _Tabs.Location = new System.Drawing.Point(0, 57);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new System.Drawing.Size(417, 395);
            _Tabs.TabIndex = 0;
            // 
            // _PrimaryTabPage
            // 
            _PrimaryTabPage.Controls.Add(_PrimaryTabLayout);
            _PrimaryTabPage.Location = new System.Drawing.Point(4, 26);
            _PrimaryTabPage.Name = "_PrimaryTabPage";
            _PrimaryTabPage.Padding = new Padding(3);
            _PrimaryTabPage.Size = new System.Drawing.Size(409, 365);
            _PrimaryTabPage.TabIndex = 0;
            _PrimaryTabPage.Text = "GPIO";
            _PrimaryTabPage.UseVisualStyleBackColor = true;
            // 
            // _PrimaryTabLayout
            // 
            _PrimaryTabLayout.ColumnCount = 3;
            _PrimaryTabLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _PrimaryTabLayout.ColumnStyles.Add(new ColumnStyle());
            _PrimaryTabLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _PrimaryTabLayout.Controls.Add(_SubsystemGroupBox, 1, 1);
            _PrimaryTabLayout.Dock = DockStyle.Fill;
            _PrimaryTabLayout.Location = new System.Drawing.Point(3, 3);
            _PrimaryTabLayout.Name = "_PrimaryTabLayout";
            _PrimaryTabLayout.RowCount = 3;
            _PrimaryTabLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _PrimaryTabLayout.RowStyles.Add(new RowStyle());
            _PrimaryTabLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _PrimaryTabLayout.Size = new System.Drawing.Size(403, 359);
            _PrimaryTabLayout.TabIndex = 8;
            // 
            // _SubsystemGroupBox
            // 
            _SubsystemGroupBox.Controls.Add(_DebounceNumeric);
            _SubsystemGroupBox.Controls.Add(__GetDebounceButton);
            _SubsystemGroupBox.Controls.Add(__SetDebounceButton);
            _SubsystemGroupBox.Controls.Add(_DebounceNumericLabel);
            _SubsystemGroupBox.Location = new System.Drawing.Point(24, 147);
            _SubsystemGroupBox.Margin = new Padding(3, 4, 3, 4);
            _SubsystemGroupBox.Name = "_SubsystemGroupBox";
            _SubsystemGroupBox.Padding = new Padding(3, 4, 3, 4);
            _SubsystemGroupBox.Size = new System.Drawing.Size(355, 64);
            _SubsystemGroupBox.TabIndex = 6;
            _SubsystemGroupBox.TabStop = false;
            _SubsystemGroupBox.Text = "GPIO Subsystem Settings";
            // 
            // _DebounceNumeric
            // 
            _DebounceNumeric.Location = new System.Drawing.Point(151, 22);
            _DebounceNumeric.Margin = new Padding(3, 4, 3, 4);
            _DebounceNumeric.Maximum = new decimal(new int[] { 32000, 0, 0, 0 });
            _DebounceNumeric.Name = "_DebounceNumeric";
            _DebounceNumeric.Size = new System.Drawing.Size(63, 25);
            _DebounceNumeric.TabIndex = 1;
            // 
            // _GetDebounceButton
            // 
            __GetDebounceButton.Location = new System.Drawing.Point(287, 19);
            __GetDebounceButton.Margin = new Padding(3, 4, 3, 4);
            __GetDebounceButton.Name = "__GetDebounceButton";
            __GetDebounceButton.Size = new System.Drawing.Size(61, 30);
            __GetDebounceButton.TabIndex = 3;
            __GetDebounceButton.Text = "Get";
            _ToolTip.SetToolTip(__GetDebounceButton, "Gets the debounce interval");
            __GetDebounceButton.UseVisualStyleBackColor = true;
            // 
            // _SetDebounceButton
            // 
            __SetDebounceButton.Location = new System.Drawing.Point(219, 19);
            __SetDebounceButton.Margin = new Padding(3, 4, 3, 4);
            __SetDebounceButton.Name = "__SetDebounceButton";
            __SetDebounceButton.Size = new System.Drawing.Size(61, 30);
            __SetDebounceButton.TabIndex = 2;
            __SetDebounceButton.Text = "Set";
            _ToolTip.SetToolTip(__SetDebounceButton, "Sets the debounce interval");
            __SetDebounceButton.UseVisualStyleBackColor = true;
            // 
            // _DebounceNumericLabel
            // 
            _DebounceNumericLabel.AutoSize = true;
            _DebounceNumericLabel.Location = new System.Drawing.Point(7, 26);
            _DebounceNumericLabel.Name = "_DebounceNumericLabel";
            _DebounceNumericLabel.Size = new System.Drawing.Size(142, 17);
            _DebounceNumericLabel.TabIndex = 0;
            _DebounceNumericLabel.Text = "Debounce Interval [µs]:";
            _DebounceNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PinTabPage
            // 
            _PinTabPage.Controls.Add(_PinTabLayout);
            _PinTabPage.Location = new System.Drawing.Point(4, 26);
            _PinTabPage.Name = "_PinTabPage";
            _PinTabPage.Size = new System.Drawing.Size(409, 365);
            _PinTabPage.TabIndex = 2;
            _PinTabPage.Text = "Pin";
            _PinTabPage.UseVisualStyleBackColor = true;
            // 
            // _PinTabLayout
            // 
            _PinTabLayout.ColumnCount = 3;
            _PinTabLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _PinTabLayout.ColumnStyles.Add(new ColumnStyle());
            _PinTabLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _PinTabLayout.Controls.Add(_PinConfigGroupBox, 1, 1);
            _PinTabLayout.Dock = DockStyle.Fill;
            _PinTabLayout.Location = new System.Drawing.Point(0, 0);
            _PinTabLayout.Name = "_PinTabLayout";
            _PinTabLayout.RowCount = 3;
            _PinTabLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _PinTabLayout.RowStyles.Add(new RowStyle());
            _PinTabLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _PinTabLayout.Size = new System.Drawing.Size(409, 365);
            _PinTabLayout.TabIndex = 9;
            // 
            // _PinConfigGroupBox
            // 
            _PinConfigGroupBox.Controls.Add(_OpenDrainCheckBox);
            _PinConfigGroupBox.Controls.Add(_PinPullupCheckBox);
            _PinConfigGroupBox.Controls.Add(_EventPeriodNumeric);
            _PinConfigGroupBox.Controls.Add(_PulldownCheckBox);
            _PinConfigGroupBox.Controls.Add(_DebounceEnabledCheckBox);
            _PinConfigGroupBox.Controls.Add(_EventPeriodNumericLabel);
            _PinConfigGroupBox.Controls.Add(_EventTypeComboBoxLabel);
            _PinConfigGroupBox.Controls.Add(_EventTypeComboBox);
            _PinConfigGroupBox.Controls.Add(__GetPinConfigButton);
            _PinConfigGroupBox.Controls.Add(__SetPinConfigButton);
            _PinConfigGroupBox.Controls.Add(_PinDirectionComboBox);
            _PinConfigGroupBox.Controls.Add(_PinDirectionComboBoxLabel);
            _PinConfigGroupBox.Controls.Add(_PinEnabledCheckBox);
            _PinConfigGroupBox.Controls.Add(__PinComboBox);
            _PinConfigGroupBox.Controls.Add(_PinComboBoxLabel);
            _PinConfigGroupBox.Location = new System.Drawing.Point(48, 58);
            _PinConfigGroupBox.Margin = new Padding(3, 4, 3, 4);
            _PinConfigGroupBox.Name = "_PinConfigGroupBox";
            _PinConfigGroupBox.Padding = new Padding(3, 4, 3, 4);
            _PinConfigGroupBox.Size = new System.Drawing.Size(313, 249);
            _PinConfigGroupBox.TabIndex = 8;
            _PinConfigGroupBox.TabStop = false;
            _PinConfigGroupBox.Text = "Pin Configuration";
            // 
            // _OpenDrainCheckBox
            // 
            _OpenDrainCheckBox.AutoSize = true;
            _OpenDrainCheckBox.Location = new System.Drawing.Point(187, 139);
            _OpenDrainCheckBox.Margin = new Padding(3, 4, 3, 4);
            _OpenDrainCheckBox.Name = "_OpenDrainCheckBox";
            _OpenDrainCheckBox.Size = new System.Drawing.Size(94, 21);
            _OpenDrainCheckBox.TabIndex = 10;
            _OpenDrainCheckBox.Text = "Open Drain";
            _ToolTip.SetToolTip(_OpenDrainCheckBox, "Pin is open drain is checked.");
            _OpenDrainCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PinPullupCheckBox
            // 
            _PinPullupCheckBox.AutoSize = true;
            _PinPullupCheckBox.Location = new System.Drawing.Point(19, 139);
            _PinPullupCheckBox.Margin = new Padding(3, 4, 3, 4);
            _PinPullupCheckBox.Name = "_PinPullupCheckBox";
            _PinPullupCheckBox.Size = new System.Drawing.Size(68, 21);
            _PinPullupCheckBox.TabIndex = 8;
            _PinPullupCheckBox.Text = "Pull Up";
            _ToolTip.SetToolTip(_PinPullupCheckBox, "pin is pulled up if checked.");
            _PinPullupCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EventPeriodNumeric
            // 
            _EventPeriodNumeric.Location = new System.Drawing.Point(133, 211);
            _EventPeriodNumeric.Maximum = new decimal(new int[] { 65535, 0, 0, 0 });
            _EventPeriodNumeric.Name = "_EventPeriodNumeric";
            _EventPeriodNumeric.Size = new System.Drawing.Size(60, 25);
            _EventPeriodNumeric.TabIndex = 14;
            _ToolTip.SetToolTip(_EventPeriodNumeric, "Selects the event period in ms");
            // 
            // _PulldownCheckBox
            // 
            _PulldownCheckBox.AutoSize = true;
            _PulldownCheckBox.Location = new System.Drawing.Point(95, 139);
            _PulldownCheckBox.Margin = new Padding(3, 4, 3, 4);
            _PulldownCheckBox.Name = "_PulldownCheckBox";
            _PulldownCheckBox.Size = new System.Drawing.Size(84, 21);
            _PulldownCheckBox.TabIndex = 9;
            _PulldownCheckBox.Text = "Pull Down";
            _ToolTip.SetToolTip(_PulldownCheckBox, "Pin is pulled down if checked");
            _PulldownCheckBox.UseVisualStyleBackColor = true;
            // 
            // _DebounceEnabledCheckBox
            // 
            _DebounceEnabledCheckBox.AutoSize = true;
            _DebounceEnabledCheckBox.Location = new System.Drawing.Point(126, 66);
            _DebounceEnabledCheckBox.Margin = new Padding(3, 4, 3, 4);
            _DebounceEnabledCheckBox.Name = "_DebounceEnabledCheckBox";
            _DebounceEnabledCheckBox.Size = new System.Drawing.Size(137, 21);
            _DebounceEnabledCheckBox.TabIndex = 5;
            _DebounceEnabledCheckBox.Text = "Debounce Enabled";
            _ToolTip.SetToolTip(_DebounceEnabledCheckBox, "Debounce is enabled if checked.");
            _DebounceEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EventPeriodNumericLabel
            // 
            _EventPeriodNumericLabel.AutoSize = true;
            _EventPeriodNumericLabel.Location = new System.Drawing.Point(18, 215);
            _EventPeriodNumericLabel.Name = "_EventPeriodNumericLabel";
            _EventPeriodNumericLabel.Size = new System.Drawing.Size(113, 17);
            _EventPeriodNumericLabel.TabIndex = 13;
            _EventPeriodNumericLabel.Text = "Event Period [ms]:";
            _EventPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EventTypeComboBoxLabel
            // 
            _EventTypeComboBoxLabel.AutoSize = true;
            _EventTypeComboBoxLabel.Location = new System.Drawing.Point(18, 180);
            _EventTypeComboBoxLabel.Name = "_EventTypeComboBoxLabel";
            _EventTypeComboBoxLabel.Size = new System.Drawing.Size(73, 17);
            _EventTypeComboBoxLabel.TabIndex = 11;
            _EventTypeComboBoxLabel.Text = "Event Type:";
            _EventTypeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EventTypeComboBox
            // 
            _EventTypeComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _EventTypeComboBox.FormattingEnabled = true;
            _EventTypeComboBox.Location = new System.Drawing.Point(94, 176);
            _EventTypeComboBox.Margin = new Padding(3, 4, 3, 4);
            _EventTypeComboBox.Name = "_EventTypeComboBox";
            _EventTypeComboBox.Size = new System.Drawing.Size(101, 25);
            _EventTypeComboBox.TabIndex = 12;
            _ToolTip.SetToolTip(_EventTypeComboBox, "Selects the event type.");
            // 
            // _GetPinConfigButton
            // 
            __GetPinConfigButton.Location = new System.Drawing.Point(158, 22);
            __GetPinConfigButton.Margin = new Padding(3, 4, 3, 4);
            __GetPinConfigButton.Name = "__GetPinConfigButton";
            __GetPinConfigButton.Size = new System.Drawing.Size(61, 30);
            __GetPinConfigButton.TabIndex = 2;
            __GetPinConfigButton.Text = "Get";
            __GetPinConfigButton.UseVisualStyleBackColor = true;
            // 
            // _SetPinConfigButton
            // 
            __SetPinConfigButton.Location = new System.Drawing.Point(236, 22);
            __SetPinConfigButton.Margin = new Padding(3, 4, 3, 4);
            __SetPinConfigButton.Name = "__SetPinConfigButton";
            __SetPinConfigButton.Size = new System.Drawing.Size(61, 30);
            __SetPinConfigButton.TabIndex = 3;
            __SetPinConfigButton.Text = "Set";
            __SetPinConfigButton.UseVisualStyleBackColor = true;
            // 
            // _PinDirectionComboBox
            // 
            _PinDirectionComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _PinDirectionComboBox.FormattingEnabled = true;
            _PinDirectionComboBox.Location = new System.Drawing.Point(81, 102);
            _PinDirectionComboBox.Margin = new Padding(3, 4, 3, 4);
            _PinDirectionComboBox.Name = "_PinDirectionComboBox";
            _PinDirectionComboBox.Size = new System.Drawing.Size(79, 25);
            _PinDirectionComboBox.TabIndex = 7;
            _ToolTip.SetToolTip(_PinDirectionComboBox, "Selects pin direction.");
            // 
            // _PinDirectionComboBoxLabel
            // 
            _PinDirectionComboBoxLabel.AutoSize = true;
            _PinDirectionComboBoxLabel.Location = new System.Drawing.Point(16, 106);
            _PinDirectionComboBoxLabel.Name = "_PinDirectionComboBoxLabel";
            _PinDirectionComboBoxLabel.Size = new System.Drawing.Size(63, 17);
            _PinDirectionComboBoxLabel.TabIndex = 6;
            _PinDirectionComboBoxLabel.Text = "Direction:";
            _PinDirectionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PinEnabledCheckBox
            // 
            _PinEnabledCheckBox.AutoSize = true;
            _PinEnabledCheckBox.Location = new System.Drawing.Point(44, 66);
            _PinEnabledCheckBox.Margin = new Padding(3, 4, 3, 4);
            _PinEnabledCheckBox.Name = "_PinEnabledCheckBox";
            _PinEnabledCheckBox.Size = new System.Drawing.Size(74, 21);
            _PinEnabledCheckBox.TabIndex = 4;
            _PinEnabledCheckBox.Text = "Enabled";
            _PinEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PinComboBox
            // 
            __PinComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            __PinComboBox.FormattingEnabled = true;
            __PinComboBox.Location = new System.Drawing.Point(40, 25);
            __PinComboBox.Margin = new Padding(3, 4, 3, 4);
            __PinComboBox.Name = "__PinComboBox";
            __PinComboBox.Size = new System.Drawing.Size(100, 25);
            __PinComboBox.TabIndex = 1;
            _ToolTip.SetToolTip(__PinComboBox, "Selects the pin number.");
            // 
            // _PinComboBoxLabel
            // 
            _PinComboBoxLabel.AutoSize = true;
            _PinComboBoxLabel.Location = new System.Drawing.Point(10, 29);
            _PinComboBoxLabel.Name = "_PinComboBoxLabel";
            _PinComboBoxLabel.Size = new System.Drawing.Size(28, 17);
            _PinComboBoxLabel.TabIndex = 0;
            _PinComboBoxLabel.Text = "Pin:";
            _PinComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EventLogTabPage
            // 
            _EventLogTabPage.Controls.Add(__EventLogTextBox);
            _EventLogTabPage.Location = new System.Drawing.Point(4, 22);
            _EventLogTabPage.Name = "_EventLogTabPage";
            _EventLogTabPage.Padding = new Padding(3);
            _EventLogTabPage.Size = new System.Drawing.Size(409, 369);
            _EventLogTabPage.TabIndex = 1;
            _EventLogTabPage.Text = "Events";
            _EventLogTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTextBox
            // 
            __EventLogTextBox.Dock = DockStyle.Fill;
            __EventLogTextBox.Location = new System.Drawing.Point(3, 3);
            __EventLogTextBox.Multiline = true;
            __EventLogTextBox.Name = "__EventLogTextBox";
            __EventLogTextBox.ScrollBars = ScrollBars.Both;
            __EventLogTextBox.Size = new System.Drawing.Size(403, 363);
            __EventLogTextBox.TabIndex = 0;
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            _ToolStripContainer.BottomToolStripPanel.Controls.Add(_BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            _ToolStripContainer.ContentPanel.Controls.Add(_Tabs);
            _ToolStripContainer.ContentPanel.Controls.Add(_GpioPinControl2);
            _ToolStripContainer.ContentPanel.Controls.Add(_GpioPinControl1);
            _ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(417, 452);
            _ToolStripContainer.Dock = DockStyle.Fill;
            _ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            _ToolStripContainer.Name = "_ToolStripContainer";
            _ToolStripContainer.Size = new System.Drawing.Size(417, 506);
            _ToolStripContainer.TabIndex = 0;
            _ToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _GpioPinControl2
            // 
            _GpioPinControl2.BorderStyle = BorderStyle.Fixed3D;
            _GpioPinControl2.Dock = DockStyle.Top;
            _GpioPinControl2.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _GpioPinControl2.Location = new System.Drawing.Point(0, 29);
            _GpioPinControl2.Modality = DeviceModalities.Gpio;
            _GpioPinControl2.Name = "_GpioPinControl2";
            _GpioPinControl2.Size = new System.Drawing.Size(417, 28);
            _GpioPinControl2.TabIndex = 22;
            // 
            // _GpioPinControl1
            // 
            _GpioPinControl1.BorderStyle = BorderStyle.Fixed3D;
            _GpioPinControl1.Dock = DockStyle.Top;
            _GpioPinControl1.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _GpioPinControl1.Location = new System.Drawing.Point(0, 0);
            _GpioPinControl1.Modality = DeviceModalities.Gpio;
            _GpioPinControl1.Name = "_GpioPinControl1";
            _GpioPinControl1.Size = new System.Drawing.Size(417, 29);
            _GpioPinControl1.TabIndex = 21;
            // 
            // GpioPanel
            // 
            Controls.Add(_ToolStripContainer);
            Name = "GpioPanel";
            Size = new System.Drawing.Size(417, 506);
            _BottomToolStrip.ResumeLayout(false);
            _BottomToolStrip.PerformLayout();
            _Tabs.ResumeLayout(false);
            _PrimaryTabPage.ResumeLayout(false);
            _PrimaryTabLayout.ResumeLayout(false);
            _SubsystemGroupBox.ResumeLayout(false);
            _SubsystemGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_DebounceNumeric).EndInit();
            _PinTabPage.ResumeLayout(false);
            _PinTabLayout.ResumeLayout(false);
            _PinConfigGroupBox.ResumeLayout(false);
            _PinConfigGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_EventPeriodNumeric).EndInit();
            _EventLogTabPage.ResumeLayout(false);
            _EventLogTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.BottomToolStripPanel.PerformLayout();
            _ToolStripContainer.ContentPanel.ResumeLayout(false);
            _ToolStripContainer.ResumeLayout(false);
            _ToolStripContainer.PerformLayout();
            ResumeLayout(false);
        }

        private ToolStrip _BottomToolStrip;
        private ToolStripTextBox _DeviceInfoTextBox;
        private ErrorProvider _ErrorProvider;
        private ToolTip _ToolTip;
        private ToolStripButton __OpenDeviceModalityButton;

        private ToolStripButton _OpenDeviceModalityButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDeviceModalityButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDeviceModalityButton != null)
                {
                    __OpenDeviceModalityButton.Click -= OpenDeviceModalityButton_Click;
                }

                __OpenDeviceModalityButton = value;
                if (__OpenDeviceModalityButton != null)
                {
                    __OpenDeviceModalityButton.Click += OpenDeviceModalityButton_Click;
                }
            }
        }

        private TabPage _PrimaryTabPage;
        private TabPage _EventLogTabPage;
        private TextBox __EventLogTextBox;

        private TextBox _EventLogTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EventLogTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EventLogTextBox != null)
                {
                    __EventLogTextBox.DoubleClick -= EventLogTextBox_DoubleClick;
                }

                __EventLogTextBox = value;
                if (__EventLogTextBox != null)
                {
                    __EventLogTextBox.DoubleClick += EventLogTextBox_DoubleClick;
                }
            }
        }

        private ToolStripContainer _ToolStripContainer;
        private ExtendedTabControl _Tabs;
        private ToolStripComboBox _DevicesComboBox;
        private ToolStripSplitButton _SelectServerButton;
        private ToolStripTextBox _ServerNameTextBox;
        private ToolStripMenuItem _DefaultServerMenuItem;
        private ToolStripSplitButton _SelectDeviceSplitButton;
        private GroupBox _SubsystemGroupBox;
        private NumericUpDown _DebounceNumeric;
        private Button __GetDebounceButton;

        private Button _GetDebounceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __GetDebounceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__GetDebounceButton != null)
                {
                    __GetDebounceButton.Click -= GetDebounceButton_Click;
                }

                __GetDebounceButton = value;
                if (__GetDebounceButton != null)
                {
                    __GetDebounceButton.Click += GetDebounceButton_Click;
                }
            }
        }

        private Button __SetDebounceButton;

        private Button _SetDebounceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SetDebounceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SetDebounceButton != null)
                {
                    __SetDebounceButton.Click -= SetDebounceButton_Click;
                }

                __SetDebounceButton = value;
                if (__SetDebounceButton != null)
                {
                    __SetDebounceButton.Click += SetDebounceButton_Click;
                }
            }
        }

        private Label _DebounceNumericLabel;
        private TableLayoutPanel _PrimaryTabLayout;
        private TabPage _PinTabPage;
        private GroupBox _PinConfigGroupBox;
        private CheckBox _OpenDrainCheckBox;
        private CheckBox _PinPullupCheckBox;
        private NumericUpDown _EventPeriodNumeric;
        private CheckBox _PulldownCheckBox;
        private CheckBox _DebounceEnabledCheckBox;
        private Label _EventPeriodNumericLabel;
        private Label _EventTypeComboBoxLabel;
        private ComboBox _EventTypeComboBox;
        private Button __GetPinConfigButton;

        private Button _GetPinConfigButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __GetPinConfigButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__GetPinConfigButton != null)
                {
                    __GetPinConfigButton.Click -= GetPinConfigButton_Click;
                }

                __GetPinConfigButton = value;
                if (__GetPinConfigButton != null)
                {
                    __GetPinConfigButton.Click += GetPinConfigButton_Click;
                }
            }
        }

        private Button __SetPinConfigButton;

        private Button _SetPinConfigButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SetPinConfigButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SetPinConfigButton != null)
                {
                    __SetPinConfigButton.Click -= SetPinConfigButton_Click;
                }

                __SetPinConfigButton = value;
                if (__SetPinConfigButton != null)
                {
                    __SetPinConfigButton.Click += SetPinConfigButton_Click;
                }
            }
        }

        private ComboBox _PinDirectionComboBox;
        private Label _PinDirectionComboBoxLabel;
        private CheckBox _PinEnabledCheckBox;
        private ComboBox __PinComboBox;

        private ComboBox _PinComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PinComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PinComboBox != null)
                {
                    __PinComboBox.SelectedIndexChanged -= PinComboBox_SelectedIndexChanged;
                }

                __PinComboBox = value;
                if (__PinComboBox != null)
                {
                    __PinComboBox.SelectedIndexChanged += PinComboBox_SelectedIndexChanged;
                }
            }
        }

        private Label _PinComboBoxLabel;
        private GpioPinControl _GpioPinControl2;
        private GpioPinControl _GpioPinControl1;
        private TableLayoutPanel _PinTabLayout;
        private ToolStripComboBox __TabComboBox;

        private ToolStripComboBox _TabComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TabComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TabComboBox != null)
                {
                    __TabComboBox.SelectedIndexChanged -= TabComboBox_SelectedIndexChanged;
                }

                __TabComboBox = value;
                if (__TabComboBox != null)
                {
                    __TabComboBox.SelectedIndexChanged += TabComboBox_SelectedIndexChanged;
                }
            }
        }

        private ToolStripButton __ConnectServerButton;

        private ToolStripButton _ConnectServerButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConnectServerButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConnectServerButton != null)
                {
                    __ConnectServerButton.Click -= ConnectServerButton_Click;
                }

                __ConnectServerButton = value;
                if (__ConnectServerButton != null)
                {
                    __ConnectServerButton.Click += ConnectServerButton_Click;
                }
            }
        }
    }
}