﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{
    [DesignerGenerated()]
    public partial class GpioPinControl
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ToolStrip = new ToolStrip();
            _PinComboBoxLabel = new ToolStripLabel();
            __PinComboBox = new ToolStripComboBox();
            __PinComboBox.SelectedIndexChanged += new EventHandler(PinComboBox_SelectedIndexChanged);
            _OutputValueComboBoxLabel = new ToolStripLabel();
            _OutputValueComboBox = new ToolStripComboBox();
            __SetOutputButton = new ToolStripButton();
            __SetOutputButton.Click += new EventHandler(SetOutputButton_Click);
            _PulseDurationTextBoxLabel = new ToolStripLabel();
            _PulseDurationTextBox = new ToolStripTextBox();
            __StartButton = new ToolStripButton();
            __StartButton.Click += new EventHandler(StartButton_Click);
            _ValueTextBox = new ToolStripTextBox();
            __GetValueButton = new ToolStripButton();
            __GetValueButton.Click += new EventHandler(GetValueButton_Click);
            _ErrorProvider = new ErrorProvider(components);
            _ToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            SuspendLayout();
            // 
            // _ToolStrip
            // 
            _ToolStrip.Dock = DockStyle.None;
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { _PinComboBoxLabel, __PinComboBox, _OutputValueComboBoxLabel, _OutputValueComboBox, __SetOutputButton, _PulseDurationTextBoxLabel, _PulseDurationTextBox, __StartButton, _ValueTextBox, __GetValueButton });
            _ToolStrip.Location = new System.Drawing.Point(0, 0);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Size = new System.Drawing.Size(433, 25);
            _ToolStrip.TabIndex = 10;
            _ToolStrip.Text = "ToolStrip1";
            // 
            // _PinComboBoxLabel
            // 
            _PinComboBoxLabel.Name = "_PinComboBoxLabel";
            _PinComboBoxLabel.Size = new System.Drawing.Size(27, 22);
            _PinComboBoxLabel.Text = "Pin:";
            // 
            // _PinComboBox
            // 
            __PinComboBox.AutoSize = false;
            __PinComboBox.DropDownWidth = 25;
            __PinComboBox.Margin = new Padding(1, 0, 3, 0);
            __PinComboBox.Name = "__PinComboBox";
            __PinComboBox.Size = new System.Drawing.Size(35, 23);
            __PinComboBox.ToolTipText = "Pin number";
            // 
            // _OutputValueComboBoxLabel
            // 
            _OutputValueComboBoxLabel.Name = "_OutputValueComboBoxLabel";
            _OutputValueComboBoxLabel.Size = new System.Drawing.Size(30, 22);
            _OutputValueComboBoxLabel.Text = "Out:";
            // 
            // _OutputValueComboBox
            // 
            _OutputValueComboBox.AutoSize = false;
            _OutputValueComboBox.DropDownWidth = 55;
            _OutputValueComboBox.Name = "_OutputValueComboBox";
            _OutputValueComboBox.Size = new System.Drawing.Size(65, 23);
            _OutputValueComboBox.Text = "Logic 0";
            _OutputValueComboBox.ToolTipText = "Output value";
            // 
            // _SetOutputButton
            // 
            __SetOutputButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __SetOutputButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __SetOutputButton.Margin = new Padding(0, 1, 3, 2);
            __SetOutputButton.Name = "__SetOutputButton";
            __SetOutputButton.Size = new System.Drawing.Size(27, 22);
            __SetOutputButton.Text = "Set";
            __SetOutputButton.ToolTipText = "Set the output";
            // 
            // _PulseDurationTextBoxLabel
            // 
            _PulseDurationTextBoxLabel.Name = "_PulseDurationTextBoxLabel";
            _PulseDurationTextBoxLabel.Size = new System.Drawing.Size(60, 22);
            _PulseDurationTextBoxLabel.Text = "Pulse, ms:";
            // 
            // _PulseDurationTextBox
            // 
            _PulseDurationTextBox.Name = "_PulseDurationTextBox";
            _PulseDurationTextBox.Size = new System.Drawing.Size(50, 25);
            _PulseDurationTextBox.Text = "10";
            _PulseDurationTextBox.ToolTipText = "Pulse duration in ms.";
            // 
            // _StartButton
            // 
            __StartButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __StartButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __StartButton.Name = "__StartButton";
            __StartButton.Size = new System.Drawing.Size(35, 22);
            __StartButton.Text = "Start";
            __StartButton.ToolTipText = "Start a single pulse.";
            // 
            // _ValueTextBox
            // 
            _ValueTextBox.BackColor = System.Drawing.Color.Black;
            _ValueTextBox.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ValueTextBox.ForeColor = System.Drawing.Color.Aqua;
            _ValueTextBox.Name = "_ValueTextBox";
            _ValueTextBox.Size = new System.Drawing.Size(25, 25);
            _ValueTextBox.Text = "0";
            _ValueTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
            _ValueTextBox.ToolTipText = "Value";
            // 
            // _GetValueButton
            // 
            __GetValueButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __GetValueButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __GetValueButton.Margin = new Padding(0, 1, 3, 2);
            __GetValueButton.Name = "__GetValueButton";
            __GetValueButton.Size = new System.Drawing.Size(29, 22);
            __GetValueButton.Text = "Get";
            __GetValueButton.ToolTipText = "Get input or output value";
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // GpioPinControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_ToolStrip);
            Name = "GpioPinControl";
            Size = new System.Drawing.Size(445, 27);
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _ToolStrip;
        private ToolStripLabel _PinComboBoxLabel;
        private ToolStripComboBox __PinComboBox;

        private ToolStripComboBox _PinComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PinComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PinComboBox != null)
                {
                    __PinComboBox.SelectedIndexChanged -= PinComboBox_SelectedIndexChanged;
                }

                __PinComboBox = value;
                if (__PinComboBox != null)
                {
                    __PinComboBox.SelectedIndexChanged += PinComboBox_SelectedIndexChanged;
                }
            }
        }

        private ToolStripLabel _OutputValueComboBoxLabel;
        private ToolStripComboBox _OutputValueComboBox;
        private ToolStripButton __SetOutputButton;

        private ToolStripButton _SetOutputButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SetOutputButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SetOutputButton != null)
                {
                    __SetOutputButton.Click -= SetOutputButton_Click;
                }

                __SetOutputButton = value;
                if (__SetOutputButton != null)
                {
                    __SetOutputButton.Click += SetOutputButton_Click;
                }
            }
        }

        private ToolStripLabel _PulseDurationTextBoxLabel;
        private ToolStripTextBox _PulseDurationTextBox;
        private ToolStripButton __StartButton;

        private ToolStripButton _StartButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartButton != null)
                {
                    __StartButton.Click -= StartButton_Click;
                }

                __StartButton = value;
                if (__StartButton != null)
                {
                    __StartButton.Click += StartButton_Click;
                }
            }
        }

        private ToolStripTextBox _ValueTextBox;
        private ToolStripButton __GetValueButton;

        private ToolStripButton _GetValueButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __GetValueButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__GetValueButton != null)
                {
                    __GetValueButton.Click -= GetValueButton_Click;
                }

                __GetValueButton = value;
                if (__GetValueButton != null)
                {
                    __GetValueButton.Click += GetValueButton_Click;
                }
            }
        }

        private ErrorProvider _ErrorProvider;
    }
}