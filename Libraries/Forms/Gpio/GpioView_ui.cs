using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core;
using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.ExceptionExtensions;
using isr.Diolan.Forms.SubsystemExtensions;

namespace isr.Diolan.Forms
{
    public partial class GpioView : Core.Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioView()
        {

            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._TabComboBox.ComboBox.DataSource = this._Tabs.TabPages;
            this._TabComboBox.ComboBox.DisplayMember = "Text";
            this.__TabComboBox.Name = "_TabComboBox";
            this.__ConnectServerButton.Name = "_ConnectServerButton";
            this.__OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton";
            this.__GetDebounceButton.Name = "_GetDebounceButton";
            this.__SetDebounceButton.Name = "_SetDebounceButton";
            this.__GetPinConfigButton.Name = "_GetPinConfigButton";
            this.__SetPinConfigButton.Name = "_SetPinConfigButton";
            this.__PinComboBox.Name = "_PinComboBox";
            this.__EventLogTextBox.Name = "_EventLogTextBox";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                if ( !this.IsDisposed && disposing && this.components is object )
                {
                    this.components.Dispose();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        private void OnDispose( bool disposing )
        {
            this.OnCustomDispose( disposing );
            if ( !this.IsDisposed && disposing )
            {
                if ( this.Device is object )
                    this.Device = null;
                if ( this._DeviceConnector is object )
                {
                    this._DeviceConnector.Dispose();
                    this._DeviceConnector = null;
                }

                if ( this.ServerConnectorThis is object )
                    this.ServerConnectorThis = null;
            }
        }

        #endregion

        #region " KNOWN STATE "

        /// <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public void InitializeKnownState()
        {
            this.OnServerConnectionChanged();
        }

        #endregion

        #region " SERVER "

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnPropertyChanged( LocalhostConnector sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( e is object )
                {
                    switch ( e.PropertyName ?? "" )
                    {
                        case nameof( LocalhostConnector.IsConnected ):
                            {
                                this.OnServerConnectionChanged();
                                break;
                            }

                        case nameof( LocalhostConnector.AttachedDevicesCount ):
                            {
                                this.OnServerAttachmentChanged();
                                break;
                            }
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToFullBlownString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, details );
            }
        }

        /// <summary> Server connector property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void ServerConnector_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ServerConnector_PropertyChanged ), new object[] { sender, e } );
            }

            this.OnPropertyChanged( sender as LocalhostConnector, e );
        }

        private LocalhostConnector _ServerConnectorThis;

        /// <summary>   Gets or sets the private server connector. </summary>
        /// <value> The private server connector. </value>
        private LocalhostConnector ServerConnectorThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ServerConnectorThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ServerConnectorThis != null )
                {
                    this._ServerConnectorThis.PropertyChanged -= this.ServerConnector_PropertyChanged;
                }

                this._ServerConnectorThis = value;
                if ( this._ServerConnectorThis != null )
                {
                    this._ServerConnectorThis.PropertyChanged += this.ServerConnector_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LocalhostConnector ServerConnector
        {
            get => this.ServerConnectorThis;

            set => this.ServerConnectorThis = value;
        }

        /// <summary> Connects a server button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConnectServerButton_Click( object sender, EventArgs e )
        {
            if ( this.ServerConnector.IsConnected )
            {
                if ( !this.ServerConnector.HasAttachedDevices() )
                {
                    this.ServerConnector.Disconnect();
                }
            }
            else
            {
                this.ServerConnector.Connect();
            }
        }

        /// <summary> Executes the server connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnServerConnectionChanged()
        {
            if ( this.ServerConnector.IsConnected )
            {
                _ = this.ServerConnector.Connection.ListDevicesById( this._DevicesComboBox );
                this._DevicesComboBox.SelectedIndex = 0;
            }

            this._OpenDeviceModalityButton.Enabled = this.ServerConnector.IsConnected;
            this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            this.OnServerAttachmentChanged();
        }

        /// <summary> Executes the server attachment changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OnServerAttachmentChanged()
        {
            this._ConnectServerButton.Image = My.Resources.Resources.WIFI_open_22;
            if ( this.ServerConnector.IsConnected )
            {
                this._ConnectServerButton.Text = this.ServerConnector.AttachedDevicesCount().ToString();
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
                this._ConnectServerButton.Text = "X";
            }
        }

        #endregion

        #region " DEVICE CONNECTOR "

#pragma warning disable IDE1006 // Naming Styles
        private DeviceConnector __DeviceConnector;

        private DeviceConnector _DeviceConnector
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__DeviceConnector;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__DeviceConnector != null )
                {

                    this.__DeviceConnector.DeviceClosed -= this.DeviceConnector_DeviceClosed;

                    this.__DeviceConnector.DeviceClosing -= this.DeviceConnector_DeviceClosing;

                    this.__DeviceConnector.DeviceOpened -= this.DeviceConnector_DeviceOpened;
                }

                this.__DeviceConnector = value;
                if ( this.__DeviceConnector != null )
                {
                    this.__DeviceConnector.DeviceClosed += this.DeviceConnector_DeviceClosed;
                    this.__DeviceConnector.DeviceClosing += this.DeviceConnector_DeviceClosing;
                    this.__DeviceConnector.DeviceOpened += this.DeviceConnector_DeviceOpened;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the device connector. </summary>
        /// <value> The device connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DeviceConnector DeviceConnector
        {
            get => this._DeviceConnector;

            set => this._DeviceConnector = value;
        }

        /// <summary> Device connector device closed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceClosed( object sender, EventArgs e )
        {
            this.OnServerAttachmentChanged();
            this._DeviceInfoTextBox.Text = "closed";
            this.OnModalityClosed();
        }

        /// <summary> Device connector device closing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void DeviceConnector_DeviceClosing( object sender, CancelEventArgs e )
        {
            this.OnModalityClosing( e );
        }

        /// <summary> Device connector device opened. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceOpened( object sender, EventArgs e )
        {
            this.OnServerAttachmentChanged();
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The device. </value>
        private Dln.Device Device { get; set; }

        /// <summary> Selected device information. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A DeviceInfo. </returns>
        private DeviceInfo SelectedDeviceInfo()
        {
            return string.IsNullOrWhiteSpace( this._DevicesComboBox.Text )
                ? throw new InvalidOperationException( "No devices selected" )
                : new DeviceInfo( this._DevicesComboBox.Text );
        }

        /// <summary> Opens the device for the modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <param name="sender">   The sender. </param>
        private void OpenDeviceModality( long deviceId, Control sender )
        {
            if ( this.ServerConnector.IsConnected )
            {
                this._OpenDeviceModalityButton.Enabled = this.ServerConnector.IsConnected;
                this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            }
            else
            {
                this.ServerConnector.Connect();
            }

            this._DeviceConnector = new DeviceConnector( this.ServerConnectorThis );

            // Open device
            var e = new ActionEventArgs();
            if ( this._DeviceConnector.TryOpenDevice( deviceId, this.Modality, e ) )
            {
                this.OnModalityOpening();
            }
            else
            {
                if ( sender is object )
                    _ = this._ErrorProvider.Annunciate( sender, e.Details );
                this._DeviceInfoTextBox.Text = "No devices";
            }
        }

        /// <summary> Closes device modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void CloseDeviceModality()
        {
            this._DeviceConnector.CloseDevice( this.Modality );
            var e = new CancelEventArgs();
            this.OnModalityClosing( e );
            if ( !e.Cancel )
            {
                this.OnModalityClosed();
            }
        }

        /// <summary> Opens device button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenDeviceModalityButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceModalityOpen )
                {
                    this.CloseDeviceModality();
                }
                else if ( string.IsNullOrWhiteSpace( this._DevicesComboBox.Text ) )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Select a device first" );
                }
                else
                {
                    this.OpenDeviceModality( this.SelectedDeviceInfo().Id, sender as Control );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
            finally
            {
                this.OnModalityConnectionChanged();
            }
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        private bool IsDeviceModalityOpen => this.Device is object && this.IsModalityOpen();

        /// <summary> Executes the modality connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityConnectionChanged()
        {
            if ( this.IsDeviceModalityOpen )
            {
                this._OpenDeviceModalityButton.Image = My.Resources.Resources.user_online_2;
                this._OpenDeviceModalityButton.Text = "Close";
            }
            else
            {
                this._OpenDeviceModalityButton.Image = My.Resources.Resources.user_offline_2;
                this._OpenDeviceModalityButton.Text = "Open";
            }
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Event log text box double click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EventLogTextBox_DoubleClick( object sender, EventArgs e )
        {
            this._EventLogTextBox.Clear();
        }

        #endregion


        #region " TABS "

        /// <summary> Selects the tab. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void TabComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._TabComboBox.SelectedIndex >= 0 && this._TabComboBox.SelectedIndex < this._Tabs.TabCount )
            {
                this._Tabs.SelectTab( this._Tabs.TabPages[this._TabComboBox.SelectedIndex] );
            }
        }


        #endregion

    }
}
