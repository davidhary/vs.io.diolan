﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{
    [DesignerGenerated()]
    public partial class GpioPinConfigControl
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ToolTip = new ToolTip(components);
            _OpenDrainCheckBox = new CheckBox();
            _PinPullupCheckBox = new CheckBox();
            _EventPeriodNumeric = new NumericUpDown();
            _PulldownCheckBox = new CheckBox();
            _DebounceEnabledCheckBox = new CheckBox();
            _EventTypeComboBox = new ComboBox();
            _PinDirectionComboBox = new ComboBox();
            _PinNumberNumeric = new NumericUpDown();
            _PinNameTextBox = new TextBox();
            _ErrorProvider = new ErrorProvider(components);
            _EventPeriodNumericLabel = new Label();
            _EventTypeComboBoxLabel = new Label();
            __GetPinConfigButton = new Button();
            __GetPinConfigButton.Click += new EventHandler(GetPinConfigButton_Click);
            __SetPinConfigButton = new Button();
            __SetPinConfigButton.Click += new EventHandler(SetPinConfigButton_Click);
            _PinDirectionComboBoxLabel = new Label();
            _PinEnabledCheckBox = new CheckBox();
            _PinNumericLabel = new Label();
            _PinNameTextBoxLabel = new Label();
            ((System.ComponentModel.ISupportInitialize)_EventPeriodNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_PinNumberNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            SuspendLayout();
            // 
            // _OpenDrainCheckBox
            // 
            _OpenDrainCheckBox.AutoSize = true;
            _OpenDrainCheckBox.Location = new System.Drawing.Point(182, 114);
            _OpenDrainCheckBox.Margin = new Padding(3, 4, 3, 4);
            _OpenDrainCheckBox.Name = "_OpenDrainCheckBox";
            _OpenDrainCheckBox.Size = new System.Drawing.Size(94, 21);
            _OpenDrainCheckBox.TabIndex = 10;
            _OpenDrainCheckBox.Text = "Open Drain";
            _ToolTip.SetToolTip(_OpenDrainCheckBox, "Pin is open drain is checked.");
            _OpenDrainCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PinPullupCheckBox
            // 
            _PinPullupCheckBox.AutoSize = true;
            _PinPullupCheckBox.Location = new System.Drawing.Point(14, 114);
            _PinPullupCheckBox.Margin = new Padding(3, 4, 3, 4);
            _PinPullupCheckBox.Name = "_PinPullupCheckBox";
            _PinPullupCheckBox.Size = new System.Drawing.Size(68, 21);
            _PinPullupCheckBox.TabIndex = 8;
            _PinPullupCheckBox.Text = "Pull Up";
            _ToolTip.SetToolTip(_PinPullupCheckBox, "pin is pulled up if checked.");
            _PinPullupCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EventPeriodNumeric
            // 
            _EventPeriodNumeric.Location = new System.Drawing.Point(128, 186);
            _EventPeriodNumeric.Maximum = new decimal(new int[] { 65535, 0, 0, 0 });
            _EventPeriodNumeric.Name = "_EventPeriodNumeric";
            _EventPeriodNumeric.Size = new System.Drawing.Size(60, 25);
            _EventPeriodNumeric.TabIndex = 14;
            _ToolTip.SetToolTip(_EventPeriodNumeric, "Selects the event period in ms");
            // 
            // _PulldownCheckBox
            // 
            _PulldownCheckBox.AutoSize = true;
            _PulldownCheckBox.Location = new System.Drawing.Point(90, 114);
            _PulldownCheckBox.Margin = new Padding(3, 4, 3, 4);
            _PulldownCheckBox.Name = "_PulldownCheckBox";
            _PulldownCheckBox.Size = new System.Drawing.Size(84, 21);
            _PulldownCheckBox.TabIndex = 9;
            _PulldownCheckBox.Text = "Pull Down";
            _ToolTip.SetToolTip(_PulldownCheckBox, "Pin is pulled down if checked");
            _PulldownCheckBox.UseVisualStyleBackColor = true;
            // 
            // _DebounceEnabledCheckBox
            // 
            _DebounceEnabledCheckBox.AutoSize = true;
            _DebounceEnabledCheckBox.Location = new System.Drawing.Point(121, 41);
            _DebounceEnabledCheckBox.Margin = new Padding(3, 4, 3, 4);
            _DebounceEnabledCheckBox.Name = "_DebounceEnabledCheckBox";
            _DebounceEnabledCheckBox.Size = new System.Drawing.Size(137, 21);
            _DebounceEnabledCheckBox.TabIndex = 5;
            _DebounceEnabledCheckBox.Text = "Debounce Enabled";
            _ToolTip.SetToolTip(_DebounceEnabledCheckBox, "Debounce is enabled if checked.");
            _DebounceEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _EventTypeComboBox
            // 
            _EventTypeComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _EventTypeComboBox.FormattingEnabled = true;
            _EventTypeComboBox.Location = new System.Drawing.Point(89, 151);
            _EventTypeComboBox.Margin = new Padding(3, 4, 3, 4);
            _EventTypeComboBox.Name = "_EventTypeComboBox";
            _EventTypeComboBox.Size = new System.Drawing.Size(101, 25);
            _EventTypeComboBox.TabIndex = 12;
            _ToolTip.SetToolTip(_EventTypeComboBox, "Selects the event type.");
            // 
            // _PinDirectionComboBox
            // 
            _PinDirectionComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            _PinDirectionComboBox.FormattingEnabled = true;
            _PinDirectionComboBox.Location = new System.Drawing.Point(76, 77);
            _PinDirectionComboBox.Margin = new Padding(3, 4, 3, 4);
            _PinDirectionComboBox.Name = "_PinDirectionComboBox";
            _PinDirectionComboBox.Size = new System.Drawing.Size(79, 25);
            _PinDirectionComboBox.TabIndex = 7;
            _ToolTip.SetToolTip(_PinDirectionComboBox, "Selects pin direction.");
            // 
            // _PinNumberNumeric
            // 
            _PinNumberNumeric.Location = new System.Drawing.Point(48, 3);
            _PinNumberNumeric.Maximum = new decimal(new int[] { 31, 0, 0, 0 });
            _PinNumberNumeric.Name = "_PinNumberNumeric";
            _PinNumberNumeric.Size = new System.Drawing.Size(35, 25);
            _PinNumberNumeric.TabIndex = 1;
            _ToolTip.SetToolTip(_PinNumberNumeric, "Pin number");
            // 
            // _PinNameTextBox
            // 
            _PinNameTextBox.Location = new System.Drawing.Point(145, 3);
            _PinNameTextBox.Name = "_PinNameTextBox";
            _PinNameTextBox.Size = new System.Drawing.Size(131, 25);
            _PinNameTextBox.TabIndex = 3;
            _ToolTip.SetToolTip(_PinNameTextBox, "Pin name");
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _EventPeriodNumericLabel
            // 
            _EventPeriodNumericLabel.AutoSize = true;
            _EventPeriodNumericLabel.Location = new System.Drawing.Point(13, 190);
            _EventPeriodNumericLabel.Name = "_EventPeriodNumericLabel";
            _EventPeriodNumericLabel.Size = new System.Drawing.Size(113, 17);
            _EventPeriodNumericLabel.TabIndex = 13;
            _EventPeriodNumericLabel.Text = "Event Period [ms]:";
            _EventPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EventTypeComboBoxLabel
            // 
            _EventTypeComboBoxLabel.AutoSize = true;
            _EventTypeComboBoxLabel.Location = new System.Drawing.Point(13, 155);
            _EventTypeComboBoxLabel.Name = "_EventTypeComboBoxLabel";
            _EventTypeComboBoxLabel.Size = new System.Drawing.Size(73, 17);
            _EventTypeComboBoxLabel.TabIndex = 11;
            _EventTypeComboBoxLabel.Text = "Event Type:";
            _EventTypeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _GetPinConfigButton
            // 
            __GetPinConfigButton.Location = new System.Drawing.Point(215, 148);
            __GetPinConfigButton.Margin = new Padding(3, 4, 3, 4);
            __GetPinConfigButton.Name = "__GetPinConfigButton";
            __GetPinConfigButton.Size = new System.Drawing.Size(61, 30);
            __GetPinConfigButton.TabIndex = 15;
            __GetPinConfigButton.Text = "Get";
            __GetPinConfigButton.UseVisualStyleBackColor = true;
            // 
            // _SetPinConfigButton
            // 
            __SetPinConfigButton.Location = new System.Drawing.Point(215, 186);
            __SetPinConfigButton.Margin = new Padding(3, 4, 3, 4);
            __SetPinConfigButton.Name = "__SetPinConfigButton";
            __SetPinConfigButton.Size = new System.Drawing.Size(61, 30);
            __SetPinConfigButton.TabIndex = 16;
            __SetPinConfigButton.Text = "Set";
            __SetPinConfigButton.UseVisualStyleBackColor = true;
            // 
            // _PinDirectionComboBoxLabel
            // 
            _PinDirectionComboBoxLabel.AutoSize = true;
            _PinDirectionComboBoxLabel.Location = new System.Drawing.Point(11, 81);
            _PinDirectionComboBoxLabel.Name = "_PinDirectionComboBoxLabel";
            _PinDirectionComboBoxLabel.Size = new System.Drawing.Size(63, 17);
            _PinDirectionComboBoxLabel.TabIndex = 6;
            _PinDirectionComboBoxLabel.Text = "Direction:";
            _PinDirectionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PinEnabledCheckBox
            // 
            _PinEnabledCheckBox.AutoSize = true;
            _PinEnabledCheckBox.Location = new System.Drawing.Point(39, 41);
            _PinEnabledCheckBox.Margin = new Padding(3, 4, 3, 4);
            _PinEnabledCheckBox.Name = "_PinEnabledCheckBox";
            _PinEnabledCheckBox.Size = new System.Drawing.Size(74, 21);
            _PinEnabledCheckBox.TabIndex = 4;
            _PinEnabledCheckBox.Text = "Enabled";
            _PinEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PinNumericLabel
            // 
            _PinNumericLabel.AutoSize = true;
            _PinNumericLabel.Location = new System.Drawing.Point(5, 7);
            _PinNumericLabel.Name = "_PinNumericLabel";
            _PinNumericLabel.Size = new System.Drawing.Size(40, 17);
            _PinNumericLabel.TabIndex = 0;
            _PinNumericLabel.Text = "Pin #:";
            _PinNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PinNameTextBoxLabel
            // 
            _PinNameTextBoxLabel.AutoSize = true;
            _PinNameTextBoxLabel.Location = new System.Drawing.Point(97, 7);
            _PinNameTextBoxLabel.Name = "_PinNameTextBoxLabel";
            _PinNameTextBoxLabel.Size = new System.Drawing.Size(46, 17);
            _PinNameTextBoxLabel.TabIndex = 2;
            _PinNameTextBoxLabel.Text = "Name:";
            // 
            // GpioPinConfigControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_PinNameTextBox);
            Controls.Add(_PinNameTextBoxLabel);
            Controls.Add(_PinNumberNumeric);
            Controls.Add(_OpenDrainCheckBox);
            Controls.Add(_PinPullupCheckBox);
            Controls.Add(_EventPeriodNumeric);
            Controls.Add(_PulldownCheckBox);
            Controls.Add(_DebounceEnabledCheckBox);
            Controls.Add(_EventPeriodNumericLabel);
            Controls.Add(_EventTypeComboBoxLabel);
            Controls.Add(_EventTypeComboBox);
            Controls.Add(__GetPinConfigButton);
            Controls.Add(__SetPinConfigButton);
            Controls.Add(_PinDirectionComboBox);
            Controls.Add(_PinDirectionComboBoxLabel);
            Controls.Add(_PinEnabledCheckBox);
            Controls.Add(_PinNumericLabel);
            Margin = new Padding(3, 4, 3, 4);
            Name = "GpioPinConfigControl";
            Size = new System.Drawing.Size(283, 222);
            ((System.ComponentModel.ISupportInitialize)_EventPeriodNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_PinNumberNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolTip _ToolTip;
        private ErrorProvider _ErrorProvider;
        private TextBox _PinNameTextBox;
        private Label _PinNameTextBoxLabel;
        private NumericUpDown _PinNumberNumeric;
        private Label _PinNumericLabel;
        private CheckBox _PinEnabledCheckBox;
        private Label _PinDirectionComboBoxLabel;
        private ComboBox _PinDirectionComboBox;
        private Button __SetPinConfigButton;

        private Button _SetPinConfigButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SetPinConfigButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SetPinConfigButton != null)
                {
                    __SetPinConfigButton.Click -= SetPinConfigButton_Click;
                }

                __SetPinConfigButton = value;
                if (__SetPinConfigButton != null)
                {
                    __SetPinConfigButton.Click += SetPinConfigButton_Click;
                }
            }
        }

        private Button __GetPinConfigButton;

        private Button _GetPinConfigButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __GetPinConfigButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__GetPinConfigButton != null)
                {
                    __GetPinConfigButton.Click -= GetPinConfigButton_Click;
                }

                __GetPinConfigButton = value;
                if (__GetPinConfigButton != null)
                {
                    __GetPinConfigButton.Click += GetPinConfigButton_Click;
                }
            }
        }

        private ComboBox _EventTypeComboBox;
        private Label _EventTypeComboBoxLabel;
        private Label _EventPeriodNumericLabel;
        private CheckBox _DebounceEnabledCheckBox;
        private CheckBox _PulldownCheckBox;
        private NumericUpDown _EventPeriodNumeric;
        private CheckBox _PinPullupCheckBox;
        private CheckBox _OpenDrainCheckBox;
    }
}