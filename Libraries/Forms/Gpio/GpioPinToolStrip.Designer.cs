﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{
    [DesignerGenerated()]
    public partial class GpioPinToolStrip
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ToolStrip = new ToolStrip();
            __ConfigureButton = new ToolStripButton();
            __ConfigureButton.Click += new EventHandler(ConfigureButton_Click);
            _PinNumberLabel = new ToolStripLabel();
            _Separator1 = new ToolStripSeparator();
            _PinNameLabel = new ToolStripLabel();
            __LogicalStateButton = new ToolStripButton();
            __LogicalStateButton.Click += new EventHandler(LogicalStateButton_Click);
            __LogicalStateButton.CheckStateChanged += new EventHandler(LogicalStateButton_CheckStateChanged);
            __ActiveLogicToggleButton = new ToolStripButton();
            __ActiveLogicToggleButton.CheckStateChanged += new EventHandler(ActiveLogicToggleButton_CheckStateChanged);
            _ErrorProvider = new ErrorProvider(components);
            _ToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            SuspendLayout();
            // 
            // _ToolStrip
            // 
            _ToolStrip.BackColor = System.Drawing.SystemColors.Control;
            _ToolStrip.Dock = DockStyle.Fill;
            _ToolStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _ToolStrip.Items.AddRange(new ToolStripItem[] { __ConfigureButton, _PinNumberLabel, _Separator1, _PinNameLabel, __LogicalStateButton, __ActiveLogicToggleButton });
            _ToolStrip.Location = new System.Drawing.Point(0, 0);
            _ToolStrip.Margin = new Padding(3);
            _ToolStrip.Name = "_ToolStrip";
            _ToolStrip.Padding = new Padding(0);
            _ToolStrip.Size = new System.Drawing.Size(230, 31);
            _ToolStrip.Stretch = true;
            _ToolStrip.TabIndex = 0;
            _ToolStrip.Text = "Tool Strip";
            // 
            // _ConfigureButton
            // 
            __ConfigureButton.AutoSize = false;
            __ConfigureButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __ConfigureButton.Image = My.Resources.Resources.configure_3;
            __ConfigureButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ConfigureButton.Margin = new Padding(0);
            __ConfigureButton.Name = "__ConfigureButton";
            __ConfigureButton.Overflow = ToolStripItemOverflow.Never;
            __ConfigureButton.Size = new System.Drawing.Size(24, 24);
            __ConfigureButton.Text = "Configure";
            // 
            // _PinNumberLabel
            // 
            _PinNumberLabel.Margin = new Padding(1, 1, 1, 2);
            _PinNumberLabel.Name = "_PinNumberLabel";
            _PinNumberLabel.Size = new System.Drawing.Size(22, 28);
            _PinNumberLabel.Text = "00";
            _PinNumberLabel.ToolTipText = "Pin number";
            // 
            // _Separator1
            // 
            _Separator1.Name = "_Separator1";
            _Separator1.Size = new System.Drawing.Size(6, 27);
            // 
            // _PinNameLabel
            // 
            _PinNameLabel.Margin = new Padding(1);
            _PinNameLabel.Name = "_PinNameLabel";
            _PinNameLabel.Overflow = ToolStripItemOverflow.Never;
            _PinNameLabel.Size = new System.Drawing.Size(58, 29);
            _PinNameLabel.Text = "<name>";
            _PinNameLabel.ToolTipText = "Pin name";
            // 
            // _LogicalStateButton
            // 
            __LogicalStateButton.Alignment = ToolStripItemAlignment.Right;
            __LogicalStateButton.CheckOnClick = true;
            __LogicalStateButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __LogicalStateButton.Image = My.Resources.Resources.circle_grey;
            __LogicalStateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __LogicalStateButton.Margin = new Padding(1);
            __LogicalStateButton.Name = "__LogicalStateButton";
            __LogicalStateButton.Size = new System.Drawing.Size(23, 29);
            __LogicalStateButton.Text = "Logical State";
            // 
            // _ActiveLogicToggleButton
            // 
            __ActiveLogicToggleButton.Alignment = ToolStripItemAlignment.Right;
            __ActiveLogicToggleButton.CheckOnClick = true;
            __ActiveLogicToggleButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __ActiveLogicToggleButton.Image = My.Resources.Resources.Minus_Grey;
            __ActiveLogicToggleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ActiveLogicToggleButton.Margin = new Padding(1);
            __ActiveLogicToggleButton.Name = "__ActiveLogicToggleButton";
            __ActiveLogicToggleButton.Size = new System.Drawing.Size(23, 29);
            __ActiveLogicToggleButton.Text = "Active Logic";
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // GpioPinToolStrip
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            BorderStyle = BorderStyle.Fixed3D;
            Controls.Add(_ToolStrip);
            Margin = new Padding(0);
            Name = "GpioPinToolStrip";
            Size = new System.Drawing.Size(230, 31);
            _ToolStrip.ResumeLayout(false);
            _ToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private ToolStrip _ToolStrip;
        private ToolStripButton __ConfigureButton;

        private ToolStripButton _ConfigureButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConfigureButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConfigureButton != null)
                {
                    __ConfigureButton.Click -= ConfigureButton_Click;
                }

                __ConfigureButton = value;
                if (__ConfigureButton != null)
                {
                    __ConfigureButton.Click += ConfigureButton_Click;
                }
            }
        }

        private ToolStripLabel _PinNumberLabel;
        private ToolStripLabel _PinNameLabel;
        private ToolStripButton __LogicalStateButton;

        private ToolStripButton _LogicalStateButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LogicalStateButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LogicalStateButton != null)
                {
                    __LogicalStateButton.Click -= LogicalStateButton_Click;
                    __LogicalStateButton.CheckStateChanged -= LogicalStateButton_CheckStateChanged;
                }

                __LogicalStateButton = value;
                if (__LogicalStateButton != null)
                {
                    __LogicalStateButton.Click += LogicalStateButton_Click;
                    __LogicalStateButton.CheckStateChanged += LogicalStateButton_CheckStateChanged;
                }
            }
        }

        private ToolStripButton __ActiveLogicToggleButton;

        private ToolStripButton _ActiveLogicToggleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ActiveLogicToggleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ActiveLogicToggleButton != null)
                {
                    __ActiveLogicToggleButton.CheckStateChanged -= ActiveLogicToggleButton_CheckStateChanged;
                }

                __ActiveLogicToggleButton = value;
                if (__ActiveLogicToggleButton != null)
                {
                    __ActiveLogicToggleButton.CheckStateChanged += ActiveLogicToggleButton_CheckStateChanged;
                }
            }
        }

        private ErrorProvider _ErrorProvider;
        private ToolStripSeparator _Separator1;
    }
}