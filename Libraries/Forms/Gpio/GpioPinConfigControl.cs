using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.ExceptionExtensions;
using isr.Diolan.Gpio;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{

    /// <summary> A gpio pin configuration control. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-04-07 </para>
    /// </remarks>
    public partial class GpioPinConfigControl : Core.Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioPinConfigControl()
        {

            // This call is required by the designer.
            this.InitializeComponent();
            this.__GetPinConfigButton.Name = "_GetPinConfigButton";
            this.__SetPinConfigButton.Name = "_SetPinConfigButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing && this.components is object )
                        this.components.Dispose();
                    this._PinDirectionComboBox.DataSource = null;
                    this._PinDirectionComboBox.Items.Clear();
                    this._EventTypeComboBox.DataSource = null;
                    this._EventTypeComboBox.Items.Clear();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PIN "

        /// <summary> Gets or sets the gpio pin. </summary>
        /// <value> The gpio pin. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public GpioPin GpioPin { get; private set; }

        /// <summary> Configures the given pin. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pin"> The pin. </param>
        public void Configure( GpioPin pin )
        {
            if ( pin is null )
                throw new ArgumentNullException( nameof( pin ) );
            this.GpioPin = pin;
            this._PinNumberNumeric.Value = this.GpioPin.PinNumber;
            this._PinNumberNumeric.ReadOnly = true;

            // Fill controls
            this._PinDirectionComboBox.DataSource = null;
            this._PinDirectionComboBox.Items.Clear();
            _ = this._PinDirectionComboBox.ListEnumNames<PinDirection>();
            this._ErrorProvider.Clear();
            this.ReadPinProperties( pin );
        }

        /// <summary> Gets pin configuration button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void GetPinConfigButton_Click( object sender, EventArgs e )
        {
            // Get pin parameters
            try
            {
                this._ErrorProvider.Clear();
                this.ReadPinProperties( this.GpioPin );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
        }

        /// <summary> Reads pin number properties. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pin"> The pin. </param>
        private void ReadPinProperties( GpioPin pin )
        {
            // Main settings are present in all devices,
            // check availability only for extra settings like pull downs, debounce and events.

            this._PinEnabledCheckBox.Checked = pin.Enabled;
            this._PinDirectionComboBox.SelectedIndex = ( int ) pin.Direction;
            this._PinNameTextBox.Text = pin.Name;
            // Pull up
            if ( pin.Pin.Restrictions.PullupEnabled == Dln.Restriction.NotSupported )
            {
                this._PinPullupCheckBox.Enabled = false;
            }
            else
            {
                this._PinPullupCheckBox.Enabled = true;
                this._PinPullupCheckBox.Checked = pin.Pin.PullupEnabled;
            }

            // Pull down
            if ( pin.Pin.Restrictions.PulldownEnabled == Dln.Restriction.NotSupported )
            {
                this._PulldownCheckBox.Enabled = false;
            }
            else
            {
                this._PulldownCheckBox.Enabled = true;
                this._PulldownCheckBox.Checked = pin.Pin.PulldownEnabled;
            }

            // Open drain
            if ( pin.Pin.Restrictions.OpendrainEnabled == Dln.Restriction.NotSupported )
            {
                this._OpenDrainCheckBox.Enabled = false;
            }
            else
            {
                this._OpenDrainCheckBox.Enabled = true;
                this._OpenDrainCheckBox.Checked = pin.Pin.OpendrainEnabled;
            }

            // Debounce
            if ( pin.Pin.Restrictions.DebounceEnabled == Dln.Restriction.NotSupported )
            {
                this._ToolTip.SetToolTip( this._DebounceEnabledCheckBox, "Not supported." );
            }
            else if ( pin.Pin.Restrictions.DebounceEnabled == Dln.Restriction.NoRestriction )
            {
                this._ToolTip.SetToolTip( this._DebounceEnabledCheckBox, "Enabled if checked." );
            }

            if ( pin.Pin.Restrictions.DebounceEnabled == Dln.Restriction.NotSupported )
            {
                this._DebounceEnabledCheckBox.Enabled = false;
            }
            else if ( pin.Pin.Restrictions.DebounceEnabled == Dln.Restriction.MustBeDisabled )
            {
                this._DebounceEnabledCheckBox.Enabled = false;
            }
            else
            {
                this._DebounceEnabledCheckBox.Enabled = true;
                this._DebounceEnabledCheckBox.Checked = pin.Pin.DebounceEnabled;
            }

            // Event type
            if ( pin.Pin.Restrictions.EventType == Dln.Restriction.NotSupported )
            {
                this._EventTypeComboBox.Enabled = false;
            }
            else
            {
                this._EventTypeComboBox.Enabled = true;
                this._EventTypeComboBox.DataSource = pin.Pin.SupportedEventTypes;
                this._EventTypeComboBox.SelectedItem = pin.Pin.EventType;
            }

            // Event period
            if ( pin.Pin.Restrictions.EventPeriod == Dln.Restriction.NotSupported )
            {
                this._EventPeriodNumeric.Enabled = false;
            }
            else
            {
                this._EventPeriodNumeric.Enabled = true;
                this._EventPeriodNumeric.Value = pin.Pin.EventPeriod;
            }
        }

        /// <summary> Applies the pin configuration described by pin. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pin"> The pin. </param>
        private void ApplyPinConfiguration( GpioPin pin )
        {
            if ( pin is object )
            {
                // Set common controls
                pin.Enabled = this._PinEnabledCheckBox.Checked;
                pin.Direction = ( PinDirection ) Conversions.ToInteger( this._PinDirectionComboBox.SelectedIndex );
                pin.Name = this._PinNameTextBox.Text.Trim();

                // Extra functions are not present if we disable controls when getting settings
                if ( this._PinPullupCheckBox.Enabled )
                {
                    pin.Pin.PullupEnabled = this._PinPullupCheckBox.Checked;
                }

                if ( this._PulldownCheckBox.Enabled )
                {
                    pin.Pin.PulldownEnabled = this._PulldownCheckBox.Checked;
                }

                if ( this._OpenDrainCheckBox.Enabled )
                {
                    pin.Pin.OpendrainEnabled = this._OpenDrainCheckBox.Checked;
                }

                if ( this._DebounceEnabledCheckBox.Enabled )
                {
                    pin.Pin.DebounceEnabled = this._DebounceEnabledCheckBox.Checked;
                }

                if ( this._EventTypeComboBox.Enabled )
                {
                    pin.Pin.SetEventConfiguration( ( Dln.Gpio.EventType ) Conversions.ToByte( this._EventTypeComboBox.SelectedItem ), Convert.ToUInt16( this._EventPeriodNumeric.Value ) );
                }
            }
        }

        /// <summary> Sets pin configuration button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SetPinConfigButton_Click( object sender, EventArgs e )
        {
            // Set pin configuration
            try
            {
                this._ErrorProvider.Clear();
                this.ApplyPinConfiguration( this.GpioPin );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
        }


        #endregion

    }
}
