﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.ExceptionExtensions;
using isr.Diolan.Gpio;

namespace isr.Diolan.Forms
{

    /// <summary> A gpio pin tool strip. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-04-07 </para>
    /// </remarks>
    public partial class GpioPinToolStrip : Core.Forma.ModelViewBase
    {

        #region " CONSTRUCTOR "

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioPinToolStrip()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();
            this.InitializingComponents = false;

            // Add any initialization after the InitializeComponent() call.
            this.Enabled = false;
            this.__ConfigureButton.Name = "_ConfigureButton";
            this.__LogicalStateButton.Name = "_LogicalStateButton";
            this.__ActiveLogicToggleButton.Name = "_ActiveLogicToggleButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing && this.components is object )
                {
                    this.components.Dispose();
                    this.Gpio_Pin = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PIN "

        private GpioPin _Gpio_Pin;

        private GpioPin Gpio_Pin
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._Gpio_Pin;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._Gpio_Pin != null )
                {
                    this._Gpio_Pin.PropertyChanged -= this.GpioPin_PropertyChanged;
                }

                this._Gpio_Pin = value;
                if ( this._Gpio_Pin != null )
                {
                    this._Gpio_Pin.PropertyChanged += this.GpioPin_PropertyChanged;
                }
            }
        }

        /// <summary> Gets the gpio pin. </summary>
        /// <value> The gpio pin. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public GpioPin GpioPin => this.Gpio_Pin;

        /// <summary> Executes the property changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioPin sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Gpio.GpioPin.Name ):
                    {
                        this._PinNameLabel.Text = sender.Name;
                        break;
                    }

                case nameof( Gpio.GpioPin.PinNumber ):
                    {
                        this._PinNumberLabel.Text = sender.PinNumber.ToString();
                        break;
                    }

                case nameof( Gpio.GpioPin.BitValue ):
                    {
                        this._LogicalStateButton.Checked = sender.LogicalState == LogicalState.Active;
                        break;
                    }

                case nameof( Gpio.GpioPin.ActiveLogic ):
                    {
                        this._ActiveLogicToggleButton.Checked = sender.ActiveLogic == ActiveLogic.ActiveHigh;
                        break;
                    }

                case nameof( Gpio.GpioPin.Direction ):
                    {
                        this._LogicalStateButton.CheckOnClick = sender.Direction == PinDirection.Output;
                        break;
                    }

                case nameof( Gpio.GpioPin.Enabled ):
                    {
                        this._ActiveLogicToggleButton.Enabled = sender.Enabled;
                        this._LogicalStateButton.Enabled = sender.Enabled;
                        break;
                    }
            }
        }

        /// <summary> Gpio pin property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void GpioPin_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.GpioPin_PropertyChanged ), new object[] { sender, e } );
                }

                this.OnPropertyChanged( sender as GpioPin, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
        }

        /// <summary> Assigns the given pin. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pin"> The pin. </param>
        public void Assign( GpioPin pin )
        {
            this.Gpio_Pin = pin;
            this.Enabled = pin is object;
        }

        /// <summary> Configure button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConfigureButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                using ( var configForm = new GpioPinConfigDialog() )
                {
                    _ = configForm.ShowDialog( this, this.GpioPin );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
        }

        /// <summary> Logical state button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LogicalStateButton_Click( object sender, EventArgs e )
        {
            ToolStripButton button = sender as ToolStripButton;
            if ( button is null )
                return;
            try
            {
                this._ErrorProvider.Clear();
                button.Image = this.GpioPin.LogicalState == LogicalState.Active ? My.Resources.Resources.circle_green : My.Resources.Resources.circle_grey;
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
        }

        /// <summary> Logical state button check state changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LogicalStateButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            ToolStripButton button = sender as ToolStripButton;
            if ( button is null )
                return;
            try
            {
                this._ErrorProvider.Clear();
                if ( this.GpioPin.Pin.Direction == ( int ) PinDirection.Output )
                {
                    this.GpioPin.LogicalState = button.Checked ? LogicalState.Active : LogicalState.Inactive;
                }
                else
                {
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
            finally
            {
                button.Image = this.GpioPin.LogicalState == LogicalState.Active ? My.Resources.Resources.circle_green : My.Resources.Resources.circle_grey;
            }
        }

        /// <summary> Active logic toggle button check state changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ActiveLogicToggleButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            ToolStripButton button = sender as ToolStripButton;
            if ( button is null )
                return;
            try
            {
                this._ErrorProvider.Clear();
                this.GpioPin.ActiveLogic = button.Checked ? ActiveLogic.ActiveHigh : ActiveLogic.ActiveLow;
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
            finally
            {
                button.Image = this.GpioPin.ActiveLogic == ActiveLogic.ActiveHigh ? My.Resources.Resources.Plus_Grey : My.Resources.Resources.Minus_Grey;
            }
        }

        #endregion

    }
}