using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core;
using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.ExceptionExtensions;
using isr.Diolan.Forms.SubsystemExtensions;
using isr.Diolan.Gpio;
using isr.Diolan.SubsystemExtensions;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Diolan.Forms
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> User interface for gpio pins. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-03 </para>
    /// </remarks>
    public partial class GpioPinsView : Core.Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioPinsView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();
            this.InitializingComponents = false;

            // Add any initialization after the InitializeComponent() call.
            this._TabComboBox.ComboBox.DataSource = this._Tabs.TabPages;
            this._TabComboBox.ComboBox.DisplayMember = "Text";
            this.InputPins = new GpioPinCollection();
            this.OutputPins = new GpioPinCollection();
            this.__TabComboBox.Name = "_TabComboBox";
            this.__ConnectServerButton.Name = "_ConnectServerButton";
            this.__OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton";
            this.__GetDebounceButton.Name = "_GetDebounceButton";
            this.__SetDebounceButton.Name = "_SetDebounceButton";
            this.__EventLogTextBox.Name = "_EventLogTextBox";
            this.__NewPinNumberTextBox.Name = "_NewPinNumberTextBox";
            this.__AddInputPinMenuItem.Name = "_AddInputPinMenuItem";
            this.__AddOutputPinMenuItem.Name = "_AddOutputPinMenuItem";
        }

        /// <summary> Updates the unrendered values. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void UpdateUnrenderedValues()
        {
            this._EventPinValue.Text = "0";
            this._EventPinValue.Invalidate();
            this._SubsystemGroupBox.Text = "GPIO Subsystem Settings";
            this._SubsystemGroupBox.Invalidate();
            this._DebounceNumericLabel.Text = $"Debounce Interval [{Convert.ToChar( 0x3BC )}s]:";
            this._DebounceNumericLabel.Invalidate();
            this._DebounceNumeric.Value = this.IsDeviceModalityOpen ? this.Device.Gpio.Debounce : 0m;
            this._DebounceNumeric.Value += 1m;
            this._DebounceNumeric.Value -= 1m;
            this._DebounceNumeric.Invalidate();
        }

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            base.OnLoad( e );
            this.UpdateUnrenderedValues();
            this.Invalidate();
            Application.DoEvents();
        }

        /// <summary> Gpio pins panel load. </summary>
        /// <remarks> This is required when not calling the owner form within a Using statement. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void GpioPinsPanel_Load( object sender, EventArgs e )
        {
            this._EventPinValue.Invalidate();
            Application.DoEvents();
            this._DebounceNumericLabel.Invalidate();
            Application.DoEvents();
            this._SubsystemGroupBox.Invalidate();
            Application.DoEvents();
            this._DebounceNumeric.Invalidate();
            Application.DoEvents();
            this.Invalidate();
            Application.DoEvents();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                if ( !this.IsDisposed && disposing && this.components is object )
                {
                    this.components.Dispose();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        private void OnDispose( bool disposing )
        {
            this.OnCustomDispose( disposing );
            if ( !this.IsDisposed && disposing )
            {
                if ( this.Device is object )
                    this.Device = null;
                if ( this._DeviceConnector is object )
                {
                    this._DeviceConnector.Dispose();
                    this._DeviceConnector = null;
                }

                if ( this._ServerConnector is object )
                    this._ServerConnector = null;
            }
        }

        #endregion

        #region " KNOWN STATE "

        /// <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public void InitializeKnownState()
        {
            this.OnServerConnectionChanged();
        }

        #endregion

        #region " SERVER "

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnPropertyChanged( LocalhostConnector sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( e is object )
                {
                    switch ( e.PropertyName ?? "" )
                    {
                        case nameof( LocalhostConnector.IsConnected ):
                            {
                                this.OnServerConnectionChanged();
                                break;
                            }

                        case nameof( LocalhostConnector.AttachedDevicesCount ):
                            {
                                this.OnServerAttachmentChanged();
                                break;
                            }
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToFullBlownString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, details );
            }
        }

        /// <summary> Server connector property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        private void ServerConnector_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ServerConnector_PropertyChanged ), new object[] { sender, e } );
            }

            this.OnPropertyChanged( sender as LocalhostConnector, e );
        }

#pragma warning disable IDE1006 // Naming Styles
        private LocalhostConnector __ServerConnector;

        private LocalhostConnector _ServerConnector
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__ServerConnector;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__ServerConnector != null )
                {
                    this.__ServerConnector.PropertyChanged -= this.ServerConnector_PropertyChanged;
                }

                this.__ServerConnector = value;
                if ( this.__ServerConnector != null )
                {
                    this.__ServerConnector.PropertyChanged += this.ServerConnector_PropertyChanged;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LocalhostConnector ServerConnector
        {
            get => this._ServerConnector;

            set => this._ServerConnector = value;
        }

        /// <summary> Connects a server button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConnectServerButton_Click( object sender, EventArgs e )
        {
            if ( this.ServerConnector.IsConnected )
            {
                if ( !this.ServerConnector.HasAttachedDevices() )
                {
                    this.ServerConnector.Disconnect();
                }
            }
            else
            {
                this.ServerConnector.Connect();
            }
        }

        /// <summary> Updates the server connection state. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void UpdateServerConnectionState()
        {
            this._OpenDeviceModalityButton.Enabled = this.ServerConnector.IsConnected;
            this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
        }

        /// <summary> Executes the server connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnServerConnectionChanged()
        {
            if ( this.ServerConnector.IsConnected )
            {
                _ = this.ServerConnector.Connection.ListDevicesById( this._DevicesComboBox );
                this._DevicesComboBox.SelectedIndex = 0;
            }

            this.UpdateServerConnectionState();
            this.OnServerAttachmentChanged();
        }

        /// <summary> Executes the server attachment changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OnServerAttachmentChanged()
        {
            this._ConnectServerButton.Image = My.Resources.Resources.WIFI_open_22;
            if ( this.ServerConnector.IsConnected )
            {
                this._ConnectServerButton.Text = this.ServerConnector.AttachedDevicesCount().ToString();
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
                this._ConnectServerButton.Text = "X";
            }
        }

        #endregion

        #region " DEVICE CONNECTOR "

#pragma warning disable IDE1006 // Naming Styles
        private DeviceConnector __DeviceConnector;

        private DeviceConnector _DeviceConnector
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__DeviceConnector;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__DeviceConnector != null )
                {

                    this.__DeviceConnector.DeviceClosed -= this.DeviceConnector_DeviceClosed;

                    this.__DeviceConnector.DeviceClosing -= this.DeviceConnector_DeviceClosing;

                    this.__DeviceConnector.DeviceOpened -= this.DeviceConnector_DeviceOpened;
                }

                this.__DeviceConnector = value;
                if ( this.__DeviceConnector != null )
                {
                    this.__DeviceConnector.DeviceClosed += this.DeviceConnector_DeviceClosed;
                    this.__DeviceConnector.DeviceClosing += this.DeviceConnector_DeviceClosing;
                    this.__DeviceConnector.DeviceOpened += this.DeviceConnector_DeviceOpened;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the device connector. </summary>
        /// <value> The device connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DeviceConnector DeviceConnector
        {
            get => this._DeviceConnector;

            set => this._DeviceConnector = value;
        }

        /// <summary> Device connector device closed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceClosed( object sender, EventArgs e )
        {
            this.OnServerAttachmentChanged();
            this._DeviceInfoTextBox.Text = "closed";
            this.OnModalityClosed();
        }

        /// <summary> Device connector device closing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void DeviceConnector_DeviceClosing( object sender, CancelEventArgs e )
        {
            this.OnModalityClosing( e );
        }

        /// <summary> Device connector device opened. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceOpened( object sender, EventArgs e )
        {
            this.OnServerAttachmentChanged();
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        [CLSCompliant( false )]
        public Dln.Device Device { get; private set; }

        /// <summary> Selected device information. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A DeviceInfo. </returns>
        private DeviceInfo SelectedDeviceInfo()
        {
            return string.IsNullOrWhiteSpace( this._DevicesComboBox.Text )
                ? throw new InvalidOperationException( "No devices selected" )
                : new DeviceInfo( this._DevicesComboBox.Text );
        }

        /// <summary> Attempts to open local device from the given data. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="id"> The identifier. </param>
        /// <param name="e">  Cancel details event information. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryOpenLocalDevice( long id, ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            try
            {
                this._ServerConnector = LocalhostConnector.SingleInstance();
                if ( this.ServerConnector.IsConnected )
                {
                    this._OpenDeviceModalityButton.Enabled = this.ServerConnector.IsConnected;
                    this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
                }
                else
                {
                    this.ServerConnector.Connect();
                }

                this._DeviceConnector = new DeviceConnector( this._ServerConnector );
                if ( this._DeviceConnector.TryOpenDevice( id, this.Modality, e ) )
                {
                    this.OnModalityOpening();
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( this._TopToolStrip, e.Details );
                    this._DeviceInfoTextBox.Text = $"device #{id} failed opening";
                    this._DeviceInfoTextBox.ToolTipText = e.Details;
                }
            }
            catch
            {
                try
                {
                    this.CloseDeviceModality();
                }
                catch
                {
                }

                throw;
            }
            finally
            {
                this.OnModalityConnectionChanged();
            }

            return !e.Failed;
        }

        /// <summary> Opens the device for the modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <param name="sender">   The sender. </param>
        private void OpenDeviceModality( long deviceId, Control sender )
        {
            if ( this.ServerConnector.IsConnected )
            {
                this._OpenDeviceModalityButton.Enabled = this.ServerConnector.IsConnected;
                this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            }
            else
            {
                this.ServerConnector.Connect();
            }

            this._DeviceConnector = new DeviceConnector( this._ServerConnector );

            // Open device
            var e = new ActionEventArgs();
            if ( this._DeviceConnector.TryOpenDevice( deviceId, this.Modality, e ) )
            {
                this.OnModalityOpening();
            }
            else
            {
                if ( sender is object )
                    _ = this._ErrorProvider.Annunciate( sender, e.Details );
                this._DeviceInfoTextBox.Text = "No devices";
            }
        }

        /// <summary> Closes device modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void CloseDeviceModality()
        {
            this._DeviceConnector.CloseDevice( this.Modality );
            var e = new CancelEventArgs();
            this.OnModalityClosing( e );
            if ( !e.Cancel )
            {
                this.OnModalityClosed();
            }
        }

        /// <summary> Opens device button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenDeviceModalityButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceModalityOpen )
                {
                    this.CloseDeviceModality();
                }
                else if ( string.IsNullOrWhiteSpace( this._DevicesComboBox.Text ) )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Select a device first" );
                }
                else
                {
                    this.OpenDeviceModality( this.SelectedDeviceInfo().Id, sender as Control );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
            finally
            {
                this.OnModalityConnectionChanged();
            }
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        private bool IsDeviceModalityOpen => this.Device is object && this.IsModalityOpen();

        /// <summary> Executes the modality connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityConnectionChanged()
        {
            if ( this.IsDeviceModalityOpen )
            {
                this._OpenDeviceModalityButton.Image = My.Resources.Resources.user_online_2;
                this._OpenDeviceModalityButton.Text = "Close";
            }
            else
            {
                this._OpenDeviceModalityButton.Image = My.Resources.Resources.user_offline_2;
                this._OpenDeviceModalityButton.Text = "Open";
            }
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Event log text box double click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EventLogTextBox_DoubleClick( object sender, EventArgs e )
        {
            this._EventLogTextBox.Clear();
        }

        #endregion

        #region " TABS "

        /// <summary> Selects the tab. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void TabComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._TabComboBox.SelectedIndex >= 0 && this._TabComboBox.SelectedIndex < this._Tabs.TabCount )
            {
                this._Tabs.SelectTab( this._Tabs.TabPages[this._TabComboBox.SelectedIndex] );
            }
        }

        #endregion

        #region " CONSTRUCTION "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.OnModalityClosed();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
        }

        #endregion

        #region " MODALITY "

        /// <summary> The modality. </summary>
        /// <value> The modality. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DeviceModalities Modality { get; set; } = DeviceModalities.Gpio;

        /// <summary> Executes the modality closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityClosed()
        {
            this.Device = null;
            this.OnModalityConnectionChanged();
        }

        /// <summary> Executes the modality closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnModalityClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsModalityOpen() )
                {
                    // un-register event handlers for all pins
                    foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                        pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                }
            }
        }

        /// <summary> Executes the modality opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityOpening()
        {

            // Assign the reference to the device
            this.Device = this._DeviceConnector.Device;
            this._ErrorProvider.Clear();

            // Get port count
            if ( this.Device.Gpio.Pins.Count == 0 )
            {
                // this is already done when opening the device.
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, "Adapter '{0}' doesn't support GPIO interface.", this.Device.Caption() );
                this.Device = null;
                this._DeviceInfoTextBox.Text = "not supported";
            }
            else
            {
                this._DeviceInfoTextBox.Text = this.Device.Caption();

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handler for all pins
                foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                    pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;

                // Get subsystem parameters: Debounce
                this.ReadDebounceProperties();
            }
        }

        /// <summary> Queries if a modality is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
        private bool IsModalityOpen()
        {
            return this.Device is object;
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Gpio.ConditionMetEventArgs e )
        {
            this.AppendEvent( e );
        }

        /// <summary> Appends an event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Condition met event information. </param>
        private void AppendEvent( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                string data = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}";

                // This event is handled in main thread,
                // so it is not needed to invoke when modifying form's controls.
                this._EventLogTextBox.AppendText( data );
            }
        }

        #endregion

        #region " SUBSYSTEM "

        /// <summary> Reads the debounce properties from the device. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void ReadDebounceProperties()
        {
            // Get subsystem parameters: Debounce
            if ( this.Device.Gpio.Restrictions.Debounce == Dln.Restriction.NotSupported )
            {
                this._DebounceNumeric.Enabled = false;
                this._SetDebounceButton.Enabled = false;
                this._GetDebounceButton.Enabled = false;
            }
            else
            {
                this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
            }
        }

        /// <summary> Gets debounce button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void GetDebounceButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceModalityOpen )
                {
                    this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.Modality );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
        }

        /// <summary> Sets debounce button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SetDebounceButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceModalityOpen )
                {
                    this.Device.Gpio.Debounce = ( int ) this._DebounceNumeric.Value;
                    this._DebounceNumeric.Value = this.Device.Gpio.Debounce;
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Device not open for {0}", this.Modality );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
        }

        #endregion

        #region " TOP TOOL BOX "

        /// <summary> Shows the top menu. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="value"> true to value. </param>
        public void ShowTopMenu( bool value )
        {
            this._TopToolStrip.Visible = value;
        }

        #endregion

        #region " PINS "

        /// <summary> Gets or sets the input pins. </summary>
        /// <value> The input pins. </value>
        public GpioPinCollection InputPins { get; private set; }

        /// <summary> Gets or sets the output pins. </summary>
        /// <value> The output pins. </value>
        public GpioPinCollection OutputPins { get; private set; }

        /// <summary> Adds pin. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pin"> The pin to add. </param>
        public void Add( GpioPin pin )
        {
            if ( pin is null )
                throw new ArgumentNullException( nameof( pin ) );
            var pinToolStrip = new GpioPinToolStrip();
            pinToolStrip.Assign( pin );
            if ( pin.Direction == PinDirection.Input )
            {
                Add( pinToolStrip, this._InputPinsLayout, this.InputPins );
            }
            else
            {
                Add( pinToolStrip, this._OutputPinsLayout, this.OutputPins );
            }
        }

        /// <summary> Adds pin tool strip. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="pinToolStrip"> The pin tool strip. </param>
        /// <param name="layout">       The layout. </param>
        /// <param name="pins">         Collection of pins. </param>
        private static void Add( GpioPinToolStrip pinToolStrip, TableLayoutPanel layout, GpioPinCollection pins )
        {
            if ( !pins.Any() )
            {
                layout.Controls.Clear();
                layout.ColumnStyles.Clear();
                layout.RowStyles.Clear();
                layout.ColumnCount = 4;
                _ = layout.ColumnStyles.Add( new ColumnStyle( SizeType.Absolute, 3f ) );
                _ = layout.ColumnStyles.Add( new ColumnStyle( SizeType.Percent, 50f ) );
                _ = layout.ColumnStyles.Add( new ColumnStyle( SizeType.Percent, 50f ) );
                _ = layout.ColumnStyles.Add( new ColumnStyle( SizeType.Absolute, 3f ) );
                layout.RowCount = 3;
                _ = layout.RowStyles.Add( new RowStyle( SizeType.Percent, 50f ) );
                _ = layout.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
                _ = layout.RowStyles.Add( new RowStyle( SizeType.Percent, 50f ) );
                layout.Dock = DockStyle.Fill;
                layout.Invalidate();
            }

            pins.Add( pinToolStrip.GpioPin );
            int rowIndex = 1 + (pins.Count - 1) / 2;
            int columnIndex = 1 + (pins.Count - 1) % 2;
            if ( rowIndex >= layout.RowCount - 1 )
            {
                layout.RowCount += 1;
                // layout.RowStyles.Insert(layout.RowStyles.Count - 2, New RowStyle(SizeType.AutoSize))
                layout.RowStyles.Insert( rowIndex, new RowStyle( SizeType.AutoSize ) );
                layout.Invalidate();
            }

            layout.Controls.Add( pinToolStrip, columnIndex, rowIndex );
            pinToolStrip.Dock = DockStyle.Top;
            pinToolStrip.Invalidate();
        }

        /// <summary> Creates a new pin number text box text changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void NewPinNumberTextBox_TextChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( sender is ToolStripTextBox textBox )
            {
                this._AddInputPinMenuItem.Text = $"Add input pin {textBox.Text}";
                this._AddOutputPinMenuItem.Text = $"Add output pin {textBox.Text}";
            }
        }

        /// <summary> Adds a pin handler to 'direction'. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">    The sender. </param>
        /// <param name="pinNumber"> The pin number. </param>
        /// <param name="direction"> The direction. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AddPinHandler( object sender, string pinNumber, PinDirection direction )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( int.TryParse( pinNumber, out int pinNo ) )
                {
                    if ( this.InputPins.Contains( pinNo ) )
                    {
                        _ = this._ErrorProvider.Annunciate( sender, $"Pin {pinNo} already exists as input" );
                    }
                    else if ( this.OutputPins.Contains( pinNo ) )
                    {
                        _ = this._ErrorProvider.Annunciate( sender, $"Pin {pinNo} already exists as output" );
                    }
                    else
                    {
                        this.Add( new GpioPin( this.Device.Gpio.Pins, pinNo, ActiveLogic.ActiveLow, direction ) );
                    }
                }
                else
                {
                    _ = this._ErrorProvider.Annunciate( sender, $"Unable to convert {pinNumber} to a pin number" );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToFullBlownString() );
            }
        }

        /// <summary> Adds an input pin menu item click to 'e'. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void AddInputPinMenuItem_Click( object sender, EventArgs e )
        {
            this.AddPinHandler( this._TopToolStrip, this._NewPinNumberTextBox.Text, PinDirection.Input );
        }

        /// <summary> Adds an output pin menu item click to 'e'. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void AddOutputPinMenuItem_Click( object sender, EventArgs e )
        {
            this.AddPinHandler( this._TopToolStrip, this._NewPinNumberTextBox.Text, PinDirection.Output );
        }

        #endregion

    }
}
