using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{
    [DesignerGenerated()]
    public partial class GpioPinConfigDialog
    {
        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(GpioPinConfigDialog));
            _GpioPinConfigControl = new GpioPinConfigControl();
            SuspendLayout();
            // 
            // GpioPinConfigControl1
            // 
            _GpioPinConfigControl.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _GpioPinConfigControl.Location = new System.Drawing.Point(12, 12);
            _GpioPinConfigControl.Margin = new Padding(3, 4, 3, 4);
            _GpioPinConfigControl.Name = "_GpioPinConfigControl";
            _GpioPinConfigControl.Size = new System.Drawing.Size(283, 222);
            _GpioPinConfigControl.TabIndex = 0;
            // 
            // GpioPinConfigDialog
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(304, 238);
            Controls.Add(_GpioPinConfigControl);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Name = "GpioPinConfigDialog";
            Text = "Gpio Pin Configuration";
            TopMost = true;
            ResumeLayout(false);
        }

        private GpioPinConfigControl _GpioPinConfigControl;
    }
}
