﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{
    [DesignerGenerated()]
    public partial class GpioPinsView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(GpioPinsView));
            _BottomToolStrip = new ToolStrip();
            __TabComboBox = new ToolStripComboBox();
            __TabComboBox.SelectedIndexChanged += new EventHandler(TabComboBox_SelectedIndexChanged);
            _SelectServerButton = new ToolStripSplitButton();
            _ServerNameTextBox = new ToolStripTextBox();
            _DefaultServerMenuItem = new ToolStripMenuItem();
            __ConnectServerButton = new ToolStripButton();
            __ConnectServerButton.Click += new EventHandler(ConnectServerButton_Click);
            __OpenDeviceModalityButton = new ToolStripButton();
            __OpenDeviceModalityButton.Click += new EventHandler(OpenDeviceModalityButton_Click);
            _SelectDeviceSplitButton = new ToolStripSplitButton();
            _DevicesComboBox = new ToolStripComboBox();
            _DeviceInfoTextBox = new ToolStripTextBox();
            _Tabs = new ExtendedTabControl();
            _PrimaryTabPage = new TabPage();
            _PrimaryTabLayout = new TableLayoutPanel();
            _SubsystemGroupBox = new GroupBox();
            _DebounceNumeric = new NumericUpDown();
            __GetDebounceButton = new Button();
            __GetDebounceButton.Click += new EventHandler(GetDebounceButton_Click);
            __SetDebounceButton = new Button();
            __SetDebounceButton.Click += new EventHandler(SetDebounceButton_Click);
            _DebounceNumericLabel = new Label();
            _InputPinsTabPage = new TabPage();
            _InputPinsLayout = new TableLayoutPanel();
            _OutputPinsTabPage = new TabPage();
            _OutputPinsLayout = new TableLayoutPanel();
            _EventLogTabPage = new TabPage();
            __EventLogTextBox = new TextBox();
            __EventLogTextBox.DoubleClick += new EventHandler(EventLogTextBox_DoubleClick);
            _ErrorProvider = new ErrorProvider(components);
            _ToolTip = new ToolTip(components);
            _ToolStripContainer = new ToolStripContainer();
            _TopToolStrip = new ToolStrip();
            _EventPinNumberLabel = new ToolStripLabel();
            _EvetnPinNameLabel = new ToolStripLabel();
            _EventPinValue = new ToolStripTextBox();
            _AddPinsSplitButton = new ToolStripSplitButton();
            __NewPinNumberTextBox = new ToolStripTextBox();
            __NewPinNumberTextBox.TextChanged += new EventHandler(NewPinNumberTextBox_TextChanged);
            __AddInputPinMenuItem = new ToolStripMenuItem();
            __AddInputPinMenuItem.Click += new EventHandler(AddInputPinMenuItem_Click);
            __AddOutputPinMenuItem = new ToolStripMenuItem();
            __AddOutputPinMenuItem.Click += new EventHandler(AddOutputPinMenuItem_Click);
            _BottomToolStrip.SuspendLayout();
            _Tabs.SuspendLayout();
            _PrimaryTabPage.SuspendLayout();
            _PrimaryTabLayout.SuspendLayout();
            _SubsystemGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_DebounceNumeric).BeginInit();
            _InputPinsTabPage.SuspendLayout();
            _OutputPinsTabPage.SuspendLayout();
            _EventLogTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            _ToolStripContainer.ContentPanel.SuspendLayout();
            _ToolStripContainer.TopToolStripPanel.SuspendLayout();
            _ToolStripContainer.SuspendLayout();
            _TopToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _BottomToolStrip
            // 
            _BottomToolStrip.Dock = DockStyle.None;
            _BottomToolStrip.Items.AddRange(new ToolStripItem[] { __TabComboBox, _SelectServerButton, __ConnectServerButton, __OpenDeviceModalityButton, _SelectDeviceSplitButton, _DeviceInfoTextBox });
            _BottomToolStrip.Location = new System.Drawing.Point(0, 0);
            _BottomToolStrip.Name = "_BottomToolStrip";
            _BottomToolStrip.Size = new System.Drawing.Size(417, 29);
            _BottomToolStrip.Stretch = true;
            _BottomToolStrip.TabIndex = 0;
            // 
            // _TabComboBox
            // 
            __TabComboBox.Name = "__TabComboBox";
            __TabComboBox.Size = new System.Drawing.Size(81, 29);
            __TabComboBox.ToolTipText = "Select panel";
            // 
            // _SelectServerButton
            // 
            _SelectServerButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectServerButton.DropDownItems.AddRange(new ToolStripItem[] { _ServerNameTextBox, _DefaultServerMenuItem });
            _SelectServerButton.Image = My.Resources.Resources.network_server;
            _SelectServerButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectServerButton.Name = "_SelectServerButton";
            _SelectServerButton.Size = new System.Drawing.Size(38, 26);
            _SelectServerButton.Text = "Select Server";
            // 
            // _ServerNameTextBox
            // 
            _ServerNameTextBox.Name = "_ServerNameTextBox";
            _ServerNameTextBox.Size = new System.Drawing.Size(100, 23);
            _ServerNameTextBox.Text = "localhost:9656";
            // 
            // _DefaultServerMenuItem
            // 
            _DefaultServerMenuItem.Checked = true;
            _DefaultServerMenuItem.CheckOnClick = true;
            _DefaultServerMenuItem.CheckState = CheckState.Checked;
            _DefaultServerMenuItem.Name = "_DefaultServerMenuItem";
            _DefaultServerMenuItem.Size = new System.Drawing.Size(198, 22);
            _DefaultServerMenuItem.Text = "User Default Server:Port";
            // 
            // _ConnectServerButton
            // 
            __ConnectServerButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ConnectServerButton.ForeColor = System.Drawing.Color.Red;
            __ConnectServerButton.Image = My.Resources.Resources.WIFI_open_22_Right_Up;
            __ConnectServerButton.ImageScaling = ToolStripItemImageScaling.None;
            __ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ConnectServerButton.Name = "__ConnectServerButton";
            __ConnectServerButton.Size = new System.Drawing.Size(41, 26);
            __ConnectServerButton.Text = "X";
            __ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            __ConnectServerButton.ToolTipText = "Connect or disconnect serve and show attached devices.";
            // 
            // _OpenDeviceModalityButton
            // 
            __OpenDeviceModalityButton.Alignment = ToolStripItemAlignment.Right;
            __OpenDeviceModalityButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __OpenDeviceModalityButton.Enabled = false;
            __OpenDeviceModalityButton.Image = My.Resources.Resources.user_offline_2;
            __OpenDeviceModalityButton.ImageScaling = ToolStripItemImageScaling.None;
            __OpenDeviceModalityButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenDeviceModalityButton.Name = "__OpenDeviceModalityButton";
            __OpenDeviceModalityButton.Size = new System.Drawing.Size(26, 26);
            __OpenDeviceModalityButton.Text = "Open";
            __OpenDeviceModalityButton.ToolTipText = "Open or close the device.";
            // 
            // _SelectDeviceSplitButton
            // 
            _SelectDeviceSplitButton.Alignment = ToolStripItemAlignment.Right;
            _SelectDeviceSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectDeviceSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _DevicesComboBox });
            _SelectDeviceSplitButton.Image = My.Resources.Resources.network_server_database;
            _SelectDeviceSplitButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton";
            _SelectDeviceSplitButton.Size = new System.Drawing.Size(38, 26);
            _SelectDeviceSplitButton.Text = "Device";
            _SelectDeviceSplitButton.ToolTipText = "Select Device";
            // 
            // _DevicesComboBox
            // 
            _DevicesComboBox.Name = "_DevicesComboBox";
            _DevicesComboBox.Size = new System.Drawing.Size(121, 23);
            _DevicesComboBox.Text = "DLN-4M.1.1";
            // 
            // _DeviceInfoTextBox
            // 
            _DeviceInfoTextBox.Alignment = ToolStripItemAlignment.Right;
            _DeviceInfoTextBox.Name = "_DeviceInfoTextBox";
            _DeviceInfoTextBox.ReadOnly = true;
            _DeviceInfoTextBox.Size = new System.Drawing.Size(100, 29);
            _DeviceInfoTextBox.Text = "closed";
            _DeviceInfoTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_PrimaryTabPage);
            _Tabs.Controls.Add(_InputPinsTabPage);
            _Tabs.Controls.Add(_OutputPinsTabPage);
            _Tabs.Controls.Add(_EventLogTabPage);
            _Tabs.Dock = DockStyle.Fill;
            _Tabs.HideTabHeaders = true;
            _Tabs.Location = new System.Drawing.Point(0, 0);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new System.Drawing.Size(417, 452);
            _Tabs.TabIndex = 0;
            // 
            // _PrimaryTabPage
            // 
            _PrimaryTabPage.Controls.Add(_PrimaryTabLayout);
            _PrimaryTabPage.Location = new System.Drawing.Point(4, 26);
            _PrimaryTabPage.Name = "_PrimaryTabPage";
            _PrimaryTabPage.Padding = new Padding(3);
            _PrimaryTabPage.Size = new System.Drawing.Size(409, 422);
            _PrimaryTabPage.TabIndex = 0;
            _PrimaryTabPage.Text = "GPIO";
            _PrimaryTabPage.UseVisualStyleBackColor = true;
            // 
            // _PrimaryTabLayout
            // 
            _PrimaryTabLayout.ColumnCount = 3;
            _PrimaryTabLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _PrimaryTabLayout.ColumnStyles.Add(new ColumnStyle());
            _PrimaryTabLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _PrimaryTabLayout.Controls.Add(_SubsystemGroupBox, 1, 1);
            _PrimaryTabLayout.Dock = DockStyle.Fill;
            _PrimaryTabLayout.Location = new System.Drawing.Point(3, 3);
            _PrimaryTabLayout.Name = "_PrimaryTabLayout";
            _PrimaryTabLayout.RowCount = 3;
            _PrimaryTabLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _PrimaryTabLayout.RowStyles.Add(new RowStyle());
            _PrimaryTabLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _PrimaryTabLayout.Size = new System.Drawing.Size(403, 416);
            _PrimaryTabLayout.TabIndex = 8;
            // 
            // _SubsystemGroupBox
            // 
            _SubsystemGroupBox.Controls.Add(_DebounceNumeric);
            _SubsystemGroupBox.Controls.Add(__GetDebounceButton);
            _SubsystemGroupBox.Controls.Add(__SetDebounceButton);
            _SubsystemGroupBox.Controls.Add(_DebounceNumericLabel);
            _SubsystemGroupBox.Location = new System.Drawing.Point(26, 176);
            _SubsystemGroupBox.Margin = new Padding(3, 4, 3, 4);
            _SubsystemGroupBox.Name = "_SubsystemGroupBox";
            _SubsystemGroupBox.Padding = new Padding(3, 4, 3, 4);
            _SubsystemGroupBox.Size = new System.Drawing.Size(350, 64);
            _SubsystemGroupBox.TabIndex = 6;
            _SubsystemGroupBox.TabStop = false;
            _SubsystemGroupBox.Text = "GPIO Subsystem Settings";
            // 
            // _DebounceNumeric
            // 
            _DebounceNumeric.Location = new System.Drawing.Point(157, 22);
            _DebounceNumeric.Margin = new Padding(3, 4, 3, 4);
            _DebounceNumeric.Maximum = new decimal(new int[] { 32000, 0, 0, 0 });
            _DebounceNumeric.Name = "_DebounceNumeric";
            _DebounceNumeric.Size = new System.Drawing.Size(63, 25);
            _DebounceNumeric.TabIndex = 1;
            // 
            // _GetDebounceButton
            // 
            __GetDebounceButton.Location = new System.Drawing.Point(282, 19);
            __GetDebounceButton.Margin = new Padding(3, 4, 3, 4);
            __GetDebounceButton.Name = "__GetDebounceButton";
            __GetDebounceButton.Size = new System.Drawing.Size(53, 30);
            __GetDebounceButton.TabIndex = 3;
            __GetDebounceButton.Text = "Get";
            _ToolTip.SetToolTip(__GetDebounceButton, "Gets the debounce interval");
            __GetDebounceButton.UseVisualStyleBackColor = true;
            // 
            // _SetDebounceButton
            // 
            __SetDebounceButton.Location = new System.Drawing.Point(222, 19);
            __SetDebounceButton.Margin = new Padding(3, 4, 3, 4);
            __SetDebounceButton.Name = "__SetDebounceButton";
            __SetDebounceButton.Size = new System.Drawing.Size(53, 30);
            __SetDebounceButton.TabIndex = 2;
            __SetDebounceButton.Text = "Set";
            _ToolTip.SetToolTip(__SetDebounceButton, "Sets the debounce interval");
            __SetDebounceButton.UseVisualStyleBackColor = true;
            // 
            // _DebounceNumericLabel
            // 
            _DebounceNumericLabel.AutoSize = true;
            _DebounceNumericLabel.Location = new System.Drawing.Point(13, 26);
            _DebounceNumericLabel.Name = "_DebounceNumericLabel";
            _DebounceNumericLabel.Size = new System.Drawing.Size(142, 17);
            _DebounceNumericLabel.TabIndex = 0;
            _DebounceNumericLabel.Text = "Debounce Interval [us]:";
            _DebounceNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _InputPinsTabPage
            // 
            _InputPinsTabPage.Controls.Add(_InputPinsLayout);
            _InputPinsTabPage.Location = new System.Drawing.Point(4, 22);
            _InputPinsTabPage.Name = "_InputPinsTabPage";
            _InputPinsTabPage.Size = new System.Drawing.Size(409, 426);
            _InputPinsTabPage.TabIndex = 2;
            _InputPinsTabPage.Text = "Inputs";
            _InputPinsTabPage.UseVisualStyleBackColor = true;
            // 
            // _InputPinsLayout
            // 
            _InputPinsLayout.ColumnCount = 2;
            _InputPinsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _InputPinsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _InputPinsLayout.Location = new System.Drawing.Point(73, 36);
            _InputPinsLayout.Name = "_InputPinsLayout";
            _InputPinsLayout.RowCount = 2;
            _InputPinsLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _InputPinsLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _InputPinsLayout.Size = new System.Drawing.Size(200, 100);
            _InputPinsLayout.TabIndex = 0;
            // 
            // _OutputPinsTabPage
            // 
            _OutputPinsTabPage.Controls.Add(_OutputPinsLayout);
            _OutputPinsTabPage.Location = new System.Drawing.Point(4, 22);
            _OutputPinsTabPage.Name = "_OutputPinsTabPage";
            _OutputPinsTabPage.Size = new System.Drawing.Size(409, 426);
            _OutputPinsTabPage.TabIndex = 2;
            _OutputPinsTabPage.Text = "Outputs";
            _OutputPinsTabPage.UseVisualStyleBackColor = true;
            // 
            // _OutputPinsLayout
            // 
            _OutputPinsLayout.ColumnCount = 2;
            _OutputPinsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _OutputPinsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _OutputPinsLayout.Location = new System.Drawing.Point(119, 55);
            _OutputPinsLayout.Name = "_OutputPinsLayout";
            _OutputPinsLayout.RowCount = 2;
            _OutputPinsLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _OutputPinsLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _OutputPinsLayout.Size = new System.Drawing.Size(200, 100);
            _OutputPinsLayout.TabIndex = 0;
            // 
            // _EventLogTabPage
            // 
            _EventLogTabPage.Controls.Add(__EventLogTextBox);
            _EventLogTabPage.Location = new System.Drawing.Point(4, 22);
            _EventLogTabPage.Name = "_EventLogTabPage";
            _EventLogTabPage.Padding = new Padding(3);
            _EventLogTabPage.Size = new System.Drawing.Size(409, 426);
            _EventLogTabPage.TabIndex = 1;
            _EventLogTabPage.Text = "Events";
            _EventLogTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTextBox
            // 
            __EventLogTextBox.Dock = DockStyle.Fill;
            __EventLogTextBox.Location = new System.Drawing.Point(3, 3);
            __EventLogTextBox.Multiline = true;
            __EventLogTextBox.Name = "__EventLogTextBox";
            __EventLogTextBox.ScrollBars = ScrollBars.Both;
            __EventLogTextBox.Size = new System.Drawing.Size(403, 420);
            __EventLogTextBox.TabIndex = 0;
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            _ToolStripContainer.BottomToolStripPanel.Controls.Add(_BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            _ToolStripContainer.ContentPanel.Controls.Add(_Tabs);
            _ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(417, 452);
            _ToolStripContainer.Dock = DockStyle.Fill;
            _ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            _ToolStripContainer.Name = "_ToolStripContainer";
            _ToolStripContainer.Size = new System.Drawing.Size(417, 506);
            _ToolStripContainer.TabIndex = 0;
            _ToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _ToolStripContainer.TopToolStripPanel
            // 
            _ToolStripContainer.TopToolStripPanel.Controls.Add(_TopToolStrip);
            // 
            // _TopToolStrip
            // 
            _TopToolStrip.Dock = DockStyle.None;
            _TopToolStrip.Items.AddRange(new ToolStripItem[] { _EventPinNumberLabel, _EvetnPinNameLabel, _EventPinValue, _AddPinsSplitButton });
            _TopToolStrip.Location = new System.Drawing.Point(3, 0);
            _TopToolStrip.Name = "_TopToolStrip";
            _TopToolStrip.Size = new System.Drawing.Size(165, 25);
            _TopToolStrip.TabIndex = 0;
            // 
            // _EventPinNumberLabel
            // 
            _EventPinNumberLabel.Name = "_EventPinNumberLabel";
            _EventPinNumberLabel.Size = new System.Drawing.Size(31, 22);
            _EventPinNumberLabel.Text = "Pin#";
            // 
            // _EvetnPinNameLabel
            // 
            _EvetnPinNameLabel.Name = "_EvetnPinNameLabel";
            _EvetnPinNameLabel.Size = new System.Drawing.Size(53, 22);
            _EvetnPinNameLabel.Text = "<name>";
            // 
            // _EventPinValue
            // 
            _EventPinValue.BackColor = System.Drawing.Color.Black;
            _EventPinValue.ForeColor = System.Drawing.Color.Aqua;
            _EventPinValue.Name = "_EventPinValue";
            _EventPinValue.Size = new System.Drawing.Size(13, 25);
            _EventPinValue.Text = "0";
            // 
            // _AddPinsSplitButton
            // 
            _AddPinsSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _AddPinsSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __NewPinNumberTextBox, __AddInputPinMenuItem, __AddOutputPinMenuItem });
            _AddPinsSplitButton.Image = (System.Drawing.Image)resources.GetObject("_AddPinsSplitButton.Image");
            _AddPinsSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _AddPinsSplitButton.Name = "_AddPinsSplitButton";
            _AddPinsSplitButton.Size = new System.Drawing.Size(54, 22);
            _AddPinsSplitButton.Text = "Add...";
            // 
            // _NewPinNumberTextBox
            // 
            __NewPinNumberTextBox.Name = "__NewPinNumberTextBox";
            __NewPinNumberTextBox.Size = new System.Drawing.Size(100, 23);
            __NewPinNumberTextBox.Text = "0";
            __NewPinNumberTextBox.ToolTipText = "Enter a new pin number to add";
            // 
            // _AddInputPinMenuItem
            // 
            __AddInputPinMenuItem.Name = "__AddInputPinMenuItem";
            __AddInputPinMenuItem.Size = new System.Drawing.Size(160, 22);
            __AddInputPinMenuItem.Text = "Add Input Pin";
            // 
            // _AddOutputPinMenuItem
            // 
            __AddOutputPinMenuItem.Name = "__AddOutputPinMenuItem";
            __AddOutputPinMenuItem.Size = new System.Drawing.Size(160, 22);
            __AddOutputPinMenuItem.Text = "Add Output Pin";
            // 
            // GpioPinsPanel
            // 
            Controls.Add(_ToolStripContainer);
            Name = "GpioPinsPanel";
            Size = new System.Drawing.Size(417, 506);
            _BottomToolStrip.ResumeLayout(false);
            _BottomToolStrip.PerformLayout();
            _Tabs.ResumeLayout(false);
            _PrimaryTabPage.ResumeLayout(false);
            _PrimaryTabLayout.ResumeLayout(false);
            _SubsystemGroupBox.ResumeLayout(false);
            _SubsystemGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_DebounceNumeric).EndInit();
            _InputPinsTabPage.ResumeLayout(false);
            _OutputPinsTabPage.ResumeLayout(false);
            _EventLogTabPage.ResumeLayout(false);
            _EventLogTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.BottomToolStripPanel.PerformLayout();
            _ToolStripContainer.ContentPanel.ResumeLayout(false);
            _ToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.TopToolStripPanel.PerformLayout();
            _ToolStripContainer.ResumeLayout(false);
            _ToolStripContainer.PerformLayout();
            _TopToolStrip.ResumeLayout(false);
            _TopToolStrip.PerformLayout();
            ResumeLayout(false);
        }

        private ToolStrip _BottomToolStrip;
        private ToolStripTextBox _DeviceInfoTextBox;
        private ErrorProvider _ErrorProvider;
        private ToolTip _ToolTip;
        private ToolStripButton __OpenDeviceModalityButton;

        private ToolStripButton _OpenDeviceModalityButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDeviceModalityButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDeviceModalityButton != null)
                {
                    __OpenDeviceModalityButton.Click -= OpenDeviceModalityButton_Click;
                }

                __OpenDeviceModalityButton = value;
                if (__OpenDeviceModalityButton != null)
                {
                    __OpenDeviceModalityButton.Click += OpenDeviceModalityButton_Click;
                }
            }
        }

        private TabPage _PrimaryTabPage;
        private TabPage _EventLogTabPage;
        private TextBox __EventLogTextBox;

        private TextBox _EventLogTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EventLogTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EventLogTextBox != null)
                {
                    __EventLogTextBox.DoubleClick -= EventLogTextBox_DoubleClick;
                }

                __EventLogTextBox = value;
                if (__EventLogTextBox != null)
                {
                    __EventLogTextBox.DoubleClick += EventLogTextBox_DoubleClick;
                }
            }
        }

        private ToolStripContainer _ToolStripContainer;
        private ExtendedTabControl _Tabs;
        private ToolStripComboBox _DevicesComboBox;
        private ToolStripSplitButton _SelectServerButton;
        private ToolStripTextBox _ServerNameTextBox;
        private ToolStripMenuItem _DefaultServerMenuItem;
        private ToolStripSplitButton _SelectDeviceSplitButton;
        private GroupBox _SubsystemGroupBox;
        private NumericUpDown _DebounceNumeric;
        private Button __GetDebounceButton;

        private Button _GetDebounceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __GetDebounceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__GetDebounceButton != null)
                {
                    __GetDebounceButton.Click -= GetDebounceButton_Click;
                }

                __GetDebounceButton = value;
                if (__GetDebounceButton != null)
                {
                    __GetDebounceButton.Click += GetDebounceButton_Click;
                }
            }
        }

        private Button __SetDebounceButton;

        private Button _SetDebounceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SetDebounceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SetDebounceButton != null)
                {
                    __SetDebounceButton.Click -= SetDebounceButton_Click;
                }

                __SetDebounceButton = value;
                if (__SetDebounceButton != null)
                {
                    __SetDebounceButton.Click += SetDebounceButton_Click;
                }
            }
        }

        private Label _DebounceNumericLabel;
        private TableLayoutPanel _PrimaryTabLayout;
        private TabPage _InputPinsTabPage;
        private TabPage _OutputPinsTabPage;
        private ToolStripComboBox __TabComboBox;

        private ToolStripComboBox _TabComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TabComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TabComboBox != null)
                {
                    __TabComboBox.SelectedIndexChanged -= TabComboBox_SelectedIndexChanged;
                }

                __TabComboBox = value;
                if (__TabComboBox != null)
                {
                    __TabComboBox.SelectedIndexChanged += TabComboBox_SelectedIndexChanged;
                }
            }
        }

        private ToolStripButton __ConnectServerButton;

        private ToolStripButton _ConnectServerButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConnectServerButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConnectServerButton != null)
                {
                    __ConnectServerButton.Click -= ConnectServerButton_Click;
                }

                __ConnectServerButton = value;
                if (__ConnectServerButton != null)
                {
                    __ConnectServerButton.Click += ConnectServerButton_Click;
                }
            }
        }

        private ToolStrip _TopToolStrip;
        private ToolStripLabel _EventPinNumberLabel;
        private ToolStripLabel _EvetnPinNameLabel;
        private ToolStripTextBox _EventPinValue;
        private ToolStripSplitButton _AddPinsSplitButton;
        private ToolStripTextBox __NewPinNumberTextBox;

        private ToolStripTextBox _NewPinNumberTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NewPinNumberTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NewPinNumberTextBox != null)
                {
                    __NewPinNumberTextBox.TextChanged -= NewPinNumberTextBox_TextChanged;
                }

                __NewPinNumberTextBox = value;
                if (__NewPinNumberTextBox != null)
                {
                    __NewPinNumberTextBox.TextChanged += NewPinNumberTextBox_TextChanged;
                }
            }
        }

        private ToolStripMenuItem __AddInputPinMenuItem;

        private ToolStripMenuItem _AddInputPinMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AddInputPinMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AddInputPinMenuItem != null)
                {
                    __AddInputPinMenuItem.Click -= AddInputPinMenuItem_Click;
                }

                __AddInputPinMenuItem = value;
                if (__AddInputPinMenuItem != null)
                {
                    __AddInputPinMenuItem.Click += AddInputPinMenuItem_Click;
                }
            }
        }

        private ToolStripMenuItem __AddOutputPinMenuItem;

        private ToolStripMenuItem _AddOutputPinMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AddOutputPinMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AddOutputPinMenuItem != null)
                {
                    __AddOutputPinMenuItem.Click -= AddOutputPinMenuItem_Click;
                }

                __AddOutputPinMenuItem = value;
                if (__AddOutputPinMenuItem != null)
                {
                    __AddOutputPinMenuItem.Click += AddOutputPinMenuItem_Click;
                }
            }
        }

        private TableLayoutPanel _InputPinsLayout;
        private TableLayoutPanel _OutputPinsLayout;
    }
}