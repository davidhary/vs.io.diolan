﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace isr.Diolan.Forms.SubsystemExtensions
{

    /// <summary> Includes extensions for <see cref="Dln.Device">DLN Devices</see>. </summary>
    /// <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-05-26, 1.0.5624.x. </para></remarks>
    public static class Methods
    {

        #region " DEVICES "

        /// <summary> List devices by identifier. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">  The connection. </param>
        /// <param name="listControl"> The list control. </param>
        /// <returns> An Integer. </returns>
        [CLSCompliant( false )]
        public static int ListDevicesById( this Dln.Connection connection, ListControl listControl )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            int count = 0;
            string details = string.Empty;
            var deviceInfos = new DeviceInfoCollection();
            if ( deviceInfos.TryEnumerateDevices( connection, details ) )
            {
                listControl.DataSource = deviceInfos.ToList();
                listControl.DisplayMember = "Caption";
                listControl.ValueMember = "Id";
                count = deviceInfos.Count;
            }
            else
            {
                listControl.DataSource = new string[] { details };
            }

            return count;
        }

        /// <summary> List devices by identifier. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="connection">  The connection. </param>
        /// <param name="listControl"> The list control. </param>
        /// <returns> An Integer. </returns>
        [CLSCompliant( false )]
        public static int ListDevicesById( this Dln.Connection connection, ToolStripComboBox listControl )
        {
            if ( connection is null )
                throw new ArgumentNullException( nameof( connection ) );
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            int count = 0;
            string details = string.Empty;
            var deviceInfos = new DeviceInfoCollection();
            if ( deviceInfos.TryEnumerateDevices( connection, details ) )
            {
                listControl.Items.Clear();
                foreach ( DeviceInfo di in deviceInfos )
                    _ = listControl.Items.Add( di );
                // this does not work with the tool bar....
                // listControl.DataSource = Me.ToArray
                listControl.ComboBox.DisplayMember = "Caption";
                listControl.ComboBox.ValueMember = "Id";
                count = deviceInfos.Count;
            }
            else
            {
                listControl.Text = details;
                listControl.Items.Clear();
            }

            return count;
        }

        #endregion

        #region " ADC CHANNELS EXTENSIONS "

        /// <summary> List channel numbers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channels">    The Channels. </param>
        /// <param name="listControl"> The list control. </param>
        [CLSCompliant( false )]
        public static void ListNumbers( this Dln.Adc.Channels channels, ListControl listControl )
        {
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            var numbers = new List<int>();
            if ( channels is object )
            {
                for ( int i = 0, loopTo = channels.Count - 1; i <= loopTo; i++ )
                    numbers.Add( i );
            }

            listControl.DataSource = numbers;
        }

        #endregion

        #region " ADC PORTS EXTENSIONS "

        /// <summary> List port numbers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ports">          The ports. </param>
        /// <param name="listControl">    The list control. </param>
        /// <param name="withResolution"> True to with resolution. </param>
        [CLSCompliant( false )]
        public static void ListNumbers( this Dln.Adc.Ports ports, ListControl listControl, bool withResolution )
        {
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            var values = new List<string>();
            if ( ports is object )
            {
                for ( int i = 0, loopTo = ports.Count - 1; i <= loopTo; i++ )
                {
                    if ( withResolution )
                    {
                        values.Add( $"{i} ({ports[i].Resolution} bits)" );
                    }
                    else
                    {
                        values.Add( $"{i}" );
                    }
                }
            }

            listControl.DataSource = values;
        }

        #endregion

        #region " COUNTER TIMER PORTS EXTENSIONS "

        /// <summary> List port numbers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ports">          The ports. </param>
        /// <param name="listControl">    The list control. </param>
        /// <param name="withResolution"> True to with resolution. </param>
        [CLSCompliant( false )]
        public static void ListNumbers( this Dln.PulseCounter.Ports ports, ListControl listControl, bool withResolution )
        {
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            var values = new List<string>();
            if ( ports is object )
            {
                for ( int i = 0, loopTo = ports.Count - 1; i <= loopTo; i++ )
                {
                    if ( withResolution )
                    {
                        values.Add( $"{i} ({ports[i].Resolution} bits)" );
                    }
                    else
                    {
                        values.Add( $"{i}" );
                    }
                }
            }

            listControl.DataSource = values;
        }

        #endregion

        #region " GPIO PINS EXTENSIONS "

        /// <summary> List pin numbers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pins">        The pins. </param>
        /// <param name="listControl"> The list control. </param>
        [CLSCompliant( false )]
        public static void ListNumbers( this Dln.Gpio.Pins pins, ListControl listControl )
        {
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            var numbers = new List<int>();
            if ( pins is object )
            {
                for ( int i = 0, loopTo = pins.Count - 1; i <= loopTo; i++ )
                    numbers.Add( i );
            }

            listControl.DataSource = numbers;
        }

        /// <summary> List channel numbers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="pins">        The pins. </param>
        /// <param name="listControl"> The list control. </param>
        [CLSCompliant( false )]
        public static void ListNumbers( this Dln.Gpio.Pins pins, ComboBox listControl )
        {
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            var numbers = new List<int>();
            if ( pins is object )
            {
                for ( int i = 0, loopTo = pins.Count - 1; i <= loopTo; i++ )
                    numbers.Add( i );
            }

            listControl.DataSource = numbers;
        }

        #endregion

        #region " LEDS EXTENSIONS "

        /// <summary> List led numbers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="leds">        The Leds. </param>
        /// <param name="listControl"> The list control. </param>
        [CLSCompliant( false )]
        public static void ListNumbers( this Dln.Led.Leds leds, ListControl listControl )
        {
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            var ledNumbers = new List<int>();
            if ( leds is object )
            {
                for ( int i = 0, loopTo = leds.Count - 1; i <= loopTo; i++ )
                    ledNumbers.Add( i );
            }

            listControl.DataSource = ledNumbers;
        }

        #endregion

        #region " PWM PORTS EXTENSIONS "

        /// <summary> List port numbers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ports">       The ports. </param>
        /// <param name="listControl"> The list control. </param>
        [CLSCompliant( false )]
        public static void ListNumbers( this Dln.Pwm.Ports ports, ListControl listControl )
        {
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            var numbers = new List<int>();
            if ( ports is object )
            {
                for ( int i = 0, loopTo = ports.Count - 1; i <= loopTo; i++ )
                    numbers.Add( i );
            }

            listControl.DataSource = numbers;
        }

        #endregion

        #region " PWM CHANNELS EXTENSIONS "

        /// <summary> List channel numbers. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="channels">    The Channels. </param>
        /// <param name="listControl"> The list control. </param>
        [CLSCompliant( false )]
        public static void ListNumbers( this Dln.Pwm.Channels channels, ListControl listControl )
        {
            if ( listControl is null )
                throw new ArgumentNullException( nameof( listControl ) );
            var numbers = new List<int>();
            if ( channels is object )
            {
                for ( int i = 0, loopTo = channels.Count - 1; i <= loopTo; i++ )
                    numbers.Add( i );
            }

            listControl.DataSource = numbers;
        }

        #endregion

    }
}