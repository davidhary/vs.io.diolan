using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.ExceptionExtensions;
using isr.Diolan.Gpio;
using isr.Diolan.SubsystemExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{

    /// <summary> User interface for managing a GPIO Handler interface. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    public partial class GpioHandlerView
    {

        #region " CONSTRUCTION "

        /// <summary> Executes the custom dispose action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to disposing. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.OnModalityClosed();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " MODALITY "

        /// <summary> The modality. </summary>
        /// <value> The modality. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public DeviceModalities Modality { get; set; } = DeviceModalities.Gpio;

        /// <summary> Executes the modality closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityClosed()
        {
            if ( this._Emulator is object )
            {
                this._Emulator.Dispose();
                this._Emulator = null;
            }

            if ( this._Driver is object )
            {
                this._Driver.Dispose();
                this._Driver = null;
            }

            this.Device = null;
            this.OnModalityConnectionChanged();
        }

        /// <summary> Executes the modality closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnModalityClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsModalityOpen() )
                {
                    // un-register event handlers for all pins
                    foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                        pin.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                }
            }
        }

        /// <summary> Executes the modality opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityOpening()
        {
            this._Driver = null;
            this.Device = this._DeviceConnector.Device;
            this._ErrorProvider.Clear();

            // Get port count
            if ( this.Device.Gpio.Pins.Count == 0 )
            {
                // this is already done when opening the device.
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, "Adapter '{0}' doesn't support GPIO interface.", this.Device.Caption() );
                this.Device = null;
                this._DeviceInfoTextBox.Text = "not supported";
            }
            else
            {
                this._DeviceInfoTextBox.Text = this.Device.Caption();

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handler for all pins
                foreach ( Dln.Gpio.Pin pin in this.Device.Gpio.Pins )
                    pin.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                this._Driver = new GpioHandlerDriver();
            }
        }

        /// <summary> Queries if a modality is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
        private bool IsModalityOpen()
        {
            return this._Driver is object;
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Gpio.ConditionMetEventArgs e )
        {
            this.AppendEvent( e );
        }

        /// <summary> Appends an event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Condition met event information. </param>
        private void AppendEvent( Dln.Gpio.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                string data = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}";
                // This event is handled in main thread,
                // so it is not needed to invoke when modifying form's controls.
                this._EventLogTextBox.AppendText( data );
            }
        }

        #endregion

        #region " DRIVER "

#pragma warning disable IDE1006 // Naming Styles
        private GpioHandlerDriver __Driver;

        private GpioHandlerDriver _Driver
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__Driver;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__Driver != null )
                {
                    this.__Driver.PropertyChanged -= this.Driver_PropertyChanged;
                }

                this.__Driver = value;
                if ( this.__Driver != null )
                {
                    this.__Driver.PropertyChanged += this.Driver_PropertyChanged;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the driver. </summary>
        /// <value> The driver. </value>
        [CLSCompliant( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandlerDriver Driver
        {
            get => this._Driver;

            set {
                this._Driver = value;
                if ( this._Driver is object )
                {
                    this._StartTestPinNumberNumeric.Value = this._Driver.StartTestPinNumber;
                    this._EndTestPinNumberNumeric.Value = this._Driver.EndTestPinNumber;
                    this._ActiveLowLogicRadioButton.Checked = this._Driver.ActiveLogic == ActiveLogic.ActiveLow;
                    this.BinPortMask = this._Driver.BinMask;
                }
            }
        }

        /// <summary> Gets or sets the handler mask. </summary>
        /// <value> The entered mask. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private long BinPortMask
        {
            get => Convert.ToInt32( this._BinPortMaskTextBox.Text, 2 );

            set => this._BinPortMaskTextBox.Text = Convert.ToString( value, 2 );
        }

        /// <summary> Bin port mask text box validating. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void BinPortMaskTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.BinPortMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                    e.Cancel = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
                e.Cancel = true;
            }
        }

        /// <summary> Handles the Configure Emulator button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConfigureHandlerInterfaceButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceModalityOpen )
                {
                    if ( !this.IsDeviceModalityOpen )
                    {
                        args.RegisterFailure( "Device not open" );
                    }
                    else if ( this.Device.Gpio is null )
                    {
                        args.RegisterFailure( $"Gpio not supported on {this.Device.Caption()}" );
                    }
                    else if ( this.Device.Gpio.Pins is null )
                    {
                        args.RegisterFailure( $"Gpio not supported on {this.Device.Caption()}" );
                    }
                    else
                    {
                        if ( this._Driver is null )
                        {
                            this._Driver = new GpioHandlerDriver();
                        }

                        this._Driver.StartTestPinNumber = ( int ) this._StartTestPinNumberNumeric.Value;
                        this._Driver.EndTestPinNumber = ( int ) this._EndTestPinNumberNumeric.Value;
                        this._Driver.ActiveLogic = this._ActiveLowLogicRadioButton.Checked ? ActiveLogic.ActiveLow : ActiveLogic.ActiveHigh;
                        this._Driver.BinEndTestOnsetDelay = TimeSpan.FromMilliseconds( ( double ) this._EndTestDelayNumeric.Value );
                        this._Driver.BinMask = this.BinPortMask;
                        if ( this._Driver.TryValidate( this.Device.Gpio.Pins, args ) )
                        {
                            this._Driver.Configure( this.Device.Gpio.Pins );
                        }
                        else
                        {
                        }
                    }
                }
                else
                {
                    args.RegisterFailure( "Device not open" );
                }
            }
            catch ( Exception ex )
            {
                args.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
                if ( args.Failed )
                    _ = this._ErrorProvider.Annunciate( sender, args.Details );
            }
        }

        /// <summary> Updates the sot indicator described by sender. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void UpdateSotIndicator( GpioHandlerDriverBase sender )
        {
            if ( sender is object )
            {
                this._SotLabel.Image = sender.StartTestLogicalValue ? My.Resources.Resources.user_available : My.Resources.Resources.user_invisible;
            }
        }

        /// <summary> Eot label click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EotLabel_Click( object sender, EventArgs e )
        {
            this.UpdateEotIndicator( this._Driver );
        }

        /// <summary> Updates the eot indicator described by sender. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void UpdateEotIndicator( GpioHandlerDriverBase sender )
        {
            if ( sender is object )
            {
                this._EotLabel.Image = sender.EndTestLogicalValue ? My.Resources.Resources.user_available : My.Resources.Resources.user_invisible;
            }
        }

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandlerDriver sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( GpioHandlerDriverBase.State ):
                    {
                        this._HandlerStateLabel.Text = sender.State.Description();
                        switch ( sender.State )
                        {
                            case HandlerState.StartTestReceived:
                                {
                                    break;
                                }

                            default:
                                {
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinValue ):
                    {
                        this._HandlerInterfaceBinValueLabel.Text = sender.BinValueCaption( "..." );
                        break;
                    }

                case nameof( GpioHandlerDriverBase.StartTestLogicalValue ):
                    {
                        this.UpdateSotIndicator( sender );
                        break;
                    }

                case nameof( GpioHandlerDriverBase.EndTestLogicalValue ):
                    {
                        this.UpdateEotIndicator( sender );
                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinMask ):
                    {
                        this.BinPortMask = sender.BinMask;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinEndTestOnsetDelay ):
                    {
                        this._EndTestDelayNumeric.Value = ( decimal ) this._Driver.BinEndTestOnsetDelay.TotalMilliseconds;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.StartTestPinNumber ):
                    {
                        this._StartTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.EndTestPinNumber ):
                    {
                        this._EndTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.ActiveLogic ):
                    {
                        this._ActiveLowLogicRadioButton.Checked = sender.ActiveLogic == ActiveLogic.ActiveLow;
                        break;
                    }
            }
        }

        /// <summary> Driver property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Driver_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.Driver_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as GpioHandlerDriver, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                details = $"Exception handling driver {e?.PropertyName} property change;.  {ex.ToFullBlownString()}";
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, details );
            }
        }


        #endregion

        #region " EMULATOR "

#pragma warning disable IDE1006 // Naming Styles
        private GpioHandlerEmulator __Emulator;

        private GpioHandlerEmulator _Emulator
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__Emulator;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__Emulator != null )
                {
                    this.__Emulator.PropertyChanged -= this.Emulator_PropertyChanged;
                }

                this.__Emulator = value;
                if ( this.__Emulator != null )
                {
                    this.__Emulator.PropertyChanged += this.Emulator_PropertyChanged;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the handler. </summary>
        /// <value> The handler. </value>
        [CLSCompliant( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandlerEmulator Emulator
        {
            get => this._Emulator;

            set {
                this._Emulator = value;
                if ( this.Emulator is object )
                {
                    this._EmulatorStartTestPinNumberNumeric.Value = this.Emulator.StartTestPinNumber;
                    this._EmulatorEndTestPinNumberNumeric.Value = this.Emulator.EndTestPinNumber;
                    this.EmulatorBinMask = this._Driver.BinMask;
                }
            }
        }

        /// <summary> Gets or sets the entered bin mask. </summary>
        /// <value> The entered mask. </value>
        private long EmulatorBinMask
        {
            get => Convert.ToInt32( this._EmulatorBinPortMaskTextBox.Text, 2 );

            set => this._EmulatorBinPortMaskTextBox.Text = Convert.ToString( value, 2 );
        }

        /// <summary> Emulator Bin port mask text box validating. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void EmulatorBinPortMaskTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.EmulatorBinMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                    e.Cancel = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
                e.Cancel = true;
            }
        }

        /// <summary> Applies the button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConfigureEmulatorButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    args.RegisterFailure( "Sender is empty" );
                }
                else if ( this.IsDeviceModalityOpen )
                {
                    if ( !this.IsDeviceModalityOpen )
                    {
                        args.RegisterFailure( "Device not open" );
                    }
                    else if ( this.Device.Gpio is null )
                    {
                        args.RegisterFailure( $"Gpio not supported on {this.Device.Caption()}" );
                    }
                    else if ( this.Device.Gpio.Pins is null )
                    {
                        args.RegisterFailure( $"Gpio not supported on {this.Device.Caption()}" );
                    }
                    else
                    {
                        if ( this._Emulator is null )
                        {
                            this._Emulator = new GpioHandlerEmulator();
                        }

                        this._Emulator.StartTestPinNumber = ( int ) this._EmulatorStartTestPinNumberNumeric.Value;
                        this._Emulator.EndTestPinNumber = ( int ) this._EmulatorEndTestPinNumberNumeric.Value;
                        this._Emulator.ActiveLogic = this._ActiveLowLogicRadioButton.Checked ? ActiveLogic.ActiveLow : ActiveLogic.ActiveHigh;
                        this._Emulator.BinMask = this.EmulatorBinMask;
                        if ( this._Emulator.TryValidate( this.Device.Gpio.Pins, args ) )
                        {
                            this._Emulator.Configure( this.Device.Gpio.Pins );
                        }
                        else
                        {
                        }
                    }
                }
                else
                {
                    args.RegisterFailure( "Device not open" );
                }
            }
            catch ( Exception ex )
            {
                args.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
                if ( args.Failed )
                    _ = this._ErrorProvider.Annunciate( sender, args.Details );
            }
        }

        /// <summary> Handles the emulator property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandlerEmulator sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( GpioHandlerDriverBase.State ):
                    {
                        this._HandlerStateLabel.Text = sender.State.Description();
                        switch ( sender.State )
                        {
                            case HandlerState.StartTestReceived:
                                {
                                    this._EmulatorStateLabel.Text = "e: Start test sent";
                                    break;
                                }

                            case HandlerState.EndTestSent:
                                {
                                    this._EmulatorStateLabel.Text = "e: End test received";
                                    break;
                                }

                            default:
                                {
                                    this._EmulatorStateLabel.Text = sender.State.Description();
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinValue ):
                    {
                        this._EmulatorBinValueLabel.Text = sender.BinValueCaption( "..." );
                        break;
                    }

                case nameof( GpioHandlerDriverBase.StartTestLogicalValue ):
                    {
                        break;
                    }

                case nameof( GpioHandlerDriverBase.EndTestLogicalValue ):
                    {
                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinMask ):
                    {
                        this.EmulatorBinMask = sender.BinMask;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinEndTestOnsetDelay ):
                    {
                        break;
                    }

                case nameof( GpioHandlerDriverBase.StartTestPinNumber ):
                    {
                        this._EmulatorStartTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.EndTestPinNumber ):
                    {
                        this._EmulatorEndTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.ActiveLogic ):
                    {
                        break;
                    }
                    // TO_DO: Must be the same as the driver.
            }
        }

        /// <summary> Emulator property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Emulator_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.Emulator_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as GpioHandlerEmulator, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                details = $"Exception handling {nameof( GpioHandlerDriverEmulator.Emulator )}.{e?.PropertyName} change;. {ex.ToFullBlownString()}";
            }
            finally
            {
                if ( !string.IsNullOrEmpty( details ) )
                    _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, details );
            }
        }


        #endregion

        #region " PLAY "

        /// <summary> Clears the start button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearStartButton_Click( object sender, EventArgs e )
        {
            string details = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( this._Driver is null )
                {
                    details = "Handler interface not set";
                }
                else if ( this._Driver.State == HandlerState.Idle || this._Driver.State == HandlerState.EndTestAcknowledged )
                {
                    this._Driver.EnableStartTest();
                }
                else
                {
                    details = $"Invalid handler state @'{this._Driver.State}'; state should be @'{HandlerState.Idle}'";
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Starts test button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StartTestButton_Click( object sender, EventArgs e )
        {
            string details = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( this._Driver is null )
                {
                    details = "Handler interface not set";
                }
                else if ( this._Emulator is null )
                {
                    details = "Handler emulator not set";
                }
                else if ( this._Driver.State != HandlerState.StartTestEnabled )
                {
                    details = $"Invalid handler state @'{this._Driver.State}'; state should be @'{HandlerState.StartTestEnabled}'";
                }
                else
                {
                    this._Emulator.OutputStartTest();
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Resets the button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ResetButton_Click( object sender, EventArgs e )
        {
            string details = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( this._Driver is null )
                {
                    details = "Handler interface not set";
                }
                else
                {
                    this._Driver.InitializeKnownState();
                    if ( this._Emulator is object )
                    {
                        this._Emulator.Dispose();
                        this._Emulator = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Outputs the end test signals. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">   The sender. </param>
        /// <param name="binValue"> The bin value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OutputEndTest( object sender, int binValue )
        {
            string details = string.Empty;
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( this._Driver is null )
                {
                    details = "Handler interface not set";
                }
                else if ( this._Driver.State == HandlerState.StartTestReceived )
                {
                    this._Driver.OutputEndTest( binValue );
                }
                else
                {
                    details = $"Invalid handler state @'{this._Driver.State}'; state should be @'{HandlerState.Idle}'";
                    this._Driver.InitializeKnownState();
                    if ( this._Emulator is object )
                    {
                        this._Emulator.InitializeKnownState();
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( sender, details );
            }
        }

        /// <summary> Output pass bin button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OutputPassBinButton_Click( object sender, EventArgs e )
        {
            this.OutputEndTest( sender, ( int ) this._PassBinValueNumeric.Value );
        }

        /// <summary> Output fail bin button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OutputFailBinButton_Click( object sender, EventArgs e )
        {
            this.OutputEndTest( sender, ( int ) this._FailBinValueNumeric.Value );
        }

        #endregion

        #region " HANDLER SELECTION "

        /// <summary> Handler combo box click event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandlerComboBox_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            try
            {
                this._ErrorProvider.Clear();
                SupportedHandler handler = ( SupportedHandler ) Conversions.ToInteger( this._HandlerComboBox.ComboBox.SelectedValue );
                switch ( handler )
                {
                    case SupportedHandler.Aetrium:
                        {
                            this.Driver.ApplyHandlerInfo( HandlerInfo.AetriumHandlerInfo );
                            this.Emulator.ApplyHandlerInfo( HandlerInfo.AetriumEmulatorInfo );
                            break;
                        }

                    case SupportedHandler.NoHau:
                        {
                            this.Driver.ApplyHandlerInfo( HandlerInfo.NoHauHandlerInfo );
                            this.Emulator.ApplyHandlerInfo( HandlerInfo.NoHauHandlerInfo );
                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
            }
        }

        #endregion

    }
}
