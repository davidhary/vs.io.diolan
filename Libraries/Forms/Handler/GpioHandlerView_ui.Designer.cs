﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{
    [DesignerGenerated()]
    public partial class GpioHandlerView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _BottomToolStrip = new ToolStrip();
            __TabComboBox = new ToolStripComboBox();
            __TabComboBox.SelectedIndexChanged += new EventHandler(TabComboBox_SelectedIndexChanged);
            __HandlerComboBox = new ToolStripComboBox();
            __HandlerComboBox.Click += new EventHandler(HandlerComboBox_Click);
            _SelectServerButton = new ToolStripSplitButton();
            _ServerNameTextBox = new ToolStripTextBox();
            _DefaultServerMenuItem = new ToolStripMenuItem();
            __ConnectServerButton = new ToolStripButton();
            __ConnectServerButton.Click += new EventHandler(ConnectServerButton_Click);
            __OpenDeviceModalityButton = new ToolStripButton();
            __OpenDeviceModalityButton.Click += new EventHandler(OpenDeviceModalityButton_Click);
            _SelectDeviceSplitButton = new ToolStripSplitButton();
            _DevicesComboBox = new ToolStripComboBox();
            _DeviceInfoTextBox = new ToolStripTextBox();
            __ResetButton = new ToolStripButton();
            __ResetButton.Click += new EventHandler(ResetButton_Click);
            _Tabs = new ExtendedTabControl();
            _DriverTabPage = new TabPage();
            _ConfigureInterfaceTabLayout = new TableLayoutPanel();
            _ConfigureGroupBox = new GroupBox();
            _EndTestDelayNumericLabel = new Label();
            _EndTestDelayNumeric = new NumericUpDown();
            __ConfigureHandlerInterfaceButton = new Button();
            __ConfigureHandlerInterfaceButton.Click += new EventHandler(ConfigureHandlerInterfaceButton_Click);
            _DigitalLogicLabel = new Label();
            _ActiveHighLogicRadioButton = new RadioButton();
            _ActiveLowLogicRadioButton = new RadioButton();
            _BinPortMaskTextBoxLabel = new Label();
            _StartTestPinNumberNumericLabel = new Label();
            __BinPortMaskTextBox = new TextBox();
            __BinPortMaskTextBox.Validating += new System.ComponentModel.CancelEventHandler(BinPortMaskTextBox_Validating);
            _EndTestPinNumberNumericLabel = new Label();
            _EndTestPinNumberNumeric = new NumericUpDown();
            _StartTestPinNumberNumeric = new NumericUpDown();
            _EmulatorTabPage = new TabPage();
            _EmulatorLayout = new TableLayoutPanel();
            _ConfigureEmulatorGroupBox = new GroupBox();
            __ConfigureEmulatorButton = new Button();
            __ConfigureEmulatorButton.Click += new EventHandler(ConfigureEmulatorButton_Click);
            _EmulatorBinPortMaskTextBoxLabel = new Label();
            _EmulatorStartTestPinNumberNumericLabel = new Label();
            __EmulatorBinPortMaskTextBox = new TextBox();
            __EmulatorBinPortMaskTextBox.Validating += new System.ComponentModel.CancelEventHandler(EmulatorBinPortMaskTextBox_Validating);
            _EmulatorEndTestPinNumberNumericLabel = new Label();
            _EmulatorEndTestPinNumberNumeric = new NumericUpDown();
            _EmulatorStartTestPinNumberNumeric = new NumericUpDown();
            _PlayTabPage = new TabPage();
            _PlayTabPageLayout = new TableLayoutPanel();
            _HandlerInterfacePlayGroupBox = new GroupBox();
            _FailBinValueNumeric = new NumericUpDown();
            __OutputFailBinButton = new Button();
            __OutputFailBinButton.Click += new EventHandler(OutputFailBinButton_Click);
            _PassBinValueNumeric = new NumericUpDown();
            __OutputPassBinButton = new Button();
            __OutputPassBinButton.Click += new EventHandler(OutputPassBinButton_Click);
            __ClearStartButton = new Button();
            __ClearStartButton.Click += new EventHandler(ClearStartButton_Click);
            _HandlerEmulatorPlayGroupBox = new GroupBox();
            __StartTestButton = new Button();
            __StartTestButton.Click += new EventHandler(StartTestButton_Click);
            _EventLogTabPage = new TabPage();
            __EventLogTextBox = new TextBox();
            __EventLogTextBox.DoubleClick += new EventHandler(EventLogTextBox_DoubleClick);
            _ErrorProvider = new ErrorProvider(components);
            _ToolTip = new ToolTip(components);
            _ToolStripContainer = new ToolStripContainer();
            _TopToolStrip = new ToolStrip();
            _HandlerStateLabel = new ToolStripLabel();
            _EmulatorStateLabel = new ToolStripLabel();
            _HandlerInterfaceBinValueLabel = new ToolStripLabel();
            _EmulatorBinValueLabel = new ToolStripLabel();
            _SotLabel = new ToolStripLabel();
            __EotLabel = new ToolStripLabel();
            __EotLabel.Click += new EventHandler(EotLabel_Click);
            _BottomToolStrip.SuspendLayout();
            _Tabs.SuspendLayout();
            _DriverTabPage.SuspendLayout();
            _ConfigureInterfaceTabLayout.SuspendLayout();
            _ConfigureGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_EndTestDelayNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_EndTestPinNumberNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_StartTestPinNumberNumeric).BeginInit();
            _EmulatorTabPage.SuspendLayout();
            _EmulatorLayout.SuspendLayout();
            _ConfigureEmulatorGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_EmulatorEndTestPinNumberNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_EmulatorStartTestPinNumberNumeric).BeginInit();
            _PlayTabPage.SuspendLayout();
            _PlayTabPageLayout.SuspendLayout();
            _HandlerInterfacePlayGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_FailBinValueNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_PassBinValueNumeric).BeginInit();
            _HandlerEmulatorPlayGroupBox.SuspendLayout();
            _EventLogTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            _ToolStripContainer.ContentPanel.SuspendLayout();
            _ToolStripContainer.TopToolStripPanel.SuspendLayout();
            _ToolStripContainer.SuspendLayout();
            _TopToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _BottomToolStrip
            // 
            _BottomToolStrip.Dock = DockStyle.None;
            _BottomToolStrip.Items.AddRange(new ToolStripItem[] { __TabComboBox, __HandlerComboBox, _SelectServerButton, __ConnectServerButton, __OpenDeviceModalityButton, _SelectDeviceSplitButton, _DeviceInfoTextBox, __ResetButton });
            _BottomToolStrip.Location = new System.Drawing.Point(0, 0);
            _BottomToolStrip.Name = "_BottomToolStrip";
            _BottomToolStrip.Size = new System.Drawing.Size(417, 29);
            _BottomToolStrip.Stretch = true;
            _BottomToolStrip.TabIndex = 0;
            // 
            // _TabComboBox
            // 
            __TabComboBox.Name = "__TabComboBox";
            __TabComboBox.Size = new System.Drawing.Size(81, 29);
            __TabComboBox.ToolTipText = "Select panel";
            // 
            // _HandlerComboBox
            // 
            __HandlerComboBox.Items.AddRange(new object[] { "Aetrium", "No Hau" });
            __HandlerComboBox.Name = "__HandlerComboBox";
            __HandlerComboBox.Size = new System.Drawing.Size(75, 29);
            __HandlerComboBox.Text = "No Hau";
            __HandlerComboBox.ToolTipText = "Handler";
            // 
            // _SelectServerButton
            // 
            _SelectServerButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectServerButton.DropDownItems.AddRange(new ToolStripItem[] { _ServerNameTextBox, _DefaultServerMenuItem });
            _SelectServerButton.Image = My.Resources.Resources.network_server;
            _SelectServerButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectServerButton.Name = "_SelectServerButton";
            _SelectServerButton.Size = new System.Drawing.Size(38, 26);
            _SelectServerButton.Text = "Select Server";
            // 
            // _ServerNameTextBox
            // 
            _ServerNameTextBox.Name = "_ServerNameTextBox";
            _ServerNameTextBox.Size = new System.Drawing.Size(100, 23);
            _ServerNameTextBox.Text = "localhost:9656";
            // 
            // _DefaultServerMenuItem
            // 
            _DefaultServerMenuItem.Checked = true;
            _DefaultServerMenuItem.CheckOnClick = true;
            _DefaultServerMenuItem.CheckState = CheckState.Checked;
            _DefaultServerMenuItem.Name = "_DefaultServerMenuItem";
            _DefaultServerMenuItem.Size = new System.Drawing.Size(198, 22);
            _DefaultServerMenuItem.Text = "User Default Server:Port";
            // 
            // _ConnectServerButton
            // 
            __ConnectServerButton.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ConnectServerButton.ForeColor = System.Drawing.Color.Red;
            __ConnectServerButton.Image = My.Resources.Resources.WIFI_open_22;
            __ConnectServerButton.ImageScaling = ToolStripItemImageScaling.None;
            __ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ConnectServerButton.Name = "__ConnectServerButton";
            __ConnectServerButton.Size = new System.Drawing.Size(41, 26);
            __ConnectServerButton.Text = "X";
            __ConnectServerButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            __ConnectServerButton.ToolTipText = "Connect or disconnect the server and show count of attached devices.";
            // 
            // _OpenDeviceModalityButton
            // 
            __OpenDeviceModalityButton.Alignment = ToolStripItemAlignment.Right;
            __OpenDeviceModalityButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __OpenDeviceModalityButton.Enabled = false;
            __OpenDeviceModalityButton.Image = My.Resources.Resources.user_offline_2;
            __OpenDeviceModalityButton.ImageScaling = ToolStripItemImageScaling.None;
            __OpenDeviceModalityButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenDeviceModalityButton.Name = "__OpenDeviceModalityButton";
            __OpenDeviceModalityButton.Size = new System.Drawing.Size(26, 26);
            __OpenDeviceModalityButton.Text = "Open";
            __OpenDeviceModalityButton.ToolTipText = "Open or close the device.";
            // 
            // _SelectDeviceSplitButton
            // 
            _SelectDeviceSplitButton.Alignment = ToolStripItemAlignment.Right;
            _SelectDeviceSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectDeviceSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _DevicesComboBox });
            _SelectDeviceSplitButton.Image = My.Resources.Resources.network_server_database;
            _SelectDeviceSplitButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton";
            _SelectDeviceSplitButton.Size = new System.Drawing.Size(38, 26);
            _SelectDeviceSplitButton.Text = "Device";
            _SelectDeviceSplitButton.ToolTipText = "Select Device";
            // 
            // _DevicesComboBox
            // 
            _DevicesComboBox.Name = "_DevicesComboBox";
            _DevicesComboBox.Size = new System.Drawing.Size(121, 23);
            _DevicesComboBox.Text = "DLN-4M.1.1";
            // 
            // _DeviceInfoTextBox
            // 
            _DeviceInfoTextBox.Alignment = ToolStripItemAlignment.Right;
            _DeviceInfoTextBox.Name = "_DeviceInfoTextBox";
            _DeviceInfoTextBox.ReadOnly = true;
            _DeviceInfoTextBox.Size = new System.Drawing.Size(50, 29);
            _DeviceInfoTextBox.Text = "closed";
            _DeviceInfoTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
            // 
            // _ResetButton
            // 
            __ResetButton.Alignment = ToolStripItemAlignment.Right;
            __ResetButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __ResetButton.Image = My.Resources.Resources.system_quick_restart;
            __ResetButton.ImageScaling = ToolStripItemImageScaling.None;
            __ResetButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ResetButton.Name = "__ResetButton";
            __ResetButton.Size = new System.Drawing.Size(26, 26);
            __ResetButton.Text = "Reset Known State";
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_DriverTabPage);
            _Tabs.Controls.Add(_EmulatorTabPage);
            _Tabs.Controls.Add(_PlayTabPage);
            _Tabs.Controls.Add(_EventLogTabPage);
            _Tabs.Dock = DockStyle.Fill;
            _Tabs.HideTabHeaders = true;
            _Tabs.Location = new System.Drawing.Point(0, 0);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new System.Drawing.Size(417, 452);
            _Tabs.TabIndex = 0;
            // 
            // _DriverTabPage
            // 
            _DriverTabPage.Controls.Add(_ConfigureInterfaceTabLayout);
            _DriverTabPage.Location = new System.Drawing.Point(4, 26);
            _DriverTabPage.Name = "_DriverTabPage";
            _DriverTabPage.Padding = new Padding(3);
            _DriverTabPage.Size = new System.Drawing.Size(409, 422);
            _DriverTabPage.TabIndex = 0;
            _DriverTabPage.Text = "Driver";
            _DriverTabPage.UseVisualStyleBackColor = true;
            // 
            // _ConfigureInterfaceTabLayout
            // 
            _ConfigureInterfaceTabLayout.ColumnCount = 3;
            _ConfigureInterfaceTabLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ConfigureInterfaceTabLayout.ColumnStyles.Add(new ColumnStyle());
            _ConfigureInterfaceTabLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ConfigureInterfaceTabLayout.Controls.Add(_ConfigureGroupBox, 1, 1);
            _ConfigureInterfaceTabLayout.Location = new System.Drawing.Point(3, 3);
            _ConfigureInterfaceTabLayout.Name = "_ConfigureInterfaceTabLayout";
            _ConfigureInterfaceTabLayout.RowCount = 3;
            _ConfigureInterfaceTabLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ConfigureInterfaceTabLayout.RowStyles.Add(new RowStyle());
            _ConfigureInterfaceTabLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _ConfigureInterfaceTabLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _ConfigureInterfaceTabLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _ConfigureInterfaceTabLayout.Size = new System.Drawing.Size(403, 416);
            _ConfigureInterfaceTabLayout.TabIndex = 8;
            // 
            // _ConfigureGroupBox
            // 
            _ConfigureGroupBox.Controls.Add(_EndTestDelayNumericLabel);
            _ConfigureGroupBox.Controls.Add(_EndTestDelayNumeric);
            _ConfigureGroupBox.Controls.Add(__ConfigureHandlerInterfaceButton);
            _ConfigureGroupBox.Controls.Add(_DigitalLogicLabel);
            _ConfigureGroupBox.Controls.Add(_ActiveHighLogicRadioButton);
            _ConfigureGroupBox.Controls.Add(_ActiveLowLogicRadioButton);
            _ConfigureGroupBox.Controls.Add(_BinPortMaskTextBoxLabel);
            _ConfigureGroupBox.Controls.Add(_StartTestPinNumberNumericLabel);
            _ConfigureGroupBox.Controls.Add(__BinPortMaskTextBox);
            _ConfigureGroupBox.Controls.Add(_EndTestPinNumberNumericLabel);
            _ConfigureGroupBox.Controls.Add(_EndTestPinNumberNumeric);
            _ConfigureGroupBox.Controls.Add(_StartTestPinNumberNumeric);
            _ConfigureGroupBox.Location = new System.Drawing.Point(57, 85);
            _ConfigureGroupBox.Name = "_ConfigureGroupBox";
            _ConfigureGroupBox.Size = new System.Drawing.Size(289, 246);
            _ConfigureGroupBox.TabIndex = 2;
            _ConfigureGroupBox.TabStop = false;
            _ConfigureGroupBox.Text = "Configure Interface";
            // 
            // _EndTestDelayNumericLabel
            // 
            _EndTestDelayNumericLabel.AutoSize = true;
            _EndTestDelayNumericLabel.Location = new System.Drawing.Point(33, 160);
            _EndTestDelayNumericLabel.Name = "_EndTestDelayNumericLabel";
            _EndTestDelayNumericLabel.Size = new System.Drawing.Size(117, 17);
            _EndTestDelayNumericLabel.TabIndex = 9;
            _EndTestDelayNumericLabel.Text = "End test delay, ms:";
            _ToolTip.SetToolTip(_EndTestDelayNumericLabel, "Time from setting the bin to turning on the end of test signal.");
            // 
            // _EndTestDelayNumeric
            // 
            _EndTestDelayNumeric.Location = new System.Drawing.Point(152, 156);
            _EndTestDelayNumeric.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            _EndTestDelayNumeric.Name = "_EndTestDelayNumeric";
            _EndTestDelayNumeric.Size = new System.Drawing.Size(77, 25);
            _EndTestDelayNumeric.TabIndex = 10;
            _EndTestDelayNumeric.Value = new decimal(new int[] { 4, 0, 0, 0 });
            // 
            // _ConfigureHandlerInterfaceButton
            // 
            __ConfigureHandlerInterfaceButton.Location = new System.Drawing.Point(206, 201);
            __ConfigureHandlerInterfaceButton.Name = "__ConfigureHandlerInterfaceButton";
            __ConfigureHandlerInterfaceButton.Size = new System.Drawing.Size(61, 30);
            __ConfigureHandlerInterfaceButton.TabIndex = 12;
            __ConfigureHandlerInterfaceButton.Text = "Apply";
            _ToolTip.SetToolTip(__ConfigureHandlerInterfaceButton, "Applies the settings to configure the handler interface");
            __ConfigureHandlerInterfaceButton.UseVisualStyleBackColor = true;
            // 
            // _DigitalLogicLabel
            // 
            _DigitalLogicLabel.AutoSize = true;
            _DigitalLogicLabel.Location = new System.Drawing.Point(29, 128);
            _DigitalLogicLabel.Name = "_DigitalLogicLabel";
            _DigitalLogicLabel.Size = new System.Drawing.Size(121, 17);
            _DigitalLogicLabel.TabIndex = 6;
            _DigitalLogicLabel.Text = "Digital Logic: Active";
            _DigitalLogicLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ActiveHighLogicRadioButton
            // 
            _ActiveHighLogicRadioButton.AutoSize = true;
            _ActiveHighLogicRadioButton.Location = new System.Drawing.Point(204, 126);
            _ActiveHighLogicRadioButton.Name = "_ActiveHighLogicRadioButton";
            _ActiveHighLogicRadioButton.Size = new System.Drawing.Size(53, 21);
            _ActiveHighLogicRadioButton.TabIndex = 8;
            _ActiveHighLogicRadioButton.TabStop = true;
            _ActiveHighLogicRadioButton.Text = "High";
            _ToolTip.SetToolTip(_ActiveHighLogicRadioButton, "Active High Logic: Logical 1 = 'High'");
            _ActiveHighLogicRadioButton.UseVisualStyleBackColor = true;
            // 
            // _ActiveLowLogicRadioButton
            // 
            _ActiveLowLogicRadioButton.AutoSize = true;
            _ActiveLowLogicRadioButton.Checked = true;
            _ActiveLowLogicRadioButton.Location = new System.Drawing.Point(155, 126);
            _ActiveLowLogicRadioButton.Name = "_ActiveLowLogicRadioButton";
            _ActiveLowLogicRadioButton.Size = new System.Drawing.Size(49, 21);
            _ActiveLowLogicRadioButton.TabIndex = 7;
            _ActiveLowLogicRadioButton.TabStop = true;
            _ActiveLowLogicRadioButton.Text = "Low";
            _ToolTip.SetToolTip(_ActiveLowLogicRadioButton, "Active low logic: Logic 1 = 'Low'");
            _ActiveLowLogicRadioButton.UseVisualStyleBackColor = true;
            // 
            // _BinPortMaskTextBoxLabel
            // 
            _BinPortMaskTextBoxLabel.AutoSize = true;
            _BinPortMaskTextBoxLabel.Location = new System.Drawing.Point(20, 97);
            _BinPortMaskTextBoxLabel.Name = "_BinPortMaskTextBoxLabel";
            _BinPortMaskTextBoxLabel.Size = new System.Drawing.Size(130, 17);
            _BinPortMaskTextBoxLabel.TabIndex = 4;
            _BinPortMaskTextBoxLabel.Text = "Bin Port Binary Mask:";
            _BinPortMaskTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _StartTestPinNumberNumericLabel
            // 
            _StartTestPinNumberNumericLabel.AutoSize = true;
            _StartTestPinNumberNumericLabel.Location = new System.Drawing.Point(12, 31);
            _StartTestPinNumberNumericLabel.Name = "_StartTestPinNumberNumericLabel";
            _StartTestPinNumberNumericLabel.Size = new System.Drawing.Size(138, 17);
            _StartTestPinNumberNumericLabel.TabIndex = 0;
            _StartTestPinNumberNumericLabel.Text = "Start Test Pin Number:";
            _StartTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _BinPortMaskTextBox
            // 
            __BinPortMaskTextBox.Location = new System.Drawing.Point(152, 93);
            __BinPortMaskTextBox.Name = "__BinPortMaskTextBox";
            __BinPortMaskTextBox.Size = new System.Drawing.Size(115, 25);
            __BinPortMaskTextBox.TabIndex = 5;
            __BinPortMaskTextBox.Text = "1100";
            _ToolTip.SetToolTip(__BinPortMaskTextBox, "Bin port binary mask");
            // 
            // _EndTestPinNumberNumericLabel
            // 
            _EndTestPinNumberNumericLabel.AutoSize = true;
            _EndTestPinNumberNumericLabel.Location = new System.Drawing.Point(17, 64);
            _EndTestPinNumberNumericLabel.Name = "_EndTestPinNumberNumericLabel";
            _EndTestPinNumberNumericLabel.Size = new System.Drawing.Size(133, 17);
            _EndTestPinNumberNumericLabel.TabIndex = 2;
            _EndTestPinNumberNumericLabel.Text = "End Test Pin Number:";
            _EndTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EndTestPinNumberNumeric
            // 
            _EndTestPinNumberNumeric.Location = new System.Drawing.Point(153, 60);
            _EndTestPinNumberNumeric.Maximum = new decimal(new int[] { 47, 0, 0, 0 });
            _EndTestPinNumberNumeric.Name = "_EndTestPinNumberNumeric";
            _EndTestPinNumberNumeric.Size = new System.Drawing.Size(41, 25);
            _EndTestPinNumberNumeric.TabIndex = 3;
            _ToolTip.SetToolTip(_EndTestPinNumberNumeric, "End test pin number");
            _EndTestPinNumberNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _StartTestPinNumberNumeric
            // 
            _StartTestPinNumberNumeric.Location = new System.Drawing.Point(153, 27);
            _StartTestPinNumberNumeric.Maximum = new decimal(new int[] { 47, 0, 0, 0 });
            _StartTestPinNumberNumeric.Name = "_StartTestPinNumberNumeric";
            _StartTestPinNumberNumeric.Size = new System.Drawing.Size(41, 25);
            _StartTestPinNumberNumeric.TabIndex = 1;
            _ToolTip.SetToolTip(_StartTestPinNumberNumeric, "Start test pin number");
            // 
            // _EmulatorTabPage
            // 
            _EmulatorTabPage.Controls.Add(_EmulatorLayout);
            _EmulatorTabPage.Location = new System.Drawing.Point(4, 26);
            _EmulatorTabPage.Name = "_EmulatorTabPage";
            _EmulatorTabPage.Size = new System.Drawing.Size(409, 422);
            _EmulatorTabPage.TabIndex = 3;
            _EmulatorTabPage.Text = "Emulator";
            _EmulatorTabPage.UseVisualStyleBackColor = true;
            // 
            // _EmulatorLayout
            // 
            _EmulatorLayout.ColumnCount = 3;
            _EmulatorLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _EmulatorLayout.ColumnStyles.Add(new ColumnStyle());
            _EmulatorLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _EmulatorLayout.Controls.Add(_ConfigureEmulatorGroupBox, 1, 1);
            _EmulatorLayout.Location = new System.Drawing.Point(3, 3);
            _EmulatorLayout.Name = "_EmulatorLayout";
            _EmulatorLayout.RowCount = 3;
            _EmulatorLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _EmulatorLayout.RowStyles.Add(new RowStyle());
            _EmulatorLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _EmulatorLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _EmulatorLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _EmulatorLayout.Size = new System.Drawing.Size(403, 416);
            _EmulatorLayout.TabIndex = 9;
            // 
            // _ConfigureEmulatorGroupBox
            // 
            _ConfigureEmulatorGroupBox.Controls.Add(__ConfigureEmulatorButton);
            _ConfigureEmulatorGroupBox.Controls.Add(_EmulatorBinPortMaskTextBoxLabel);
            _ConfigureEmulatorGroupBox.Controls.Add(_EmulatorStartTestPinNumberNumericLabel);
            _ConfigureEmulatorGroupBox.Controls.Add(__EmulatorBinPortMaskTextBox);
            _ConfigureEmulatorGroupBox.Controls.Add(_EmulatorEndTestPinNumberNumericLabel);
            _ConfigureEmulatorGroupBox.Controls.Add(_EmulatorEndTestPinNumberNumeric);
            _ConfigureEmulatorGroupBox.Controls.Add(_EmulatorStartTestPinNumberNumeric);
            _ConfigureEmulatorGroupBox.Location = new System.Drawing.Point(58, 122);
            _ConfigureEmulatorGroupBox.Name = "_ConfigureEmulatorGroupBox";
            _ConfigureEmulatorGroupBox.Size = new System.Drawing.Size(286, 171);
            _ConfigureEmulatorGroupBox.TabIndex = 2;
            _ConfigureEmulatorGroupBox.TabStop = false;
            _ConfigureEmulatorGroupBox.Text = "Configure Emulator";
            // 
            // _ConfigureEmulatorButton
            // 
            __ConfigureEmulatorButton.Location = new System.Drawing.Point(208, 131);
            __ConfigureEmulatorButton.Name = "__ConfigureEmulatorButton";
            __ConfigureEmulatorButton.Size = new System.Drawing.Size(61, 30);
            __ConfigureEmulatorButton.TabIndex = 6;
            __ConfigureEmulatorButton.Text = "Apply";
            _ToolTip.SetToolTip(__ConfigureEmulatorButton, "Applies the settings to configure the handler emulator");
            __ConfigureEmulatorButton.UseVisualStyleBackColor = true;
            // 
            // _EmulatorBinPortMaskTextBoxLabel
            // 
            _EmulatorBinPortMaskTextBoxLabel.AutoSize = true;
            _EmulatorBinPortMaskTextBoxLabel.Location = new System.Drawing.Point(22, 97);
            _EmulatorBinPortMaskTextBoxLabel.Name = "_EmulatorBinPortMaskTextBoxLabel";
            _EmulatorBinPortMaskTextBoxLabel.Size = new System.Drawing.Size(130, 17);
            _EmulatorBinPortMaskTextBoxLabel.TabIndex = 4;
            _EmulatorBinPortMaskTextBoxLabel.Text = "Bin Port Binary Mask:";
            _EmulatorBinPortMaskTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EmulatorStartTestPinNumberNumericLabel
            // 
            _EmulatorStartTestPinNumberNumericLabel.AutoSize = true;
            _EmulatorStartTestPinNumberNumericLabel.Location = new System.Drawing.Point(14, 31);
            _EmulatorStartTestPinNumberNumericLabel.Name = "_EmulatorStartTestPinNumberNumericLabel";
            _EmulatorStartTestPinNumberNumericLabel.Size = new System.Drawing.Size(138, 17);
            _EmulatorStartTestPinNumberNumericLabel.TabIndex = 0;
            _EmulatorStartTestPinNumberNumericLabel.Text = "Start Test Pin Number:";
            _EmulatorStartTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EmulatorBinPortMaskTextBox
            // 
            __EmulatorBinPortMaskTextBox.Location = new System.Drawing.Point(154, 93);
            __EmulatorBinPortMaskTextBox.Name = "__EmulatorBinPortMaskTextBox";
            __EmulatorBinPortMaskTextBox.Size = new System.Drawing.Size(115, 25);
            __EmulatorBinPortMaskTextBox.TabIndex = 5;
            __EmulatorBinPortMaskTextBox.Text = "11000000";
            _ToolTip.SetToolTip(__EmulatorBinPortMaskTextBox, "Emulator bin port binary mask");
            // 
            // _EmulatorEndTestPinNumberNumericLabel
            // 
            _EmulatorEndTestPinNumberNumericLabel.AutoSize = true;
            _EmulatorEndTestPinNumberNumericLabel.Location = new System.Drawing.Point(19, 64);
            _EmulatorEndTestPinNumberNumericLabel.Name = "_EmulatorEndTestPinNumberNumericLabel";
            _EmulatorEndTestPinNumberNumericLabel.Size = new System.Drawing.Size(133, 17);
            _EmulatorEndTestPinNumberNumericLabel.TabIndex = 2;
            _EmulatorEndTestPinNumberNumericLabel.Text = "End Test Pin Number:";
            _EmulatorEndTestPinNumberNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EmulatorEndTestPinNumberNumeric
            // 
            _EmulatorEndTestPinNumberNumeric.Location = new System.Drawing.Point(155, 60);
            _EmulatorEndTestPinNumberNumeric.Maximum = new decimal(new int[] { 47, 0, 0, 0 });
            _EmulatorEndTestPinNumberNumeric.Name = "_EmulatorEndTestPinNumberNumeric";
            _EmulatorEndTestPinNumberNumeric.Size = new System.Drawing.Size(41, 25);
            _EmulatorEndTestPinNumberNumeric.TabIndex = 3;
            _ToolTip.SetToolTip(_EmulatorEndTestPinNumberNumeric, "Emulator end test pin number");
            _EmulatorEndTestPinNumberNumeric.Value = new decimal(new int[] { 5, 0, 0, 0 });
            // 
            // _EmulatorStartTestPinNumberNumeric
            // 
            _EmulatorStartTestPinNumberNumeric.Location = new System.Drawing.Point(155, 27);
            _EmulatorStartTestPinNumberNumeric.Maximum = new decimal(new int[] { 47, 0, 0, 0 });
            _EmulatorStartTestPinNumberNumeric.Name = "_EmulatorStartTestPinNumberNumeric";
            _EmulatorStartTestPinNumberNumeric.Size = new System.Drawing.Size(41, 25);
            _EmulatorStartTestPinNumberNumeric.TabIndex = 1;
            _ToolTip.SetToolTip(_EmulatorStartTestPinNumberNumeric, "Emulator start test pin number");
            _EmulatorStartTestPinNumberNumeric.Value = new decimal(new int[] { 4, 0, 0, 0 });
            // 
            // _PlayTabPage
            // 
            _PlayTabPage.Controls.Add(_PlayTabPageLayout);
            _PlayTabPage.Location = new System.Drawing.Point(4, 26);
            _PlayTabPage.Name = "_PlayTabPage";
            _PlayTabPage.Size = new System.Drawing.Size(409, 422);
            _PlayTabPage.TabIndex = 1;
            _PlayTabPage.Text = "Play";
            _PlayTabPage.UseVisualStyleBackColor = true;
            // 
            // _PlayTabPageLayout
            // 
            _PlayTabPageLayout.ColumnCount = 3;
            _PlayTabPageLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _PlayTabPageLayout.ColumnStyles.Add(new ColumnStyle());
            _PlayTabPageLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _PlayTabPageLayout.Controls.Add(_HandlerInterfacePlayGroupBox, 1, 1);
            _PlayTabPageLayout.Controls.Add(_HandlerEmulatorPlayGroupBox, 1, 2);
            _PlayTabPageLayout.Dock = DockStyle.Fill;
            _PlayTabPageLayout.Location = new System.Drawing.Point(0, 0);
            _PlayTabPageLayout.Name = "_PlayTabPageLayout";
            _PlayTabPageLayout.RowCount = 4;
            _PlayTabPageLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _PlayTabPageLayout.RowStyles.Add(new RowStyle());
            _PlayTabPageLayout.RowStyles.Add(new RowStyle());
            _PlayTabPageLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50.0f));
            _PlayTabPageLayout.Size = new System.Drawing.Size(409, 422);
            _PlayTabPageLayout.TabIndex = 0;
            // 
            // _HandlerInterfacePlayGroupBox
            // 
            _HandlerInterfacePlayGroupBox.Controls.Add(_FailBinValueNumeric);
            _HandlerInterfacePlayGroupBox.Controls.Add(__OutputFailBinButton);
            _HandlerInterfacePlayGroupBox.Controls.Add(_PassBinValueNumeric);
            _HandlerInterfacePlayGroupBox.Controls.Add(__OutputPassBinButton);
            _HandlerInterfacePlayGroupBox.Controls.Add(__ClearStartButton);
            _HandlerInterfacePlayGroupBox.Location = new System.Drawing.Point(108, 95);
            _HandlerInterfacePlayGroupBox.Name = "_HandlerInterfacePlayGroupBox";
            _HandlerInterfacePlayGroupBox.Size = new System.Drawing.Size(192, 147);
            _HandlerInterfacePlayGroupBox.TabIndex = 0;
            _HandlerInterfacePlayGroupBox.TabStop = false;
            _HandlerInterfacePlayGroupBox.Text = "Handler Interface";
            // 
            // _FailBinValueNumeric
            // 
            _FailBinValueNumeric.Location = new System.Drawing.Point(135, 110);
            _FailBinValueNumeric.Name = "_FailBinValueNumeric";
            _FailBinValueNumeric.Size = new System.Drawing.Size(40, 25);
            _FailBinValueNumeric.TabIndex = 4;
            _ToolTip.SetToolTip(_FailBinValueNumeric, "Failed bin value");
            _FailBinValueNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _OutputFailBinButton
            // 
            __OutputFailBinButton.Location = new System.Drawing.Point(18, 106);
            __OutputFailBinButton.Name = "__OutputFailBinButton";
            __OutputFailBinButton.Size = new System.Drawing.Size(111, 33);
            __OutputFailBinButton.TabIndex = 3;
            __OutputFailBinButton.Text = "Output Fail Bin";
            _ToolTip.SetToolTip(__OutputFailBinButton, "Sends the failed end of test sequence");
            __OutputFailBinButton.UseVisualStyleBackColor = true;
            // 
            // _PassBinValueNumeric
            // 
            _PassBinValueNumeric.Location = new System.Drawing.Point(135, 70);
            _PassBinValueNumeric.Name = "_PassBinValueNumeric";
            _PassBinValueNumeric.Size = new System.Drawing.Size(40, 25);
            _PassBinValueNumeric.TabIndex = 2;
            _ToolTip.SetToolTip(_PassBinValueNumeric, "Pass bin value");
            _PassBinValueNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _OutputPassBinButton
            // 
            __OutputPassBinButton.Location = new System.Drawing.Point(18, 66);
            __OutputPassBinButton.Name = "__OutputPassBinButton";
            __OutputPassBinButton.Size = new System.Drawing.Size(111, 33);
            __OutputPassBinButton.TabIndex = 1;
            __OutputPassBinButton.Text = "Output Pass Bin";
            _ToolTip.SetToolTip(__OutputPassBinButton, "Sends the passed end of test sequence");
            __OutputPassBinButton.UseVisualStyleBackColor = true;
            // 
            // _ClearStartButton
            // 
            __ClearStartButton.Location = new System.Drawing.Point(17, 25);
            __ClearStartButton.Name = "__ClearStartButton";
            __ClearStartButton.Size = new System.Drawing.Size(156, 34);
            __ClearStartButton.TabIndex = 0;
            __ClearStartButton.Text = "Clear to Start";
            _ToolTip.SetToolTip(__ClearStartButton, "Allows handler to send start test.");
            __ClearStartButton.UseVisualStyleBackColor = true;
            // 
            // _HandlerEmulatorPlayGroupBox
            // 
            _HandlerEmulatorPlayGroupBox.Controls.Add(__StartTestButton);
            _HandlerEmulatorPlayGroupBox.Location = new System.Drawing.Point(108, 248);
            _HandlerEmulatorPlayGroupBox.Name = "_HandlerEmulatorPlayGroupBox";
            _HandlerEmulatorPlayGroupBox.Size = new System.Drawing.Size(192, 78);
            _HandlerEmulatorPlayGroupBox.TabIndex = 1;
            _HandlerEmulatorPlayGroupBox.TabStop = false;
            _HandlerEmulatorPlayGroupBox.Text = "Handler Emulator";
            // 
            // _StartTestButton
            // 
            __StartTestButton.Location = new System.Drawing.Point(18, 29);
            __StartTestButton.Name = "__StartTestButton";
            __StartTestButton.Size = new System.Drawing.Size(156, 34);
            __StartTestButton.TabIndex = 0;
            __StartTestButton.Text = "Start Test";
            _ToolTip.SetToolTip(__StartTestButton, "Sends start test from handler to interface");
            __StartTestButton.UseVisualStyleBackColor = true;
            // 
            // _EventLogTabPage
            // 
            _EventLogTabPage.Controls.Add(__EventLogTextBox);
            _EventLogTabPage.Location = new System.Drawing.Point(4, 26);
            _EventLogTabPage.Name = "_EventLogTabPage";
            _EventLogTabPage.Size = new System.Drawing.Size(409, 422);
            _EventLogTabPage.TabIndex = 2;
            _EventLogTabPage.Text = "Events";
            _EventLogTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTextBox
            // 
            __EventLogTextBox.Dock = DockStyle.Fill;
            __EventLogTextBox.Location = new System.Drawing.Point(0, 0);
            __EventLogTextBox.Multiline = true;
            __EventLogTextBox.Name = "__EventLogTextBox";
            __EventLogTextBox.ScrollBars = ScrollBars.Vertical;
            __EventLogTextBox.Size = new System.Drawing.Size(409, 422);
            __EventLogTextBox.TabIndex = 1;
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            _ToolStripContainer.BottomToolStripPanel.Controls.Add(_BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            _ToolStripContainer.ContentPanel.Controls.Add(_Tabs);
            _ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(417, 452);
            _ToolStripContainer.Dock = DockStyle.Fill;
            _ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            _ToolStripContainer.Name = "_ToolStripContainer";
            _ToolStripContainer.Size = new System.Drawing.Size(417, 506);
            _ToolStripContainer.TabIndex = 0;
            _ToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _ToolStripContainer.TopToolStripPanel
            // 
            _ToolStripContainer.TopToolStripPanel.Controls.Add(_TopToolStrip);
            // 
            // _TopToolStrip
            // 
            _TopToolStrip.AutoSize = false;
            _TopToolStrip.Dock = DockStyle.None;
            _TopToolStrip.Items.AddRange(new ToolStripItem[] { _HandlerStateLabel, _EmulatorStateLabel, _HandlerInterfaceBinValueLabel, _EmulatorBinValueLabel, _SotLabel, __EotLabel });
            _TopToolStrip.Location = new System.Drawing.Point(0, 0);
            _TopToolStrip.Name = "_TopToolStrip";
            _TopToolStrip.RenderMode = ToolStripRenderMode.System;
            _TopToolStrip.Size = new System.Drawing.Size(417, 25);
            _TopToolStrip.Stretch = true;
            _TopToolStrip.TabIndex = 0;
            // 
            // _HandlerStateLabel
            // 
            _HandlerStateLabel.BackColor = System.Drawing.Color.Black;
            _HandlerStateLabel.BackgroundImageLayout = ImageLayout.None;
            _HandlerStateLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _HandlerStateLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _HandlerStateLabel.ForeColor = System.Drawing.Color.Aqua;
            _HandlerStateLabel.Name = "_HandlerStateLabel";
            _HandlerStateLabel.Size = new System.Drawing.Size(89, 22);
            _HandlerStateLabel.Text = "interface state";
            _HandlerStateLabel.ToolTipText = "Handler interface state";
            // 
            // _EmulatorStateLabel
            // 
            _EmulatorStateLabel.Alignment = ToolStripItemAlignment.Right;
            _EmulatorStateLabel.BackColor = System.Drawing.Color.Black;
            _EmulatorStateLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _EmulatorStateLabel.ForeColor = System.Drawing.Color.Aqua;
            _EmulatorStateLabel.Name = "_EmulatorStateLabel";
            _EmulatorStateLabel.Size = new System.Drawing.Size(89, 22);
            _EmulatorStateLabel.Text = "emulator state";
            _EmulatorStateLabel.ToolTipText = "Handler Emulator State";
            // 
            // _HandlerInterfaceBinValueLabel
            // 
            _HandlerInterfaceBinValueLabel.BackColor = System.Drawing.Color.Black;
            _HandlerInterfaceBinValueLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _HandlerInterfaceBinValueLabel.ForeColor = System.Drawing.Color.SpringGreen;
            _HandlerInterfaceBinValueLabel.Margin = new Padding(1, 1, 0, 2);
            _HandlerInterfaceBinValueLabel.Name = "_HandlerInterfaceBinValueLabel";
            _HandlerInterfaceBinValueLabel.Size = new System.Drawing.Size(24, 22);
            _HandlerInterfaceBinValueLabel.Text = "bin";
            _HandlerInterfaceBinValueLabel.ToolTipText = "Bin value";
            // 
            // _EmulatorBinValueLabel
            // 
            _EmulatorBinValueLabel.Alignment = ToolStripItemAlignment.Right;
            _EmulatorBinValueLabel.BackColor = System.Drawing.Color.Black;
            _EmulatorBinValueLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _EmulatorBinValueLabel.ForeColor = System.Drawing.Color.SpringGreen;
            _EmulatorBinValueLabel.Margin = new Padding(0, 1, 1, 2);
            _EmulatorBinValueLabel.Name = "_EmulatorBinValueLabel";
            _EmulatorBinValueLabel.Size = new System.Drawing.Size(24, 22);
            _EmulatorBinValueLabel.Text = "bin";
            _EmulatorBinValueLabel.ToolTipText = "Emulate bin value";
            // 
            // _SotLabel
            // 
            _SotLabel.BackColor = System.Drawing.Color.Transparent;
            _SotLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            _SotLabel.Image = My.Resources.Resources.user_invisible;
            _SotLabel.ImageScaling = ToolStripItemImageScaling.None;
            _SotLabel.Name = "_SotLabel";
            _SotLabel.Size = new System.Drawing.Size(28, 22);
            _SotLabel.Text = "SOT";
            _SotLabel.TextImageRelation = TextImageRelation.Overlay;
            _SotLabel.ToolTipText = "Start test pin logical status";
            // 
            // _EotLabel
            // 
            __EotLabel.BackColor = System.Drawing.Color.Transparent;
            __EotLabel.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            __EotLabel.Image = My.Resources.Resources.user_invisible;
            __EotLabel.ImageScaling = ToolStripItemImageScaling.None;
            __EotLabel.Name = "__EotLabel";
            __EotLabel.Size = new System.Drawing.Size(28, 22);
            __EotLabel.Text = "EOT";
            __EotLabel.TextImageRelation = TextImageRelation.Overlay;
            // 
            // GpioHandlerPanel
            // 
            Controls.Add(_ToolStripContainer);
            Name = "GpioHandlerPanel";
            Size = new System.Drawing.Size(417, 506);
            _BottomToolStrip.ResumeLayout(false);
            _BottomToolStrip.PerformLayout();
            _Tabs.ResumeLayout(false);
            _DriverTabPage.ResumeLayout(false);
            _ConfigureInterfaceTabLayout.ResumeLayout(false);
            _ConfigureGroupBox.ResumeLayout(false);
            _ConfigureGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_EndTestDelayNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_EndTestPinNumberNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_StartTestPinNumberNumeric).EndInit();
            _EmulatorTabPage.ResumeLayout(false);
            _EmulatorLayout.ResumeLayout(false);
            _ConfigureEmulatorGroupBox.ResumeLayout(false);
            _ConfigureEmulatorGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_EmulatorEndTestPinNumberNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_EmulatorStartTestPinNumberNumeric).EndInit();
            _PlayTabPage.ResumeLayout(false);
            _PlayTabPageLayout.ResumeLayout(false);
            _HandlerInterfacePlayGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)_FailBinValueNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_PassBinValueNumeric).EndInit();
            _HandlerEmulatorPlayGroupBox.ResumeLayout(false);
            _EventLogTabPage.ResumeLayout(false);
            _EventLogTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.BottomToolStripPanel.PerformLayout();
            _ToolStripContainer.ContentPanel.ResumeLayout(false);
            _ToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.ResumeLayout(false);
            _ToolStripContainer.PerformLayout();
            _TopToolStrip.ResumeLayout(false);
            _TopToolStrip.PerformLayout();
            ResumeLayout(false);
        }

        private ToolStrip _BottomToolStrip;
        private ToolStripTextBox _DeviceInfoTextBox;
        private ErrorProvider _ErrorProvider;
        private ToolTip _ToolTip;
        private ToolStripButton __OpenDeviceModalityButton;

        private ToolStripButton _OpenDeviceModalityButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDeviceModalityButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDeviceModalityButton != null)
                {
                    __OpenDeviceModalityButton.Click -= OpenDeviceModalityButton_Click;
                }

                __OpenDeviceModalityButton = value;
                if (__OpenDeviceModalityButton != null)
                {
                    __OpenDeviceModalityButton.Click += OpenDeviceModalityButton_Click;
                }
            }
        }

        private TabPage _DriverTabPage;
        private ToolStripContainer _ToolStripContainer;
        private ExtendedTabControl _Tabs;
        private ToolStripComboBox _DevicesComboBox;
        private ToolStripSplitButton _SelectServerButton;
        private ToolStripTextBox _ServerNameTextBox;
        private ToolStripMenuItem _DefaultServerMenuItem;
        private ToolStripButton __ConnectServerButton;

        private ToolStripButton _ConnectServerButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConnectServerButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConnectServerButton != null)
                {
                    __ConnectServerButton.Click -= ConnectServerButton_Click;
                }

                __ConnectServerButton = value;
                if (__ConnectServerButton != null)
                {
                    __ConnectServerButton.Click += ConnectServerButton_Click;
                }
            }
        }

        private ToolStripSplitButton _SelectDeviceSplitButton;
        private TableLayoutPanel _ConfigureInterfaceTabLayout;
        private GroupBox _ConfigureGroupBox;
        private Label _DigitalLogicLabel;
        private RadioButton _ActiveHighLogicRadioButton;
        private RadioButton _ActiveLowLogicRadioButton;
        private Label _BinPortMaskTextBoxLabel;
        private Label _StartTestPinNumberNumericLabel;
        private TextBox __BinPortMaskTextBox;

        private TextBox _BinPortMaskTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BinPortMaskTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BinPortMaskTextBox != null)
                {
                    __BinPortMaskTextBox.Validating -= BinPortMaskTextBox_Validating;
                }

                __BinPortMaskTextBox = value;
                if (__BinPortMaskTextBox != null)
                {
                    __BinPortMaskTextBox.Validating += BinPortMaskTextBox_Validating;
                }
            }
        }

        private Label _EndTestPinNumberNumericLabel;
        private NumericUpDown _EndTestPinNumberNumeric;
        private NumericUpDown _StartTestPinNumberNumeric;
        private TabPage _PlayTabPage;
        private TableLayoutPanel _PlayTabPageLayout;
        private TabPage _EventLogTabPage;
        private TextBox __EventLogTextBox;

        private TextBox _EventLogTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EventLogTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EventLogTextBox != null)
                {
                    __EventLogTextBox.DoubleClick -= EventLogTextBox_DoubleClick;
                }

                __EventLogTextBox = value;
                if (__EventLogTextBox != null)
                {
                    __EventLogTextBox.DoubleClick += EventLogTextBox_DoubleClick;
                }
            }
        }

        private ToolStripComboBox __TabComboBox;

        private ToolStripComboBox _TabComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TabComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TabComboBox != null)
                {
                    __TabComboBox.SelectedIndexChanged -= TabComboBox_SelectedIndexChanged;
                }

                __TabComboBox = value;
                if (__TabComboBox != null)
                {
                    __TabComboBox.SelectedIndexChanged += TabComboBox_SelectedIndexChanged;
                }
            }
        }

        private Button __ConfigureHandlerInterfaceButton;

        private Button _ConfigureHandlerInterfaceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConfigureHandlerInterfaceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConfigureHandlerInterfaceButton != null)
                {
                    __ConfigureHandlerInterfaceButton.Click -= ConfigureHandlerInterfaceButton_Click;
                }

                __ConfigureHandlerInterfaceButton = value;
                if (__ConfigureHandlerInterfaceButton != null)
                {
                    __ConfigureHandlerInterfaceButton.Click += ConfigureHandlerInterfaceButton_Click;
                }
            }
        }

        private TabPage _EmulatorTabPage;
        private ToolStrip _TopToolStrip;
        private TableLayoutPanel _EmulatorLayout;
        private GroupBox _ConfigureEmulatorGroupBox;
        private Button __ConfigureEmulatorButton;

        private Button _ConfigureEmulatorButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConfigureEmulatorButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConfigureEmulatorButton != null)
                {
                    __ConfigureEmulatorButton.Click -= ConfigureEmulatorButton_Click;
                }

                __ConfigureEmulatorButton = value;
                if (__ConfigureEmulatorButton != null)
                {
                    __ConfigureEmulatorButton.Click += ConfigureEmulatorButton_Click;
                }
            }
        }

        private Label _EmulatorBinPortMaskTextBoxLabel;
        private Label _EmulatorStartTestPinNumberNumericLabel;
        private TextBox __EmulatorBinPortMaskTextBox;

        private TextBox _EmulatorBinPortMaskTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EmulatorBinPortMaskTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EmulatorBinPortMaskTextBox != null)
                {
                    __EmulatorBinPortMaskTextBox.Validating -= EmulatorBinPortMaskTextBox_Validating;
                }

                __EmulatorBinPortMaskTextBox = value;
                if (__EmulatorBinPortMaskTextBox != null)
                {
                    __EmulatorBinPortMaskTextBox.Validating += EmulatorBinPortMaskTextBox_Validating;
                }
            }
        }

        private Label _EmulatorEndTestPinNumberNumericLabel;
        private NumericUpDown _EmulatorEndTestPinNumberNumeric;
        private NumericUpDown _EmulatorStartTestPinNumberNumeric;
        private GroupBox _HandlerInterfacePlayGroupBox;
        private GroupBox _HandlerEmulatorPlayGroupBox;
        private Button __ClearStartButton;

        private Button _ClearStartButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearStartButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearStartButton != null)
                {
                    __ClearStartButton.Click -= ClearStartButton_Click;
                }

                __ClearStartButton = value;
                if (__ClearStartButton != null)
                {
                    __ClearStartButton.Click += ClearStartButton_Click;
                }
            }
        }

        private Button __StartTestButton;

        private Button _StartTestButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartTestButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartTestButton != null)
                {
                    __StartTestButton.Click -= StartTestButton_Click;
                }

                __StartTestButton = value;
                if (__StartTestButton != null)
                {
                    __StartTestButton.Click += StartTestButton_Click;
                }
            }
        }

        private ToolStripButton __ResetButton;

        private ToolStripButton _ResetButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ResetButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ResetButton != null)
                {
                    __ResetButton.Click -= ResetButton_Click;
                }

                __ResetButton = value;
                if (__ResetButton != null)
                {
                    __ResetButton.Click += ResetButton_Click;
                }
            }
        }

        private NumericUpDown _FailBinValueNumeric;
        private Button __OutputFailBinButton;

        private Button _OutputFailBinButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OutputFailBinButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OutputFailBinButton != null)
                {
                    __OutputFailBinButton.Click -= OutputFailBinButton_Click;
                }

                __OutputFailBinButton = value;
                if (__OutputFailBinButton != null)
                {
                    __OutputFailBinButton.Click += OutputFailBinButton_Click;
                }
            }
        }

        private NumericUpDown _PassBinValueNumeric;
        private Button __OutputPassBinButton;

        private Button _OutputPassBinButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OutputPassBinButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OutputPassBinButton != null)
                {
                    __OutputPassBinButton.Click -= OutputPassBinButton_Click;
                }

                __OutputPassBinButton = value;
                if (__OutputPassBinButton != null)
                {
                    __OutputPassBinButton.Click += OutputPassBinButton_Click;
                }
            }
        }

        private Label _EndTestDelayNumericLabel;
        private NumericUpDown _EndTestDelayNumeric;
        private ToolStripLabel _SotLabel;
        private ToolStripLabel __EotLabel;

        private ToolStripLabel _EotLabel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EotLabel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EotLabel != null)
                {
                    __EotLabel.Click -= EotLabel_Click;
                }

                __EotLabel = value;
                if (__EotLabel != null)
                {
                    __EotLabel.Click += EotLabel_Click;
                }
            }
        }

        private ToolStripLabel _HandlerStateLabel;
        private ToolStripLabel _EmulatorBinValueLabel;
        private ToolStripLabel _EmulatorStateLabel;
        private ToolStripLabel _HandlerInterfaceBinValueLabel;
        private ToolStripComboBox __HandlerComboBox;

        private ToolStripComboBox _HandlerComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __HandlerComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__HandlerComboBox != null)
                {
                    __HandlerComboBox.Click -= HandlerComboBox_Click;
                }

                __HandlerComboBox = value;
                if (__HandlerComboBox != null)
                {
                    __HandlerComboBox.Click += HandlerComboBox_Click;
                }
            }
        }
    }
}