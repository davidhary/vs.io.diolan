using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.ExceptionExtensions;
using isr.Diolan.Gpio;
using isr.Diolan.SubsystemExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{

    /// <summary> User interface for managing a GPIO Handler driver and emulator. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    public partial class GpioHandlerDriverView
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes the modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void InitializeModality()
        {
            this._DriverEmulator = GpioHandlerDriverEmulator.Get();

            // enabled once the driver emulator is assigned.
            this._HandlerComboBox.ComboBox.Enabled = false;
            this._HandlerComboBox.ComboBox.DataSource = null;
            this._HandlerComboBox.ComboBox.Items.Clear();
            this._HandlerComboBox.ComboBox.DataSource = typeof( SupportedHandler ).ValueDescriptionPairs().ToList();
            this._HandlerComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._HandlerComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._EndTestModeComboBox.DataSource = null;
            this._EndTestModeComboBox.Items.Clear();
            this._EndTestModeComboBox.DataSource = typeof( EndTestMode ).ValueDescriptionPairs().ToList();
            this._EndTestModeComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._EndTestModeComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._EndTestModeComboBox.Enabled = true;
            this._HandlerComboBox.Enabled = this._DriverEmulator?.Driver is object;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this._DriverEmulator is object )
                    {
                        this._DriverEmulator.Dispose();
                        this._DriverEmulator = null;
                    }

                    if ( GpioHandlerDriverEmulator.Instantiated )
                        GpioHandlerDriverEmulator.DisposeInstance();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " GPIO HANDLER DRIVER AND EMULATOR "

#pragma warning disable IDE1006 // Naming Styles
        private GpioHandlerDriverEmulator __DriverEmulator;

        private GpioHandlerDriverEmulator _DriverEmulator
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__DriverEmulator;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__DriverEmulator != null )
                {
                    this.__DriverEmulator.PropertyChanged -= this.DriverEmulator_PropertyChanged;
                }

                this.__DriverEmulator = value;
                if ( this.__DriverEmulator != null )
                {
                    this.__DriverEmulator.PropertyChanged += this.DriverEmulator_PropertyChanged;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the Gpio handler driver and emulator. </summary>
        /// <value> The Gpio test handler. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public GpioHandlerDriverEmulator DriverEmulator
        {
            get => this._DriverEmulator;

            set {
                this._DriverEmulator = value;
                if ( this._DriverEmulator is object )
                {
                    this.UpdateGui( this._DriverEmulator.Driver );
                    this.UpdateGui( this._DriverEmulator.Emulator );
                }

                this._HandlerComboBox.Enabled = this._DriverEmulator?.Driver is object;
            }
        }

        /// <summary> Handles the server connector, driver and emulator property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandlerDriverEmulator sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            if ( propertyName.StartsWith( sender.ServerConnectorSource, StringComparison.OrdinalIgnoreCase ) )
            {
                this.OnPropertyChanged( sender.ServerConnector, propertyName.Remove( 0, sender.ServerConnectorSource.Length ) );
            }
            else if ( propertyName.StartsWith( sender.DriverSource, StringComparison.OrdinalIgnoreCase ) )
            {
                this.OnPropertyChanged( sender.Driver, propertyName.Remove( 0, sender.DriverSource.Length ) );
            }
            else if ( propertyName.StartsWith( sender.EmulatorSource, StringComparison.OrdinalIgnoreCase ) )
            {
                this.OnPropertyChanged( sender.Emulator, propertyName.Remove( 0, sender.EmulatorSource.Length ) );
            }
            else
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( GpioHandlerDriverEmulator.DeviceCaption ):
                        {
                            this._DeviceInfoTextBox.Text = sender.DeviceCaption;
                            break;
                        }

                    case nameof( GpioHandlerDriverEmulator.IsDeviceModalityOpen ):
                        {
                            if ( sender.IsDeviceModalityOpen )
                            {
                                this._OpenDeviceModalityButton.Image = My.Resources.Resources.user_online_2;
                                this._OpenDeviceModalityButton.Text = "Close";
                            }
                            else
                            {
                                this._OpenDeviceModalityButton.Image = My.Resources.Resources.user_offline_2;
                                this._OpenDeviceModalityButton.Text = "Open";
                            }

                            this._HandlerComboBox.Enabled = sender.Driver is object;
                            if ( sender.Driver is object )
                                this.OnHandlerSelected( this._HandlerComboBox );
                            sender.Driver?.PublishAll();
                            sender.Emulator?.PublishAll();
                            break;
                        }

                    case nameof( GpioHandlerDriverEmulator.EventCaption ):
                        {
                            this._EventLogTextBox.AppendText( sender.EventCaption );
                            break;
                        }
                }
            }
        }

        /// <summary> Driver emulator property changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DriverEmulator_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string details = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.DriverEmulator_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as GpioHandlerDriverEmulator, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                details = $"Exception handling driver {e?.PropertyName} property change;.  {ex.ToFullBlownString()}";
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, details );
            }
        }


        #endregion

        #region " DRIVER "

        /// <summary> Updates the graphical user interface described by driver. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="driver"> The driver. </param>
        private void UpdateGui( GpioHandlerDriver driver )
        {
            if ( driver is object )
            {
                this._StartTestPinNumberNumeric.Value = driver.StartTestPinNumber;
                this._EndTestPinNumberNumeric.Value = driver.EndTestPinNumber;
                this._ActiveLowLogicRadioButton.Checked = driver.ActiveLogic == ActiveLogic.ActiveLow;
                this.BinPortMask = driver.BinMask;
                this.EndTestMode = driver.EndTestMode;
            }
        }

        /// <summary> Gets or sets the handler mask. </summary>
        /// <value> The entered mask. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private long BinPortMask
        {
            get => Convert.ToInt32( this._BinPortMaskTextBox.Text, 2 );

            set => this._BinPortMaskTextBox.Text = Convert.ToString( value, 2 );
        }

        /// <summary> Gets or sets the end test mode. </summary>
        /// <value> The end test mode. </value>
        private EndTestMode EndTestMode
        {
            get => ( EndTestMode ) Conversions.ToInteger( this._EndTestModeComboBox.SelectedValue );

            set => this._EndTestModeComboBox.SelectedValue = value;
        }

        /// <summary> Bin port mask text box validating. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void BinPortMaskTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.BinPortMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                    e.Cancel = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
                e.Cancel = true;
            }
        }

        /// <summary> Ends test mode combo box selected index changed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void EndTestModeComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.BinPortMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
        }

        /// <summary> Handles the Configure Emulator button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConfigureDriverButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            try
            {
                this._ErrorProvider.Clear();
                if ( !this.DriverEmulator.IsDeviceModalityOpen )
                {
                    args.RegisterFailure( "Device not open" );
                }
                else if ( this.DriverEmulator.DeviceConnector.Device.Gpio is null )
                {
                    args.RegisterFailure( $"Gpio not supported on {this.DriverEmulator.DeviceConnector.Device.Caption()}" );
                }
                else if ( this.DriverEmulator.DeviceConnector.Device.Gpio.Pins is null )
                {
                    args.RegisterFailure( $"Gpio not supported on {this.DriverEmulator.DeviceConnector.Device.Caption()}" );
                }
                else
                {
                    this.DriverEmulator.Driver.StartTestPinNumber = ( int ) this._StartTestPinNumberNumeric.Value;
                    this.DriverEmulator.Driver.EndTestPinNumber = ( int ) this._EndTestPinNumberNumeric.Value;
                    this.DriverEmulator.Driver.ActiveLogic = this._ActiveLowLogicRadioButton.Checked ? ActiveLogic.ActiveLow : ActiveLogic.ActiveHigh;
                    this.DriverEmulator.Driver.BinEndTestOnsetDelay = TimeSpan.FromMilliseconds( ( double ) this._EndTestDelayNumeric.Value );
                    this.DriverEmulator.Driver.BinMask = this.BinPortMask;
                    this.DriverEmulator.Driver.EndTestMode = this.EndTestMode;
                    if ( this.DriverEmulator.TryConfigureDriver( args ) )
                    {
                    }
                }
            }
            catch ( Exception ex )
            {
                args.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
                if ( args.Failed )
                    _ = this._ErrorProvider.Annunciate( sender, args.Details );
            }
        }

        /// <summary> Sot label click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SotLabel_Click( object sender, EventArgs e )
        {
            this.UpdateSotIndicator( this.DriverEmulator.Driver );
        }

        /// <summary> Updates the sot indicator described by sender. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void UpdateSotIndicator( GpioHandlerDriverBase sender )
        {
            if ( sender is object )
            {
                this._SotLabel.Image = sender.StartTestLogicalValue ? My.Resources.Resources.user_available : My.Resources.Resources.user_invisible;
            }
        }

        /// <summary> Eot label click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EotLabel_Click( object sender, EventArgs e )
        {
            this.UpdateEotIndicator( this.DriverEmulator.Driver );
        }

        /// <summary> Updates the eot indicator described by sender. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void UpdateEotIndicator( GpioHandlerDriverBase sender )
        {
            if ( sender is object )
            {
                this._EotLabel.Image = sender.EndTestLogicalValue ? My.Resources.Resources.user_available : My.Resources.Resources.user_invisible;
            }
        }

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandlerDriver sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( GpioHandlerDriver.State ):
                    {
                        this._DriverStateLabel.Text = sender.State.Description();
                        switch ( sender.State )
                        {
                            case HandlerState.StartTestReceived:
                                {
                                    break;
                                }

                            default:
                                {
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( GpioHandlerDriver.BinValue ):
                    {
                        this._DriverBinValueLabel.Text = sender.BinValueCaption( "..." );
                        break;
                    }

                case nameof( GpioHandlerDriver.StartTestLogicalValue ):
                    {
                        this.UpdateSotIndicator( sender );
                        break;
                    }

                case nameof( GpioHandlerDriver.EndTestLogicalValue ):
                    {
                        this.UpdateEotIndicator( sender );
                        break;
                    }

                case nameof( GpioHandlerDriver.BinMask ):
                    {
                        this.BinPortMask = sender.BinMask;
                        break;
                    }

                case nameof( GpioHandlerDriver.BinEndTestOnsetDelay ):
                    {
                        this._EndTestDelayNumeric.Value = ( decimal ) sender.BinEndTestOnsetDelay.TotalMilliseconds;
                        break;
                    }

                case nameof( GpioHandlerDriver.StartTestPinNumber ):
                    {
                        this._StartTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerDriver.EndTestPinNumber ):
                    {
                        this._EndTestPinNumberNumeric.Value = sender.EndTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerDriver.EndTestMode ):
                    {
                        this.EndTestMode = sender.EndTestMode;
                        break;
                    }

                case nameof( GpioHandlerDriver.ActiveLogic ):
                    {
                        this._ActiveLowLogicRadioButton.Checked = sender.ActiveLogic == ActiveLogic.ActiveLow;
                        this._ActiveHighLogicRadioButton.Checked = sender.ActiveLogic == ActiveLogic.ActiveHigh;
                        break;
                    }

                case nameof( GpioHandlerDriver.IsConfigured ):
                    {
                        this._ConfigureDriverButton.Image = sender.IsConfigured ? My.Resources.Resources.dialog_ok_2 : My.Resources.Resources.configure_2;
                        break;
                    }
            }
        }

        #endregion

        #region " EMULATOR "

        /// <summary> Updates the graphical user interface described by driver. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="value"> The value. </param>
        private void UpdateGui( GpioHandlerEmulator value )
        {
            if ( value is object )
            {
                this._EmulatorStartTestPinNumberNumeric.Value = value.StartTestPinNumber;
                this._EmulatorEndTestPinNumberNumeric.Value = value.EndTestPinNumber;
                this.EmulatorBinMask = this.DriverEmulator.Driver.BinMask;
            }
        }

        /// <summary> Gets or sets the entered bin mask. </summary>
        /// <value> The entered mask. </value>
        private long EmulatorBinMask
        {
            get => Convert.ToInt32( this._EmulatorBinPortMaskTextBox.Text, 2 );

            set => this._EmulatorBinPortMaskTextBox.Text = Convert.ToString( value, 2 );
        }

        /// <summary> Emulator Bin port mask text box validating. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void EmulatorBinPortMaskTextBox_Validating( object sender, CancelEventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.EmulatorBinMask <= 0L )
                {
                    _ = this._ErrorProvider.Annunciate( sender, "Value must be positive" );
                    e.Cancel = true;
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
                e.Cancel = true;
            }
        }

        /// <summary> Applies the button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConfigureEmulatorButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            try
            {
                this._ErrorProvider.Clear();
                if ( sender is null )
                {
                    args.RegisterFailure( "Sender is empty" );
                }
                else if ( this.DriverEmulator.IsDeviceModalityOpen )
                {
                    if ( !this.DriverEmulator.IsDeviceModalityOpen )
                    {
                        args.RegisterFailure( "Device not open" );
                    }
                    else if ( this.DriverEmulator.DeviceConnector.Device.Gpio is null )
                    {
                        args.RegisterFailure( $"Gpio not supported on {this.DriverEmulator.DeviceConnector.Device.Caption()}" );
                    }
                    else if ( this.DriverEmulator.DeviceConnector.Device.Gpio.Pins is null )
                    {
                        args.RegisterFailure( $"Gpio not supported on {this.DriverEmulator.DeviceConnector.Device.Caption()}" );
                    }
                    else
                    {
                        this.DriverEmulator.Emulator.StartTestPinNumber = ( int ) this._EmulatorStartTestPinNumberNumeric.Value;
                        this.DriverEmulator.Emulator.EndTestPinNumber = ( int ) this._EmulatorEndTestPinNumberNumeric.Value;
                        this.DriverEmulator.Emulator.ActiveLogic = this._ActiveLowLogicRadioButton.Checked ? ActiveLogic.ActiveLow : ActiveLogic.ActiveHigh;
                        this.DriverEmulator.Emulator.BinMask = this.EmulatorBinMask;
                        this.DriverEmulator.Emulator.EndTestMode = this.EndTestMode;
                        _ = this.DriverEmulator.TryConfigureEmulator( args );
                    }
                }
                else
                {
                    args.RegisterFailure( "Device not open" );
                }
            }
            catch ( Exception ex )
            {
                args.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
                if ( args.Failed )
                    _ = this._ErrorProvider.Annunciate( sender, args.Details );
            }
        }

        /// <summary> Handles the emulator property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( GpioHandlerEmulator sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( GpioHandlerDriverBase.State ):
                    {
                        switch ( sender.State )
                        {
                            case HandlerState.StartTestReceived:
                                {
                                    this._EmulatorStateLabel.Text = "e: Start test sent";
                                    break;
                                }

                            case HandlerState.EndTestSent:
                                {
                                    this._EmulatorStateLabel.Text = "e: End test received";
                                    break;
                                }

                            default:
                                {
                                    this._EmulatorStateLabel.Text = sender.State.Description();
                                    break;
                                }
                        }

                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinValue ):
                    {
                        this._EmulatorBinValueLabel.Text = sender.BinValueCaption( "..." );
                        break;
                    }

                case nameof( GpioHandlerDriverBase.StartTestLogicalValue ):
                    {
                        break;
                    }

                case nameof( GpioHandlerDriverBase.EndTestLogicalValue ):
                    {
                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinMask ):
                    {
                        this.EmulatorBinMask = sender.BinMask;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.BinEndTestOnsetDelay ):
                    {
                        break;
                    }

                case nameof( GpioHandlerDriverBase.StartTestPinNumber ):
                    {
                        this._EmulatorStartTestPinNumberNumeric.Value = sender.StartTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.EndTestPinNumber ):
                    {
                        this._EmulatorEndTestPinNumberNumeric.Value = sender.EndTestPinNumber;
                        break;
                    }

                case nameof( GpioHandlerDriverBase.ActiveLogic ):
                    {
                        break;
                    }
                // TO_DO: Must be the same as the driver.
                case nameof( GpioHandlerDriverBase.IsConfigured ):
                    {
                        this._ConfigureEmulatorButton.Image = sender.IsConfigured ? My.Resources.Resources.dialog_ok_2 : My.Resources.Resources.configure_2;
                        break;
                    }
            }
        }

        #endregion

        #region " PLAY "

        /// <summary> Clears the start button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ClearStartButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            this._ErrorProvider.Clear();
            _ = this.DriverEmulator.TryClearStart( args );
            if ( args.Failed )
                _ = this._ErrorProvider.Annunciate( sender, args.Details );
        }

        /// <summary> Starts test button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void StartTestButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            this._ErrorProvider.Clear();
            _ = this.DriverEmulator.TryStartTest( args );
            if ( args.Failed )
                _ = this._ErrorProvider.Annunciate( sender, args.Details );
        }

        /// <summary> Resets the button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ResetButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            this._ErrorProvider.Clear();
            _ = this.DriverEmulator.TryInitializeKnownState( args );
            if ( args.Failed )
                _ = this._ErrorProvider.Annunciate( sender, args.Details );
        }

        /// <summary> Outputs the end test signals. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">   The sender. </param>
        /// <param name="binValue"> The bin value. </param>
        private void OutputEndTest( object sender, int binValue )
        {
            var args = new ActionEventArgs();
            this._ErrorProvider.Clear();
            _ = this.DriverEmulator.TryOutputEndTest( binValue, args );
            if ( args.Failed )
                _ = this._ErrorProvider.Annunciate( sender, args.Details );
        }

        /// <summary> Output pass bin button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OutputPassBinButton_Click( object sender, EventArgs e )
        {
            this.OutputEndTest( sender, ( int ) this._PassBinValueNumeric.Value );
        }

        /// <summary> Output fail bin button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void OutputFailBinButton_Click( object sender, EventArgs e )
        {
            this.OutputEndTest( sender, ( int ) this._FailBinValueNumeric.Value );
        }

        #endregion

        #region " HANDLER SELECTION "

        /// <summary> Executes the 'handler selected' action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        private void OnHandlerSelected( ToolStripComboBox sender )
        {
            if ( this.InitializingComponents || sender is null )
                return;
            SupportedHandler handler = ( SupportedHandler ) Conversions.ToInteger( sender.ComboBox.SelectedValue );
            switch ( handler )
            {
                case SupportedHandler.Aetrium:
                    {
                        this.DriverEmulator.Driver.ApplyHandlerInfo( HandlerInfo.AetriumHandlerInfo );
                        this.DriverEmulator.Emulator.ApplyHandlerInfo( HandlerInfo.AetriumEmulatorInfo );
                        break;
                    }

                case SupportedHandler.NoHau:
                    {
                        this.DriverEmulator.Driver.ApplyHandlerInfo( HandlerInfo.NoHauHandlerInfo );
                        this.DriverEmulator.Emulator.ApplyHandlerInfo( HandlerInfo.NoHauEmulatorInfo );
                        break;
                    }
            }
        }

        /// <summary> Handler combo box click event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandlerComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            try
            {
                this._ErrorProvider.Clear();
                this.OnHandlerSelected( ( ToolStripComboBox ) sender );
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
            }
        }

        #endregion

    }
}
