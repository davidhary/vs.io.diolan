using System;
using System.Diagnostics;

using isr.Core;
using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.ExceptionExtensions;
using isr.Diolan.Forms.SubsystemExtensions;

namespace isr.Diolan.Forms
{
    public partial class GpioHandlerDriverView : Core.Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public GpioHandlerDriverView()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();

            // Add any initialization after the InitializeComponent() call.
            this._TabComboBox.ComboBox.DataSource = this._Tabs.TabPages;
            this._TabComboBox.ComboBox.DisplayMember = "Text";
            this.InitializeModality();
            this.InitializingComponents = false;
            this.__TabComboBox.Name = "_TabComboBox";
            this.__HandlerComboBox.Name = "_HandlerComboBox";
            this.__ConnectServerButton.Name = "_ConnectServerButton";
            this.__OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton";
            this.__ResetButton.Name = "_ResetButton";
            this.__EndTestModeComboBox.Name = "_EndTestModeComboBox";
            this.__ConfigureDriverButton.Name = "_ConfigureDriverButton";
            this.__BinPortMaskTextBox.Name = "_BinPortMaskTextBox";
            this.__ConfigureEmulatorButton.Name = "_ConfigureEmulatorButton";
            this.__EmulatorBinPortMaskTextBox.Name = "_EmulatorBinPortMaskTextBox";
            this.__OutputFailBinButton.Name = "_OutputFailBinButton";
            this.__OutputPassBinButton.Name = "_OutputPassBinButton";
            this.__ClearStartButton.Name = "_ClearStartButton";
            this.__StartTestButton.Name = "_StartTestButton";
            this.__EventLogTextBox.Name = "_EventLogTextBox";
            this.__SotLabel.Name = "_SotLabel";
            this.__EotLabel.Name = "_EotLabel";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Executes the dispose action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to disposing. </param>
        private void OnDispose( bool disposing )
        {
            this.OnCustomDispose( disposing );
        }

        #endregion

        #region " KNOWN STATE "

        /// <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public void InitializeKnownState()
        {
            this.OnServerConnectionChanged();
        }

        #endregion

        #region " SERVER "

        /// <summary> Raises the server connector property changed event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnPropertyChanged( LocalhostConnector sender, string propertyName )
        {
            string details = string.Empty;
            try
            {
                if ( sender is null )
                {
                    details = "Sender is empty";
                }
                else if ( !string.IsNullOrWhiteSpace( propertyName ) )
                {
                    switch ( propertyName ?? "" )
                    {
                        case nameof( LocalhostConnector.IsConnected ):
                            {
                                this.OnServerConnectionChanged();
                                break;
                            }

                        case nameof( LocalhostConnector.AttachedDevicesCount ):
                            {
                                this.OnServerAttachmentChanged();
                                break;
                            }
                    }
                }
            }
            catch ( Exception ex )
            {
                details = ex.ToString();
            }
            finally
            {
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, details );
            }
        }

        /// <summary> Connects a server button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConnectServerButton_Click( object sender, EventArgs e )
        {
            if ( this.DriverEmulator.ServerConnector.IsConnected )
            {
                if ( !this.DriverEmulator.ServerConnector.HasAttachedDevices() )
                {
                    this.DriverEmulator.ServerConnector.Disconnect();
                }
            }
            else
            {
                this.DriverEmulator.ServerConnector.Connect();
            }
        }

        /// <summary> Executes the server connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnServerConnectionChanged()
        {
            if ( this.DriverEmulator.ServerConnector.IsConnected )
            {
                _ = this.DriverEmulator.ServerConnector.Connection.ListDevicesById( this._DevicesComboBox );
                this._DevicesComboBox.SelectedIndex = 0;
            }

            this._OpenDeviceModalityButton.Enabled = this.DriverEmulator.ServerConnector.IsConnected;
            this._SelectDeviceSplitButton.Enabled = this.DriverEmulator.ServerConnector.IsConnected;
            this.OnServerAttachmentChanged();
        }

        /// <summary> Executes the server attachment changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OnServerAttachmentChanged()
        {
            this._ConnectServerButton.Image = My.Resources.Resources.WIFI_open_22;
            if ( this.DriverEmulator.ServerConnector.IsConnected )
            {
                this._ConnectServerButton.Text = this.DriverEmulator.ServerConnector.AttachedDevicesCount().ToString();
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
                this._ConnectServerButton.Text = "X";
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Selected device information. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A DeviceInfo. </returns>
        private DeviceInfo SelectedDeviceInfo()
        {
            return string.IsNullOrWhiteSpace( this._DevicesComboBox.Text )
                ? throw new InvalidOperationException( "No devices selected" )
                : new DeviceInfo( this._DevicesComboBox.Text );
        }

        /// <summary> Opens device button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenDeviceModalityButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            try
            {
                this._ErrorProvider.Clear();
                if ( this.DriverEmulator.IsDeviceModalityOpen )
                {
                    this.DriverEmulator.CloseDeviceModality();
                }
                else
                {
                    _ = this.DriverEmulator.TryOpenDeviceModality( this.SelectedDeviceInfo().Id, args );
                }
            }
            catch ( Exception ex )
            {
                args.RegisterError( ex.ToFullBlownString() );
            }
            finally
            {
                if ( args.Failed )
                    _ = this._ErrorProvider.Annunciate( sender, args.Details );
            }
        }

        #endregion

        #region " EVENT LOG "

        /// <summary> Event log text box double click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EventLogTextBox_DoubleClick( object sender, EventArgs e )
        {
            this._EventLogTextBox.Clear();
        }

        #endregion

        #region " TABS "

        /// <summary> Selects the tab. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void TabComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this._TabComboBox.SelectedIndex >= 0 && this._TabComboBox.SelectedIndex < this._Tabs.TabCount )
            {
                this._Tabs.SelectTab( this._Tabs.TabPages[this._TabComboBox.SelectedIndex] );
            }
        }

        #endregion


    }
}
