using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.SubsystemExtensions;
using isr.Diolan.SubsystemExtensions;

namespace isr.Diolan.Forms
{

    /// <summary> User interface for editing the device modality. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-06-01 </para>
    /// </remarks>
    public partial class DeviceModalityView : Core.Forma.ModelViewBase
    {

        #region " CONSTRUCTION "

        /// <summary> Executes the custom dispose action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to disposing. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnCustomDispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.OnModalityClosed();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
        }

        #endregion

        #region " MODALITY "

        /// <summary> The modality. </summary>
        /// <value> The modality. </value>
        public DeviceModalities Modality { get; set; } = DeviceModalities.Adc;

        /// <summary> Executes the modality closed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityClosed()
        {
            this._PortComboBox.DataSource = null;
            this._PortComboBox.Items.Clear();
            // Me._ChannelComboBox.DataSource = Nothing
            // Me._ChannelComboBox.Items.Clear()
            this._Device = null;
        }

        /// <summary> Executes the modality closing actions. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnModalityClosing( CancelEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                if ( this.IsDeviceModalityOpen )
                {
                    // un-register event handlers for all channels
                    foreach ( Dln.Adc.Port port in this._Device.Adc.Ports )
                    {
                        foreach ( Dln.Adc.Channel channel in port.Channels )
                            channel.ConditionMetThreadSafe -= this.ConditionMetEventHandler;
                    }
                }
            }
        }

        /// <summary> Executes the modality opening action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityOpening()
        {
            this._Device = this._DeviceConnector.Device;
            this._PortComboBox.DataSource = null;
            this._PortComboBox.Items.Clear();
            this.OpenModality( this._Device.Adc );
        }

        /// <summary> Opens a modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="modality"> The modality. </param>
        private void OpenModality( Dln.Adc.Module modality )
        {

            // Get port count
            if ( modality.Ports.Count == 0 )
            {
                // this is already done when opening the device.
                _ = this._ErrorProvider.Annunciate( this._OpenDeviceModalityButton, "Adapter '{0}' doesn't support ADC interface.", this._Device.Caption() );
                this._Device = null;
                this._DeviceInfoTextBox.Text = "not supported";
            }
            else
            {
                this._DeviceInfoTextBox.Text = this._Device.Caption();
                modality.Ports.ListNumbers( this._PortComboBox, true );

                // Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current;

                // Register event handlers for all channels
                foreach ( Dln.Adc.Port port in modality.Ports )
                {
                    foreach ( Dln.Adc.Channel channel in port.Channels )
                        channel.ConditionMetThreadSafe += this.ConditionMetEventHandler;
                }

                this._PortComboBox.SelectedIndex = 0;
            }    // this will cause "OnChange" event
        }

        /// <summary> Queries if a modality is open. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
        private bool IsModalityOpen()
        {
            return this._PortComboBox.DataSource is object && this._PortComboBox.Items.Count > 0;
        }

        #endregion

        #region " DEVICE "

        /// <summary> The port combo box. </summary>
        private readonly ComboBox _PortComboBox;

        #endregion

        #region " EVENT LOG "

        /// <summary> Handler, called when the condition met event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Condition met event information. </param>
        private void ConditionMetEventHandler( object sender, Dln.Adc.ConditionMetEventArgs e )
        {
            this.AppendEvent( e );
        }

        /// <summary> Appends an event. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="e"> Condition met event information. </param>
        private void AppendEvent( Dln.Adc.ConditionMetEventArgs e )
        {
            if ( e is object )
            {
                string data = $"{DateTimeOffset.Now:hh:mm:ss.fff} {e.Port:D2}!{e.Channel:D2}={e.Value:D5} {e.EventType}{Environment.NewLine}";
                // This event is handled in main thread--invoke is not needed when modifying form's controls.
                this._EventLogTextBox.AppendText( data );
            }
        }


        #endregion

    }
}
