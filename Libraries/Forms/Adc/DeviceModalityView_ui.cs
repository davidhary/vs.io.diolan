using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.ErrorProviderExtensions;
using isr.Diolan.Forms.SubsystemExtensions;

namespace isr.Diolan.Forms
{
    public partial class DeviceModalityView
    {
        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-11-23. </remarks>
        public DeviceModalityView()
        {
            this.InitializeComponent();
            this.__EventLogTextBox.Name = "_EventLogTextBox";
            this.__SelectPrimaryTabMenuItem.Name = "_SelectPrimaryTabMenuItem";
            this.__ConnectServerButton.Name = "_ConnectServerButton";
            this.__OpenDeviceModalityButton.Name = "_OpenDeviceModalityButton";
            this._PortComboBox = new ComboBox();
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                this.OnDispose( disposing );
                if ( disposing && this.components is object )
                {
                    this.components.Dispose();
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Executes the dispose action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="disposing"> true to disposing. </param>
        private void OnDispose( bool disposing )
        {
            this.OnCustomDispose( disposing );
            if ( disposing )
            {
                if ( this._Device is object )
                {
                    this._Device = null;
                }

                if ( this._DeviceConnector is object )
                {
                    this._DeviceConnector.Dispose();
                    this._DeviceConnector = null;
                }
            }
        }

        #endregion

        #region " SERVER "


        /// <summary> Gets or sets the server connector. </summary>
        /// <value> The server connector. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LocalhostConnector ServerConnector { get; set; }

        /// <summary> Connects a server button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void ConnectServerButton_Click( object sender, EventArgs e )
        {
            if ( this.ServerConnector.IsConnected )
            {
                if ( !this.ServerConnector.HasAttachedDevices() )
                {
                    this.ServerConnector.Disconnect();
                }
            }
            else
            {
                this.ServerConnector.Connect();
            }
        }


        /// <summary> Executes the server connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void OnServerConnectionChanged()
        {
            if ( this.ServerConnector.IsConnected )
            {
                _ = this.ServerConnector.Connection.ListDevicesById( this._DevicesComboBox );
                this._DevicesComboBox.SelectedIndex = 0;
            }

            this._OpenDeviceModalityButton.Enabled = this.ServerConnector.IsConnected;
            this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            this._OpenDeviceModalityButton.Enabled = this.ServerConnector.IsConnected;
            this.OnServerAttachmentChanged();
        }

        /// <summary> Executes the server attachment changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        public void OnServerAttachmentChanged()
        {
            this._ConnectServerButton.Image = My.Resources.Resources.WIFI_open_22;
            if ( this.ServerConnector.IsConnected )
            {
                this._ConnectServerButton.Text = this.ServerConnector.AttachedDevicesCount().ToString();
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                this._ConnectServerButton.ForeColor = System.Drawing.Color.Red;
                this._ConnectServerButton.Text = "X";
            }
        }

        #endregion

        #region " DEVICE CONNECTOR "

#pragma warning disable IDE1006 // Naming Styles
        private DeviceConnector __DeviceConnector;

        private DeviceConnector _DeviceConnector
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this.__DeviceConnector;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this.__DeviceConnector != null )
                {

                    this.__DeviceConnector.DeviceClosed -= this.DeviceConnector_DeviceClosed;

                    this.__DeviceConnector.DeviceClosing -= this.DeviceConnector_DeviceClosing;

                    this.__DeviceConnector.DeviceOpened -= this.DeviceConnector_DeviceOpened;
                }

                this.__DeviceConnector = value;
                if ( this.__DeviceConnector != null )
                {
                    this.__DeviceConnector.DeviceClosed += this.DeviceConnector_DeviceClosed;
                    this.__DeviceConnector.DeviceClosing += this.DeviceConnector_DeviceClosing;
                    this.__DeviceConnector.DeviceOpened += this.DeviceConnector_DeviceOpened;
                }
            }
        }
#pragma warning restore IDE1006 // Naming Styles

        /// <summary> Gets or sets the device connector. </summary>
        /// <value> The device connector. </value>
        public DeviceConnector DeviceConnector
        {
            get => this._DeviceConnector;

            set => this._DeviceConnector = value;
        }

        /// <summary> Device connector device closed. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceClosed( object sender, EventArgs e )
        {
            this._DeviceInfoTextBox.Text = "closed";
            this.OnModalityClosed();
        }

        /// <summary> Device connector device closing. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Cancel event information. </param>
        private void DeviceConnector_DeviceClosing( object sender, CancelEventArgs e )
        {
            this.OnModalityClosing( e );
        }

        /// <summary> Device connector device opened. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void DeviceConnector_DeviceOpened( object sender, EventArgs e )
        {
            DeviceConnector connector = sender as DeviceConnector;
            this._Device = connector.Device;
            this._DeviceInfoTextBox.Text = this._Device.Name(); // was .caption
            this.OnModalityOpening();
        }


        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>
        private Dln.Device _Device;

        /// <summary> Selected device information. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A DeviceInfo. </returns>
        private DeviceInfo SelectedDeviceInfo()
        {
            return string.IsNullOrWhiteSpace( this._DevicesComboBox.Text )
                ? throw new InvalidOperationException( "No devices selected" )
                : new DeviceInfo( this._DevicesComboBox.Text );
        }

        /// <summary> Opens the device for the modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="deviceId"> Identifier for the device. </param>
        /// <param name="sender">   The sender. </param>
        private void OpenDeviceModality( long deviceId, Control sender )
        {
            if ( this.ServerConnector.IsConnected )
            {
                this._OpenDeviceModalityButton.Enabled = this.ServerConnector.IsConnected;
                this._SelectDeviceSplitButton.Enabled = this.ServerConnector.IsConnected;
            }
            else
            {
                this.ServerConnector.Connect();
            }

            this._DeviceConnector = new DeviceConnector( this.ServerConnector );

            // Open device
            var e = new ActionEventArgs();
            if ( this._DeviceConnector.TryOpenDevice( deviceId, this.Modality, e ) )
            {
                this.OnModalityOpening();
            }
            else
            {
                if ( sender is object )
                    _ = this._ErrorProvider.Annunciate( sender, e.Details );
                this._DeviceInfoTextBox.Text = "No devices";
            }
        }

        /// <summary> Closes device modality. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void CloseDeviceModality()
        {
            this._DeviceConnector.CloseDevice( this.Modality );
            var e = new CancelEventArgs();
            this.OnModalityClosing( e );
            if ( !e.Cancel )
            {
                this.OnModalityClosed();
            }
        }

        /// <summary> Opens device button click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenDeviceModalityButton_Click( object sender, EventArgs e )
        {
            try
            {
                this._ErrorProvider.Clear();
                if ( this.IsDeviceModalityOpen )
                {
                    this.CloseDeviceModality();
                }
                else
                {
                    long deviceId = DeviceInfo.DefaultDeviceId;
                    if ( !string.IsNullOrWhiteSpace( this._DevicesComboBox.Text ) )
                    {
                        deviceId = this.SelectedDeviceInfo().Id;
                    }

                    this.OpenDeviceModality( deviceId, sender as Control );
                }
            }
            catch ( Exception ex )
            {
                _ = this._ErrorProvider.Annunciate( sender, ex.ToString() );
            }
            finally
            {
                this.OnModalityConnectionChanged();
            }
        }

        /// <summary> Gets a value indicating whether a device is open. </summary>
        /// <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
        private bool IsDeviceModalityOpen => this._Device is object && this.IsModalityOpen();

        /// <summary> Executes the modality connection changed action. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        private void OnModalityConnectionChanged()
        {
            if ( this.IsDeviceModalityOpen )
            {
                this._OpenDeviceModalityButton.Image = My.Resources.Resources.user_online_2;
                this._OpenDeviceModalityButton.Text = "Close";
            }
            else
            {
                this._OpenDeviceModalityButton.Image = My.Resources.Resources.user_offline_2;
                this._OpenDeviceModalityButton.Text = "Open";
            }
        }

        #endregion

        #region " MODALITIES "

        /// <summary> Gets or sets the modalities. </summary>
        /// <value> The modalities. </value>
        public DeviceModalities SupportedModalities { get; set; }

        #endregion

        #region " EVENT LOG "

        /// <summary> Event log text box double click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void EventLogTextBox_DoubleClick( object sender, EventArgs e )
        {
            this._EventLogTextBox.Clear();
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Select tab menu item click. </summary>
        /// <remarks> David, 2020-10-24. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Event information. </param>
        private void SelectTabMenuItem_Click( object sender, EventArgs e )
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if ( item is object )
                this._SelectTabButton.Image = item.Image;
            switch ( item.Name ?? "" )
            {
                case "_SelectPrimaryTabMenuItem":
                    {
                        this._Tabs.SelectTab( this._PrimaryTabPage );
                        break;
                    }

                case "_SelectEventsTabMenuItem":
                    {
                        this._Tabs.SelectTab( this._EventLogTabPage );
                        break;
                    }
            }
        }

        #endregion

    }
}
