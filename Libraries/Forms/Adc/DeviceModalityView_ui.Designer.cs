﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Diolan.Forms
{
    [DesignerGenerated()]
    public partial class DeviceModalityView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _BottomToolStrip = new ToolStrip();
            _DeviceInfoTextBox = new ToolStripTextBox();
            _Tabs = new ExtendedTabControl();
            _PrimaryTabPage = new TabPage();
            _EventLogTabPage = new TabPage();
            __EventLogTextBox = new TextBox();
            __EventLogTextBox.DoubleClick += new EventHandler(EventLogTextBox_DoubleClick);
            _TopToolStrip = new ToolStrip();
            _ValueTextBox = new ToolStripTextBox();
            _ErrorProvider = new ErrorProvider(components);
            _ToolTip = new ToolTip(components);
            _ToolStripContainer = new ToolStripContainer();
            _SelectTabButton = new ToolStripSplitButton();
            __SelectPrimaryTabMenuItem = new ToolStripMenuItem();
            __SelectPrimaryTabMenuItem.Click += new EventHandler(SelectTabMenuItem_Click);
            _SelectEventsTabMenuItem = new ToolStripMenuItem();
            _SelectServerButton = new ToolStripSplitButton();
            _ServerNameTextBox = new ToolStripTextBox();
            DefaultServerToolStripMenuItem = new ToolStripMenuItem();
            __ConnectServerButton = new ToolStripButton();
            __ConnectServerButton.Click += new EventHandler(ConnectServerButton_Click);
            _SelectDeviceSplitButton = new ToolStripSplitButton();
            _DevicesComboBox = new ToolStripComboBox();
            __OpenDeviceModalityButton = new ToolStripButton();
            __OpenDeviceModalityButton.Click += new EventHandler(OpenDeviceModalityButton_Click);
            _GetValueButton = new ToolStripButton();
            _BottomToolStrip.SuspendLayout();
            _Tabs.SuspendLayout();
            _EventLogTabPage.SuspendLayout();
            _TopToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _ToolStripContainer.BottomToolStripPanel.SuspendLayout();
            _ToolStripContainer.ContentPanel.SuspendLayout();
            _ToolStripContainer.TopToolStripPanel.SuspendLayout();
            _ToolStripContainer.SuspendLayout();
            SuspendLayout();
            // 
            // _BottomToolStrip
            // 
            _BottomToolStrip.Dock = DockStyle.None;
            _BottomToolStrip.Items.AddRange(new ToolStripItem[] { _SelectTabButton, _SelectServerButton, __ConnectServerButton, _DeviceInfoTextBox, _SelectDeviceSplitButton, __OpenDeviceModalityButton });
            _BottomToolStrip.Location = new System.Drawing.Point(3, 0);
            _BottomToolStrip.Name = "_BottomToolStrip";
            _BottomToolStrip.Size = new System.Drawing.Size(280, 29);
            _BottomToolStrip.TabIndex = 0;
            // 
            // _DeviceInfoTextBox
            // 
            _DeviceInfoTextBox.Name = "_DeviceInfoTextBox";
            _DeviceInfoTextBox.ReadOnly = true;
            _DeviceInfoTextBox.Size = new System.Drawing.Size(100, 29);
            _DeviceInfoTextBox.Text = "closed";
            _DeviceInfoTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_PrimaryTabPage);
            _Tabs.Controls.Add(_EventLogTabPage);
            _Tabs.Dock = DockStyle.Fill;
            _Tabs.HideTabHeaders = true;
            _Tabs.Location = new System.Drawing.Point(0, 0);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new System.Drawing.Size(364, 427);
            _Tabs.TabIndex = 0;
            // 
            // _PrimaryTabPage
            // 
            _PrimaryTabPage.Location = new System.Drawing.Point(4, 26);
            _PrimaryTabPage.Name = "_PrimaryTabPage";
            _PrimaryTabPage.Padding = new Padding(3);
            _PrimaryTabPage.Size = new System.Drawing.Size(356, 397);
            _PrimaryTabPage.TabIndex = 0;
            _PrimaryTabPage.Text = "Primary";
            _PrimaryTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTabPage
            // 
            _EventLogTabPage.Controls.Add(__EventLogTextBox);
            _EventLogTabPage.Location = new System.Drawing.Point(4, 22);
            _EventLogTabPage.Name = "_EventLogTabPage";
            _EventLogTabPage.Padding = new Padding(3);
            _EventLogTabPage.Size = new System.Drawing.Size(356, 401);
            _EventLogTabPage.TabIndex = 1;
            _EventLogTabPage.Text = "Events";
            _EventLogTabPage.UseVisualStyleBackColor = true;
            // 
            // _EventLogTextBox
            // 
            __EventLogTextBox.Dock = DockStyle.Fill;
            __EventLogTextBox.Location = new System.Drawing.Point(3, 3);
            __EventLogTextBox.Multiline = true;
            __EventLogTextBox.Name = "__EventLogTextBox";
            __EventLogTextBox.ScrollBars = ScrollBars.Both;
            __EventLogTextBox.Size = new System.Drawing.Size(350, 395);
            __EventLogTextBox.TabIndex = 0;
            // 
            // _TopToolStrip
            // 
            _TopToolStrip.Dock = DockStyle.None;
            _TopToolStrip.Items.AddRange(new ToolStripItem[] { _ValueTextBox, _GetValueButton });
            _TopToolStrip.Location = new System.Drawing.Point(3, 0);
            _TopToolStrip.Name = "_TopToolStrip";
            _TopToolStrip.Size = new System.Drawing.Size(140, 33);
            _TopToolStrip.TabIndex = 0;
            // 
            // _ValueTextBox
            // 
            _ValueTextBox.BackColor = System.Drawing.Color.Black;
            _ValueTextBox.Font = new System.Drawing.Font("Segoe UI", 14.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ValueTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _ValueTextBox.Name = "_ValueTextBox";
            _ValueTextBox.Size = new System.Drawing.Size(100, 33);
            _ValueTextBox.Text = "0.0 V";
            _ValueTextBox.TextBoxTextAlign = HorizontalAlignment.Center;
            _ValueTextBox.ToolTipText = "Value";
            // 
            // _ErrorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _ToolStripContainer
            // 
            // 
            // _ToolStripContainer.BottomToolStripPanel
            // 
            _ToolStripContainer.BottomToolStripPanel.Controls.Add(_BottomToolStrip);
            // 
            // _ToolStripContainer.ContentPanel
            // 
            _ToolStripContainer.ContentPanel.Controls.Add(_Tabs);
            _ToolStripContainer.ContentPanel.Size = new System.Drawing.Size(364, 427);
            _ToolStripContainer.Dock = DockStyle.Fill;
            _ToolStripContainer.Location = new System.Drawing.Point(0, 0);
            _ToolStripContainer.Name = "_ToolStripContainer";
            _ToolStripContainer.Size = new System.Drawing.Size(364, 489);
            _ToolStripContainer.TabIndex = 0;
            _ToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _ToolStripContainer.TopToolStripPanel
            // 
            _ToolStripContainer.TopToolStripPanel.Controls.Add(_TopToolStrip);
            // 
            // _SelectTabButton
            // 
            _SelectTabButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectTabButton.DropDownItems.AddRange(new ToolStripItem[] { __SelectPrimaryTabMenuItem, _SelectEventsTabMenuItem });
            _SelectTabButton.Image = My.Resources.Resources.run_build_configure;
            _SelectTabButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectTabButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectTabButton.Name = "_SelectTabButton";
            _SelectTabButton.Size = new System.Drawing.Size(38, 26);
            _SelectTabButton.Text = "Tab";
            // 
            // _SelectPrimaryTabMenuItem
            // 
            __SelectPrimaryTabMenuItem.Image = My.Resources.Resources.run_build_configure;
            __SelectPrimaryTabMenuItem.Name = "__SelectPrimaryTabMenuItem";
            __SelectPrimaryTabMenuItem.Size = new System.Drawing.Size(115, 22);
            __SelectPrimaryTabMenuItem.Text = "Primary";
            // 
            // _SelectEventsTabMenuItem
            // 
            _SelectEventsTabMenuItem.Image = My.Resources.Resources.view_calendar_upcoming_events;
            _SelectEventsTabMenuItem.Name = "_SelectEventsTabMenuItem";
            _SelectEventsTabMenuItem.Size = new System.Drawing.Size(115, 22);
            _SelectEventsTabMenuItem.Text = "Events";
            // 
            // _SelectServerButton
            // 
            _SelectServerButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectServerButton.DropDownItems.AddRange(new ToolStripItem[] { _ServerNameTextBox, DefaultServerToolStripMenuItem });
            _SelectServerButton.Image = My.Resources.Resources.network_server;
            _SelectServerButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectServerButton.Name = "_SelectServerButton";
            _SelectServerButton.Size = new System.Drawing.Size(38, 26);
            _SelectServerButton.Text = "Select Server";
            // 
            // _ServerNameTextBox
            // 
            _ServerNameTextBox.Name = "_ServerNameTextBox";
            _ServerNameTextBox.Size = new System.Drawing.Size(100, 23);
            _ServerNameTextBox.Text = "localhost:9656";
            // 
            // DefaultServerToolStripMenuItem
            // 
            DefaultServerToolStripMenuItem.Checked = true;
            DefaultServerToolStripMenuItem.CheckOnClick = true;
            DefaultServerToolStripMenuItem.CheckState = CheckState.Checked;
            DefaultServerToolStripMenuItem.Name = "DefaultServerToolStripMenuItem";
            DefaultServerToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            DefaultServerToolStripMenuItem.Text = "User Default Server:Port";
            // 
            // _ConnectServerButton
            // 
            __ConnectServerButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __ConnectServerButton.Image = My.Resources.Resources.network_disconnect_2;
            __ConnectServerButton.ImageScaling = ToolStripItemImageScaling.None;
            __ConnectServerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ConnectServerButton.Name = "__ConnectServerButton";
            __ConnectServerButton.Size = new System.Drawing.Size(26, 26);
            __ConnectServerButton.Text = "Connect Server";
            // 
            // _SelectDeviceSplitButton
            // 
            _SelectDeviceSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _SelectDeviceSplitButton.DropDownItems.AddRange(new ToolStripItem[] { _DevicesComboBox });
            _SelectDeviceSplitButton.Enabled = false;
            _SelectDeviceSplitButton.Image = My.Resources.Resources.network_server_database;
            _SelectDeviceSplitButton.ImageScaling = ToolStripItemImageScaling.None;
            _SelectDeviceSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _SelectDeviceSplitButton.Name = "_SelectDeviceSplitButton";
            _SelectDeviceSplitButton.Size = new System.Drawing.Size(38, 26);
            _SelectDeviceSplitButton.Text = "Device";
            _SelectDeviceSplitButton.ToolTipText = "Select Device";
            // 
            // _DevicesComboBox
            // 
            _DevicesComboBox.Name = "_DevicesComboBox";
            _DevicesComboBox.Size = new System.Drawing.Size(121, 23);
            _DevicesComboBox.Text = "DLN-4M.1.1";
            // 
            // _OpenDeviceModalityButton
            // 
            __OpenDeviceModalityButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            __OpenDeviceModalityButton.Enabled = false;
            __OpenDeviceModalityButton.Image = My.Resources.Resources.user_offline_2;
            __OpenDeviceModalityButton.ImageScaling = ToolStripItemImageScaling.None;
            __OpenDeviceModalityButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenDeviceModalityButton.Name = "__OpenDeviceModalityButton";
            __OpenDeviceModalityButton.Size = new System.Drawing.Size(26, 26);
            __OpenDeviceModalityButton.Text = "Open";
            __OpenDeviceModalityButton.ToolTipText = "Open or close the device.";
            // 
            // _GetValueButton
            // 
            _GetValueButton.DisplayStyle = ToolStripItemDisplayStyle.Image;
            _GetValueButton.Image = My.Resources.Resources.media_playback_start_4;
            _GetValueButton.ImageScaling = ToolStripItemImageScaling.None;
            _GetValueButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _GetValueButton.Name = "_GetValueButton";
            _GetValueButton.Size = new System.Drawing.Size(26, 30);
            _GetValueButton.Text = "Read Value";
            // 
            // DeviceModalityView
            // 
            Controls.Add(_ToolStripContainer);
            Name = "DeviceModalityView";
            Size = new System.Drawing.Size(364, 489);
            _BottomToolStrip.ResumeLayout(false);
            _BottomToolStrip.PerformLayout();
            _Tabs.ResumeLayout(false);
            _EventLogTabPage.ResumeLayout(false);
            _EventLogTabPage.PerformLayout();
            _TopToolStrip.ResumeLayout(false);
            _TopToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _ToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.BottomToolStripPanel.PerformLayout();
            _ToolStripContainer.ContentPanel.ResumeLayout(false);
            _ToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            _ToolStripContainer.TopToolStripPanel.PerformLayout();
            _ToolStripContainer.ResumeLayout(false);
            _ToolStripContainer.PerformLayout();
            ResumeLayout(false);
        }

        private ToolStrip _BottomToolStrip;
        private ToolStripTextBox _DeviceInfoTextBox;
        private ErrorProvider _ErrorProvider;
        private ToolTip _ToolTip;
        private ToolStripButton __OpenDeviceModalityButton;

        private ToolStripButton _OpenDeviceModalityButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDeviceModalityButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDeviceModalityButton != null)
                {
                    __OpenDeviceModalityButton.Click -= OpenDeviceModalityButton_Click;
                }

                __OpenDeviceModalityButton = value;
                if (__OpenDeviceModalityButton != null)
                {
                    __OpenDeviceModalityButton.Click += OpenDeviceModalityButton_Click;
                }
            }
        }

        private ToolStrip _TopToolStrip;
        private ToolStripTextBox _ValueTextBox;
        private ToolStripButton _GetValueButton;
        private TabPage _PrimaryTabPage;
        private TabPage _EventLogTabPage;
        private TextBox __EventLogTextBox;

        private TextBox _EventLogTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EventLogTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EventLogTextBox != null)
                {
                    __EventLogTextBox.DoubleClick -= EventLogTextBox_DoubleClick;
                }

                __EventLogTextBox = value;
                if (__EventLogTextBox != null)
                {
                    __EventLogTextBox.DoubleClick += EventLogTextBox_DoubleClick;
                }
            }
        }

        private ToolStripContainer _ToolStripContainer;
        private ExtendedTabControl _Tabs;
        private ToolStripSplitButton _SelectTabButton;
        private ToolStripMenuItem _SelectEventsTabMenuItem;
        private ToolStripComboBox _DevicesComboBox;
        private ToolStripSplitButton _SelectServerButton;
        private ToolStripTextBox _ServerNameTextBox;
        private ToolStripMenuItem DefaultServerToolStripMenuItem;
        private ToolStripMenuItem __SelectPrimaryTabMenuItem;

        private ToolStripMenuItem _SelectPrimaryTabMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SelectPrimaryTabMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SelectPrimaryTabMenuItem != null)
                {
                    __SelectPrimaryTabMenuItem.Click -= SelectTabMenuItem_Click;
                }

                __SelectPrimaryTabMenuItem = value;
                if (__SelectPrimaryTabMenuItem != null)
                {
                    __SelectPrimaryTabMenuItem.Click += SelectTabMenuItem_Click;
                }
            }
        }

        private ToolStripButton __ConnectServerButton;

        private ToolStripButton _ConnectServerButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConnectServerButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConnectServerButton != null)
                {
                    __ConnectServerButton.Click -= ConnectServerButton_Click;
                }

                __ConnectServerButton = value;
                if (__ConnectServerButton != null)
                {
                    __ConnectServerButton.Click += ConnectServerButton_Click;
                }
            }
        }

        private ToolStripSplitButton _SelectDeviceSplitButton;
    }
}