Imports System.Windows.Threading

Imports isr.Core
Imports isr.Core.DispatcherExtensions
Imports isr.Diolan.SubsystemExtensions

''' <summary> Default Server connector. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-30 </para>
''' </remarks>
Public NotInheritable Class LocalhostConnector
    Inherits isr.Core.Models.ViewModelBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub New()
        MyBase.New()
        Me._AttachedDevices = New AttachedDevices
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property Instance As LocalhostConnector

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class. This class uses lazy
    ''' instantiation, meaning the instance isn't created until the first time it's retrieved.
    ''' </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function SingleInstance() As LocalhostConnector
        If Instance Is Nothing Then
            SyncLock SyncLocker
                Instance = New LocalhostConnector
            End SyncLock
        End If
        Return Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock SyncLocker
                Return Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

    ''' <summary> Dispose instance. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Shared Sub DisposeInstance()
        SyncLock SyncLocker
            If Instance IsNot Nothing Then
                Instance = Nothing
            End If
        End SyncLock
    End Sub

#End Region

#Region " CONNECTION "

    ''' <summary> The default host. </summary>
    Public Const DefaultHost As String = "localhost"

    ''' <summary> Gets the default Address. </summary>
    ''' <value> The default Address. </value>
    Public Shared ReadOnly Property DefaultAddress As String
        Get
            Return $"{LocalhostConnector.DefaultHost}:{Dln.Connection.DefaultPort}"
        End Get
    End Property

    ''' <summary> Gets URL of the document. </summary>
    ''' <value> The URL. </value>
    Public ReadOnly Property Address As String
        Get
            Return If(Me.IsConnected, Me.Connection.Address, "closed")
        End Get
    End Property

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Connection As Dln.Connection
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the connection. </summary>
    ''' <value> The connection. </value>
    <CLSCompliant(False)>
    Public ReadOnly Property Connection As Dln.Connection
        Get
            Return Me._Connection
        End Get
    End Property

    ''' <summary> Connects this object. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub Connect()
        ' Connect to DLN server
        If Not Me.IsConnected Then
            Me._Connection = Dln.Library.Connect()
            Me.SyncNotifyPropertyChanged(NameOf(Diolan.LocalhostConnector.IsConnected))
        End If
    End Sub

    ''' <summary> Connection lost. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Connection_ConnectionLost(sender As Object, e As EventArgs) Handles _Connection.ConnectionLost
        Me.DisconnectWithoutEventsThis()
        Me.SyncNotifyPropertyChanged(NameOf(Diolan.LocalhostConnector.IsConnected))
    End Sub

    ''' <summary> Disconnects this object without raising events. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub DisconnectWithoutEventsThis()
        If Me._AttachedDevices IsNot Nothing Then
            Me._AttachedDevices.Clear()
        End If
        If Me.IsConnected Then Dln.Library.Disconnect(Me.Connection)

    End Sub

    ''' <summary> Disconnects this object. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub Disconnect()
        If Me.IsConnected AndAlso Not Me.HasAttachedDevices Then
            Me.DisconnectWithoutEventsThis()
            Me.SyncNotifyPropertyChanged(NameOf(Diolan.LocalhostConnector.IsConnected))
        End If
    End Sub

    ''' <summary> Gets a value indicating whether this object is connected. </summary>
    ''' <value> <c>true</c> if this object is connected; otherwise <c>false</c> </value>
    Public ReadOnly Property IsConnected As Boolean
        Get
            Return Me.Connection IsNot Nothing AndAlso Me.Connection.IsConnected
        End Get
    End Property

#End Region

#Region " ATTACHED DEVICES "

    ''' <summary> The attached devices. </summary>
    Private ReadOnly _AttachedDevices As AttachedDevices

    ''' <summary> Attached device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="id"> The identifier. </param>
    ''' <returns> A Dln.Device. </returns>
    <CLSCompliant(False)>
    Public Function AttachedDevice(ByVal id As Long) As Dln.Device
        Return Me._AttachedDevices.AttachedDevice(id)
    End Function

    ''' <summary> Query if 'device' is attached. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device"> The device. </param>
    ''' <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
    <CLSCompliant(False)>
    Public Function IsAttached(ByVal device As Dln.Device) As Boolean
        Return Me._AttachedDevices IsNot Nothing AndAlso Me._AttachedDevices.IsAttached(device)
    End Function

    ''' <summary> Query if 'device' is attached. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="deviceId"> Identifier for the device. </param>
    ''' <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
    Public Function IsAttached(ByVal deviceId As Long) As Boolean
        Return Me._AttachedDevices IsNot Nothing AndAlso Me._AttachedDevices.IsAttached(deviceId)
    End Function

    ''' <summary> Query if this object has attached devices. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if attached devices; otherwise <c>false</c> </returns>
    Public Function HasAttachedDevices() As Boolean
        Return Me._AttachedDevices IsNot Nothing AndAlso Me._AttachedDevices.HasAttachedDevices
    End Function

    ''' <summary> Attached devices count. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> An Integer. </returns>
    Public Function AttachedDevicesCount() As Integer
        Return If(Me._AttachedDevices Is Nothing, 0, Me._AttachedDevices.AttachedDevicesCount)
    End Function

    ''' <summary> Attaches the given device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device"> The device. </param>
    <CLSCompliant(False)>
    Public Sub Attach(ByVal device As Dln.Device)
        Me._AttachedDevices.Attach(device)
        isr.Core.ApplianceBase.DoEvents()
        Me.SyncNotifyPropertyChanged(NameOf(Diolan.LocalhostConnector.AttachedDevicesCount))
    End Sub

    ''' <summary> Detaches the given device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device"> The device. </param>
    <CLSCompliant(False)>
    Public Sub Detach(ByVal device As Dln.Device)
        Me._AttachedDevices.Detach(device)
        isr.Core.ApplianceBase.DoEvents()
        Me.SyncNotifyPropertyChanged(NameOf(Diolan.LocalhostConnector.AttachedDevicesCount))
    End Sub

    ''' <summary> An attached devices. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Class AttachedDevices
        Inherits Dictionary(Of Long, Integer)

        ''' <summary>
        ''' Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2" />
        ''' class that is empty, has the default initial capacity, and uses the default equality comparer
        ''' for the key type.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Sub New()
            MyBase.New
            Me._Devices = New Dictionary(Of Long, Dln.Device)
        End Sub

        ''' <summary>
        ''' Removes all keys and values from the <see cref="T:System.Collections.Generic.Dictionary`2" />.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Shadows Sub Clear()
            MyBase.Clear()
            Me._Devices = New Dictionary(Of Long, Dln.Device)
        End Sub

        ''' <summary> The devices. </summary>
        Private _Devices As Dictionary(Of Long, Dln.Device)

        ''' <summary> Attached device. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="id"> The identifier. </param>
        ''' <returns> A Dln.Device. </returns>
        Public Function AttachedDevice(ByVal id As Long) As Dln.Device
            Return If(Me._Devices IsNot Nothing AndAlso Me._Devices.ContainsKey(id), Me._Devices.Item(id), Nothing)
        End Function

        ''' <summary> Query if this object has attached devices. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="id"> The identifier. </param>
        ''' <returns> <c>true</c> if attached devices; otherwise <c>false</c> </returns>
        Public Function AttachedDeviceCount(ByVal id As Long) As Integer
            Return If(Me.IsAttached(id), Me.Item(id), 0)
        End Function

        ''' <summary> Attaches the given device. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device"> The device. </param>
        ''' <returns> An Integer. </returns>
        Public Function Attach(ByVal device As Dln.Device) As Integer
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Dim id As Integer = CInt(device.ID)
            If Me.ContainsKey(id) Then
                Me.Item(id) += 1
            Else
                Me.Add(id, 1)
                Me._Devices.Add(id, device)
            End If
            Return Me.Item(id)
        End Function

        ''' <summary> Detaches the given device. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device"> The device. </param>
        ''' <returns> An Integer. </returns>
        Public Function Detach(ByVal device As Dln.Device) As Integer
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Dim id As Integer = CInt(device.ID)
            If Me.ContainsKey(id) Then
                Me.Item(id) -= 1
            Else
                Me.Add(id, 0)
            End If
            Return Me.Item(id)
        End Function

        ''' <summary> Query if 'device' is attached. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="deviceId"> Identifier for the device. </param>
        ''' <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
        Public Function IsAttached(ByVal deviceId As Long) As Boolean
            Return Me.Any AndAlso Me.ContainsKey(deviceId) AndAlso Me.Item(deviceId) > 0
        End Function

        ''' <summary> Query if 'device' is attached. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="device"> The device. </param>
        ''' <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
        Public Function IsAttached(ByVal device As Dln.Device) As Boolean
            Return device IsNot Nothing AndAlso Me.IsAttached(CInt(device.ID))
        End Function

        ''' <summary> Gets the number of attached devices. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <returns> <c>true</c> if attached devices; otherwise <c>false</c> </returns>
        Public Function AttachedDevicesCount() As Integer
            Dim result As Integer = 0
            If Me.Any Then
                For Each v As Integer In Me.Values
                    result += v
                Next
            End If
            Return result
        End Function

        ''' <summary> Query if this object has attached devices. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <returns> <c>true</c> if attached devices; otherwise <c>false</c> </returns>
        Public Function HasAttachedDevices() As Boolean
            Dim result As Boolean = False
            If Me.Any Then
                For Each v As Integer In Me.Values
                    If v > 0 Then
                        result = True
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

    End Class

#End Region

End Class

