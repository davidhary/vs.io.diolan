''' <summary> Led subsystem. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-28 </para>
''' </remarks>
Public Class LedSubsystem
    Implements IDisposable

#Region " CONSTRUCTOR "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

#Region "I Disposable Support"

    ''' <summary> Gets or sets the disposed sentinel. </summary>
    ''' <value> The disposed sentinel. </value>
    Private Property Disposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                                            <c>False</c> to
    '''                                                                            release only
    '''                                                                            unmanaged resources
    '''                                                                            when called from
    '''                                                                            the runtime
    '''                                                                            finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed AndAlso disposing Then
                If Me._PulseTimer IsNot Nothing Then Me._PulseTimer.Dispose() : Me._PulseTimer = Nothing
            End If
        Finally
            Me.Disposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

#Region " Led "

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> The pulse timer. </summary>
    Private WithEvents _PulseTimer As System.Timers.Timer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> The pulsed Led. </summary>
    Private _PulsedLed As Dln.Led.Led

    ''' <summary> Pulse LED. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="led">      The LED. </param>
    ''' <param name="duration"> The duration. </param>
    <CLSCompliant(False)>
    Public Sub PulseLed(ByVal led As Dln.Led.Led, ByVal duration As TimeSpan)
        If led Is Nothing Then Throw New ArgumentNullException(NameOf(led))
        If Me._PulseTimer Is Nothing Then
            Me._PulseTimer = New System.Timers.Timer
        End If
        Me._PulseTimer.AutoReset = False
        Me._PulseTimer.Interval = duration.TotalMilliseconds
        ' toggle the Led value
        Me._PulsedLed = led
        Me._PulsedLed.State = Dln.Led.State.On
        Me._PulseTimer.Start()
    End Sub

    ''' <summary> Pulse timer elapsed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Elapsed event information. </param>
    Private Sub PulseTimer_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles _PulseTimer.Elapsed
        If Me._PulsedLed IsNot Nothing Then
            Me._PulsedLed.State = Dln.Led.State.Off
        End If
        ' the timer will not start again.
    End Sub

#End Region

End Class

