Imports System.ComponentModel

Imports isr.Core
Imports isr.Diolan.SubsystemExtensions

''' <summary> Connector for device modality or modalities. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-06-18, 5647. </para>
''' </remarks>
Public MustInherit Class ModalityConnectorBase
    Inherits isr.Core.Models.ViewModelBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

#Region " I DISPOSABLE SUPPORT "

    ''' <summary> Gets or sets the disposed sentinel. </summary>
    ''' <value> The disposed sentinel. </value>
    Protected ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                                            <c>False</c> to
    '''                                                                            release only
    '''                                                                            unmanaged resources
    '''                                                                            when called from
    '''                                                                            the runtime
    '''                                                                            finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Try
                    Me.OnModalityClosed()
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                End Try
                If Me._Device IsNot Nothing Then Me._Device = Nothing
                If Me._DeviceConnector IsNot Nothing Then Me._DeviceConnector.Dispose() : Me._DeviceConnector = Nothing
                If Me._ServerConnector IsNot Nothing Then Me._ServerConnector = Nothing
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

#Region " SERVER "

    ''' <summary> Gets or sets the server connector source. </summary>
    ''' <value> The server connector source. </value>
    Public ReadOnly Property ServerConnectorSource As String = $"{NameOf(Diolan.ModalityConnectorBase.ServerConnector)}."

    ''' <summary> Server connector property changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub ServerConnector_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _ServerConnector.PropertyChanged
        If sender IsNot Nothing AndAlso e IsNot Nothing Then Me.SyncNotifyPropertyChanged($"{Me.ServerConnectorSource}{e.PropertyName}")
    End Sub

        Private WithEvents _ServerConnector As LocalhostConnector

    ''' <summary> Gets or sets the server connector. </summary>
    ''' <value> The server connector. </value>
    Public Property ServerConnector As LocalhostConnector
        Get

            Return Me._ServerConnector
        End Get
        Set(value As LocalhostConnector)
            Me._ServerConnector = value
        End Set
    End Property

#End Region

#Region " DEVICE CONNECTOR "

        Private WithEvents _DeviceConnector As DeviceConnector

    ''' <summary> Gets or sets the device connector. </summary>
    ''' <value> The device connector. </value>
    Public Property DeviceConnector As DeviceConnector
        Get
            Return Me._DeviceConnector
        End Get
        Set(value As DeviceConnector)
            Me._DeviceConnector = value
        End Set
    End Property

    ''' <summary> Device connector device closed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceClosed(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceClosed
        Me._DeviceCaption = "closed"
        Me.SyncNotifyPropertyChanged(NameOf(Diolan.ModalityConnectorBase.DeviceCaption))
        Me.OnModalityClosed()
    End Sub

    ''' <summary> Device connector device closing. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub DeviceConnector_DeviceClosing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles _DeviceConnector.DeviceClosing
        Me.OnModalityClosing(e)
    End Sub

    ''' <summary> Device connector device opened. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DeviceConnector_DeviceOpened(sender As Object, e As System.EventArgs) Handles _DeviceConnector.DeviceOpened
        Me._DeviceCaption = Me._DeviceConnector.Device.Caption

        Me.SyncNotifyPropertyChanged(NameOf(Diolan.ModalityConnectorBase.DeviceCaption))
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device caption. </summary>
    Private _DeviceCaption As String

    ''' <summary> Gets information describing the device. </summary>
    ''' <value> Information describing the device. </value>
    Public ReadOnly Property DeviceCaption As String
        Get
            Return Me._DeviceCaption
        End Get
    End Property

    ''' <summary> Gets the device. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The device. </value>
    <CLSCompliant(False)>
    Protected ReadOnly Property Device As Dln.Device

    ''' <summary> Opens the device for the modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="deviceId"> Identifier for the device. </param>
    ''' <param name="e">        Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryOpenDeviceModality(ByVal deviceId As Long, ByVal e As ActionEventArgs) As Boolean

        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        ' connect if not connected
        Me.ServerConnector.Connect()
        Me._DeviceConnector = New DeviceConnector(Me._ServerConnector)

        ' Open device
        If Me.DeviceConnector.TryOpenDevice(deviceId, Me.Modality, e) Then
            Me._Device = Me._DeviceConnector.Device
            If Me.TryOpenModality(e) Then
            Else
                Me._Device = Nothing
            End If
        Else
            Me._Device = Nothing
            Me._DeviceCaption = "failed"
            Me.SyncNotifyPropertyChanged(NameOf(Diolan.ModalityConnectorBase.DeviceCaption))
        End If
        Me.SyncNotifyPropertyChanged(NameOf(Diolan.ModalityConnectorBase.IsDeviceModalityOpen))
        Return Not e.Failed

    End Function

    ''' <summary> Closes device modality. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub CloseDeviceModality()
        Me.DeviceConnector.CloseDevice(Me.Modality)
        Dim e As New System.ComponentModel.CancelEventArgs
        Me.OnModalityClosing(e)
        If Not e.Cancel Then
            Me.OnModalityClosed()
            Me.SyncNotifyPropertyChanged(NameOf(Diolan.ModalityConnectorBase.IsDeviceModalityOpen))
        End If
    End Sub

    ''' <summary> Gets a value indicating whether a device is open. </summary>
    ''' <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
    Public ReadOnly Property IsDeviceModalityOpen As Boolean
        Get
            Return Me._Device IsNot Nothing AndAlso Me.IsModalityOpen
        End Get
    End Property

#End Region

#Region " SUBSYSTEM MODALITIES "

    ''' <summary> The modality. </summary>
    ''' <value> The modality. </value>
    Public MustOverride ReadOnly Property Modality As DeviceModalities

    ''' <summary> Executes the modality closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Protected Overridable Sub OnModalityClosed()
        Me._Device = Nothing
        If Me.ServerConnector?.IsConnected Then
            If Not Me.ServerConnector.HasAttachedDevices Then
                Me.ServerConnector.Disconnect()
            End If
        End If
    End Sub

    ''' <summary> Executes the modality closing actions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected MustOverride Sub OnModalityClosing(ByVal e As System.ComponentModel.CancelEventArgs)

    ''' <summary> Executes the modality opening action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Protected MustOverride Function TryOpenModality(ByVal e As ActionEventArgs) As Boolean

    ''' <summary> Queries if a modality is open. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
    Public MustOverride Function IsModalityOpen() As Boolean

#End Region

#Region " EVENT LOG "

    ''' <summary> The event caption. </summary>
    Private _EventCaption As String

    ''' <summary> Gets or sets the event caption. </summary>
    ''' <value> The event caption. </value>
    Public Property EventCaption As String
        Get
            Return Me._EventCaption
        End Get
        Protected Set(value As String)
            If Not String.Equals(value, Me.EventCaption) Then
                Me._EventCaption = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class
