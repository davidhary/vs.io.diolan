Imports isr.Diolan.SubsystemExtensions

''' <summary> Information about the device. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-06-01 </para>
''' </remarks>
<CLSCompliant(False)>
Public Class DeviceInfo

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As Dln.Device)
        MyBase.New()
        If device IsNot Nothing Then
            Me._Id = device.ID
            Me._SerialNumber = device.SN
            Me._HardwareType = device.HardwareType
        End If
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="caption"> The caption. </param>
    Public Sub New(ByVal caption As String)
        MyBase.New
        Me.ParseThis(caption)
    End Sub

    ''' <summary> The default device identifier. </summary>
    Public Const DefaultDeviceId As Integer = 0

    ''' <summary> Gets the identifier. </summary>
    ''' <value> The identifier. </value>
    Public Property Id As Long

    ''' <summary> Gets the type of the hardware. </summary>
    ''' <value> The type of the hardware. </value>
    Public Property HardwareType As Dln.HardwareType

    ''' <summary> Gets the serial number. </summary>
    ''' <value> The serial number. </value>
    Public Property SerialNumber As Long

    ''' <summary> Gets the caption. </summary>
    ''' <value> The caption. </value>
    Public ReadOnly Property Caption As String
        Get
            Return $"{Me.HardwareType}.{Me.Id}.{Me.SerialNumber}"
        End Get
    End Property

    ''' <summary> Parses. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="caption"> The caption. </param>
    Private Sub ParseThis(ByVal caption As String)
        If String.IsNullOrWhiteSpace(caption) Then Throw New ArgumentNullException(NameOf(caption))
        Dim elements As Stack(Of String) = New Stack(Of String)(caption.Split("."c))
        If elements.Count < 3 Then
            Throw New InvalidOperationException($"Unable to parse {caption} because it has fewer than 3 elements")
        Else
            Dim value As String = elements.Pop
            If Not Long.TryParse(value, Me.SerialNumber) Then
                Throw New InvalidOperationException($"Unable to parse {value} into a serial number")
            End If
            value = elements.Pop
            If Not Long.TryParse(value, Me.Id) Then
                Throw New InvalidOperationException($"Unable to parse {value} into a device id")
            End If
            value = elements.Pop
            If Not [Enum].TryParse(Of Dln.HardwareType)(value, Me.HardwareType) Then
                Throw New InvalidOperationException($"Unable to parse {value} into hardware type")
            End If
        End If
    End Sub

End Class

''' <summary> Collection of device information. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-06-01 </para>
''' </remarks>
<CLSCompliant(False)>
Public Class DeviceInfoCollection
    Inherits Collections.ObjectModel.Collection(Of DeviceInfo)

    ''' <summary> Attempts to enumerate devices from the given data. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="connection"> The connection. </param>
    ''' <param name="details">    The details. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryEnumerateDevices(ByVal connection As Dln.Connection, ByVal details As String) As Boolean
        Me.Clear()
        If connection Is Nothing Then
            details = "Not created"
        ElseIf Not connection.IsConnected Then
            details = "Not connected"
        ElseIf Dln.Device.Count() = 0 Then
            details = $"No DLN-series adapters found @{connection.Address}."
        Else
            For i As Long = 0 To Dln.Device.Count - 1
                Dim d As Dln.Device = Dln.Device.Open(CInt(i))
                If d IsNot Nothing Then Me.Add(New DeviceInfo(d))
            Next
        End If
        Return String.IsNullOrWhiteSpace(details)
    End Function

End Class

