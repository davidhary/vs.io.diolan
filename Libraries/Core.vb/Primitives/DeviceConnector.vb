Imports isr.Core
Imports isr.Diolan.SubsystemExtensions

''' <summary> Device connector. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2015-05-30 </para>
''' </remarks>
Public Class DeviceConnector
    Implements IDisposable

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="serverConnector"> The server connector. </param>
    <CLSCompliant(False)>
    Public Sub New(ByVal serverConnector As LocalhostConnector)
        MyBase.New()
        Me._ServerConnector = serverConnector
    End Sub

#Region " DISPOSABLE SUPPORT "

    ''' <summary> Gets the disposed sentinel. </summary>
    ''' <value> The disposed sentinel. </value>
    Private Property Disposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                                            <c>False</c> to
    '''                                                                            release only
    '''                                                                            unmanaged resources
    '''                                                                            when called from
    '''                                                                            the runtime
    '''                                                                            finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.Disposed AndAlso disposing Then
                ' dispose managed state (managed objects).
                Me.RemoveDeviceOpenedEventHandlers()
                Me.RemoveDeviceClosedEventHandlers()
                Me.RemoveDeviceClosingEventHandlers()
                If Me.Device IsNot Nothing Then Me.Device = Nothing
                If Me._ServerConnector IsNot Nothing Then Me._ServerConnector = Nothing
            End If
        Finally
            Me.Disposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

#Region " CONNECTION "

    ''' <summary> The server connector. </summary>
    Private _ServerConnector As LocalhostConnector

    ''' <summary> Gets the connection. </summary>
    ''' <value> The connector. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <CLSCompliant(False)>
    Public ReadOnly Property Connection As Dln.Connection
        Get
            Return If(Me._ServerConnector Is Nothing, Nothing, Me._ServerConnector.Connection)
        End Get
    End Property

    ''' <summary> Gets a value indicating whether this object is local connection. </summary>
    ''' <value> <c>true</c> if this object is local connection; otherwise <c>false</c> </value>
    Public ReadOnly Property IsLocalConnection As Boolean
        Get
            Return Me.Connection IsNot Nothing AndAlso Me.Connection.IsConnected AndAlso
                Me.Connection.Host.Equals(LocalhostConnector.DefaultHost, System.StringComparison.OrdinalIgnoreCase)
        End Get
    End Property

    ''' <summary> Attaches. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="serverConnector"> The server connector. </param>
    Private Sub Attach(ByVal serverConnector As LocalhostConnector)
        If serverConnector IsNot Nothing Then serverConnector.Attach(Me._Device)
    End Sub

    ''' <summary> Detaches the given device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="serverConnector"> The server connector. </param>
    Private Sub Detach(ByVal serverConnector As LocalhostConnector)
        If serverConnector IsNot Nothing Then serverConnector.Detach(Me._Device)
    End Sub

#End Region

#Region " DEVICE "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Device As Dln.Device
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <CLSCompliant(False)>
    Public Property Device As Dln.Device
        Get
            Return Me._Device
        End Get
        Set(value As Dln.Device)
            Me._Device = value
        End Set
    End Property

    ''' <summary> Attempts to open device from the given data. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="deviceId">         Identifier for the device. </param>
    ''' <param name="requiredModality"> The required modality. </param>
    ''' <param name="e">                Cancel details event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryOpenDevice(ByVal deviceId As Long, ByVal requiredModality As DeviceModalities, ByVal e As isr.Core.ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If Me.Connection Is Nothing Then
            e.RegisterFailure("Device connection not set")
        ElseIf Me.Connection.IsConnected Then
            ' Open device
            If Dln.Device.Count() = 0 Then
                e.RegisterFailure("No DLN-series adapters have been detected.")
            ElseIf Not Me.IsDeviceOpen Then
                Me._Device = If(Me._ServerConnector.IsAttached(deviceId), Me._ServerConnector.AttachedDevice(deviceId), Dln.Device.OpenById(CUInt(deviceId)))
                If requiredModality = DeviceModalities.None OrElse Me.Device.SupportsAny(requiredModality) Then
                    Me.OnDeviceOpened(requiredModality)
                Else
                    e.RegisterFailure($"{Me.Device.HardwareType} does not support {requiredModality}.")
                    Me._Device = Nothing
                End If
            End If
        Else
            e.RegisterFailure("Device not connected")
        End If
        Return Not e.Failed
    End Function

    ''' <summary> Gets a value indicating whether a device is open. </summary>
    ''' <value> <c>true</c> if a device is open; otherwise <c>false</c> </value>
    Public ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return Me._Device IsNot Nothing
        End Get
    End Property

    ''' <summary> Closes the device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="requiredModality"> The required modality. </param>
    Public Sub CloseDevice(ByVal requiredModality As DeviceModalities)
        DeviceConnector.Detach(Me._Device, requiredModality)
        If Me.IsDeviceOpen AndAlso Not DeviceConnector.HasAttachedModalities(Me._Device) Then
            Dim e As New System.ComponentModel.CancelEventArgs
            Me.OnDeviceClosing(e)
            If Not e.Cancel Then
                Me.OnDeviceClosed()
            End If
        End If
    End Sub


#Region " DeviceOpened "

    ''' <summary> Executes the device Open action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="requiredModality"> The required modality. </param>
    Private Sub OnDeviceOpened(ByVal requiredModality As DeviceModalities)
        DeviceConnector.Attach(Me._Device, requiredModality)
        If Not Me._ServerConnector.IsAttached(Me._Device) Then
            Me.Attach(Me._ServerConnector)
            Me.SyncNotifyDeviceOpened(System.EventArgs.Empty)
        End If
    End Sub

    ''' <summary> Removes the DeviceOpened event handlers. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Protected Sub RemoveDeviceOpenedEventHandlers()
        Me._DeviceOpenedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The DeviceOpened event handlers. </summary>
    Private ReadOnly _DeviceOpenedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in DeviceOpened events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DeviceOpened As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._DeviceOpenedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._DeviceOpenedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._DeviceOpenedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DeviceOpened">DeviceOpened Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyDeviceOpened(ByVal e As System.EventArgs)
        Me._DeviceOpenedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " DEVICE CLOSING "

    ''' <summary> Allows taking actions before DeviceClosing. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If Not e.Cancel Then
            Me.SyncNotifyDeviceClosing(e)
        End If
    End Sub

    ''' <summary> Removes the DeviceClosing event handlers. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Protected Sub RemoveDeviceClosingEventHandlers()
        Me._DeviceClosingEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The DeviceClosing event handlers. </summary>
    Private ReadOnly _DeviceClosingEventHandlers As New EventHandlerContextCollection(Of System.ComponentModel.CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in DeviceClosing events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DeviceClosing As EventHandler(Of System.ComponentModel.CancelEventArgs)
        AddHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._DeviceClosingEventHandlers.Add(New EventHandlerContext(Of System.ComponentModel.CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._DeviceClosingEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Me._DeviceClosingEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DeviceClosing">DeviceClosing Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Me._DeviceClosingEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " DEVICE CLOSED "

    ''' <summary> Executes the device closed action. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub OnDeviceClosed()
        If Me.IsDeviceOpen AndAlso Not DeviceConnector.HasAttachedModalities(Me._Device) Then
            Me.Detach(Me._ServerConnector)
            Me._Device = Nothing
            Me.SyncNotifyDeviceClosed(EventArgs.Empty)
        End If
    End Sub

    ''' <summary> Device removed event hander. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device"> The device. </param>
    <CLSCompliant(False)>
    Protected Overridable Sub OnDeviceRemoved(ByVal device As Dln.Device)
        If device IsNot Nothing AndAlso Me._Device IsNot Nothing AndAlso Me.Device.ID = device.ID Then
            DeviceConnector.Detach(device)
            Me.Detach(Me._ServerConnector)
            Me._Device = Nothing
            Me.SyncNotifyDeviceClosed(EventArgs.Empty)
        End If
    End Sub

    ''' <summary> Device removed thread safe. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Device_DeviceRemovedThreadSafe(sender As Object, e As System.EventArgs) Handles _Device.DeviceRemovedThreadSafe
        Me.OnDeviceRemoved(TryCast(sender, Dln.Device))
    End Sub

    ''' <summary> Removes the DeviceClosed event handlers. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Protected Sub RemoveDeviceClosedEventHandlers()
        Me._DeviceClosedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The DeviceClosed event handlers. </summary>
    Private ReadOnly _DeviceClosedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)


    ''' <summary> Event queue for all listeners interested in DeviceClosed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event DeviceClosed As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._DeviceClosedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._DeviceClosedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._DeviceClosedEventHandlers.Post(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="DeviceClosed">DeviceClosed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyDeviceClosed(ByVal e As System.EventArgs)
        Me._DeviceClosedEventHandlers.Send(Me, e)
    End Sub

#End Region

#End Region

#Region " ATTACHED MODALITIES "

    ''' <summary> Query if 'device' is attached. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device">   The device. </param>
    ''' <param name="modality"> The modality. </param>
    ''' <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
    <CLSCompliant(False)>
    Public Shared Function IsAttached(ByVal device As Dln.Device, ByVal modality As DeviceModalities) As Boolean
        Return DeviceConnector._AttachedModalities IsNot Nothing AndAlso
            DeviceConnector._AttachedModalities.IsAttached(device, modality)
    End Function

    ''' <summary> The attached modalities. </summary>
    Private Shared _AttachedModalities As AttachedDeviceModalities

    ''' <summary> Query if 'device' has attached modalities. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device"> The device. </param>
    ''' <returns> True if attached modalities, false if not. </returns>
    <CLSCompliant(False)>
    Public Shared Function HasAttachedModalities(ByVal device As Dln.Device) As Boolean
        Return DeviceConnector._AttachedModalities IsNot Nothing AndAlso
            DeviceConnector._AttachedModalities.HasAttachedModalities(device)
    End Function

    ''' <summary> Attaches. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device">   The device. </param>
    ''' <param name="modality"> The modality. </param>
    ''' <returns> An Integer. </returns>
    <CLSCompliant(False)>
    Public Shared Function Attach(ByVal device As Dln.Device, ByVal modality As DeviceModalities) As Integer
        If DeviceConnector._AttachedModalities Is Nothing Then
            DeviceConnector._AttachedModalities = New AttachedDeviceModalities
        End If
        Return DeviceConnector._AttachedModalities.Attach(device, modality)
    End Function

    ''' <summary> Detaches the given device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device">   The device. </param>
    ''' <param name="modality"> The modality. </param>
    ''' <returns> An Integer. </returns>
    <CLSCompliant(False)>
    Public Shared Function Detach(ByVal device As Dln.Device, ByVal modality As DeviceModalities) As Integer
        If DeviceConnector._AttachedModalities Is Nothing Then
            DeviceConnector._AttachedModalities = New AttachedDeviceModalities
        End If
        Return DeviceConnector._AttachedModalities.Detach(device, modality)
    End Function

    ''' <summary> Detaches the given device. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="device"> The device. </param>
    <CLSCompliant(False)>
    Public Shared Sub Detach(ByVal device As Dln.Device)
        If DeviceConnector._AttachedModalities IsNot Nothing Then
            DeviceConnector._AttachedModalities.Detach(device)
        End If
    End Sub

    ''' <summary> Attached device modalities. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-02 </para>
    ''' </remarks>
    Private Class AttachedDeviceModalities
        Inherits Dictionary(Of UInteger, AttachedModalities)

        ''' <summary> Attaches. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="device">   The device. </param>
        ''' <param name="modality"> The modality. </param>
        ''' <returns> An Integer. </returns>
        Public Function Attach(ByVal device As Dln.Device, ByVal modality As DeviceModalities) As Integer
            If Not Me.ContainsKey(device.ID) Then
                Me.Add(device.ID, New AttachedModalities)
            End If
            Return Me.Item(device.ID).Attach(modality)
        End Function

        ''' <summary> Detaches. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="device"> The device. </param>
        Public Sub Detach(ByVal device As Dln.Device)
            If Me.ContainsKey(device.ID) Then
                Me.Remove(device.ID)
            End If
        End Sub

        ''' <summary> Detaches. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="device">   The device. </param>
        ''' <param name="modality"> The modality. </param>
        ''' <returns> An Integer. </returns>
        Public Function Detach(ByVal device As Dln.Device, ByVal modality As DeviceModalities) As Integer
            If Not Me.ContainsKey(device.ID) Then
                Me.Add(device.ID, New AttachedModalities)
                Return 0
            Else
                Return Me.Item(device.ID).Detach(modality)
            End If
        End Function

        ''' <summary> Query if 'device' is attached. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="device">   The device. </param>
        ''' <param name="modality"> The modality. </param>
        ''' <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
        Public Function IsAttached(ByVal device As Dln.Device, ByVal modality As DeviceModalities) As Boolean
            Return Me.Any AndAlso Me.ContainsKey(device.ID) AndAlso Me.Item(device.ID).Item(modality) > 0
        End Function

        ''' <summary> Query if this device has attached Modalities. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="device"> The device. </param>
        ''' <returns> <c>true</c> if attached Modalities; otherwise <c>false</c> </returns>
        Public Function HasAttachedModalities(ByVal device As Dln.Device) As Boolean
            Dim result As Boolean = False
            If Me.Any Then
                For Each v As Integer In Me.Item(device.ID).Values
                    If v > 0 Then
                        result = True
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

    End Class

    ''' <summary> Attached modalities. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-02 </para>
    ''' </remarks>
    Private Class AttachedModalities
        Inherits Dictionary(Of DeviceModalities, Integer)

        ''' <summary> Attaches the given modality. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="modality"> The modality. </param>
        ''' <returns> An Integer. </returns>
        Public Function Attach(ByVal modality As DeviceModalities) As Integer
            If Me.ContainsKey(modality) Then
                Me.Item(modality) += 1
            Else
                Me.Add(modality, 1)
            End If
            Return Me.Item(modality)
        End Function

        ''' <summary> Detaches the given modality. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="modality"> The modality. </param>
        ''' <returns> An Integer. </returns>
        Public Function Detach(ByVal modality As DeviceModalities) As Integer
            If Me.ContainsKey(modality) Then
                Me.Item(modality) -= 1
            Else
                Me.Add(modality, 0)
            End If
            Return Me.Item(modality)
        End Function

        ''' <summary> Query if 'modality' is attached. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="modality"> The modality. </param>
        ''' <returns> <c>true</c> if attached; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function IsAttached(ByVal modality As DeviceModalities) As Boolean
            Return Me.Any AndAlso Me.ContainsKey(modality) AndAlso Me.Item(modality) > 0
        End Function

        ''' <summary> Query if this object has attached Modalities. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <returns> <c>true</c> if attached Modalities; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function HasAttachedModalities() As Boolean
            Dim result As Boolean = False
            If Me.Any Then
                For Each v As Integer In Me.Values
                    If v > 0 Then
                        result = True
                        Exit For
                    End If
                Next
            End If
            Return result
        End Function

    End Class


#End Region

End Class
