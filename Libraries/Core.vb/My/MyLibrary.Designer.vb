Imports System

Imports isr.Core

Namespace My

    ''' <summary>   Implements the <see cref="isr.Core.ApplianceBase"/> for this class library. </summary>
    ''' <remarks>   David, 2020-09-29. </remarks>
    <ComponentModel.EditorBrowsable(ComponentModel.EditorBrowsableState.Never)>
    Public Class Appliance
        Inherits ApplianceBase

#Region " CONSTRUCTION "

        ''' <summary>   Default constructor. </summary>
        ''' <remarks>   David, 2020-09-29. </remarks>
        Public Sub New()
            MyBase.New(My.MyProject.Application)
            ' the application info gets by the assembly that reads it for the first time.
            Dim info As Microsoft.VisualBasic.ApplicationServices.AssemblyInfo = Me.Application.Info
        End Sub

#End Region

        ''' <summary>   Gets the identifier of the trace source. </summary>
        ''' <value> The identifier of the trace event. </value>
        Public Overrides ReadOnly Property TraceEventId As Integer
            Get
                Return MyLibrary.TraceEventId
            End Get
        End Property

        ''' <summary>   The assembly title. </summary>
        ''' <value> The assembly title. </value>
        Public Overrides ReadOnly Property AssemblyTitle As String
            Get
                Return MyLibrary.AssemblyTitle
            End Get
        End Property

        ''' <summary>   Information describing the assembly. </summary>
        ''' <value> Information describing the assembly. </value>
        Public Overrides ReadOnly Property AssemblyDescription As String
            Get
                Return MyLibrary.AssemblyDescription
            End Get
        End Property

        ''' <summary>   The assembly product. </summary>
        ''' <value> The assembly product. </value>
        Public Overrides ReadOnly Property AssemblyProduct As String
            Get
                Return MyLibrary.AssemblyProduct
            End Get
        End Property
    End Class

    Partial Public Class MyLibrary

        ''' <summary>   The appliance object provider. </summary>
        Private Shared ReadOnly _ApplianeObjectProvider As ThreadSafeObjectProvider(Of Appliance) = New ThreadSafeObjectProvider(Of Appliance)()

        ''' <summary>   Gets the implementation of the <see cref="isr.Core.ApplianceBase"/> for this class library. </summary>  
        ''' <value> The implementation of the <see cref="isr.Core.ApplianceBase"/> for this class library. </value>
        <ComponentModel.Design.HelpKeyword("My.MyLibrary.Appliance")>
        Public Shared ReadOnly Property Appliance As Appliance
            Get
                Return _ApplianeObjectProvider.GetInstance
            End Get
        End Property

        ''' <summary>   Gets the logger. </summary>
        ''' <value> The logger. </value>
        Public Shared ReadOnly Property Logger As Logger
            Get
                Return MyLibrary.Appliance.Logger
            End Get
        End Property

        ''' <summary>   Gets the unpublished trace messages. </summary>
        ''' <value> The unpublished trace messages. </value>
        Public Shared ReadOnly Property UnpublishedTraceMessages As TraceMessagesQueue
            Get
                Return Appliance.UnpublishedTraceMessages
            End Get
        End Property
    End Class

    ''' <summary>   A thread safe object provider. This class cannot be inherited. </summary>
    ''' <remarks>   David, 2020-09-23. </remarks>
    ''' <typeparam name="T">    Generic type parameter. </typeparam>
    <ComponentModel.EditorBrowsable(ComponentModel.EditorBrowsableState.Never)>
    Friend NotInheritable Class ThreadSafeObjectProvider(Of T As New)
        ''' <summary>   The context. </summary>
        Private ReadOnly _Context As Microsoft.VisualBasic.MyServices.Internal.ContextValue(Of T) = New Microsoft.VisualBasic.MyServices.Internal.ContextValue(Of T)()

        ''' <summary>   Gets the get instance. </summary>
        ''' <value> The get instance. </value>
        Friend ReadOnly Property GetInstance As T
            <DebuggerHidden()>
            Get
                Dim Value As T = _Context.Value

                If Value Is Nothing Then
                    Value = New T()
                    _Context.Value = Value
                End If

                Return Value
            End Get
        End Property

        ''' <summary>   Default constructor. </summary>
        ''' <remarks>   David, 2020-09-23. </remarks>
        <DebuggerHidden()>
        <ComponentModel.EditorBrowsable(ComponentModel.EditorBrowsableState.Never)>
        Public Sub New()
            MyBase.New()
        End Sub
    End Class


End Namespace

