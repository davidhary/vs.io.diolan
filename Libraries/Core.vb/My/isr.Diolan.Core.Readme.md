## ISR Diolan Core<sub>&trade;</sub>: Diolan I/O Core Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*1.3.6667 2018-04-03*  
2018 release.

*1.2.6333 2017-05-04*  
Uses cancel details event arguments instead of
referenced details.

*1.1.6097 2016-09-10*  
Uses DLN Library 3.4.x. Adds support for No Hau
handler.

*1.1.5941 2016-04-07*  
Uses DLN Library 3.1.x. Fixes disconnection. Handles
the lost connection event. Uses device ID instead of serial numbers as
keys.

*1.0.5654 2015-06-25 Resets emulator before and after driver.

*1.0.5649 2015-06-20 Separates configuration from initialization to prevent
driver from registering bogus emulator events. Validates configuration.

*1.0.5647 2015-06-18 Adds a base class for modality connector. Uses the
base class for the handler Driver Emulator.

*1.0.5646 2015-06-17 Adds an integrated handler driver+emulator classes
with the console.

*1.0.5644 2015-06-15 Auto list panels. Change to using active low
nomenclature. Improves handling of open server with detached
instruments. Publishes known state.

*1.0.5624 2015-05-26 Created.

\(C\) 2015 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:


### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[DIOLAN Libraries](https://bitbucket.org/davidhary/vs.IOdiolan)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[DLN Library](http://www.dlnware.com)
