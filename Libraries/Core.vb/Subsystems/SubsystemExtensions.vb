Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Namespace SubsystemExtensions

    ''' <summary> Includes extensions for <see cref="Dln.Device">DLN Devices</see>. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-05-26, 1.0.5624.x. </para></remarks>
    Public Module Methods

#Region " CONNECTION EXTENSIONS "

        ''' <summary> Return the host and port of the given connection. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="connection"> The connection. </param>
        ''' <returns> A String. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function Address(ByVal connection As Dln.Connection) As String
            If connection Is Nothing Then
                Return "Connection not created"
            ElseIf connection.IsConnected Then
                Return $"{connection.Host}:{connection.Port}"
            Else
                Return "Connection not connected"
            End If
        End Function

        ''' <summary> Connection information. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="connection"> The connection. </param>
        ''' <returns>
        ''' A Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
        ''' </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ConnectionInfo(ByVal connection As Dln.Connection) As Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
            If connection Is Nothing Then Throw New ArgumentNullException(NameOf(connection))
            Dim keyValueList As New List(Of KeyValuePair(Of String, String)) From {
                New KeyValuePair(Of String, String)("Host", connection.Host),
                New KeyValuePair(Of String, String)("Port", connection.Port.ToString)
            }
            Return New Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))(keyValueList)
        End Function

#End Region

#Region " DEVICE EXTENSIONS "

        ''' <summary> Returns a Captions the device. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device"> The device. </param>
        ''' <returns> A String. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function Caption(ByVal device As Dln.Device) As String
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Return $"{device.HardwareType}.{device.ID}.{device.SN}"
        End Function

        ''' <summary> Converts a value to a version. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> value as a Version. </returns>
        Private Function ToVersion(ByVal value As UInteger) As Version
            Return New Version(CInt((value >> 24) And &HFF), CInt((value >> 16) And &HFF), CInt((value >> 8) And &HFF), CInt(value And &HFF))
        End Function

        ''' <summary> Converts a value to a version string. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> value as a String. </returns>
        Private Function ToVersionString(value As UInteger) As String
            Return $"{(value >> 24) And &HFF}.{((value >> 16) And &HFF)}.{((value >> 8) And &HFF)}.{(value And &HFF)}"
        End Function

        ''' <summary> Converts a value to a version string. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device">        The device. </param>
        ''' <param name="deviceVersion"> The device version. </param>
        ''' <returns> value as a String. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ToVersionString(ByVal device As Dln.Device, ByVal deviceVersion As DeviceVersion) As String
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Select Case deviceVersion
                Case Diolan.DeviceVersion.Firmware
                    Return Methods.ToVersionString(device.FirmwareVersion)
                Case Diolan.DeviceVersion.Hardware
                    Return Methods.ToVersionString(device.HardwareVersion)
                Case Diolan.DeviceVersion.Library
                    Return Methods.ToVersionString(device.LibraryVersion)
                Case Diolan.DeviceVersion.Server
                    Return Methods.ToVersionString(device.ServerVersion)
                Case Else
                    Return Methods.ToVersionString(device.HardwareVersion)
            End Select
        End Function

        ''' <summary> Version number. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device">        The device. </param>
        ''' <param name="deviceVersion"> The device version. </param>
        ''' <returns> A Version. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ToVersion(ByVal device As Dln.Device, ByVal deviceVersion As DeviceVersion) As Version
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Select Case deviceVersion
                Case Diolan.DeviceVersion.Firmware
                    Return Methods.ToVersion(device.FirmwareVersion)
                Case Diolan.DeviceVersion.Hardware
                    Return Methods.ToVersion(device.HardwareVersion)
                Case Diolan.DeviceVersion.Library
                    Return Methods.ToVersion(device.LibraryVersion)
                Case Diolan.DeviceVersion.Server
                    Return Methods.ToVersion(device.ServerVersion)
                Case Else
                    Return Methods.ToVersion(device.HardwareVersion)
            End Select
        End Function

        ''' <summary> Supports. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device">   The device. </param>
        ''' <param name="modality"> The modality. </param>
        ''' <returns> <c>true</c> if the device supports the modality; otherwise <c>false</c> </returns>
        <Extension(), CLSCompliant(False)>
        Public Function Supports(ByVal device As Dln.Device, ByVal modality As DeviceModalities) As Boolean
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Select Case modality
                Case DeviceModalities.Adc
                    Return device.Adc.Ports.Any
                Case DeviceModalities.Gpio
                    Return device.Gpio.Pins.Any
                Case DeviceModalities.I2CMaster
                    Return device.I2cMaster.Ports.Any
                Case DeviceModalities.I2CSlave
                    Return device.I2cSlave.Ports.Any
                Case DeviceModalities.Leds
                    Return device.Led.Leds.Any
                Case DeviceModalities.PulseCounter
                    Return device.PulseCounter.Ports.Any
                Case DeviceModalities.Pwm
                    Return device.Pwm.Ports.Any
                Case DeviceModalities.SpiMaster
                    Return device.SpiMaster.Ports.Any
                Case DeviceModalities.SpiSlave
                    Return device.SpiSlave.Ports.Any
                Case DeviceModalities.Uart
                    Return device.Uart.Ports.Any
                Case Else
                    Return False
            End Select
        End Function

        ''' <summary> Supports any. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device">     The device. </param>
        ''' <param name="modalities"> The modalities. </param>
        ''' <returns> <c>true</c> if the device supports any modality; otherwise <c>false</c> </returns>
        <Extension(), CLSCompliant(False)>
        Public Function SupportsAny(ByVal device As Dln.Device, ByVal modalities As DeviceModalities) As Boolean
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Dim affirmative As Boolean = False
            For Each modality As DeviceModalities In [Enum].GetValues(GetType(DeviceModalities))
                If (modalities And modality) <> 0 AndAlso device.Supports(modality) Then
                    affirmative = True
                    Exit For
                End If
            Next
            Return affirmative
        End Function

        ''' <summary> Device information. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device"> The device. </param>
        ''' <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function DeviceInfo(ByVal device As Dln.Device) As Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Dim keyValueList As New List(Of KeyValuePair(Of String, String)) From {
                New KeyValuePair(Of String, String)("Hardware Type", device.HardwareType.ToString),
                New KeyValuePair(Of String, String)("Id", device.ID.ToString),
                New KeyValuePair(Of String, String)("S/N", device.SN.ToString),
                New KeyValuePair(Of String, String)("hardware Value", CInt(device.HardwareType).ToString),
                New KeyValuePair(Of String, String)("Firmware Version", device.ToVersionString(DeviceVersion.Firmware)),
                New KeyValuePair(Of String, String)("Hardware Version", device.ToVersionString(DeviceVersion.Hardware)),
                New KeyValuePair(Of String, String)("Library Version", device.ToVersionString(DeviceVersion.Library)),
                New KeyValuePair(Of String, String)("Server Version", device.ToVersionString(DeviceVersion.Server))
            }
            Return New Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))(keyValueList)
        End Function

        ''' <summary> Modality information. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ports"> The ports. </param>
        ''' <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ModalityInfo(ByVal ports As Dln.Adc.Ports) As Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
            If ports Is Nothing Then Throw New ArgumentNullException(NameOf(ports))
            Dim keyValueList As New List(Of KeyValuePair(Of String, String)) From {
                New KeyValuePair(Of String, String)("Adc Ports", ports.Count.ToString)
            }

            If ports.Any Then
                Dim i As Integer = 0
                For Each port As Dln.Adc.Port In ports
                    i += 1
                    keyValueList.Add(New KeyValuePair(Of String, String)($"Adc Port {i} Enabled", $"{CInt(port.Enabled):true;0;false}"))
                    keyValueList.Add(New KeyValuePair(Of String, String)($"Adc Port {i} Channels", port.Channels.Count.ToString))
                    keyValueList.Add(New KeyValuePair(Of String, String)($"Adc Port {i} Resolution", port.Resolution.ToString))
                Next
            End If
            Return New Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))(keyValueList)
        End Function

        ''' <summary> Modality information. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ports"> The ports. </param>
        ''' <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ModalityInfo(ByVal ports As Dln.PulseCounter.Ports) As Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
            If ports Is Nothing Then Throw New ArgumentNullException(NameOf(ports))
            Dim keyValueList As New List(Of KeyValuePair(Of String, String)) From {
                New KeyValuePair(Of String, String)("Pulse Counter Ports", ports.Count.ToString)
            }

            If ports.Any Then
                Dim i As Integer = 0
                For Each port As Dln.PulseCounter.Port In ports
                    i += 1
                    keyValueList.Add(New KeyValuePair(Of String, String)($"P/C Port {i} Enabled", $"{CInt(port.Enabled):true;0;false}"))
                    keyValueList.Add(New KeyValuePair(Of String, String)($"P/C Port {i} Resolution", port.Resolution.ToString))
                    keyValueList.Add(New KeyValuePair(Of String, String)($"P/C Port {i} Mode", port.Mode.ToString))
                Next
            End If
            Return New Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))(keyValueList)
        End Function

        ''' <summary> Modality information. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ports"> The ports. </param>
        ''' <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ModalityInfo(ByVal ports As Dln.Pwm.Ports) As Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
            If ports Is Nothing Then Throw New ArgumentNullException(NameOf(ports))
            Dim keyValueList As New List(Of KeyValuePair(Of String, String)) From {
                New KeyValuePair(Of String, String)("PWM Ports", ports.Count.ToString)
            }

            If ports.Any Then
                Dim i As Integer = 0
                For Each port As Dln.Pwm.Port In ports
                    i += 1
                    keyValueList.Add(New KeyValuePair(Of String, String)($"PWM Port {i} Enabled", $"{CInt(port.Enabled):true;0;false}"))
                    keyValueList.Add(New KeyValuePair(Of String, String)($"PWM Port {i} Channels", port.Channels.Count.ToString))
                    keyValueList.Add(New KeyValuePair(Of String, String)($"PWM Port {i} Max Frequency", port.MaxFrequency.ToString))
                Next
            End If
            Return New Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))(keyValueList)
        End Function

        ''' <summary> Modality information. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device"> The device. </param>
        ''' <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function ModalityInfo(ByVal device As Dln.Device) As Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Dim keyValueList As New List(Of KeyValuePair(Of String, String)) From {
                New KeyValuePair(Of String, String)("Gpio Pins", device.Gpio.Pins.Count.ToString),
                New KeyValuePair(Of String, String)("I2c Master Ports", device.I2cMaster.Ports.Count.ToString),
                New KeyValuePair(Of String, String)("I2c Slave Ports", device.I2cSlave.Ports.Count.ToString),
                New KeyValuePair(Of String, String)("Leds", device.Led.Leds.Count.ToString),
                New KeyValuePair(Of String, String)("SPI Flash Ports", device.SpiFlash.Ports.Count.ToString),
                New KeyValuePair(Of String, String)("SPI Master Ports", device.SpiMaster.Ports.Count.ToString),
                New KeyValuePair(Of String, String)("SPI Slave Ports", device.SpiSlave.Ports.Count.ToString)
            }
            keyValueList.AddRange(device.Adc.Ports.ModalityInfo)
            keyValueList.AddRange(device.PulseCounter.Ports.ModalityInfo)
            keyValueList.AddRange(device.Pwm.Ports.ModalityInfo)
            Return New Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))(keyValueList)
        End Function

        ''' <summary> Server information. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="device"> The device. </param>
        ''' <returns> A Collections.ObjectModel.ReadOnlyCollection(Of. </returns>
        <Extension(), CLSCompliant(False), Obsolete("Not supported rev 3.0.1")>
        Public Function ServerInfo(ByVal device As Dln.Device) As Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))
            If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
            Dim keyValueList As New List(Of KeyValuePair(Of String, String))
            Dim uuid As Byte() = Array.Empty(Of Byte)()
            Dim deviceHandle As UShort
            Dim macAddress As Byte() = Array.Empty(Of Byte)()
            Dim ipAddress As Byte() = Array.Empty(Of Byte)()
            Dim subnetMask As Byte() = Array.Empty(Of Byte)()
            Dim gatewayIp As Byte() = Array.Empty(Of Byte)()
            Dim port As UShort
            device.GetSrvParams(uuid, deviceHandle, macAddress, ipAddress, subnetMask, gatewayIp, port)
            keyValueList.Add(New KeyValuePair(Of String, String)("UUID", New System.Net.IPAddress(uuid).ToString))
            keyValueList.Add(New KeyValuePair(Of String, String)("IP Address", New System.Net.IPAddress(ipAddress).ToString))
            Return New Collections.ObjectModel.ReadOnlyCollection(Of KeyValuePair(Of String, String))(keyValueList)
        End Function

#End Region

#Region " GPIO PIN EXTENSIONS "

        ''' <summary> Input value. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pin"> The pin. </param>
        ''' <returns> A Unsigned long. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function InputValue(ByVal pin As Dln.Gpio.Pin) As PinValue
            If pin Is Nothing Then Throw New ArgumentNullException(NameOf(pin))
            Return CType(pin.Value, PinValue)
        End Function

        ''' <summary> Output value getter. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pin"> The pin. </param>
        ''' <returns> A Unsigned long. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function OutputValueGetter(ByVal pin As Dln.Gpio.Pin) As PinValue
            If pin Is Nothing Then Throw New ArgumentNullException(NameOf(pin))
            Return CType(pin.OutputValue, PinValue)
        End Function

        ''' <summary> Output value setter. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pin">   The pin. </param>
        ''' <param name="value"> The value. </param>
        ''' <returns> A Unsigned long. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function OutputValueSetter(ByVal pin As Dln.Gpio.Pin, ByVal value As PinValue) As PinValue
            If pin Is Nothing Then Throw New ArgumentNullException(NameOf(pin))
            pin.OutputValue = CInt(value)
            Return Methods.OutputValueGetter(pin)
        End Function

        ''' <summary> Configures the destination pin based on the source pin configuration. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">      Source for the. </param>
        ''' <param name="destination"> Destination for the. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub Configure(ByVal source As Dln.Gpio.Pin, ByVal destination As Dln.Gpio.Pin)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If destination Is Nothing Then Throw New ArgumentNullException(NameOf(destination))
            destination.Enabled = source.Enabled
            If destination.Restrictions.DebounceEnabled <> Dln.Restriction.NotSupported Then
                destination.DebounceEnabled = source.DebounceEnabled
            End If
            If destination.Restrictions.Direction <> Dln.Restriction.NotSupported Then
                destination.Direction = source.Direction
            End If
            If destination.Restrictions.EventType <> Dln.Restriction.NotSupported Then
                destination.SetEventConfiguration(source.EventType, source.EventPeriod)
            End If

            If destination.Restrictions.OpendrainEnabled <> Dln.Restriction.NotSupported Then
                destination.OpendrainEnabled = source.OpendrainEnabled
            End If
            If destination.Restrictions.PulldownEnabled <> Dln.Restriction.NotSupported Then
                destination.PulldownEnabled = source.PulldownEnabled
            End If
            If destination.Restrictions.PullupEnabled <> Dln.Restriction.NotSupported Then
                destination.PullupEnabled = source.PullupEnabled
            End If
        End Sub

#End Region

#Region " GPIO PINS EXTENSIONS "

        ''' <summary> Input value. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins"> The pins. </param>
        ''' <param name="mask"> The mask. </param>
        ''' <returns> A Unsigned long. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function InputValue(ByVal pins As Dln.Gpio.Pins, ByVal mask As ULong) As ULong
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            Dim value As ULong = 0
            Dim pinValue As ULong = 1
            Dim pinNumber As Integer = 0
            Do While mask > 0 AndAlso pinNumber >= 0 AndAlso pinNumber < pins.Count
                If (mask And CULng(1)) > 0 Then
                    If pins.Item(pinNumber).Value = 1 Then
                        value += pinValue
                    End If
                    pinValue <<= 1
                End If
                pinNumber += 1
                mask >>= 1
            Loop
            Return value
        End Function

        ''' <summary> Output value getter. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins"> The pins. </param>
        ''' <param name="mask"> The mask. </param>
        ''' <returns> A Unsigned long. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function OutputValueGetter(ByVal pins As Dln.Gpio.Pins, ByVal mask As ULong) As ULong
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            Dim value As ULong = 0
            Dim pinValue As ULong = 1
            Dim pinNumber As Integer = 0
            Do While mask > 0 AndAlso pinNumber >= 0 AndAlso pinNumber < pins.Count
                If (mask And CULng(1)) > 0 Then
                    If pins.Item(pinNumber).OutputValue = 1 Then
                        value += pinValue
                    End If
                    pinValue <<= 1
                End If
                pinNumber += 1
                mask >>= 1
            Loop
            Return value
        End Function

        ''' <summary> First pin number. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="mask"> The mask. </param>
        ''' <returns> An Integer; -1 if mask is 0. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function FirstPinNumber(ByVal mask As ULong) As Integer
            Dim pinNumber As Integer = -1
            If mask > 0 Then pinNumber = 0
            Do While mask > 0
                If (mask And CULng(1)) > 0 Then
                    Exit Do
                Else
                    pinNumber += 1
                    mask >>= 1
                End If
            Loop
            Return pinNumber
        End Function

        ''' <summary> Output value setter. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins">  The pins. </param>
        ''' <param name="mask">  The mask. </param>
        ''' <param name="value"> The value. </param>
        ''' <returns> A Unsigned long. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function OutputValueSetter(ByVal pins As Dln.Gpio.Pins, ByVal mask As ULong, ByVal value As ULong) As ULong
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            Dim pinNumber As Integer = 0
            Do While mask > 0 AndAlso pinNumber >= 0 AndAlso pinNumber < pins.Count
                If (mask And CULng(1)) > 0 Then
                    pins.Item(pinNumber).OutputValue = CInt(value And CULng(1))
                    value >>= 1
                End If
                pinNumber += 1
                mask >>= 1
            Loop
            Return Methods.OutputValueGetter(pins, mask)
        End Function

        ''' <summary> Configures the pins based on the specified pin configuration. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins">   The pins. </param>
        ''' <param name="mask">   The mask. </param>
        ''' <param name="source"> Source for the. </param>
        <Extension(), CLSCompliant(False)>
        Public Sub Configure(ByVal pins As Dln.Gpio.Pins, ByVal mask As ULong, ByVal source As Dln.Gpio.Pin)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            For Each i As Integer In pins.SelectMulti(mask)
                Methods.Configure(source, pins(i))
            Next
        End Sub

        ''' <summary> Selects pins using the mask to index the pins. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins"> The pins. </param>
        ''' <param name="mask"> The mask. </param>
        ''' <returns> A list of. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function SelectPins(ByVal pins As Dln.Gpio.Pins, ByVal mask As ULong) As ReadOnlyPinCollection
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            Dim l As New List(Of Dln.Gpio.Pin)
            For Each pinNumber As Integer In pins.SelectMulti(mask)
                l.Add(pins.Item(pinNumber))
            Next
            Return New ReadOnlyPinCollection(l)
        End Function

        ''' <summary> Selects pin numbers using the mask to index the pins. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins"> The pins. </param>
        ''' <param name="mask"> The mask. </param>
        ''' <returns> A list of. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function SelectMulti(ByVal pins As Dln.Gpio.Pins, ByVal mask As ULong) As Collections.ObjectModel.ReadOnlyCollection(Of Integer)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            Dim l As New List(Of Integer)
            Dim pinNumber As Integer = 0
            Do While mask > 0 AndAlso pinNumber >= 0 AndAlso pinNumber < pins.Count
                If (mask And CULng(1)) > 0 Then
                    l.Add(pinNumber)
                End If
                pinNumber += 1
                mask >>= 1
            Loop
            Return New Collections.ObjectModel.ReadOnlyCollection(Of Integer)(l)
        End Function

        ''' <summary> Bit count. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="mask"> The mask. </param>
        ''' <returns> An Integer. </returns>
        <Extension(), CLSCompliant(False)>
        Public Function BitCount(ByVal mask As ULong) As Integer
            Dim result As Integer = 0
            Do While mask > 0
                If (mask And CULng(1)) > 0 Then
                    result += 1
                End If
                mask >>= 1
            Loop
            Return result
        End Function

#End Region

    End Module

End Namespace

''' <summary> Values that represent device versions. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Enum DeviceVersion

    ''' <summary> An enum constant representing the hardware option. </summary>
    <Description("Hardware")> Hardware

    ''' <summary> An enum constant representing the firmware option. </summary>
    <Description("Firmware")> Firmware

    ''' <summary> An enum constant representing the server option. </summary>
    <Description("Server")> Server

    ''' <summary> An enum constant representing the library option. </summary>
    <Description("Library")> Library
End Enum

''' <summary> Values that represent LED color indexes for DLN-4ME. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Enum LedColorIndex

    <Description("Read")> Red = 0

    <Description("Green")> Green = 1
End Enum

''' <summary> Collection of read only pins. </summary>
''' <remarks> David, 2020-10-24. </remarks>
<CLSCompliant(False)>
Public Class ReadOnlyPinCollection
    Inherits Collections.ObjectModel.ReadOnlyCollection(Of Dln.Gpio.Pin)

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> class that is a read-
    ''' only wrapper around the specified list.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="pins"> The list to wrap. </param>
    Public Sub New(ByVal pins As Global.System.Collections.Generic.IList(Of Global.Dln.Gpio.Pin))
        MyBase.New(pins)
    End Sub
End Class

''' <summary> Values that represent device modalities. </summary>
''' <remarks> David, 2020-10-24. </remarks>
<Flags()>
Public Enum DeviceModalities

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not specified")> None

    <Description("A/D Conversion")> Adc = 1

    <Description("General purpose I/O")> Gpio = 2

    <Description("Pulse Counter")> PulseCounter = 4

    <Description("Pulse Width Modulation")> Pwm = 8

    <Description("I2c Master")> I2CMaster = 16

    <Description("I2C Slave")> I2CSlave = 32

    <Description("SPI Master")> SpiMaster = 64

    <Description("SPI Slave")> SpiSlave = 128

    <Description("Serial")> Uart = 256

    <Description("Leds")> Leds = 512
End Enum
