﻿Imports System.ComponentModel
#Region " GPIO "

''' <summary> Values that represent pin directions. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Enum PinDirection

    <Description("Input")> Input = 0

    <Description("Output")> Output = 1
End Enum

''' <summary> Values that represent pin values. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Enum PinValue

    <Description("Logic 0")> LogicZero = 0

    <Description("Logic 1")> LogicOne = 1
End Enum


#End Region

