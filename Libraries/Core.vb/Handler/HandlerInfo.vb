Imports System.ComponentModel

Namespace Gpio

    ''' <summary> Information about the handler. </summary>
    ''' <remarks>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2016-09-09 </para>
    ''' </remarks>
    Public Class HandlerInfo

        ''' <summary> Gets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name As String

        ''' <summary> Gets the bin mask. </summary>
        ''' <value> The bin mask. </value>
        Public Property BinMask As Long

        ''' <summary> Gets the end of test delay after bin onset. </summary>
        ''' <value> The bin to end of test onset delay. </value>
        Public Property BinEndTestOnsetDelay As TimeSpan

        ''' <summary> Gets the end test pin number. </summary>
        ''' <value> The end test pin number. </value>
        Public Property EndTestPinNumber As Integer

        ''' <summary> Gets the start test pin number. </summary>
        ''' <value> The start test pin number. </value>
        Public Property StartTestPinNumber As Integer

        ''' <summary> Gets the active logic. </summary>
        ''' <value> The active logic. </value>
        Public Property ActiveLogic As ActiveLogic

        ''' <summary> Gets the end test mode. </summary>
        ''' <value> The end test mode. </value>
        Public Property EndTestMode As EndTestMode

        ''' <summary> Gets the open drain enabled. </summary>
        ''' <value> The open drain enabled. </value>
        Public Property OpenDrainEnabled As Boolean

        ''' <summary> Gets the pull-up enabled. </summary>
        ''' <value> The pull-up enabled. </value>
        Public Property PullupEnabled As Boolean

#Region " NO HAU HANDLER "

        ''' <summary> Information describing the no hau handler. </summary>
        Private Shared _NoHauHandlerInfo As HandlerInfo

        ''' <summary> Gets information describing the No Hau handler. </summary>
        ''' <value> Information describing the No Hau handler. </value>
        Public Shared ReadOnly Property NoHauHandlerInfo As HandlerInfo
            Get
                If HandlerInfo._NoHauHandlerInfo Is Nothing Then
                    HandlerInfo._NoHauHandlerInfo = New HandlerInfo With {
                        .Name = "No Hau",
                        .BinMask = Convert.ToInt32("1100", 2),
                        .BinEndTestOnsetDelay = TimeSpan.FromTicks(TimeSpan.TicksPerMillisecond * 5),
                        .StartTestPinNumber = 0,
                        .EndTestPinNumber = 1,
                        .ActiveLogic = ActiveLogic.ActiveHigh,
                        .PullupEnabled = True,
                        .OpenDrainEnabled = False,
                        .EndTestMode = EndTestMode.ActiveTest
                    }
                End If
                Return HandlerInfo._NoHauHandlerInfo
            End Get
        End Property

        ''' <summary> Information describing the no hau emulator. </summary>
        Private Shared _NoHauEmulatorInfo As HandlerInfo

        ''' <summary> Gets information describing the No Hau handler. </summary>
        ''' <value> Information describing the No Hau handler. </value>
        Public Shared ReadOnly Property NoHauEmulatorInfo As HandlerInfo
            Get
                If HandlerInfo._NoHauEmulatorInfo Is Nothing Then
                    HandlerInfo._NoHauEmulatorInfo = New HandlerInfo With {
                        .Name = "No Hau",
                        .BinMask = Convert.ToInt32("11000000", 2),
                        .BinEndTestOnsetDelay = TimeSpan.FromTicks(TimeSpan.TicksPerMillisecond * 5),
                        .StartTestPinNumber = 4,
                        .EndTestPinNumber = 5,
                        .ActiveLogic = ActiveLogic.ActiveHigh,
                        .PullupEnabled = True,
                        .OpenDrainEnabled = False,
                        .EndTestMode = EndTestMode.ActiveTest
                    }
                End If
                Return HandlerInfo._NoHauEmulatorInfo
            End Get
        End Property

#End Region

#Region " AETRIUM HANDLER "

        ''' <summary> Information describing the aetrium handler. </summary>
        Private Shared _AetriumHandlerInfo As HandlerInfo

        ''' <summary> Gets information describing the Aetrium handler. </summary>
        ''' <value> Information describing the no how handler. </value>
        Public Shared ReadOnly Property AetriumHandlerInfo As HandlerInfo
            Get
                If HandlerInfo._AetriumHandlerInfo Is Nothing Then
                    HandlerInfo._AetriumHandlerInfo = New HandlerInfo With {
                        .Name = "Aetrium",
                        .BinMask = Convert.ToInt32("1100", 2),
                        .BinEndTestOnsetDelay = TimeSpan.FromTicks(TimeSpan.TicksPerMillisecond * 4),
                        .StartTestPinNumber = 0,
                        .EndTestPinNumber = 1,
                        .ActiveLogic = ActiveLogic.ActiveLow,
                        .PullupEnabled = True,
                        .OpenDrainEnabled = False,
                        .EndTestMode = EndTestMode.ActiveBin
                    }
                End If
                Return HandlerInfo._AetriumHandlerInfo
            End Get
        End Property

        ''' <summary> Information describing the aetrium emulator. </summary>
        Private Shared _AetriumEmulatorInfo As HandlerInfo

        ''' <summary> Gets information describing the Aetrium handler. </summary>
        ''' <value> Information describing the no how handler. </value>
        Public Shared ReadOnly Property AetriumEmulatorInfo As HandlerInfo
            Get
                If HandlerInfo._AetriumEmulatorInfo Is Nothing Then
                    HandlerInfo._AetriumEmulatorInfo = New HandlerInfo With {
                        .Name = "Aetrium",
                        .BinMask = Convert.ToInt32("11000000", 2),
                        .BinEndTestOnsetDelay = TimeSpan.FromTicks(TimeSpan.TicksPerMillisecond * 4),
                        .StartTestPinNumber = 4,
                        .EndTestPinNumber = 5,
                        .ActiveLogic = ActiveLogic.ActiveLow,
                        .PullupEnabled = True,
                        .OpenDrainEnabled = False,
                        .EndTestMode = EndTestMode.ActiveBin
                    }
                End If
                Return HandlerInfo._AetriumEmulatorInfo
            End Get
        End Property

#End Region

    End Class

    ''' <summary> Values that represent supported handlers. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Enum SupportedHandler

        ''' <summary> Aetrium handler. </summary>
        <Description("Aetrium")> Aetrium

        ''' <summary> No Hau Handler. </summary>
        <Description("No Hau")> NoHau
    End Enum

    ''' <summary> Values that represent end test modes. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Enum EndTestMode

        ''' <summary> The end of test signal remains low during the test and turns on after turning on the binning bits. </summary>
        <Description("Active Bin")> ActiveBin

        ''' <summary> The end of test signal turns high during the test and turns low after turning on the binning bits. </summary>
        <Description("Active Test")> ActiveTest
    End Enum

End Namespace
