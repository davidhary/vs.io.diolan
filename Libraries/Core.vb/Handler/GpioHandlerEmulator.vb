Imports isr.Diolan.SubsystemExtensions
Namespace Gpio

    ''' <summary> Gpio handler emulator. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    <CLSCompliant(False)>
    Public Class GpioHandlerEmulator
        Inherits GpioHandlerDriverBase

#Region " CONSTRUCTOR "

        ''' <summary>
        ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        ''' resources.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
        '''                          release only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not Me.IsDisposed Then
                    Me.ReleasePins()
                End If
            Catch ex As Exception
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        ''' <remarks>
        ''' This is handled at the top level class in case the inheriting class added property over the
        ''' base class.
        ''' </remarks>
        Public Overrides Sub PublishAll()
            For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                Me.AsyncNotifyPropertyChanged(p.Name)
            Next
        End Sub

#End Region

#Region " CONFIGURE "

        ''' <summary> Configures start test signal. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected Overrides Sub ConfigureStartTest(ByVal pins As Dln.Gpio.Pins)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            If pins.Count = 0 Then Throw New InvalidOperationException("GPIO Port has no pins to configure for the handler")
            Me.StartTestPin = New GpioOutputPin(pins, Me.StartTestPinNumber, Me.ActiveLogic)
        End Sub

        ''' <summary> Configures end test signal. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected Overrides Sub ConfigureEndTest(ByVal pins As Dln.Gpio.Pins)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            If pins.Count = 0 Then Throw New InvalidOperationException("GPIO Port has no pins to configure for the handler")
            Me.EndTestPin = New GpioInputPin(pins, Me.EndTestPinNumber, Me.ActiveLogic)
        End Sub

        ''' <summary> Registers the End test pin events. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Sub RegisterEndTestPinEvents()
            If Not Me.RegisteredEndTestPinEvents Then
                Dim pin As Dln.Gpio.Pin = Me.Pins(Me.EndTestPin.PinNumber)
                AddHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                pin.SetEventConfiguration(Dln.Gpio.EventType.Change, 0)
                MyBase.OnRegisteringEndTestPinEvents()
            End If
        End Sub

        ''' <summary> Configures bin port. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected Overrides Sub ConfigureBinPort(ByVal pins As Dln.Gpio.Pins)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            If pins.Count = 0 Then Throw New InvalidOperationException("GPIO Port has no pins to configure for the handler")
            Me.BinPort = New GpioInputPort(pins, Me.BinMask, Me.ActiveLogic)
        End Sub

        ''' <summary> Registers the bin port pins events. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Sub RegisterBinPortPinEvents()
            If Not Me.RegisteredBinPortPinEvents Then
                Dim pin As Dln.Gpio.Pin
                For Each i As Integer In Me.Pins.SelectMulti(CULng(Me.BinPort.Mask))
                    pin = Me.Pins(i)
                    AddHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                    pin.SetEventConfiguration(Dln.Gpio.EventType.Change, 0)
                Next
                MyBase.OnRegisteringBinPortPinEvents()
            End If
        End Sub

        ''' <summary> Executes the initialize known state action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Overrides Sub InitializeKnownState()
            Me.RegisterEndTestPinEvents()
            Me.RegisterBinPortPinEvents()
            Me.StartTestPin.LogicalValue = 0
            MyBase.InitializeKnownState()
        End Sub

#End Region

#Region " EVENTS "

        ''' <summary> Filters the condition met event for end test events. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnEndTestEventOccurred(ByVal e As Dln.Gpio.ConditionMetEventArgs)
            If Not e Is Nothing Then
                MyBase.OnEndTestEventOccurred(e)
                Select Case Me.EndTestMode
                    Case EndTestMode.ActiveBin
                        ' with active bin, end of test is received only after binning.
                        If Me.IsNotActiveState(e.Value) Then
                            ' if logical 0, start of test was enabled
                            Me.OnStartTestEnabled()
                        Else
                            ' end of test enables binning now.
                            Me.OnEndTestSent()
                            ' now read the bin value
                            Me.BinValue = Me.ReadEndTest
                            Me.AcknowledgeEndTest()
                        End If
                    Case EndTestMode.ActiveTest
                        ' with active test, end of test is turned on by the handler after receiving
                        ' start of test. 
                        If Me.IsActiveState(e.Value) Then
                            ' if the state is active, it means the start test was acknowledged.
                            Me.OnStartTestReceived()
                        Else
                            Me.OnEndTestSent()
                            ' now read the bin value
                            Me.BinValue = Me.ReadEndTest
                            Me.AcknowledgeEndTest()
                        End If
                    Case Else
                        Debug.Assert(Not Debugger.IsAttached, "Unhandled end test mode",
                                     "End Test Mode {0} could not be handled at Emulator On End Test Event", Me.EndTestMode)
                End Select
            End If
        End Sub

#End Region

#Region " EMULATOR ACTIONS "

        ''' <summary> Tests enable start. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overrides Sub OnStartTestEnabled()
            ' Turn off the start test signal
            Me.StartTestPin.LogicalValue = 0
            MyBase.OnStartTestEnabled()
            Me.StartTestLogicalValue = Me.StartTestPin.LogicalValue = 1
        End Sub

        ''' <summary> Output start test signal. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Sub OutputStartTest()

            ' TO_DO?! Do we need this?
            ' check state and input value in case the sequence was not started or went awry.
            ' If Me.EndTestPin.LogicalValue = 0 OrElse Me.State <> HandlerState.StartTestEnabled Then
            '     Me.OnStartTestEnabled()
            '     My.MyLibrary.DoEventsDelay(10)
            ' End If

            ' turn on the start test pin
            Me.StartTestPin.LogicalValue = 1
            If Me.EndTestMode = EndTestMode.ActiveBin Then
                Me.OnStartTestReceived()
            ElseIf Me.EndTestMode = EndTestMode.ActiveTest Then
                ' with active test, the end of test signal will move to the 
                ' acknowledge start of test.
            Else


                Debug.Assert(Not Debugger.IsAttached, "Unhandled end test mode",
                             "End Test Mode {0} could not be handled at Emulator Output Start Test", Me.EndTestMode)
            End If
            Me.StartTestLogicalValue = Me.StartTestPin.LogicalValue = 1
        End Sub

        ''' <summary> Tests read end. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <returns> The end test. </returns>
        Public Function ReadEndTest() As Long
            Dim value As Long = Me.BinPort.MaskedValue
            ' turn off the start test signal, to acknowledge reading the end test signal.
            Me.StartTestPin.LogicalValue = 0
            Me.StartTestLogicalValue = Me.StartTestPin.LogicalValue = 1
            Return value
        End Function

        ''' <summary> Executes the start test received action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overrides Sub OnStartTestReceived()
            MyBase.OnStartTestReceived()
        End Sub

        ''' <summary> Acknowledges end test reception. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Private Sub AcknowledgeEndTest()
            ' turn off the start test signal, to acknowledge reading the end test signal.
            Me.StartTestPin.LogicalValue = 0
            Me.OnEndTestAcknowledged()
            Me.StartTestLogicalValue = Me.StartTestPin.LogicalValue = 1
        End Sub

#End Region

    End Class

End Namespace
