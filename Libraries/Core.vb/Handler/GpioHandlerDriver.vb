Imports isr.Core.DispatcherExtensions
Namespace Gpio

    ''' <summary> Gpio handler driver. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    <CLSCompliant(False)>
    Public Class GpioHandlerDriver
        Inherits GpioHandlerDriverBase

#Region " CONSTRUCTOR "

        ''' <summary>
        ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        ''' resources.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
        '''                          release only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not Me.IsDisposed Then
                    Me.ReleasePins()
                End If
            Catch ex As Exception
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        ''' <remarks>
        ''' This is handled at the top level class in case the inheriting class added property over the
        ''' base class.
        ''' </remarks>
        Public Overrides Sub PublishAll()
            For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                Me.AsyncNotifyPropertyChanged(p.Name)
            Next
        End Sub

#End Region

#Region " CONFIGURE "

        ''' <summary> Configures start test signal. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected Overrides Sub ConfigureStartTest(ByVal pins As Dln.Gpio.Pins)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            If pins.Count = 0 Then Throw New InvalidOperationException("GPIO Port has no pins to configure for the handler")
            Me.StartTestPin = New GpioInputPin(pins, Me.StartTestPinNumber, Me.ActiveLogic)
        End Sub

        ''' <summary> Registers the start test pin events. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Sub RegisterStartTestPinEvents()
            If Not Me.RegisteredStartTestPinEvents Then
                Dim pin As Dln.Gpio.Pin = Me.Pins(Me.StartTestPin.PinNumber)
                AddHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                pin.SetEventConfiguration(Dln.Gpio.EventType.Change, 0)
                MyBase.OnRegisteringStartTestPinEvents()
            End If
        End Sub

        ''' <summary> Configures end test signal. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected Overrides Sub ConfigureEndTest(ByVal pins As Dln.Gpio.Pins)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            If pins.Count = 0 Then Throw New InvalidOperationException("GPIO Port has no pins to configure for the handler")
            Me.EndTestPin = New GpioOutputPin(pins, Me.EndTestPinNumber, Me.ActiveLogic)
        End Sub

        ''' <summary> Configures bin port. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected Overrides Sub ConfigureBinPort(ByVal pins As Dln.Gpio.Pins)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            If pins.Count = 0 Then Throw New InvalidOperationException("GPIO Port has no pins to configure for the handler")
            Me.BinPort = New GpioOutputPort(pins, Me.BinMask, Me.ActiveLogic)
        End Sub

        ''' <summary> Executes the initialize known state action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Overrides Sub InitializeKnownState()
            ' this needs to happen after the emulator is configured.
            Me.RegisterStartTestPinEvents()
            Me.EndTestPin.LogicalValue = 0
            MyBase.InitializeKnownState()
        End Sub

#End Region

#Region " EVENTS "

        ''' <summary> Raises the condition met event. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnStartTestEventOccurred(ByVal e As Dln.Gpio.ConditionMetEventArgs)
            If e IsNot Nothing Then
                MyBase.OnStartTestEventOccurred(e)
                If e.Value = Me.BitValue(1) Then
                    ' if logical 1, start of test was received.
                    Me.OnStartTestReceived()
                ElseIf Me.State = HandlerState.EndTestSent Then
                    ' acknowledge is value if end of test was sent so as to prevent acknowledging when enabling start test.
                    ' if logical zero, end of test was acknowledged if using continuous duty.
                    Me.OnEndTestAcknowledged()
                End If
            End If
        End Sub

#End Region

#Region " ACTIONS "

        ''' <summary> Tests enable start. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Sub EnableStartTest()

            ' clear to start
            Me.EndTestPin.LogicalValue = 0
            Me.EndTestLogicalValue = Me.EndTestPin.LogicalValue = 1

            Me.OnStartTestEnabled()

        End Sub

        ''' <summary> Outputs the end of test sequence. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="binValue"> The bin value. </param>
        Public Sub OutputEndTest(ByVal binValue As Long)

            ' Set the bin port -- this is turned off upon receipt of Start Of Test.
            Me.BinPort.MaskedValue = binValue

            ' show the bin value
            Me.BinValue = binValue

            ' wait a bit
            isr.Core.ApplianceBase.DoEventsWait(Me.BinEndTestOnsetDelay)

            Me.OnEndTestSent()

            ' send the end of test signal.
            Select Case Me.EndTestMode
                Case EndTestMode.ActiveBin
                    ' in active bin, the EOT is logical high to signal binning
                    Me.EndTestPin.LogicalValue = 1
                Case EndTestMode.ActiveTest
                    ' in active Test, the EOT is turns from logical high to low to signal binning

                    Me.EndTestPin.LogicalValue = 0
                Case Else
                    Debug.Assert(Not Debugger.IsAttached, $"Unhandled end test mode {Me.EndTestMode} at driver On Start Test Received")
            End Select
            Me.EndTestLogicalValue = Me.EndTestPin.LogicalValue = 1

        End Sub

        ''' <summary> Executes the start test received action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overrides Sub OnStartTestReceived()
            ' clear the bin output
            Me.BinPort.MaskedValue = 0
            Select Case Me.EndTestMode
                Case EndTestMode.ActiveBin
                    ' in active bin, the EOT is logical low during the test.
                    Me.EndTestPin.LogicalValue = 0
                Case EndTestMode.ActiveTest
                    ' in active Test, the EOT is logical high during the test.
                    Me.EndTestPin.LogicalValue = 1
                Case Else
                    Debug.Assert(Not Debugger.IsAttached, $"Unhandled end test mode {Me.EndTestMode} at driver On Start Test Received")
            End Select
            Me.EndTestLogicalValue = Me.EndTestPin.LogicalValue = 1
            MyBase.OnStartTestReceived()
        End Sub

        ''' <summary> Executes the end test acknowledged action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overrides Sub OnEndTestAcknowledged()
            ' possibly, the end of test acknowledged does not indicate that the handler saw the Bin, in which case
            ' the bin can be left and rest after SOT is received again. 
            ' Me._BinPort.Value = 0
            MyBase.OnEndTestAcknowledged()
        End Sub

#End Region

    End Class


End Namespace

