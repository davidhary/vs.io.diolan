﻿Imports System.ComponentModel
Imports isr.Diolan.SubsystemExtensions
Imports isr.Core.EventHandlerExtensions
Imports isr.Core

Namespace Gpio

    ''' <summary> Handler driver base. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    Public MustInherit Class HandlerDriverBase
        Inherits isr.Core.Models.ViewModelBase
        Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> Default constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Sub New()
            MyBase.New()
        End Sub

#Region " DISPOSABLE SUPPORT "

        ''' <summary> Gets the disposed sentinel. </summary>
        ''' <value> The disposed sentinel. </value>
        Protected ReadOnly Property IsDisposed As Boolean

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                                            <c>False</c> to
        '''                                                                            release only
        '''                                                                            unmanaged resources
        '''                                                                            when called from
        '''                                                                            the runtime
        '''                                                                            finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overridable Sub Dispose(disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    ' dispose managed state (managed objects).
                    Me.RemovePropertyChangedEventHandlers()
                    Me.RemoveStateChangedEventHandlers()
                End If
            Finally
                Me._IsDisposed = True
            End Try
        End Sub

        ''' <summary>
        ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        ''' resources.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Me.Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

#End Region

#End Region

#Region " STATE "

        ''' <summary> State of the previous. </summary>
        Private _PreviousState As HandlerState

        ''' <summary> Gets the state of the previous. </summary>
        ''' <value> The previous state. </value>
        Public ReadOnly Property PreviousState As HandlerState
            Get
                Return Me._PreviousState
            End Get
        End Property

        ''' <summary> The current state. </summary>
        Private _CurrentState As HandlerState

        ''' <summary> Gets the state. </summary>
        ''' <value> The state. </value>
        Public ReadOnly Property State As HandlerState
            Get
                Return Me._CurrentState
            End Get
        End Property

#Region " STATE CHANGED "

        ''' <summary> Raises the system. event. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Public Overridable Sub OnStateChanged(ByVal e As System.EventArgs)
            Me.SyncNotifyPropertyChanged(NameOf(Gpio.GpioHandlerDriverBase.State))
            Me.SyncNotifyStateChanged(e)
        End Sub

        ''' <summary> Raises the system. event. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Private Sub OnStateChanged()
            If Me.State <> Me.PreviousState Then Me.OnStateChanged(System.EventArgs.Empty)
        End Sub

        ''' <summary> Removes the StateChanged event handlers. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Sub RemoveStateChangedEventHandlers()
            Me.StateChangedEventHandlers?.RemoveAll()
        End Sub

        ''' <summary> The StateChanged event handlers. </summary>
        Private ReadOnly StateChangedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

        ''' <summary> Event queue for all listeners interested in StateChanged events. </summary>
        ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
        ''' Using a custom Raise method lets you iterate through the delegate list. 
        ''' </remarks>
        Public Custom Event StateChanged As EventHandler(Of System.EventArgs)
            AddHandler(value As EventHandler(Of System.EventArgs))
                Me.StateChangedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
            End AddHandler
            RemoveHandler(value As EventHandler(Of System.EventArgs))
                Me.StateChangedEventHandlers.RemoveValue(value)
            End RemoveHandler
            RaiseEvent(sender As Object, e As System.EventArgs)
                Me.StateChangedEventHandlers.Post(sender, e)
            End RaiseEvent
        End Event

        ''' <summary>
        ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
        ''' <see cref="StateChanged">StateChanged Event</see>.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        Protected Sub SyncNotifyStateChanged(ByVal e As System.EventArgs)
            Me.StateChangedEventHandlers.Send(Me, e)
        End Sub

#End Region

#End Region

#Region " STATE ACTIONS "

        ''' <summary> Change state. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="newState"> State of the new. </param>
        Protected Sub ChangeState(ByVal newState As HandlerState)
            Me._PreviousState = Me._CurrentState
            Me._CurrentState = newState
            Me.OnStateChanged()
        End Sub

        ''' <summary> Executes the start test enabled action. </summary>
        ''' <remarks> This is the same as cleared to start. </remarks>
        Protected Overridable Sub OnStartTestEnabled()
            Me.ChangeState(HandlerState.StartTestEnabled)
        End Sub

        ''' <summary> Executes the start test received action. </summary>
        ''' <remarks> The start test signal came in from the handler. </remarks>
        Protected Overridable Sub OnStartTestReceived()
            Me.ChangeState(HandlerState.StartTestReceived)
        End Sub

        ''' <summary> Executes the end test sent action. </summary>
        ''' <remarks> Program sent the end of test signal. </remarks>
        Protected Overridable Sub OnEndTestSent()
            Me.ChangeState(HandlerState.EndTestSent)
        End Sub

        ''' <summary> Executes the end test acknowledged action. </summary>
        ''' <remarks> Handler received the end test and responded. </remarks>
        Protected Overridable Sub OnEndTestAcknowledged()
            Me.ChangeState(HandlerState.EndTestAcknowledged)
        End Sub

#End Region

#Region " CONFIGURATION "

        ''' <summary> The name. </summary>
        Private _Name As String

        ''' <summary> Gets or sets the bin mask. </summary>
        ''' <value> The bin mask. </value>
        Public Property Name As String
            Get
                Return Me._Name
            End Get
            Set(value As String)
                Me._Name = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> The bin mask. </summary>
        Private _BinMask As Long

        ''' <summary> Gets or sets the bin mask. </summary>
        ''' <value> The bin mask. </value>
        Public Property BinMask As Long
            Get
                Return Me._BinMask
            End Get
            Set(value As Long)
                Me._BinMask = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> The bin end test onset delay. </summary>
        Private _BinEndTestOnsetDelay As TimeSpan

        ''' <summary> Gets or sets the end of test delay after bin onset. </summary>
        ''' <value> The bin to end of test onset delay. </value>
        Public Property BinEndTestOnsetDelay As TimeSpan
            Get
                Return Me._BinEndTestOnsetDelay
            End Get
            Set(value As TimeSpan)
                Me._BinEndTestOnsetDelay = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> The end test pin number. </summary>
        Private _EndTestPinNumber As Integer

        ''' <summary> Gets or sets the end test pin number. </summary>
        ''' <value> The end test pin number. </value>
        Public Property EndTestPinNumber As Integer
            Get
                Return Me._EndTestPinNumber
            End Get
            Set(value As Integer)
                Me._EndTestPinNumber = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> The start test pin number. </summary>
        Private _StartTestPinNumber As Integer

        ''' <summary> Gets or sets the Start test pin number. </summary>
        ''' <value> The Start test pin number. </value>
        Public Property StartTestPinNumber As Integer
            Get
                Return Me._StartTestPinNumber
            End Get
            Set(value As Integer)
                Me._StartTestPinNumber = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> The active logic. </summary>
        Private _ActiveLogic As ActiveLogic

        ''' <summary> Gets or sets the active logic. </summary>
        ''' <value> The active logic. </value>
        Public Property ActiveLogic As ActiveLogic
            Get
                Return Me._ActiveLogic
            End Get
            Set(value As ActiveLogic)
                Me._ActiveLogic = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> The end test mode. </summary>
        Private _EndTestMode As EndTestMode

        ''' <summary> Gets or sets the End Test Mode. </summary>
        ''' <value> The End Test Mode. </value>
        Public Property EndTestMode As EndTestMode
            Get
                Return Me._EndTestMode
            End Get
            Set(value As EndTestMode)
                Me._EndTestMode = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> True to enable, false to disable the open drain. </summary>
        Private _OpenDrainEnabled As Boolean

        ''' <summary> Gets or sets the Open Drain Enabled. </summary>
        ''' <value> The Open Drain Enabled. </value>
        Public Property OpenDrainEnabled As Boolean
            Get
                Return Me._OpenDrainEnabled
            End Get
            Set(value As Boolean)
                Me._OpenDrainEnabled = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> True to enable, false to disable the pullup. </summary>
        Private _PullupEnabled As Boolean

        ''' <summary> Gets or sets the Pull-up Enabled. </summary>
        ''' <value> The Pull-up Enabled. </value>
        Public Property PullupEnabled As Boolean
            Get
                Return Me._PullupEnabled
            End Get
            Set(value As Boolean)
                Me._PullupEnabled = value
                Me.AsyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> Executes the initialize known state action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Overridable Sub InitializeKnownState()
            Me.BinValue = 0
            Me.ChangeState(HandlerState.Idle)
            Me.PublishStateInfo()
        End Sub

        ''' <summary> Releases the pins. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overridable Sub ReleasePins()
            Me.BinCount = 0
        End Sub

        ''' <summary> Gets a value indicating whether this object is configured. </summary>
        ''' <value> <c>true</c> if this object is configured; otherwise <c>false</c> </value>
        Public Overridable ReadOnly Property IsConfigured As Boolean
            Get
                Return Me.BinCount > 0
            End Get
        End Property

        ''' <summary> Configures this object. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Sub Configure()
            Me.BinCount = CULng(Me.BinMask).BitCount
        End Sub

        ''' <summary> Applies the handler information described by value. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="value"> The value. </param>
        Public Sub ApplyHandlerInfo(ByVal value As HandlerInfo)
            If value IsNot Nothing Then
                ' turn off previous configuration
                Me.ReleasePins()
                Me.Name = value.Name
                Me.BinMask = value.BinMask
                Me.BinEndTestOnsetDelay = value.BinEndTestOnsetDelay
                Me.EndTestPinNumber = value.EndTestPinNumber
                Me.StartTestPinNumber = value.StartTestPinNumber
                Me.ActiveLogic = value.ActiveLogic
                Me.OpenDrainEnabled = value.OpenDrainEnabled
                Me.PullupEnabled = value.PullupEnabled
                Me.EndTestMode = value.EndTestMode
            End If
        End Sub

        ''' <summary> Publish all. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public MustOverride Sub PublishAll()

#End Region

#Region " STATE AND STATUS "

        ''' <summary>
        ''' Publishes this object. Required to initialize known state of the user interface.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Overridable Sub PublishStateInfo()
            Me.SyncNotifyPropertyChanged(NameOf(Gpio.GpioHandlerDriverBase.State))
            Me.SyncNotifyPropertyChanged(NameOf(Gpio.GpioHandlerDriverBase.BinValue))
            Me.SyncNotifyPropertyChanged(NameOf(Gpio.GpioHandlerDriverBase.EndTestLogicalValue))
            Me.SyncNotifyPropertyChanged(NameOf(Gpio.GpioHandlerDriverBase.StartTestLogicalValue))
        End Sub

        ''' <summary> Number of bins. </summary>
        Private _BinCount As Integer

        ''' <summary> Gets or sets the number of bins. </summary>
        ''' <value> The number of bins. </value>
        Public Property BinCount As Integer
            Get
                Return Me._BinCount
            End Get
            Protected Set(value As Integer)
                Me._BinCount = value
                Me.AsyncNotifyPropertyChanged()
                Me.AsyncNotifyPropertyChanged(NameOf(Gpio.GpioHandlerDriverBase.IsConfigured))
            End Set
        End Property

        ''' <summary> Bin value caption. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="zeroValueCaption"> The zero value caption. </param>
        ''' <returns> A String. </returns>
        Public Function BinValueCaption(ByVal zeroValueCaption As String) As String
            Return If(Me.BinValue = 0, zeroValueCaption, Convert.ToString(Me.BinValue, 2).PadLeft(Me.BinCount, "0"c))
        End Function

        ''' <summary> The bin value. </summary>
        Private _BinValue As Long

        ''' <summary> Gets or sets the bin value. </summary>
        ''' <value> The bin value. </value>
        Public Property BinValue As Long
            Get
                Return Me._BinValue
            End Get
            Set(value As Long)
                Me._BinValue = value
                Me.SyncNotifyPropertyChanged()
            End Set
        End Property

        ''' <summary> True to start test logical value. </summary>
        Private _StartTestLogicalValue As Boolean

        ''' <summary> Gets or sets the start test logical value. </summary>
        ''' <value> The start test logical value. </value>
        Public Property StartTestLogicalValue As Boolean
            Get
                Return Me._StartTestLogicalValue
            End Get
            Set(value As Boolean)
                If value <> Me.StartTestLogicalValue Then
                    Me._StartTestLogicalValue = value
                    Me.SyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> True to end test logical value. </summary>
        Private _EndTestLogicalValue As Boolean

        ''' <summary> Gets or sets the End test logical value. </summary>
        ''' <value> The End test logical value. </value>
        Public Property EndTestLogicalValue As Boolean
            Get
                Return Me._EndTestLogicalValue
            End Get
            Set(value As Boolean)
                If value <> Me.EndTestLogicalValue Then
                    Me._EndTestLogicalValue = value
                    Me.SyncNotifyPropertyChanged()
                End If
            End Set
        End Property

#End Region

    End Class

    ''' <summary> Values that represent handler states. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Enum HandlerState

        ''' <summary> Handler was not initialized. </summary>
        <Description("None -- Not specified")> None

        ''' <summary> Handler is idle. </summary>
        <Description("Idle -- Not started")> Idle

        ''' <summary> Start of test command was applied. </summary>
        <Description("Start Test Enabled")> StartTestEnabled

        ''' <summary> The handler driver received the start of test signal from the handler. </summary>
        <Description("Start Test Received")> StartTestReceived

        ''' <summary> The end of test and bin signals were sent. </summary>
        <Description("End Test Sent")> EndTestSent

        ''' <summary> The handler acknowledged receipt of the end of test signal by turning off the start
        ''' of test signal (using . </summary> 
        <Description("End Test Acknowledged")> EndTestAcknowledged
    End Enum

End Namespace
