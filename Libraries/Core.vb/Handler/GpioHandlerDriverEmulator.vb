Imports System.ComponentModel

Imports isr.Core
Imports isr.Diolan.ExceptionExtensions
Imports isr.Diolan.SubsystemExtensions
Namespace Gpio

    ''' <summary> Gpio handler driver and emulator. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-16, 5645. </para>
    ''' </remarks>
    Public Class GpioHandlerDriverEmulator
        Inherits ModalityConnectorBase

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary>
        ''' Gets the locking object to enforce thread safety when creating the singleton instance.
        ''' </summary>
        ''' <value> The sync locker. </value>
        Private Shared Property SyncLocker As New Object

        ''' <summary> Gets the instance. </summary>
        ''' <value> The instance. </value>
        Private Shared Property Instance As GpioHandlerDriverEmulator

        ''' <summary> Instantiates the class. </summary>
        ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
        ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        ''' <returns> A new or existing instance of the class. </returns>
        Public Shared Function [Get]() As GpioHandlerDriverEmulator
            If Instance Is Nothing OrElse Instance.IsDisposed Then
                SyncLock SyncLocker
                    Instance = New GpioHandlerDriverEmulator
                End SyncLock
            End If
            Return Instance
        End Function

        ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
        ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        Public Shared ReadOnly Property Instantiated() As Boolean
            Get
                SyncLock SyncLocker
                    Return Instance IsNot Nothing AndAlso Not Instance.IsDisposed
                End SyncLock
            End Get
        End Property

        ''' <summary> Dispose instance. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Shared Sub DisposeInstance()
            SyncLock SyncLocker
                If Instance IsNot Nothing AndAlso Not Instance.IsDisposed Then
                    Instance.Dispose()
                    Instance = Nothing
                End If
            End SyncLock
        End Sub

#Region " I DISPOSABLE SUPPORT "

        ''' <summary>
        ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        ''' and its child controls and optionally releases the managed resources.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        '''                                                                            <c>False</c> to
        '''                                                                            release only
        '''                                                                            unmanaged resources
        '''                                                                            when called from
        '''                                                                            the runtime
        '''                                                                            finalize. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not Me.IsDisposed AndAlso disposing Then
                    If Me._Driver IsNot Nothing Then Me._Driver.Dispose() : Me._Driver = Nothing
                    If Me._Emulator IsNot Nothing Then Me._Emulator.Dispose() : Me._Emulator = Nothing
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#End Region

#Region " SUBSYSTEM MODALITIES "

        ''' <summary> The modality. </summary>
        Private ReadOnly _Modality As DeviceModalities = DeviceModalities.Gpio

        ''' <summary> The modality. </summary>
        ''' <value> The modality. </value>
        Public Overrides ReadOnly Property Modality As DeviceModalities
            Get
                Return Me._Modality
            End Get
        End Property

        ''' <summary> Executes the modality closed action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overrides Sub OnModalityClosed()
            If Me._Emulator IsNot Nothing Then
                Me._Emulator.Dispose()
                Me._Emulator = Nothing
            End If
            If Me._Driver IsNot Nothing Then
                Me._Driver.Dispose()
                Me._Driver = Nothing
            End If
            MyBase.OnModalityClosed()
        End Sub

        ''' <summary> Executes the modality closing actions. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overrides Sub OnModalityClosing(ByVal e As System.ComponentModel.CancelEventArgs)
            If e IsNot Nothing AndAlso Not e.Cancel Then
                If Me.IsModalityOpen Then
                    ' un-register event handlers for all pins
                    For Each pin As Dln.Gpio.Pin In Me.Device.Gpio.Pins
                        RemoveHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                    Next
                End If
            End If
        End Sub

        ''' <summary> Executes the modality opening action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Protected Overrides Function TryOpenModality(ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Me._Driver = Nothing
            Me._Emulator = Nothing

            ' Get port count
            If Me.Device.Gpio.Pins.Count() = 0 Then
                ' this is already done when opening the device.
                e.RegisterFailure($"Adapter '{Me.Device.Caption}' doesn't support GPIO interface.")
            Else

                'Set current context to run thread safe events in main form thread
                Dln.Library.SynchronizationContext = System.Threading.SynchronizationContext.Current

                ' Register event handler for all pins
                For Each pin As Dln.Gpio.Pin In Me.Device.Gpio.Pins
                    AddHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                Next

                Me._Driver = New GpioHandlerDriver()
                Me._Emulator = New GpioHandlerEmulator()

            End If
            Return Not e.Failed

        End Function

        ''' <summary> Queries if a modality is open. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <returns> <c>true</c> if a modality is open; otherwise <c>false</c> </returns>
        Public Overrides Function IsModalityOpen() As Boolean
            Return Me._Driver IsNot Nothing
        End Function

#End Region

#Region " EVENT LOG "

        ''' <summary> Handler, called when the condition met event. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="sender"> The sender. </param>
        ''' <param name="e">      Condition met event information. </param>
        Private Sub ConditionMetEventHandler(ByVal sender As Object, ByVal e As Dln.Gpio.ConditionMetEventArgs)
            If e IsNot Nothing Then
                Me.EventCaption = $"{DateTimeOffset.Now:hh:mm:ss.fff} Pin{e.Pin:D2}={e.Value} {e.EventType}{Environment.NewLine}"
            End If
        End Sub

#End Region

#Region " DRIVER + EMULATOR "

        ''' <summary> Attempts to configure from the given data. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Function TryConfigure(ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            If Me.TryConfigureDriver(e) AndAlso Me.TryConfigureEmulator(e) AndAlso
                Me.TryInitializeKnownState(e) Then
            End If
            Return Not e.Failed
        End Function

        ''' <summary> Attempts to initialize know state. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> True if it succeeds, false if it fails. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Public Function TryInitializeKnownState(ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Try
                If Me.Driver Is Nothing Then
                    e.RegisterFailure("Handler driver not set")
                ElseIf Me.Emulator Is Nothing Then
                    e.RegisterFailure("Handler emulator not set")
                ElseIf Not Me.Driver.IsConfigured Then
                    e.RegisterFailure("Handler driver not configured")
                ElseIf Not Me.Emulator.IsConfigured Then
                    e.RegisterFailure("Handler emulator Not configured")
                Else
                    Me.Emulator.InitializeKnownState()
                    Me.Driver.InitializeKnownState()
                    Me.Emulator.InitializeKnownState()
                End If
            Catch ex As Exception
                e.RegisterError(ex.ToFullBlownString)
            Finally
            End Try
            Return Not e.Failed
        End Function

#End Region

#Region " DRIVER "

        ''' <summary> Gets or sets the driver source. </summary>
        ''' <value> The driver source. </value>
        Public ReadOnly Property DriverSource As String = $"{NameOf(Gpio.GpioHandlerDriverEmulator.Driver)}."

#Disable Warning IDE1006 ' Naming Styles
        Private WithEvents _Driver As GpioHandlerDriver
#Enable Warning IDE1006 ' Naming Styles

        ''' <summary> Gets or sets the driver. </summary>
        ''' <value> The driver. </value>
        <CLSCompliant(False)>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Property Driver As GpioHandlerDriver
            Get
                Return Me._Driver
            End Get
            Set(value As GpioHandlerDriver)
                Me._Driver = value
            End Set
        End Property

        ''' <summary> Attempts to configure driver from the given data. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Public Function TryConfigureDriver(ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Try
                If Not Me.IsDeviceModalityOpen Then
                    e.RegisterFailure("Device not open")
                ElseIf Me.Device.Gpio Is Nothing Then
                    e.RegisterFailure($"Gpio not supported on {Me.Device.Caption}")
                ElseIf Me.Device.Gpio.Pins Is Nothing Then
                    e.RegisterFailure($"Gpio not supported on { Me.Device.Caption}")
                ElseIf Me.Driver Is Nothing Then
                    e.RegisterFailure($"Driver not configured {Me.Device.Caption}")
                ElseIf Me.Driver.TryValidate(Me.Device.Gpio.Pins, e) Then
                    Me.Driver.Configure(Me.Device.Gpio.Pins)
                End If
            Catch ex As Exception
                e.RegisterError(ex.ToFullBlownString)
            Finally
            End Try
            Return Not e.Failed
        End Function

        ''' <summary> Driver property changed. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="sender"> The sender. </param>
        ''' <param name="e">      Property changed event information. </param>
        Private Sub Driver_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _Driver.PropertyChanged
            If sender IsNot Nothing AndAlso e IsNot Nothing Then
                Me.SyncNotifyPropertyChanged($"{NameOf(Gpio.GpioHandlerDriverEmulator.Driver)}.{e.PropertyName}")
            End If
        End Sub


#End Region

#Region " EMULATOR "

        ''' <summary> Gets or sets the emulator source. </summary>
        ''' <value> The emulator source. </value>
        Public ReadOnly Property EmulatorSource As String = $"{NameOf(Gpio.GpioHandlerDriverEmulator.Emulator)}."

#Disable Warning IDE1006 ' Naming Styles
        Private WithEvents _Emulator As GpioHandlerEmulator
#Enable Warning IDE1006 ' Naming Styles

        ''' <summary> Gets or sets the handler. </summary>
        ''' <value> The handler. </value>
        <CLSCompliant(False)>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Property Emulator As GpioHandlerEmulator
            Get
                Return Me._Emulator
            End Get
            Set(value As GpioHandlerEmulator)
                Me._Emulator = value
            End Set
        End Property

        ''' <summary> Attempts to configure emulator from the given data. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Action event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Public Function TryConfigureEmulator(ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Try
                If Not Me.IsDeviceModalityOpen Then
                    e.RegisterFailure("Device not open")
                ElseIf Me.Device.Gpio Is Nothing Then
                    e.RegisterFailure($"Gpio not supported on {Me.Device.Caption}")
                ElseIf Me.Device.Gpio.Pins Is Nothing Then
                    e.RegisterFailure($"Gpio not supported on {Me.Device.Caption}")
                ElseIf Me.Emulator Is Nothing Then
                    e.RegisterFailure("Emulator not created")
                ElseIf Me.Emulator.TryValidate(Me.Device.Gpio.Pins, e) Then
                    Me.Emulator.Configure(Me.Device.Gpio.Pins)
                End If
            Catch ex As Exception
                e.RegisterError(ex.ToFullBlownString)
            Finally
            End Try
            Return Not e.Failed
        End Function

        ''' <summary> Emulator property changed. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="sender"> The sender. </param>
        ''' <param name="e">      Property changed event information. </param>
        Private Sub Emulator_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _Emulator.PropertyChanged
            If sender IsNot Nothing AndAlso e IsNot Nothing Then
                Me.SyncNotifyPropertyChanged($"{Me.EmulatorSource}{e.PropertyName}")
            End If
        End Sub

#End Region

#Region " PLAY "

        ''' <summary> Attempts to clear start from the given data. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Public Function TryClearStart(ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Try
                If Me.Driver Is Nothing Then
                    e.RegisterFailure("Handler driver not set")
                ElseIf Not Me.Driver.IsConfigured Then
                    e.RegisterFailure("Handler driver not configured")
                ElseIf Me.Driver.State = HandlerState.Idle OrElse
                       Me.Driver.State = HandlerState.EndTestAcknowledged Then
                    Me.Driver.EnableStartTest()
                Else
                    e.RegisterFailure($"Invalid handler state @'{Me._Driver.State}'; state should be '{HandlerState.Idle}' or '{HandlerState.EndTestAcknowledged}'")
                End If
            Catch ex As Exception
                e.RegisterError(ex.ToFullBlownString)
            End Try
            Return Not e.Failed
        End Function

        ''' <summary> Attempts to start test from the given data. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="e"> Cancel details event information. </param>
        ''' <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Public Function TryStartTest(ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            Try
                If Me.Driver Is Nothing Then
                    e.RegisterFailure("Handler driver not set")
                ElseIf Not Me.Driver.IsConfigured Then
                    e.RegisterFailure("Handler driver not configured")
                ElseIf Me.Emulator Is Nothing Then
                    e.RegisterFailure("Handler emulator not set")
                ElseIf Not Me.Emulator.IsConfigured Then
                    e.RegisterFailure("Handler emulator not configured")
                ElseIf Me.Driver.State <> HandlerState.StartTestEnabled Then
                    e.RegisterFailure($"Invalid handler state @'{Me.Driver.State}'; state should be @'{HandlerState.StartTestEnabled}'")
                Else
                    Me.Emulator.OutputStartTest()
                End If
            Catch ex As Exception
                e.RegisterError(ex.ToFullBlownString)
            Finally
            End Try
            Return Not e.Failed
        End Function

        ''' <summary> Sends the end test command. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="binValue"> The bin value. </param>
        ''' <param name="e">        Cancel details event information. </param>
        ''' <returns> <c>true</c> if the test passes, <c>false</c> if the test fails. </returns>
        Public Function TryOutputEndTest(ByVal binValue As Integer, ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            If Me.Driver Is Nothing Then
                e.RegisterFailure("Handler driver not set")
            ElseIf Not Me.Driver.IsConfigured Then
                e.RegisterFailure("Handler driver not configured")
            ElseIf Me.Driver.State = HandlerState.StartTestReceived Then
                Me.Driver.OutputEndTest(binValue)
            Else
                If Not Me.TryInitializeKnownState(e) Then
                    '? 20170504 was if me.try....
                    e.RegisterFailure($"Invalid handler state @'{Me.Driver.State}'; state should be @'{HandlerState.Idle}'; {e.Details}")
                End If
            End If
            Return Not e.Failed
        End Function

#End Region

    End Class
End Namespace
