Imports isr.Core
Imports isr.Diolan.SubsystemExtensions
Namespace Gpio

    ''' <summary> Gpio handler driver Base. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    <CLSCompliant(False)>
    Public MustInherit Class GpioHandlerDriverBase
        Inherits HandlerDriverBase

#Region " CONSTRUCTOR "

        ''' <summary>
        ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        ''' resources.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
        '''                          release only unmanaged resources. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Protected Overrides Sub Dispose(disposing As Boolean)
            Try
                If Not Me.IsDisposed Then
                    Me.ReleasePins()
                    Me._StartTestPin = Nothing
                    Me._EndTestPin = Nothing
                    Me._BinPort = Nothing
                End If
            Catch ex As Exception
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

#End Region

#Region " PINS AND PORTS "

        ''' <summary> Gets the start test pin. </summary>
        ''' <value> The start test pin. </value>
        Protected Property StartTestPin As PinBase

        ''' <summary> Gets the end test pin. </summary>
        ''' <value> The end test pin. </value>
        Protected Property EndTestPin As PinBase

        ''' <summary> Gets the bin port. </summary>
        ''' <value> The bin port. </value>
        Protected Property BinPort As PortBase

        ''' <summary> Gets a value indicating whether the registered start test pin events. </summary>
        ''' <value> <c>true</c> if registered start test pin events; otherwise <c>false</c> </value>
        Protected Property RegisteredStartTestPinEvents As Boolean

        ''' <summary> Gets a value indicating whether the registered end test pin events. </summary>
        ''' <value> <c>true</c> if registered end test pin events; otherwise <c>false</c> </value>
        Protected Property RegisteredEndTestPinEvents As Boolean

        ''' <summary> Gets a value indicating whether the registered port pin events. </summary>
        ''' <value> <c>true</c> if registered port pin events; otherwise <c>false</c> </value>
        Protected Property RegisteredBinPortPinEvents As Boolean

        ''' <summary> Executes the registering start test pin events action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overridable Sub OnRegisteringStartTestPinEvents()
            Me.RegisteredStartTestPinEvents = True
        End Sub

        ''' <summary> Executes the registering end test pin events action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overridable Sub OnRegisteringEndTestPinEvents()
            Me.RegisteredEndTestPinEvents = True
        End Sub

        ''' <summary> Executes the 'registering bin port pin events' action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overridable Sub OnRegisteringBinPortPinEvents()
            Me.RegisteredBinPortPinEvents = True
        End Sub

        ''' <summary> Releases the pins. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Protected Overrides Sub ReleasePins()
            If Me.RegisteredStartTestPinEvents AndAlso Me.StartTestPin IsNot Nothing AndAlso Me.Pins IsNot Nothing Then
                Dim pin As Dln.Gpio.Pin = Me.Pins(Me.StartTestPin.PinNumber)
                RemoveHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                pin.SetEventConfiguration(Dln.Gpio.EventType.None, 0)
            End If
            Me.RegisteredStartTestPinEvents = False
            Me.StartTestPin = Nothing
            If Me.RegisteredEndTestPinEvents AndAlso Me.EndTestPin IsNot Nothing AndAlso Me.Pins IsNot Nothing Then
                Dim pin As Dln.Gpio.Pin = Me.Pins(Me.EndTestPin.PinNumber)
                RemoveHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                pin.SetEventConfiguration(Dln.Gpio.EventType.None, 0)
            End If
            Me.RegisteredEndTestPinEvents = False
            Me.EndTestPin = Nothing
            If Me.RegisteredBinPortPinEvents AndAlso Me.BinPort IsNot Nothing Then
                Dim pin As Dln.Gpio.Pin
                For Each i As Integer In Me.Pins.SelectMulti(CULng(Me.BinPort.Mask))
                    pin = Me.Pins(i)
                    RemoveHandler pin.ConditionMetThreadSafe, AddressOf Me.ConditionMetEventHandler
                    pin.SetEventConfiguration(Dln.Gpio.EventType.None, 0)
                Next
            End If
            Me.RegisteredBinPortPinEvents = False
            Me.BinPort = Nothing
            Me._Pins = Nothing
            MyBase.ReleasePins()
        End Sub

#End Region

#Region " CONFIGURE "

        ''' <summary> Gets a value indicating whether this object is configured. </summary>
        ''' <value> <c>true</c> if this object is configured; otherwise <c>false</c> </value>
        Public Overrides ReadOnly Property IsConfigured As Boolean
            Get
                Return MyBase.IsConfigured AndAlso Me.Pins IsNot Nothing AndAlso Me.StartTestPin IsNot Nothing AndAlso
                    Me.EndTestPin IsNot Nothing AndAlso Me.BinPort IsNot Nothing
            End Get
        End Property

        ''' <summary> The pins. </summary>
        Private _Pins As Dln.Gpio.Pins

        ''' <summary> Gets the pins. </summary>
        ''' <value> The pins. </value>
        Protected ReadOnly Property Pins As Dln.Gpio.Pins
            Get
                Return Me._Pins
            End Get
        End Property

        ''' <summary> Tests configure start. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected MustOverride Sub ConfigureStartTest(ByVal pins As Dln.Gpio.Pins)

        ''' <summary> Tests configure end. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected MustOverride Sub ConfigureEndTest(ByVal pins As Dln.Gpio.Pins)

        ''' <summary> Configure bin port. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Protected MustOverride Sub ConfigureBinPort(ByVal pins As Dln.Gpio.Pins)

        ''' <summary> Configures the given pins. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        '''                                              null. </exception>
        ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        ''' <param name="pins"> The pins. </param>
        <CLSCompliant(False)>
        Public Overloads Sub Configure(ByVal pins As Dln.Gpio.Pins)
            If pins Is Nothing Then Throw New ArgumentNullException(NameOf(pins))
            If pins.Count = 0 Then Throw New InvalidOperationException("GPIO Port has no pins to configure for the handler")
            Me.ReleasePins()
            Me.Configure()
            Me._Pins = pins
            Me.ConfigureStartTest(pins)
            Me.ConfigureEndTest(pins)
            Me.ConfigureBinPort(pins)
            Me.AsyncNotifyPropertyChanged(NameOf(Gpio.GpioHandlerDriverBase.IsConfigured))
        End Sub

        ''' <summary> Attempts to validate from the given data. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="pins"> The pins. </param>
        ''' <param name="e">    Action event information. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <CLSCompliant(False)>
        Public Function TryValidate(ByVal pins As Dln.Gpio.Pins, ByVal e As ActionEventArgs) As Boolean
            If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
            If pins Is Nothing Then
                e.RegisterFailure("Gpio not supported--pins not instantiated")
            ElseIf pins.Count < 4 Then
                e.RegisterFailure($"Gpio has only {pins.Count} pins")
            ElseIf Me.StartTestPinNumber < 0 OrElse
                   Me.StartTestPinNumber >= pins.Count Then
                e.RegisterFailure($"Start test pin number {Me.StartTestPinNumber} must be between 0 and {pins.Count - 1}")
            ElseIf Me.EndTestPinNumber < 0 OrElse
                   Me.EndTestPinNumber >= pins.Count Then
                e.RegisterFailure($"End test pin number {Me.EndTestPinNumber} must be between 0 and {pins.Count - 1}")
            ElseIf Me.StartTestPinNumber = Me.EndTestPinNumber Then
                e.RegisterFailure($"Start test pin number {Me.StartTestPinNumber} must be different from end test pin number {Me.EndTestPinNumber}")
            ElseIf Me.BinMask < 0 Then
                e.RegisterFailure($"Bin mask {Me.BinMask} must be positive")
            ElseIf (Me.BinMask And CInt(Math.Pow(2, Me.StartTestPinNumber))) <> 0 Then
                e.RegisterFailure($"Bin mask {Me.BinMask} must not contain the start test pin number")
            ElseIf (Me.BinMask And CInt(Math.Pow(2, Me.EndTestPinNumber))) <> 0 Then
                e.RegisterFailure($"Bin mask {Me.BinMask} must not contain the end test pin number")
            End If
            Return Not e.Failed
        End Function

        ''' <summary> Executes the initialize known state action. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Overrides Sub InitializeKnownState()
            Me.StartTestLogicalValue = Me.StartTestPin.LogicalValue = 1
            Me.EndTestLogicalValue = Me.EndTestPin.LogicalValue = 1
            MyBase.InitializeKnownState()
        End Sub

#End Region

#Region " BIT VALUE AND LOGICAL VALUES "

        ''' <summary> Query if 'value' is not active state. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> <c>true</c> if not active state; otherwise <c>false</c> </returns>
        Public Function IsNotActiveState(ByVal value As Integer) As Boolean
            Return value = Me.BitValue(0)
        End Function

        ''' <summary> Query if 'value' is active state. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> <c>true</c> if active state; otherwise <c>false</c> </returns>
        Public Function IsActiveState(ByVal value As Integer) As Boolean
            Return value = Me.BitValue(1)
        End Function

        ''' <summary> Converts a value to a logical state. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> value as a LogicalState. </returns>
        Public Function ToLogicalState(ByVal value As Integer) As LogicalState
            Return If(Me.IsActiveState(value), LogicalState.Active, LogicalState.Inactive)
        End Function

        ''' <summary> Active logical state bit value. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <returns> An Integer. </returns>
        Public Function ActiveLogicalStateBitValue() As Integer
            Return Me.BitValue(1)
        End Function

        ''' <summary> Not active logical state bit value. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <returns> An Integer. </returns>
        Public Function NotActiveLogicalStateBitValue() As Integer
            Return Me.BitValue(0)
        End Function

        ''' <summary> Bit value. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="logicalValue"> The logical value. </param>
        ''' <returns> An Integer. </returns>
        Public Function BitValue(ByVal logicalValue As Integer) As Integer
            Return If(Me.ActiveLogic = ActiveLogic.ActiveLow, PinBase.BitZeroMask And Not logicalValue, PinBase.BitZeroMask And logicalValue)
        End Function

#End Region

#Region " EVENTS "

        ''' <summary> Raises the condition met event. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overridable Sub OnStartTestEventOccurred(ByVal e As Dln.Gpio.ConditionMetEventArgs)
            If e IsNot Nothing Then
                Me.StartTestLogicalValue = e.Value = Me.BitValue(1)
            End If
        End Sub

        ''' <summary> Raises the condition met event. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overridable Sub OnEndTestEventOccurred(ByVal e As Dln.Gpio.ConditionMetEventArgs)
            If e IsNot Nothing Then
                Me.EndTestLogicalValue = e.Value = Me.BitValue(1)
            End If
        End Sub

        ''' <summary> Raises the condition met event. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="e"> Event information to send to registered event handlers. </param>
        Protected Overridable Sub OnBinPortPinEventOccurred(ByVal e As Dln.Gpio.ConditionMetEventArgs)
        End Sub

        ''' <summary> Handler, called when start test signal was send by the handler. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="sender"> The sender. </param>
        ''' <param name="e">      Condition met event information. </param>
        Protected Overridable Sub ConditionMetEventHandler(ByVal sender As Object, ByVal e As Dln.Gpio.ConditionMetEventArgs)
            If sender Is Nothing OrElse e Is Nothing Then Return
            Select Case e.Pin
                Case Me.StartTestPinNumber
                    Me.OnStartTestEventOccurred(e)
                Case Me.EndTestPinNumber
                    Me.OnEndTestEventOccurred(e)
                Case Else
                    Dim pinMask As Integer = CInt(Math.Pow(2, e.Pin))
                    If (Me.BinMask And pinMask) = pinMask Then
                        Me.OnBinPortPinEventOccurred(e)
                    End If
            End Select
        End Sub

#End Region

    End Class


End Namespace

