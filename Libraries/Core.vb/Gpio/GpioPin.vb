Imports Dln.Gpio

Namespace Gpio

    ''' <summary> A gpio pin. </summary>
    ''' <remarks>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2016-04-07 </para>
    ''' </remarks>
    Public Class GpioPin
        Inherits PinBase

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="direction">   The direction. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As Dln.Gpio.Pins, ByVal pinNumber As Integer,
                       ByVal activeLogic As ActiveLogic, ByVal direction As PinDirection)
            MyBase.New(pinNumber, activeLogic)
            If pins IsNot Nothing AndAlso pins.Count > Me.PinNumber Then
                Me._Pin = pins(Me.PinNumber)
                Me._Pin.Direction = direction
                Me._Pin.Enabled = True
            End If
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="direction">   The direction. </param>
        ''' <param name="name">        The name. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As Dln.Gpio.Pins, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic,
                       ByVal direction As PinDirection, ByVal name As String)
            MyBase.New(pinNumber, activeLogic, name)
            If pins IsNot Nothing AndAlso pins.Count > Me.PinNumber Then
                Me._Pin = pins(Me.PinNumber)
                Me._Pin.Direction = direction
                Me._Pin.Enabled = True
            End If
        End Sub

        ''' <summary> The byte mask. </summary>
        Public Const ByteMask As Byte = &HFF

#Disable Warning IDE1006 ' Naming Styles
        Private WithEvents _Pin As Dln.Gpio.Pin
#Enable Warning IDE1006 ' Naming Styles

        ''' <summary> Gets the pin. </summary>
        ''' <value> The pin. </value>
        <CLSCompliant(False)>
        Public ReadOnly Property Pin As Dln.Gpio.Pin
            Get
                Return Me._Pin
            End Get
        End Property

        ''' <summary> Gets or sets the enabled. </summary>
        ''' <value> The enabled. </value>
        Public Property Enabled As Boolean
            Get
                Return Me.Pin.Enabled
            End Get
            Set(value As Boolean)
                If value <> Me.Enabled Then
                    Me.Pin.Enabled = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the pin direction. </summary>
        ''' <value> The pin direction. </value>
        Public Property Direction As PinDirection
            Get
                Return CType(Me._Pin.Direction, PinDirection)
            End Get
            Set(value As PinDirection)
                If Me.Direction <> value Then
                    Me._Pin.Direction = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the Open Drain Enabled conditions. </summary>
        ''' <value> <c>True</c> if Open Drain Enabled; otherwise, <c>False</c>. </value>
        Public Property OpenDrainEnabled As Boolean
            Get
                Return Me.Pin.OpendrainEnabled
            End Get
            Set(value As Boolean)
                If value <> Me.OpenDrainEnabled Then
                    Me.Pin.OpendrainEnabled = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the Pull-up Enabled conditions. </summary>
        ''' <value> <c>True</c> if Pull-up Enabled; otherwise, <c>False</c>. </value>
        Public Property PullupEnabled As Boolean
            Get
                Return Me.Pin.PullupEnabled
            End Get
            Set(value As Boolean)
                If value <> Me.PullupEnabled Then
                    Me.Pin.PullupEnabled = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the output bit value. </summary>
        ''' <value> The bit value. </value>
        Public Overrides Property BitValue As Byte
            Get
                Return If(Me.Direction = PinDirection.Output,
                    CByte(GpioPin.ByteMask And Me._Pin.OutputValue),
                    CByte(GpioPin.ByteMask And Me._Pin.Value))
            End Get
            Set(value As Byte)
                If CType(Me._Pin.Direction, PinDirection) = PinDirection.Output Then
                    If Me._Pin.OutputValue <> value Then
                        Me._Pin.OutputValue = value
                        Me.SyncNotifyPropertyChanged()
                    End If
                End If
            End Set
        End Property

        ''' <summary> Pin condition met thread safe. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Condition met event information. </param>
        Private Sub Pin_ConditionMetThreadSafe(sender As Object, e As ConditionMetEventArgs) Handles _Pin.ConditionMetThreadSafe
            Me.SyncNotifyPropertyChanged(NameOf(Gpio.GpioPin.BitValue))
        End Sub

    End Class

    ''' <summary> Gpio input pin. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    Public Class GpioInputPin
        Inherits GpioPin

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As Dln.Gpio.Pins, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic)
            MyBase.New(pins, pinNumber, activeLogic, PinDirection.Input)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="name">        The name. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As Dln.Gpio.Pins, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic, ByVal name As String)
            MyBase.New(pins, pinNumber, activeLogic, PinDirection.Input, name)
        End Sub

    End Class

    ''' <summary> Gpio output pin. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    Public Class GpioOutputPin
        Inherits GpioPin

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As Dln.Gpio.Pins, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic)
            MyBase.New(pins, pinNumber, activeLogic, PinDirection.Output)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="name">        The name. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As Dln.Gpio.Pins, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic, ByVal name As String)
            MyBase.New(pins, pinNumber, activeLogic, PinDirection.Output, name)
        End Sub

    End Class

    ''' <summary> Collection of gpio pins. </summary>
    ''' <remarks>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2016-04-07 </para>
    ''' </remarks>
    Public Class GpioPinCollection
        Inherits PinBaseCollection
    End Class

End Namespace

