Imports System.Collections.ObjectModel
Imports System.Reflection

Imports Dln.Gpio

Namespace Gpio

    ''' <summary> A dummy gpio pin. </summary>
    ''' <remarks>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2016-04-07 </para>
    ''' </remarks>
    Public Class GpioPinDummy
        Inherits PinBase

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="direction">   The direction. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As GpioPinDummyReadOnlyCollection, ByVal pinNumber As Integer,
                       ByVal activeLogic As ActiveLogic, ByVal direction As PinDirection)
            MyBase.New(pinNumber, activeLogic)
            If pins IsNot Nothing AndAlso pins.Count > Me.PinNumber Then
                Me._Pin = pins(Me.PinNumber)
                Me._Pin.Direction = direction
                Me._Pin.Enabled = True
                If activeLogic = ActiveLogic.ActiveLow Then
                    If direction = PinDirection.Input Then
                        Me._Pin.Value = 1
                    Else
                        Me._Pin.OutputValue = 1
                    End If
                End If
            End If
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="direction">   The direction. </param>
        ''' <param name="name">        The name. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As GpioPinDummyReadOnlyCollection, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic,
                       ByVal direction As PinDirection, ByVal name As String)
            MyBase.New(pinNumber, activeLogic, name)
            If pins IsNot Nothing AndAlso pins.Count > Me.PinNumber Then
                Me._Pin = pins(Me.PinNumber)
                Me._Pin.Direction = direction
                Me._Pin.Enabled = True
                If activeLogic = ActiveLogic.ActiveLow Then
                    If direction = PinDirection.Input Then
                        Me._Pin.Value = 1
                    Else
                        Me._Pin.OutputValue = 1
                    End If
                End If
            End If
        End Sub

        ''' <summary> The byte mask. </summary>
        Public Const ByteMask As Byte = &HFF

#Disable Warning IDE1006 ' Naming Styles
        Private WithEvents _Pin As PinDummy
#Enable Warning IDE1006 ' Naming Styles

        ''' <summary> Gets the pin. </summary>
        ''' <value> The pin. </value>
        <CLSCompliant(False)>
        Public ReadOnly Property Pin As PinDummy
            Get
                Return Me._Pin
            End Get
        End Property

        ''' <summary> Gets or sets the pin enabled state. </summary>
        ''' <value> The pin enabled state. </value>
        Public Property Enabled As Boolean
            Get
                Return Me.Pin.Enabled
            End Get
            Set(value As Boolean)
                If value <> Me.Enabled Then
                    Me.Pin.Enabled = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the pin direction. </summary>
        ''' <value> The pin direction. </value>
        Public Property Direction As PinDirection
            Get
                Return CType(Me._Pin.Direction, PinDirection)
            End Get
            Set(value As PinDirection)
                If Me.Direction <> value Then
                    Me._Pin.Direction = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the Open Drain Enabled conditions. </summary>
        ''' <value> <c>True</c> if Open Drain Enabled; otherwise, <c>False</c>. </value>
        Public Property OpenDrainEnabled As Boolean
            Get
                Return Me.Pin.OpenDrainEnabled
            End Get
            Set(value As Boolean)
                If value <> Me.OpenDrainEnabled Then
                    Me.Pin.OpenDrainEnabled = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the Pull-up Enabled conditions. </summary>
        ''' <value> <c>True</c> if Pull-up Enabled; otherwise, <c>False</c>. </value>
        Public Property PullupEnabled As Boolean
            Get
                Return Me.Pin.PullupEnabled
            End Get
            Set(value As Boolean)
                If value <> Me.PullupEnabled Then
                    Me.Pin.PullupEnabled = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the output bit value. </summary>
        ''' <value> The bit value. </value>
        Public Overrides Property BitValue As Byte
            Get
                Return If(Me.Direction = PinDirection.Output,
                    CByte(GpioPin.ByteMask And Me._Pin.OutputValue),
                    CByte(GpioPin.ByteMask And Me._Pin.Value))
            End Get
            Set(value As Byte)
                If CType(Me._Pin.Direction, PinDirection) = PinDirection.Output Then
                    If Me._Pin.OutputValue <> value Then
                        Me._Pin.OutputValue = value
                        Me.SyncNotifyPropertyChanged()
                    End If
                Else
                    If Me._Pin.Value <> value Then
                        Me._Pin.Value = value
                        Me.SyncNotifyPropertyChanged()
                    End If
                End If
            End Set
        End Property

        ''' <summary> Pin condition met thread safe. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Condition met event information. </param>
        Private Sub Pin_ConditionMetThreadSafe(sender As Object, e As ConditionMetEventArgs) Handles _Pin.ConditionMetThreadSafe
            Me.SyncNotifyPropertyChanged(NameOf(Gpio.GpioPin.BitValue))
        End Sub

    End Class

    ''' <summary> Gpio input pin. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    Public Class GpioInputPinDummy
        Inherits GpioPinDummy

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As GpioPinDummyReadOnlyCollection, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic)
            MyBase.New(pins, pinNumber, activeLogic, PinDirection.Input)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="name">        The name. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As GpioPinDummyReadOnlyCollection, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic, ByVal name As String)
            MyBase.New(pins, pinNumber, activeLogic, PinDirection.Input, name)
        End Sub

    End Class

    ''' <summary> Gpio output pin. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    Public Class GpioOutputPinDummy
        Inherits GpioPinDummy

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As GpioPinDummyReadOnlyCollection, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic)
            MyBase.New(pins, pinNumber, activeLogic, PinDirection.Output)
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="name">        The name. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As GpioPinDummyReadOnlyCollection, ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic, ByVal name As String)
            MyBase.New(pins, pinNumber, activeLogic, PinDirection.Output, name)
        End Sub

    End Class

    ''' <summary> Collection of gpio pins. </summary>
    ''' <remarks>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2016-04-07 </para>
    ''' </remarks>
    Public Class GpioPinDummyCollection
        Inherits PinBaseCollection
    End Class

    ''' <summary> A pin dummy. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Class PinDummy

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        Public Sub New()
            MyBase.New
        End Sub

        ''' <summary> Gets or sets the enabled. </summary>
        ''' <value> The enabled. </value>
        Public Property Enabled As Boolean

        ''' <summary> Gets or sets the direction. </summary>
        ''' <value> The direction. </value>
        Public Property Direction As Integer

        ''' <summary> Gets or sets the output value. </summary>
        ''' <value> The output value. </value>
        Public Property OutputValue As Integer

        ''' <summary> Gets or sets the value. </summary>
        ''' <value> The value. </value>
        Public Property Value As Integer

        ''' <summary> Gets or sets the open drain enabled. </summary>
        ''' <value> The open drain enabled. </value>
        Public Property OpenDrainEnabled As Boolean

        ''' <summary> Gets or sets the pullup enabled. </summary>
        ''' <value> The pullup enabled. </value>
        Public Property PullupEnabled As Boolean

        ''' <summary> Gets or sets the pulldown enabled. </summary>
        ''' <value> The pulldown enabled. </value>
        Public Property PulldownEnabled As Boolean

        ''' <summary> Gets or sets the debounce enabled. </summary>
        ''' <value> The debounce enabled. </value>
        Public Property DebounceEnabled As Boolean

        ''' <summary> Gets or sets the type of the event. </summary>
        ''' <value> The type of the event. </value>
        <CLSCompliant(False)>
        Public ReadOnly Property EventType As EventType

        ''' <summary> Gets or sets the event period. </summary>
        ''' <value> The event period. </value>
        Public ReadOnly Property EventPeriod As Integer

        ''' <summary> Gets or sets a list of types of the supported events. </summary>
        ''' <value> A list of types of the supported events. </value>
        <CLSCompliant(False)>
        Public ReadOnly Property SupportedEventTypes As ReadOnlyCollection(Of EventType)

        ''' <summary> Gets or sets the restrictions. </summary>
        ''' <value> The restrictions. </value>
        <CLSCompliant(False)>
        Public ReadOnly Property Restrictions As New DummyRestrictions

        ''' <summary> Event queue for all listeners interested in ConditionMet events. </summary>
        <CLSCompliant(False)>
        Public Event ConditionMet As EventHandler(Of ConditionMetEventArgs)

        ''' <summary> Event queue for all listeners interested in ConditionMetThreadSafe events. </summary>
        <CLSCompliant(False)>
        Public Event ConditionMetThreadSafe As EventHandler(Of ConditionMetEventArgs)

        ''' <summary> Sets event configuration. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="eventType">   Type of the event. </param>
        ''' <param name="eventPeriod"> The event period. </param>
        <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <CLSCompliant(False)>
        Public Sub SetEventConfiguration(eventType As EventType, eventPeriod As Integer)
        End Sub
    End Class

    ''' <summary> A dummy restrictions. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    <CLSCompliant(False)>
    Public Class DummyRestrictions

        ''' <summary> Gets or sets the enabled. </summary>
        ''' <value> The enabled. </value>
        Public ReadOnly Property Enabled As Dln.Restriction

        ''' <summary> Gets or sets the direction. </summary>
        ''' <value> The direction. </value>
        Public ReadOnly Property Direction As Dln.Restriction

        ''' <summary> Gets or sets the output value. </summary>
        ''' <value> The output value. </value>
        Public ReadOnly Property OutputValue As Dln.Restriction

        ''' <summary> Gets or sets the value. </summary>
        ''' <value> The value. </value>
        Public ReadOnly Property Value As Dln.Restriction

        ''' <summary> Gets or sets the open drain enabled. </summary>
        ''' <value> The open drain enabled. </value>
        Public ReadOnly Property OpenDrainEnabled As Dln.Restriction

        ''' <summary> Gets or sets the pullup enabled. </summary>
        ''' <value> The pullup enabled. </value>
        Public ReadOnly Property PullupEnabled As Dln.Restriction

        ''' <summary> Gets or sets the pulldown enabled. </summary>
        ''' <value> The pulldown enabled. </value>
        Public ReadOnly Property PulldownEnabled As Dln.Restriction

        ''' <summary> Gets or sets the debounce enabled. </summary>
        ''' <value> The debounce enabled. </value>
        Public ReadOnly Property DebounceEnabled As Dln.Restriction

        ''' <summary> Gets or sets the type of the event. </summary>
        ''' <value> The type of the event. </value>
        Public ReadOnly Property EventType As Dln.Restriction

        ''' <summary> Gets or sets the event period. </summary>
        ''' <value> The event period. </value>
        Public ReadOnly Property EventPeriod As Dln.Restriction

        ''' <summary> Gets or sets the set event configuration. </summary>
        ''' <value> The set event configuration. </value>
        Public ReadOnly Property SetEventConfiguration As Dln.Restriction
    End Class

    ''' <summary> Collection of dummy gpio pins. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    <DefaultMember("Item")>
    Public Class GpioPinDummyReadOnlyCollection
        Inherits ReadOnlyCollection(Of PinDummy)

        ''' <summary>
        ''' Initializes a new instance of the
        ''' <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> class that is a read-
        ''' only wrapper around the specified list.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins"> The list to wrap. </param>
        Public Sub New(ByVal pins As IList(Of PinDummy))
            MyBase.New(pins)
        End Sub
    End Class


End Namespace


