Imports System.ComponentModel

Namespace Gpio

    ''' <summary> Pin base. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-08 </para>
    ''' </remarks>
    Public MustInherit Class PinBase
        Inherits isr.Core.Models.ViewModelBase

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        Protected Sub New(ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic)
            MyBase.New()
            Me._PinNumber = pinNumber
            Me._ActiveLogic = activeLogic
            Me._Name = CStr(Me._PinNumber)
        End Sub

        ''' <summary> Specialized constructor for use only by derived class. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pinNumber">   The pin number. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        ''' <param name="name">        The name. </param>
        Protected Sub New(ByVal pinNumber As Integer, ByVal activeLogic As ActiveLogic, ByVal name As String)
            Me.New(pinNumber, activeLogic)
            Me._Name = name
        End Sub

        ''' <summary> The name. </summary>
        Private _Name As String

        ''' <summary> Gets or sets the name. </summary>
        ''' <value> The name. </value>
        Public Property Name As String
            Get
                Return Me._Name
            End Get
            Set(value As String)
                If Not String.Equals(value, Me.Name) Then
                    Me._Name = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the pin number. </summary>
        ''' <value> The pin number. </value>
        Public ReadOnly Property PinNumber As Integer

        ''' <summary> The active logic. </summary>
        Private _ActiveLogic As ActiveLogic

        ''' <summary> Gets or sets the active logic. </summary>
        ''' <value> The active logic. </value>
        Public Property ActiveLogic As ActiveLogic
            Get
                Return Me._ActiveLogic
            End Get
            Set(value As ActiveLogic)
                If value <> Me._ActiveLogic Then
                    Me._ActiveLogic = value
                    Me.AsyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the bit value. </summary>
        ''' <value> The bit value. </value>
        Public MustOverride Property BitValue As Byte

        ''' <summary> The bit zero mask. </summary>
        Public Const BitZeroMask As Byte = 1

        ''' <summary> Gets or sets the logical value. </summary>
        ''' <value> The logical value. </value>
        Public Property LogicalValue As Byte
            Get
                Return If(Me.ActiveLogic = ActiveLogic.ActiveLow, PinBase.BitZeroMask And Not Me.BitValue, PinBase.BitZeroMask And Me.BitValue)
            End Get
            Set(value As Byte)
                Dim v As Byte = If(Me.ActiveLogic = ActiveLogic.ActiveLow, PinBase.BitZeroMask And Not value, PinBase.BitZeroMask And value)
                ' If Me.ActiveLogic = ActiveLogic.ActiveLow Then
                '     Me.BitValue = PinBase.BitZeroMask And Not value
                ' Else
                '     Me.BitValue = PinBase.BitZeroMask And value
                ' End If
                If v <> Me.BitValue Then
                    Me.BitValue = v
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the logical state of the pin. </summary>
        ''' <value> The logical state of the pin. </value>
        Public Property LogicalState As LogicalState
            Get
                Return If(Me.LogicalValue = 0, LogicalState.Inactive, LogicalState.Active)
            End Get
            Set(value As LogicalState)
                Dim v As Byte = If(value = LogicalState.Inactive, CByte(0), CByte(1))
                If Me.LogicalValue <> v Then
                    ' Me.LogicalValue = If(value = LogicalState.Inactive, CByte(0), CByte(1))
                    Me.LogicalValue = v
                End If
            End Set
        End Property

        ''' <summary>
        ''' Executes the property changed on a different thread, and waits for the result.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="value"> The value. </param>
        Public Sub InvokePropertyChanged(ByVal value As String)
            Me.AsyncNotifyPropertyChanged(value)
        End Sub

    End Class

    ''' <summary> Collection of pin bases. </summary>
    ''' <remarks>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2017-07-31 </para>
    ''' </remarks>
    Public Class PinBaseCollection
        Inherits Collections.ObjectModel.KeyedCollection(Of Integer, PinBase)

        ''' <summary>
        ''' When implemented in a derived class, extracts the key from the specified element.
        ''' </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="item"> The element from which to extract the key. </param>
        ''' <returns> The key for the specified element. </returns>
        Protected Overrides Function GetKeyForItem(item As PinBase) As Integer
            If item Is Nothing Then Throw New ArgumentNullException(NameOf(item))
            Return item.PinNumber
        End Function
    End Class

End Namespace

''' <summary> Values that represent active logics. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Enum ActiveLogic

    <Description("Active Low")> ActiveLow = 0

    <Description("Active High")> ActiveHigh = 1
End Enum

''' <summary> Values that represent logical states. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Enum LogicalState

    <Description("Inactive (Off)")> Inactive = 0

    <Description("Active (On)")> Active = 1
End Enum
