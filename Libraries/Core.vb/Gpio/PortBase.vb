Namespace Gpio

    ''' <summary> Port base. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-08, 1.0.. </para>
    ''' </remarks>
    Public MustInherit Class PortBase

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="mask">        The mask. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        Protected Sub New(ByVal mask As Long, ByVal activeLogic As ActiveLogic)
            MyBase.New()
            Me._Mask = mask
            Me._ActiveLogic = activeLogic
        End Sub

        ''' <summary> Gets or sets the active logic. </summary>
        ''' <value> The active logic. </value>
        Public ReadOnly Property ActiveLogic As ActiveLogic

        ''' <summary> Gets or sets the port value. </summary>
        ''' <value> The port value. </value>
        Public MustOverride Property PortValue As Long

        ''' <summary> Gets or sets the mask. </summary>
        ''' <value> The mask. </value>
        Public Property Mask As Long

        ''' <summary> Gets or sets the value mask. </summary>
        ''' <value> The value mask. </value>
        Public Property ValueMask As Long

        ''' <summary> Gets or sets the number of bits. </summary>
        ''' <value> The number of bits. </value>
        Public Property BitCount As Integer

        ''' <summary> Gets or sets the masked value. </summary>
        ''' <value> The masked value. </value>
        Public Property MaskedValue As Long
            Get
                Return If(Me.ActiveLogic = ActiveLogic.ActiveLow, Me.ValueMask And Not Me.PortValue, Me.ValueMask And Me.PortValue)
            End Get
            Set(ByVal value As Long)
                Me.PortValue = If(Me.ActiveLogic = ActiveLogic.ActiveLow, Me.ValueMask And Not value, Me.ValueMask And value)
            End Set
        End Property

    End Class

End Namespace
