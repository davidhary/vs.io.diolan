Imports isr.Diolan.SubsystemExtensions
Namespace Gpio

    ''' <summary> Gpio input port. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    Public Class GpioInputPort
        Inherits PortBase

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="mask">        The mask. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As Dln.Gpio.Pins, ByVal mask As Long, ByVal activeLogic As ActiveLogic)
            MyBase.New(mask, activeLogic)
            Me._Pins = pins
            Dim firstPin As Integer = CULng(mask).FirstPinNumber
            Dim pin As Dln.Gpio.Pin = Me._Pins(firstPin)
            pin.Direction = PinDirection.Input
            pin.Enabled = True
            Me._Pins.Configure(CULng(mask), pin)
            Me.BitCount = CULng(mask).BitCount
            Me.ValueMask = CLng(Math.Pow(2, Me.BitCount) - 1)
        End Sub

        ''' <summary> The pins. </summary>
        Private ReadOnly _Pins As Dln.Gpio.Pins

        ''' <summary> Gets or sets the input port value. </summary>
        ''' <value> The port value. </value>
        Public Overrides Property PortValue As Long
            Get
                Return CLng(Me._Pins.InputValue(CULng(Me.Mask)))
            End Get
            Set(value As Long)
            End Set
        End Property

    End Class

    ''' <summary> Gpio output port. </summary>
    ''' <remarks>
    ''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2015-06-09 </para>
    ''' </remarks>
    Public Class GpioOutputPort
        Inherits PortBase

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-10-24. </remarks>
        ''' <param name="pins">        The pins. </param>
        ''' <param name="mask">        The mask. </param>
        ''' <param name="activeLogic"> The active logic. </param>
        <CLSCompliant(False)>
        Public Sub New(ByVal pins As Dln.Gpio.Pins, ByVal mask As Long, ByVal activeLogic As ActiveLogic)
            MyBase.New(mask, activeLogic)
            Me._Pins = pins
            Dim firstPin As Integer = CULng(mask).FirstPinNumber
            Dim pin As Dln.Gpio.Pin = Me._Pins(firstPin)
            pin.Direction = PinDirection.Output
            pin.Enabled = True
            Me._Pins.Configure(CULng(mask), pin)
            Me.BitCount = CULng(mask).BitCount
            Me.ValueMask = CLng(Math.Pow(2, Me.BitCount) - 1)
        End Sub

        ''' <summary> The pins. </summary>
        Private ReadOnly _Pins As Dln.Gpio.Pins

        ''' <summary> Gets or sets the output port value. </summary>
        ''' <value> The port value. </value>
        Public Overrides Property PortValue As Long
            Get
                Return CLng(Me._Pins.OutputValueGetter(CULng(Me.Mask)))
            End Get
            Set(value As Long)
                Me._Pins.OutputValueSetter(CULng(Me.Mask), CULng(value))
            End Set
        End Property

    End Class

End Namespace
